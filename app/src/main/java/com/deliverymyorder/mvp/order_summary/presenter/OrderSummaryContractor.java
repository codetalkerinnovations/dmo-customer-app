package com.deliverymyorder.mvp.order_summary.presenter;

import com.deliverymyorder.mvp.order_summary.model.OrderSummaryModel;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryPickAndDrop;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;

import org.json.JSONObject;

import java.util.ArrayList;

public class OrderSummaryContractor {

    public interface OrderSummaryPresenter {
        void getOrderSummaryData(int orderId);

        void updateConfirmPurchaseData(int orderId);
    }

    public interface OrderSummaryView {
        void getOrderSummaryDataSuccess(OrderSummaryModel.Data orderSummaryModel);

        void getOrderSummaryDataSuccess(OrderSummaryPickAndDrop.Data orderSummaryModel);

        void getOrderSummaryDataUnSucess(String message);

        void OrderSummaryDataInternetError();


        void updateOrderStatusSuccess(OrderSummaryModel.Data orderSummaryModel);

        void updateOrderStatusUnSucess(String message);

        void updateOrderStatusInternetError();




    }
}
