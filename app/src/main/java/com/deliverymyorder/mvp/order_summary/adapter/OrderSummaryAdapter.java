package com.deliverymyorder.mvp.order_summary.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deliverymyorder.R;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryModel;
import com.deliverymyorder.mvp.track_order.adapter.PreviousOrderItemAdapter;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderSummaryAdapter extends RecyclerView.Adapter<OrderSummaryAdapter.Holder> {


    Activity activity;
    ArrayList<OrderSummaryModel.Data.Item> datum;
    String favBit;

    public OrderSummaryAdapter(Activity activity, ArrayList<OrderSummaryModel.Data.Item> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_oreviousorder_subitem, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final OrderSummaryModel.Data.Item result = datum.get(position);

        holder.txtViewItemName.setText(String.valueOf(result.getProductName()));
        holder.txtViewQty.setText(String.valueOf("qty "+result.getQty()));
        holder.txtViewPrice.setText(String.valueOf("₹"+result.getProductPrice()));

    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        LinearLayout relLayItem;

        @BindView(R.id.txtViewItemName)
        TextView txtViewItemName;

        @BindView(R.id.txtViewQty)
        TextView txtViewQty;

        @BindView(R.id.txtViewPrice)
        TextView txtViewPrice;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}