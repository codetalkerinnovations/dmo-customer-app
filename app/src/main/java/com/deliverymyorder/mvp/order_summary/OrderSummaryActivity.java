package com.deliverymyorder.mvp.order_summary;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.CircleTransform;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.coupen.CoupenModel;
import com.deliverymyorder.mvp.coupen.presenter.CoupenContractor;
import com.deliverymyorder.mvp.coupen.presenter.CoupenPresenterImpl;
import com.deliverymyorder.mvp.order_summary.adapter.OrderSummaryAdapter;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryModel;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryPickAndDrop;
import com.deliverymyorder.mvp.order_summary.presenter.OrderSummaryContractor;
import com.deliverymyorder.mvp.order_summary.presenter.OrderSummaryPresenterImpl;
import com.deliverymyorder.mvp.payment_activity.PayUPaymentActivity;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderSummaryActivity extends AppCompatActivity implements OrderSummaryContractor.OrderSummaryView, CoupenContractor.CoupenView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewOrderId)
    TextView txtViewOrderId;

    @BindView(R.id.txtViewDate)
    TextView txtViewDate;

    @BindView(R.id.recyclerViewItems)
    RecyclerView recyclerViewItems;

    @BindView(R.id.relLayDiscount)
    RelativeLayout relLayDiscount;

    @BindView(R.id.txtViewDiscount)
    TextView txtViewDiscount;

    @BindView(R.id.relLayTotal)
    RelativeLayout relLayTotal;

    @BindView(R.id.txtViewTotal)
    TextView txtViewTotal;

    @BindView(R.id.relLayCoupen)
    RelativeLayout relLayCoupen;

    @BindView(R.id.edtTextCoupen)
    EditText edtTextCoupen;

    @BindView(R.id.txtViewApply)
    TextView txtViewApply;

    @BindView(R.id.txtViewConfirm)
    TextView txtViewConfirm;

    @BindView(R.id.txtViewDeny)
    TextView txtViewDeny;


    @BindView(R.id.imgBill)
    ImageView imgBill;


    @BindView(R.id.txtViewDeliveryCharge)
            TextView txtViewDeliveryCharge;



    OrderSummaryModel.Data orderSummaryModel;
    OrderSummaryPickAndDrop.Data orderSummaryModelPickAndDrop;

    OrderSummaryPresenterImpl orderSummaryPresenter;
    CoupenPresenterImpl coupenPresenter;
    OrderSummaryAdapter orderSummaryAdapter;

    int orderId;

    int isComeFromYourBill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        ButterKnife.bind(this);

        orderSummaryPresenter = new OrderSummaryPresenterImpl(this, this);
        coupenPresenter = new CoupenPresenterImpl(this, this);

        if (getIntent() != null) {

            orderId = getIntent().getIntExtra(ConstIntent.CONST_ORDER_ID, 0);
            isComeFromYourBill = getIntent().getIntExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
        }


        Object obj = OrderSession.getSharedInstance().getCurrentOrder();


        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            OrderHistoryModel.Data.Ongoing data = (OrderHistoryModel.Data.Ongoing)obj;
            orderId = data.getId();
        }else if (obj instanceof PickAndDropOnGoingData){
            PickAndDropOnGoingData data = (PickAndDropOnGoingData)obj;
            orderId = data.getId();
        }


        if (isComeFromYourBill == 0) {

            relLayCoupen.setVisibility(View.GONE);
            txtViewDeny.setVisibility(View.GONE);
            txtViewConfirm.setVisibility(View.GONE);
        } else if (isComeFromYourBill == 1) {
            relLayCoupen.setVisibility(View.VISIBLE);
            txtViewDeny.setVisibility(View.VISIBLE);
            txtViewConfirm.setVisibility(View.VISIBLE);
        } else if (isComeFromYourBill == 2) {
            relLayCoupen.setVisibility(View.VISIBLE);
            txtViewDeny.setVisibility(View.VISIBLE);
            txtViewConfirm.setVisibility(View.VISIBLE);
            txtViewConfirm.setText("PAY");

        } else {
            relLayCoupen.setVisibility(View.VISIBLE);
            txtViewDeny.setVisibility(View.VISIBLE);
            txtViewConfirm.setVisibility(View.VISIBLE);
        }

        getOrderSummaryDataApi();
    }

    private void manageRecyclerViewOrderSummary() {

        orderSummaryAdapter = new OrderSummaryAdapter(this, orderSummaryModel.getItems());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerViewItems.setLayoutManager(mLayoutManager);
        recyclerViewItems.setItemAnimator(new DefaultItemAnimator());
        recyclerViewItems.setAdapter(orderSummaryAdapter);
    }

    private void getOrderSummaryDataApi() {

        orderSummaryPresenter.getOrderSummaryData(orderId);
    }

    @Override
    public void getOrderSummaryDataSuccess(OrderSummaryModel.Data orderSummaryModel) {

        this.orderSummaryModel = orderSummaryModel;
        manageRecyclerViewOrderSummary();

        txtViewOrderId.setText(String.valueOf(orderSummaryModel.getInvoiceNo()));
        txtViewDate.setText(Utils.getStringDateFromDate(this,orderSummaryModel.getCreatedAt().getCreateDate()));
//        txtViewDate.setText(String.valueOf(orderSummaryModel.getCreatedAt().getDate()));



        if (orderSummaryModel.getTotalDiscount()==null)
        {
            txtViewDiscount.setText("Free Delivery");
        }else
        {
            txtViewDiscount.setText("₹"+String.valueOf(orderSummaryModel.getTotalDiscount()));
        }


        if (orderSummaryModel.getAppliedCouponId() != 0)
        {

            edtTextCoupen.setText(orderSummaryModel.getCouponCode());

            edtTextCoupen.setEnabled(false);

            txtViewApply.setBackground(getResources().getDrawable(R.drawable.drawable_round_coner_red));

            txtViewApply.setText("Remove");


        }else
        {
            edtTextCoupen.setText("");

            edtTextCoupen.setEnabled(true);

            txtViewApply.setBackground(getResources().getDrawable(R.drawable.drawable_round_coner_green));

            txtViewApply.setText("Apply");
        }

        if (orderSummaryModel.getBillImage()!=null)
        {
            imgBill.setVisibility(View.VISIBLE);
            Picasso.with(this).load(orderSummaryModel.getBillImage()).into(imgBill);
        }else
        {
            imgBill.setVisibility(View.GONE);
        }

        txtViewTotal.setText("₹"+String.valueOf(orderSummaryModel.getTotalAmount()));
        txtViewDeliveryCharge.setText("₹"+String.valueOf(orderSummaryModel.getDeliveryCost()));


    }


    @Override
    public void getOrderSummaryDataSuccess(OrderSummaryPickAndDrop.Data orderSummaryModel) {

        this.orderSummaryModelPickAndDrop = orderSummaryModel;
        //manageRecyclerViewOrderSummary();
        recyclerViewItems.setVisibility(View.INVISIBLE);

        txtViewOrderId.setText(String.valueOf(orderSummaryModel.getInvoiceNo()));
        txtViewDate.setText(Utils.getStringDateFromDate(this,orderSummaryModel.getCreateDate()));
//        txtViewDate.setText(String.valueOf(orderSummaryModel.getCreatedAt().getDate()));
        txtViewDiscount.setText("₹"+String.valueOf(0.0));

        txtViewTotal.setText("₹"+String.valueOf(orderSummaryModel.getTotalCost()));
        txtViewDeliveryCharge.setText("₹"+String.valueOf(orderSummaryModel.getDeliveryCost()));


        if (orderSummaryModel.getTotalDiscount()==null)
        {
            txtViewDiscount.setText("0.0");
        }else
        {
            txtViewDiscount.setText("₹"+String.valueOf(orderSummaryModel.getTotalDiscount()));
        }


        if (orderSummaryModel.getBillImage()!=null)
        {
            imgBill.setVisibility(View.VISIBLE);
            Picasso.with(this).load(orderSummaryModel.getBillImage()).into(imgBill);
        }else
        {
            imgBill.setVisibility(View.GONE);
        }


        if (orderSummaryModel.getAppliedCouponId() != 0)
        {

            edtTextCoupen.setText(orderSummaryModel.getCouponCode());

            edtTextCoupen.setEnabled(false);

            txtViewApply.setBackground(getResources().getDrawable(R.drawable.drawable_round_coner_red));

            txtViewApply.setText("Remove");


            txtViewApply.setTextColor(getResources().getColor(R.color.red));

        }else
        {

            edtTextCoupen.setText("");

            edtTextCoupen.setEnabled(true);

            txtViewApply.setBackground(getResources().getDrawable(R.drawable.drawable_round_coner_green));

            txtViewApply.setText("Apply");

            txtViewApply.setTextColor(getResources().getColor(R.color.color_green));

        }

    }

    @Override
    public void getOrderSummaryDataUnSucess(String message) {

    }

    @Override
    public void OrderSummaryDataInternetError() {

    }

    @Override
    public void updateOrderStatusSuccess(OrderSummaryModel.Data orderSummaryModel) {

        finish();
    }

    @Override
    public void updateOrderStatusUnSucess(String message) {

    }

    @Override
    public void updateOrderStatusInternetError() {

    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        finish();
    }


    @OnClick(R.id.txtViewApply)
    public void txtViewApplyClicked() {


        String coupenCode = String.valueOf(edtTextCoupen.getText());

        Boolean isCouponApplied = false;

        if (orderSummaryModel!=null)
        {
            if (orderSummaryModel.getAppliedCouponId() != 0)
            {
                isCouponApplied = true;
            }

        }else if (orderSummaryModelPickAndDrop!=null)
        {
            if (orderSummaryModelPickAndDrop.getAppliedCouponId() != 0)
            {
                isCouponApplied = true;
            }
        }else
        {

        }

        Utils.hideKeyboardIfOpen(this);

        if (isCouponApplied)
        {
            if (!Utils.isEmptyOrNull(coupenCode)) {
                coupenPresenter.cancelCoupen(coupenCode,orderSummaryModel.getBusinessId());
            } else {
                SnackNotify.showMessage("Please enter coupon code.", relLayMain);
            }
        }else
        {
            if (!Utils.isEmptyOrNull(coupenCode)) {
                coupenPresenter.updateCoupen(coupenCode,orderSummaryModel.getBusinessId());
            } else {
                SnackNotify.showMessage("Please enter coupon code.", relLayMain);
            }
        }


    }


    @OnClick(R.id.txtViewDeny)
    public void txtViewDenyClicked() {

        finish();
    }


    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirmClicked() {

        Utils.hideKeyboardIfOpen(this);

        if (isComeFromYourBill == 2) {




            Object obj = OrderSession.getSharedInstance().getCurrentOrder();


            if (obj instanceof OrderHistoryModel.Data.Ongoing) {

                Intent intent = new Intent(this, PayUPaymentActivity.class);
                intent.putExtra(ConstIntent.CONST_AMOUNT, orderSummaryModel.getTotalAmount().toString());
                intent.putExtra(ConstIntent.CONST_ORDER_ID, String.valueOf(orderId));

                intent.putExtra(ConstIntent.CONST_ORDER_TYPE,"1");

                startActivity(intent);
            }else
            {

                Intent intent = new Intent(this, PayUPaymentActivity.class);
                intent.putExtra(ConstIntent.CONST_AMOUNT, orderSummaryModelPickAndDrop.getTotalCost().toString());
                intent.putExtra(ConstIntent.CONST_ORDER_ID, String.valueOf(orderId));

                intent.putExtra(ConstIntent.CONST_ORDER_TYPE,"2");

                startActivity(intent);
            }

            finish();



        } else {

            orderSummaryPresenter.updateConfirmPurchaseData(orderId);
        }
    }

    @Override
    public void onSuccessCoupenList(ArrayList<CoupenModel.Datum> arrayListCoupen) {
        //Unused
        orderSummaryPresenter.getOrderSummaryData(orderId);
    }

    @Override
    public void onUnSuccessCoupenList(String message) {
//Unused
        AlertDialogHelper.showMessage(this,message);
    }

    @Override
    public void onInternetErrorCoupenList() {
//Unused
    }

    @Override
    public void onSuccessUpdateCoupen() {
        AlertDialogHelper.showMessage(this,"Coupon applied successfully");
        getOrderSummaryDataApi();
    }

    @Override
    public void onUnSuccessUpdateCoupen(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onInternetErrorUpdateCoupen() {

    }

    @Override
    public void onSuccessRemoveCoupen() {
        AlertDialogHelper.showMessage(this,"Coupon Removed successfully");
        getOrderSummaryDataApi();
    }
}
