package com.deliverymyorder.mvp.order_summary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderSummaryPickAndDrop {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }



    public class Customer {

        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("login_type")
        @Expose
        private String loginType;
        @SerializedName("social_id")
        @Expose
        private String socialId;
        @SerializedName("firstname")
        @Expose
        private String firstname;
        @SerializedName("lastname")
        @Expose
        private String lastname;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("ip_address")
        @Expose
        private String ipAddress;
        @SerializedName("dob")
        @Expose
        private Object dob;
        @SerializedName("otp_code")
        @Expose
        private String otpCode;
        @SerializedName("refer_code")
        @Expose
        private String referCode;
        @SerializedName("referer_id")
        @Expose
        private Integer refererId;
        @SerializedName("coupon_generate_status")
        @Expose
        private Integer couponGenerateStatus;
        @SerializedName("current_address_id")
        @Expose
        private Integer currentAddressId;
        @SerializedName("country_id")
        @Expose
        private Object countryId;
        @SerializedName("state_id")
        @Expose
        private Object stateId;
        @SerializedName("city_id")
        @Expose
        private Object cityId;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public String getLoginType() {
            return loginType;
        }

        public void setLoginType(String loginType) {
            this.loginType = loginType;
        }

        public String getSocialId() {
            return socialId;
        }

        public void setSocialId(String socialId) {
            this.socialId = socialId;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public void setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
        }

        public Object getDob() {
            return dob;
        }

        public void setDob(Object dob) {
            this.dob = dob;
        }

        public String getOtpCode() {
            return otpCode;
        }

        public void setOtpCode(String otpCode) {
            this.otpCode = otpCode;
        }

        public String getReferCode() {
            return referCode;
        }

        public void setReferCode(String referCode) {
            this.referCode = referCode;
        }

        public Integer getRefererId() {
            return refererId;
        }

        public void setRefererId(Integer refererId) {
            this.refererId = refererId;
        }

        public Integer getCouponGenerateStatus() {
            return couponGenerateStatus;
        }

        public void setCouponGenerateStatus(Integer couponGenerateStatus) {
            this.couponGenerateStatus = couponGenerateStatus;
        }

        public Integer getCurrentAddressId() {
            return currentAddressId;
        }

        public void setCurrentAddressId(Integer currentAddressId) {
            this.currentAddressId = currentAddressId;
        }

        public Object getCountryId() {
            return countryId;
        }

        public void setCountryId(Object countryId) {
            this.countryId = countryId;
        }

        public Object getStateId() {
            return stateId;
        }

        public void setStateId(Object stateId) {
            this.stateId = stateId;
        }

        public Object getCityId() {
            return cityId;
        }

        public void setCityId(Object cityId) {
            this.cityId = cityId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("invoice_no")
        @Expose
        private String invoiceNo;
        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("pickup_contact_name")
        @Expose
        private String pickupContactName;
        @SerializedName("pickup_contact_no")
        @Expose
        private String pickupContactNo;
        @SerializedName("pickup_flat_number")
        @Expose
        private String pickupFlatNumber;
        @SerializedName("pickup_landmark")
        @Expose
        private String pickupLandmark;
        @SerializedName("pickup_city")
        @Expose
        private String pickupCity;
        @SerializedName("pickup_state")
        @Expose
        private String pickupState;
        @SerializedName("pickup_country")
        @Expose
        private String pickupCountry;
        @SerializedName("pickup_lat")
        @Expose
        private Double pickupLat;

        public Double getTotalDiscount() {
            return totalDiscount;
        }

        public void setTotalDiscount(Double totalDiscount) {
            this.totalDiscount = totalDiscount;
        }

        @SerializedName("pickup_long")

        @Expose
        private Double pickupLong;
        @SerializedName("current_latitude")
        @Expose
        private Double currentLatitude;
        @SerializedName("current_longitude")
        @Expose
        private Double currentLongitude;
        @SerializedName("drop_contact_name")
        @Expose
        private String dropContactName;
        @SerializedName("drop_contact_no")
        @Expose
        private String dropContactNo;
        @SerializedName("drop_flat_number")
        @Expose
        private String dropFlatNumber;
        @SerializedName("drop_landmark")
        @Expose
        private String dropLandmark;
        @SerializedName("drop_city")
        @Expose
        private String dropCity;
        @SerializedName("drop_state")
        @Expose
        private String dropState;
        @SerializedName("drop_country")
        @Expose
        private String dropCountry;
        @SerializedName("drop_lat")
        @Expose
        private Double dropLat;
        @SerializedName("drop_long")
        @Expose
        private Double dropLong;
        @SerializedName("product_type_id")
        @Expose
        private Integer productTypeId;
        @SerializedName("weight_id")
        @Expose
        private Integer weightId;
        @SerializedName("size_id")
        @Expose
        private Integer sizeId;
        @SerializedName("total_items")
        @Expose
        private Integer totalItems;
        @SerializedName("total_km")
        @Expose
        private String totalKm;
        @SerializedName("total_cost")
        @Expose
        private Double totalCost;
        @SerializedName("delivery_cost")
        @Expose
        private Double deliveryCost;
        @SerializedName("additional_cost")
        @Expose
        private Double additionalCost;
        @SerializedName("transaction_id")
        @Expose
        private Object transactionId;
        @SerializedName("transaction_otp")
        @Expose
        private Object transactionOtp;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("is_settled")
        @Expose
        private Integer isSettled;
        @SerializedName("cancel_reason_id")
        @Expose
        private Integer cancelReasonId;
        @SerializedName("start_delivery_status")
        @Expose
        private Integer startDeliveryStatus;
        @SerializedName("dlp_start_latitude")
        @Expose
        private Double dlpStartLatitude;
        @SerializedName("dlp_start_longitude")
        @Expose
        private String dlpStartLongitude;

        @SerializedName("delivery_type_id")
        @Expose
        private Integer deliveryTypeId;
        @SerializedName("delivery_partner_id")
        @Expose
        private Integer deliveryPartnerId;
        @SerializedName("delivery_partner_assigned")
        @Expose
        private Integer deliveryPartnerAssigned;
        @SerializedName("dlp_assigned_time")
        @Expose
        private String dlpAssignedTime;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("customer")
        @Expose
        private Customer customer;

        @SerializedName("total_discount")
        @Expose
        private Double totalDiscount;

        @SerializedName("applied_coupon_id")
        @Expose
        private Integer appliedCouponId;

        @SerializedName("coupon_code")
        @Expose
        private String couponCode;

        @SerializedName("bill_type")
        @Expose
        private Integer billType;

        @SerializedName("bill_image")
        @Expose
        private String billImage;

        public Integer getBillType() {
            return billType;
        }

        public void setBillType(Integer billType) {
            this.billType = billType;
        }
//        @SerializedName("bill_image")
//        @Expose
//        private String billImage;
//

        public Integer getAppliedCouponId() {
            return appliedCouponId;
        }

        public void setAppliedCouponId(Integer appliedCouponId) {
            this.appliedCouponId = appliedCouponId;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public String getPickupContactName() {
            return pickupContactName;
        }

        public void setPickupContactName(String pickupContactName) {
            this.pickupContactName = pickupContactName;
        }

        public String getPickupContactNo() {
            return pickupContactNo;
        }

        public void setPickupContactNo(String pickupContactNo) {
            this.pickupContactNo = pickupContactNo;
        }

        public String getPickupFlatNumber() {
            return pickupFlatNumber;
        }

        public void setPickupFlatNumber(String pickupFlatNumber) {
            this.pickupFlatNumber = pickupFlatNumber;
        }

        public String getPickupLandmark() {
            return pickupLandmark;
        }

        public void setPickupLandmark(String pickupLandmark) {
            this.pickupLandmark = pickupLandmark;
        }

        public String getPickupCity() {
            return pickupCity;
        }

        public void setPickupCity(String pickupCity) {
            this.pickupCity = pickupCity;
        }

        public String getPickupState() {
            return pickupState;
        }

        public void setPickupState(String pickupState) {
            this.pickupState = pickupState;
        }

        public String getPickupCountry() {
            return pickupCountry;
        }

        public void setPickupCountry(String pickupCountry) {
            this.pickupCountry = pickupCountry;
        }

        public Double getPickupLat() {
            return pickupLat;
        }

        public void setPickupLat(Double pickupLat) {
            this.pickupLat = pickupLat;
        }

        public Double getPickupLong() {
            return pickupLong;
        }

        public void setPickupLong(Double pickupLong) {
            this.pickupLong = pickupLong;
        }

        public Double getCurrentLatitude() {
            return currentLatitude;
        }

        public void setCurrentLatitude(Double currentLatitude) {
            this.currentLatitude = currentLatitude;
        }

        public Double getCurrentLongitude() {
            return currentLongitude;
        }

        public void setCurrentLongitude(Double currentLongitude) {
            this.currentLongitude = currentLongitude;
        }

        public String getDropContactName() {
            return dropContactName;
        }

        public void setDropContactName(String dropContactName) {
            this.dropContactName = dropContactName;
        }

        public String getDropContactNo() {
            return dropContactNo;
        }

        public void setDropContactNo(String dropContactNo) {
            this.dropContactNo = dropContactNo;
        }

        public String getDropFlatNumber() {
            return dropFlatNumber;
        }

        public void setDropFlatNumber(String dropFlatNumber) {
            this.dropFlatNumber = dropFlatNumber;
        }

        public String getDropLandmark() {
            return dropLandmark;
        }

        public void setDropLandmark(String dropLandmark) {
            this.dropLandmark = dropLandmark;
        }

        public String getDropCity() {
            return dropCity;
        }

        public void setDropCity(String dropCity) {
            this.dropCity = dropCity;
        }

        public String getDropState() {
            return dropState;
        }

        public void setDropState(String dropState) {
            this.dropState = dropState;
        }

        public String getDropCountry() {
            return dropCountry;
        }

        public void setDropCountry(String dropCountry) {
            this.dropCountry = dropCountry;
        }

        public Double getDropLat() {
            return dropLat;
        }

        public void setDropLat(Double dropLat) {
            this.dropLat = dropLat;
        }

        public Double getDropLong() {
            return dropLong;
        }

        public void setDropLong(Double dropLong) {
            this.dropLong = dropLong;
        }

        public Integer getProductTypeId() {
            return productTypeId;
        }

        public void setProductTypeId(Integer productTypeId) {
            this.productTypeId = productTypeId;
        }

        public Integer getWeightId() {
            return weightId;
        }

        public void setWeightId(Integer weightId) {
            this.weightId = weightId;
        }

        public Integer getSizeId() {
            return sizeId;
        }

        public void setSizeId(Integer sizeId) {
            this.sizeId = sizeId;
        }

        public Integer getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(Integer totalItems) {
            this.totalItems = totalItems;
        }

        public String getTotalKm() {
            return totalKm;
        }

        public void setTotalKm(String totalKm) {
            this.totalKm = totalKm;
        }

        public Double getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(Double totalCost) {
            this.totalCost = totalCost;
        }

        public Double getDeliveryCost() {
            return deliveryCost;
        }

        public void setDeliveryCost(Double deliveryCost) {
            this.deliveryCost = deliveryCost;
        }

        public Double getAdditionalCost() {
            return additionalCost;
        }

        public void setAdditionalCost(Double additionalCost) {
            this.additionalCost = additionalCost;
        }

        public Object getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(Object transactionId) {
            this.transactionId = transactionId;
        }

        public Object getTransactionOtp() {
            return transactionOtp;
        }

        public void setTransactionOtp(Object transactionOtp) {
            this.transactionOtp = transactionOtp;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getIsSettled() {
            return isSettled;
        }

        public void setIsSettled(Integer isSettled) {
            this.isSettled = isSettled;
        }

        public Integer getCancelReasonId() {
            return cancelReasonId;
        }

        public void setCancelReasonId(Integer cancelReasonId) {
            this.cancelReasonId = cancelReasonId;
        }

        public Integer getStartDeliveryStatus() {
            return startDeliveryStatus;
        }

        public void setStartDeliveryStatus(Integer startDeliveryStatus) {
            this.startDeliveryStatus = startDeliveryStatus;
        }

        public Double getDlpStartLatitude() {
            return dlpStartLatitude;
        }

        public void setDlpStartLatitude(Double dlpStartLatitude) {
            this.dlpStartLatitude = dlpStartLatitude;
        }

        public String getDlpStartLongitude() {
            return dlpStartLongitude;
        }

        public void setDlpStartLongitude(String dlpStartLongitude) {
            this.dlpStartLongitude = dlpStartLongitude;
        }

        public String getBillImage() {
            return billImage;
        }

        public void setBillImage(String billImage) {
            this.billImage = billImage;
        }

        public Integer getDeliveryTypeId() {
            return deliveryTypeId;
        }

        public void setDeliveryTypeId(Integer deliveryTypeId) {
            this.deliveryTypeId = deliveryTypeId;
        }

        public Integer getDeliveryPartnerId() {
            return deliveryPartnerId;
        }

        public void setDeliveryPartnerId(Integer deliveryPartnerId) {
            this.deliveryPartnerId = deliveryPartnerId;
        }

        public Integer getDeliveryPartnerAssigned() {
            return deliveryPartnerAssigned;
        }

        public void setDeliveryPartnerAssigned(Integer deliveryPartnerAssigned) {
            this.deliveryPartnerAssigned = deliveryPartnerAssigned;
        }

        public String getDlpAssignedTime() {
            return dlpAssignedTime;
        }

        public void setDlpAssignedTime(String dlpAssignedTime) {
            this.dlpAssignedTime = dlpAssignedTime;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public Date getCreateDate()
        {

            Date convertedDate = new Date();
            String stringDateFormat = createdAt; //2019-01-03 21:32:41
            SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            try {
                convertedDate = (Date) formatter.parse(stringDateFormat);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return convertedDate;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

    }

}
