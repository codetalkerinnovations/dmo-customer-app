package com.deliverymyorder.mvp.order_summary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OrderSummaryModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Data {

        @SerializedName("invoice_no")
        @Expose
        private String invoiceNo;
        @SerializedName("total_items")
        @Expose
        private Integer totalItems;
        @SerializedName("customer_name")
        @Expose
        private String customerName;
        @SerializedName("address_id")
        @Expose
        private int addressId;
        @SerializedName("total_amount")
        @Expose
        private Double totalAmount;
        @SerializedName("delivery_type_id")
        @Expose
        private int deliveryTypeId;
        @SerializedName("delivery_cost")
        @Expose
        private Double deliveryCost;
        @SerializedName("total_discount")
        @Expose
        private Double totalDiscount;
        @SerializedName("transaction_otp")
        @Expose
        private String transactionOtp;
        @SerializedName("transaction_id")
        @Expose
        private String transactionId;
        @SerializedName("applied_coupon_id")
        @Expose
        private Integer appliedCouponId;

        @SerializedName("coupon_code")
        @Expose
        private String couponCode;

        @SerializedName("status")
        @Expose
        private Integer status;

        @SerializedName("cancel_reason_id")
        @Expose
        private Integer cancelReasonId;

        @SerializedName("bill_type")
        @Expose
        private Integer billType;

        @SerializedName("bill_image")
        @Expose
        private String billImage;

        //
        //business_id
        @SerializedName("business_id")
        @Expose
        private Integer businessId;

        @SerializedName("cancel_reason_description")
        @Expose
        private String cancelReasonDescription;
        @SerializedName("created_at")
        @Expose
        private CreatedAt createdAt;
        @SerializedName("items")
        @Expose
        private ArrayList<Item> items = null;


        public Integer getBillType() {
            return billType;
        }

        public void setBillType(Integer billType) {
            this.billType = billType;
        }

        public String getBillImage() {
            return billImage;
        }

        public void setBillImage(String billImage) {
            this.billImage = billImage;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }


        public Integer getBusinessId() {
            return businessId;
        }

        public void setBusinessId(Integer businessId) {
            this.businessId = businessId;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public Integer getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(Integer totalItems) {
            this.totalItems = totalItems;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public int getAddressId() {
            return addressId;
        }

        public void setAddressId(int addressId) {
            this.addressId = addressId;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public int getDeliveryTypeId() {
            return deliveryTypeId;
        }

        public void setDeliveryTypeId(int deliveryTypeId) {
            this.deliveryTypeId = deliveryTypeId;
        }

        public Double getDeliveryCost() {
            return deliveryCost;
        }

        public void setDeliveryCost(Double deliveryCost) {
            this.deliveryCost = deliveryCost;
        }

        public Double getTotalDiscount() {
            return totalDiscount;
        }

        public void setTotalDiscount(Double totalDiscount) {
            this.totalDiscount = totalDiscount;
        }

        public String getTransactionOtp() {
            return transactionOtp;
        }

        public void setTransactionOtp(String transactionOtp) {
            this.transactionOtp = transactionOtp;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public Integer getAppliedCouponId() {
            return appliedCouponId;
        }

        public void setAppliedCouponId(Integer appliedCouponId) {
            this.appliedCouponId = appliedCouponId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getCancelReasonId() {
            return cancelReasonId;
        }

        public void setCancelReasonId(Integer cancelReasonId) {
            this.cancelReasonId = cancelReasonId;
        }

        public String getCancelReasonDescription() {
            return cancelReasonDescription;
        }

        public void setCancelReasonDescription(String cancelReasonDescription) {
            this.cancelReasonDescription = cancelReasonDescription;
        }

        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

        public ArrayList<Item> getItems() {
            return items;
        }

        public void setItems(ArrayList<Item> items) {
            this.items = items;
        }

        public class CreatedAt {

            @SerializedName("date")
            @Expose
            private String date;
            @SerializedName("timezone_type")
            @Expose
            private Integer timezoneType;
            @SerializedName("timezone")
            @Expose
            private String timezone;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public Integer getTimezoneType() {
                return timezoneType;
            }

            public void setTimezoneType(Integer timezoneType) {
                this.timezoneType = timezoneType;
            }

            public String getTimezone() {
                return timezone;
            }

            public void setTimezone(String timezone) {
                this.timezone = timezone;
            }

            public Date getCreateDate()
            {

                Date convertedDate = new Date();
                String stringDateFormat = date; //2019-01-03 21:32:41
                SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

                try {
                    convertedDate = (Date) formatter.parse(stringDateFormat);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                return convertedDate;
            }


        }


        public class Item {

            @SerializedName("item_id")
            @Expose
            private Integer itemId;
            @SerializedName("product_name")
            @Expose
            private String productName;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("product_price")
            @Expose
            private Integer productPrice;
            @SerializedName("product_img")
            @Expose
            private String productImg;
            @SerializedName("qty")
            @Expose
            private Integer qty;

            public Integer getItemId() {
                return itemId;
            }

            public void setItemId(Integer itemId) {
                this.itemId = itemId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public Integer getProductPrice() {
                return productPrice;
            }

            public void setProductPrice(Integer productPrice) {
                this.productPrice = productPrice;
            }

            public String getProductImg() {
                return productImg;
            }

            public void setProductImg(String productImg) {
                this.productImg = productImg;
            }

            public Integer getQty() {
                return qty;
            }

            public void setQty(Integer qty) {
                this.qty = qty;
            }
        }
    }
}