package com.deliverymyorder.mvp.order_summary.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryModel;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryPickAndDrop;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailContractor;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderSummaryPresenterImpl implements OrderSummaryContractor.OrderSummaryPresenter {

    Activity activity;
    OrderSummaryContractor.OrderSummaryView orderSummaryView;

    public OrderSummaryPresenterImpl(Activity activity, OrderSummaryContractor.OrderSummaryView orderSummaryView) {
        this.activity = activity;
        this.orderSummaryView = orderSummaryView;
    }

    @Override
    public void getOrderSummaryData(int orderId) {
        try {
            ApiAdapter.getInstance(activity);
            callGetOrderSummaryApi(orderId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            orderSummaryView.OrderSummaryDataInternetError();
        }
    }

    @Override
    public void updateConfirmPurchaseData(int orderId) {
        try {
            ApiAdapter.getInstance(activity);
           callUpdateOrderStatusApi(orderId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            orderSummaryView.updateOrderStatusInternetError();
        }
    }


    private void callGetOrderSummaryApi(int orderId) {
        Progress.start(activity);

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        JSONObject jsonObject = new JSONObject();


        int orderType = 0;
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType =1;


            try {
                jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
                jsonObject.put(Const.PARAM_ORDER_ID, orderId);
                jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
            Call<OrderSummaryModel> getCreatePasswordResult = ApiAdapter.getApiService().getOrderDetails("application/json", "no-cache", body);
            getCreatePasswordResult.enqueue(new Callback<OrderSummaryModel>() {
                @Override
                public void onResponse(Call<OrderSummaryModel> call, Response<OrderSummaryModel> response) {

                    Progress.stop();
                    try {
                        OrderSummaryModel orderSummaryModel = response.body();

                        if (orderSummaryModel.getError() == 0) {
                            orderSummaryView.getOrderSummaryDataSuccess(orderSummaryModel.getData());
                        } else {
                            orderSummaryView.getOrderSummaryDataUnSucess(orderSummaryModel.getMessage());
                        }
                    } catch (NullPointerException exp) {
                        orderSummaryView.getOrderSummaryDataUnSucess(activity.getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<OrderSummaryModel> call, Throwable t) {
                    Progress.stop();
                    orderSummaryView.getOrderSummaryDataUnSucess(activity.getString(R.string.server_error));
                }
            });

        }else if (obj instanceof PickAndDropOnGoingData)
        {
            orderType =2;

            try {
                jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
                jsonObject.put(Const.PARAM_ORDER_ID, orderId);
                jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
            Call<OrderSummaryPickAndDrop> getCreatePasswordResult = ApiAdapter.getApiService().getOrderDetailsPickAndDrop("application/json", "no-cache", body);
            getCreatePasswordResult.enqueue(new Callback<OrderSummaryPickAndDrop>() {
                @Override
                public void onResponse(Call<OrderSummaryPickAndDrop> call, Response<OrderSummaryPickAndDrop> response) {

                    Progress.stop();
                    try {
                        OrderSummaryPickAndDrop orderSummaryModel = response.body();

                        if (orderSummaryModel.getError() == 0) {
                            orderSummaryView.getOrderSummaryDataSuccess(orderSummaryModel.getData());
                        } else {
                            orderSummaryView.getOrderSummaryDataUnSucess(orderSummaryModel.getMessage());
                        }
                    } catch (NullPointerException exp) {
                        orderSummaryView.getOrderSummaryDataUnSucess(activity.getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<OrderSummaryPickAndDrop> call, Throwable t) {
                    Progress.stop();
                    orderSummaryView.getOrderSummaryDataUnSucess(activity.getString(R.string.server_error));
                }
            });

        }



    }

    private void callUpdateOrderStatusApi(int orderId) {
        Progress.start(activity);

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        int orderType = 0;
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType =1;
        }else if (obj instanceof PickAndDropOnGoingData)
        {
            orderType =2;
        }

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<OrderSummaryModel> getCreatePasswordResult = ApiAdapter.getApiService().updateOrderStatus("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<OrderSummaryModel>() {
            @Override
            public void onResponse(Call<OrderSummaryModel> call, Response<OrderSummaryModel> response) {

                Progress.stop();
                try {
                    OrderSummaryModel orderSummaryModel = response.body();

                    if (orderSummaryModel.getError() == 0) {
                        orderSummaryView.updateOrderStatusSuccess(orderSummaryModel.getData());
                    } else {
                        orderSummaryView.updateOrderStatusUnSucess(orderSummaryModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    orderSummaryView.updateOrderStatusUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<OrderSummaryModel> call, Throwable t) {
                Progress.stop();
                orderSummaryView.updateOrderStatusUnSucess(activity.getString(R.string.server_error));
            }
        });
    }


}
