package com.deliverymyorder.mvp.order_summary.model;

import com.deliverymyorder.mvp.coupen.CoupenModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApplyCoupenRes {

        @SerializedName("error")
        @Expose
        private Integer error;
        @SerializedName("message")
        @Expose
        private String message;

        @SerializedName("reason")
        @Expose
        private String reason;

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }


        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

}
