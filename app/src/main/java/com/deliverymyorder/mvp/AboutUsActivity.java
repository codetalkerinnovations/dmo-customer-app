package com.deliverymyorder.mvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.deliverymyorder.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutUsActivity extends AppCompatActivity {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.webViewAboutUs)
    WebView webViewAboutUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);

        webViewAboutUs.getSettings().setJavaScriptEnabled(true);
        webViewAboutUs.getSettings().setBuiltInZoomControls(true);
      //  String url = "https://www.paymeon.com/Vouchers/?v=%C80%8D%B1x%D9%CFqh%FA%84%C35%0A%1F%CE&iv=%25%EE%BEi%F4%DAT%E1";
        //webView.loadUrl(url); // Not Working... Showing blank
        webViewAboutUs.loadUrl("http://delivermyorders.in/about-us"); // its working
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked(){

        finish();
    }
}
