package com.deliverymyorder.mvp.address;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.helpers.WorkaroundMapFragment;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.address.model.AddAddressModel;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.address.model.AddressListModel;
import com.deliverymyorder.mvp.address.model.DeleteAddressModel;
import com.deliverymyorder.mvp.address.model.UpdateCurrentAddressModel;
import com.deliverymyorder.mvp.address.presenter.AddressContractor;
import com.deliverymyorder.mvp.address.presenter.AddressDetailContractor;
import com.deliverymyorder.mvp.address.presenter.AddressDetailPresenterImpl;
import com.deliverymyorder.mvp.address.presenter.AddressPresenterImpl;
import com.deliverymyorder.mvp.pick_drop.pickup_location.Result;
import com.deliverymyorder.mvp.service_not_aviailble_page.CheckServiceAvailblityContractor;
import com.deliverymyorder.mvp.service_not_aviailble_page.CheckServiceAvailblityImpl;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddAddressActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, AddressContractor.AddressView, AddressDetailContractor.AddressDetailView {


    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.edtTextName)
    EditText edtTextName;

    @BindView(R.id.edtTextFlatNo)
    EditText edtTextFlatNo;

    @BindView(R.id.edtTextLandmark)
    EditText edtTextLandmark;

    @BindView(R.id.edtTextContactInformation)
    EditText edtTextContactInformation;

    @BindView(R.id.edtTextCity)
    EditText edtTextCity;

    @BindView(R.id.edtTextState)
    EditText edtTextState;

    @BindView(R.id.edtTextCountry)
    EditText edtTextCountry;

    @BindView(R.id.txtViewConfirm)
    TextView txtViewConfirm;

    private GoogleMap mMap;

    JSONObject jsonObject;

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private final static int PERMISSION_CODE__LOCATION = 2910;
    private final static int PLACE_PICKER_REQUEST = 112;

    private LocationManager locationManager;

    double latitude = 0.0;
    double longitude = 0.0;
    int count = 1;

    boolean isInitializedMap = false;

    List<Address> addresses;
    String address;

    AddressPresenterImpl addressPresenter;
    AddressDetailPresenterImpl addressDetailPresenter;

    boolean isComeForAdd = true;
    int addressId;

    AddressDetailModel.Data addressDetailModel;

    //CheckServiceAvailblityImpl checkServiceAvailblityImpl;

    Boolean isFirstTime = true;


    String pickUpLandMark;
    String pickUpFlatNumber;
    String pickUpCountry;
    String pickUpCity;
    String pickUpState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);

        addressPresenter = new AddressPresenterImpl(this, this);
        addressDetailPresenter = new AddressDetailPresenterImpl(this, this);

        if (getIntent() != null) {
            isComeForAdd = getIntent().getBooleanExtra(ConstIntent.CONST_IS_COME_FOR_ADD, true);
            addressId = getIntent().getIntExtra(ConstIntent.CONST_ADDRESS_ID, 0);
        }

        // initialized map
        initializeMap();

        if (isComeForAdd) {

            txtViewTitle.setText("Add Address");

            getCurrentLocation();
        } else {

            txtViewTitle.setText("Edit Address");

            callGetAddressDetailDataApi();
        }
    }

    @OnClick(R.id.imgSearchView)
    public void imgViewSearch(View view) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            AlertDialogHelper.showAlertDialog(this, e.getLocalizedMessage());
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            AlertDialogHelper.showAlertDialog(this, e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private void callGetAddressDetailDataApi() {

        // int addressId = PrefUtil.getInt(this, Const.KEY_ADDRESS_ID, 0);

        addressDetailPresenter.getAddressDetailData(addressId);
    }

    private void initializeMap() {

        // custom map fragment to handle map scroll in nested scroll view
        WorkaroundMapFragment fragment_map = (WorkaroundMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.mapView);

        fragment_map.getMapAsync(this);

        fragment_map.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                //nestedScroll.requestDisallowInterceptTouchEvent(true);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        //  getCurrentLocation();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        //  getCurrentLocation();
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        //googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);


        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                if (!isFirstTime) {
                    final LatLng mPosition = mMap.getCameraPosition().target;
                    checkService(mPosition.latitude, mPosition.longitude, new Result() {
                        @Override
                        public void resultDidFinish(boolean status) {

                            if (status) {
                                latitude = mPosition.latitude;
                                longitude = mPosition.longitude;

                                getAddressFromLatLongUpdated();
                            }
                        }
                    });

                }else
                {
                    isFirstTime = false;
                }

            }
        });
    }

    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {

        if (checkIsPermissionGranted()) {

            // enable current selectedItemLocationId in map
            //mMap.setMyLocationEnabled(true);

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                isInitializedMap = true;

            } else {
                getLocation();
            }
        } else {
            askLocationPermission();
        }
    }

    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 2910);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

                final Place place = PlacePicker.getPlace(this,data);

                        //PlacePicker.getPlace(data, this);

                //Place mpace = PlacePicker.getPlace(this,data);

                checkService(place.getLatLng().latitude, place.getLatLng().longitude, new Result() {
                    @Override
                    public void resultDidFinish(boolean status) {
                        if (status)
                        {
                            latitude = place.getLatLng().latitude;
                            longitude = place.getLatLng().longitude;
                            getAddressFromLatLong();
                            getAddressFromLatLongUpdated();
                        }
                    }
                });

                // String toastMsg = String.format("Place: %s", place.getName());
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }


    private void getLocation() {

        Progress.start(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
    }

    private boolean checkIsPermissionGranted() {

        int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if ((permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        Progress.stop();

        latitude = location.getLatitude();
        longitude = location.getLongitude();


        if (!isComeForAdd && count == 1) {

            latitude = Double.parseDouble(addressDetailModel.getLatitude());
            longitude = Double.parseDouble(addressDetailModel.getLongitude());

            count++;
        }

        Log.e("latitude", "------>" + latitude);
        Log.e("longitude", "------>" + longitude);

        getAddressFromLatLong();
        getAddressFromLatLongUpdated();

     /*   // get selectedItemLocationId
        getLocations();*/

    }


    private void getAddressFromLatLong() {

        mMap.clear();

        // add marker on google map
        LatLng myLatLng = new LatLng(latitude, longitude);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        //  MarkerOptions markerOptMyLoc = new MarkerOptions().position(myLatLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_red)).title("You are here");
        // mMap.addMarker(markerOptMyLoc);

        // adding marker
        builder.include(myLatLng);

        LatLngBounds boundsMyLoc = builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsMyLoc, 4));

        //nestedScroll.fullScroll(View.FOCUS_DOWN);
    }

    private void getAddressFromLatLongUpdated() {

        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country =  "India";//addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                edtTextFlatNo.setText(knownName);
                edtTextLandmark.setText(addresses.get(0).getSubAdminArea());
                edtTextContactInformation.setText(address);
                edtTextCity.setText(city);
                edtTextState.setText(state);
                edtTextCountry.setText(country);

                Log.e("Addr Lat", city + " " + state + " " + country + " " + postalCode + " " + knownName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {
        finish();
    }

    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirmClicked() {

        if (validation()) {
            if (isComeForAdd) {

                String name = String.valueOf(edtTextName.getText());
                String flatNo = String.valueOf(edtTextFlatNo.getText());
                String landMark = String.valueOf(edtTextLandmark.getText());
                String contactInfo = String.valueOf(edtTextContactInformation.getText());
                String city = String.valueOf(edtTextCity.getText());
                String state = String.valueOf(edtTextState.getText());
                String country = String.valueOf(edtTextCountry.getText());

                if (Utils.isEmptyOrNull(name)) {
                    SnackNotify.showMessage("Please enter name.", relLayMain);
                } else {
                    jsonObject = new JSONObject();

                    try {
                        jsonObject.put(Const.APP_KEY, Const.APP_KEY_VALUE);
                        jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
                        jsonObject.put(Const.KEY_NAME, name);
                        jsonObject.put(Const.PARAM_FLAT_OR_HOUSE, flatNo);
                        jsonObject.put(Const.PARAM_AREA_LOCALITY, flatNo);
                        jsonObject.put(Const.PARAM_LANDMARK, landMark);
                        jsonObject.put(Const.PARAM_LATITUDE, latitude);
                        jsonObject.put(Const.PARAM_LONGITUDE, longitude);
                        jsonObject.put(Const.PARAM_STREET, flatNo);
                        jsonObject.put(Const.PARAM_COUNTRY, country);
                        jsonObject.put(Const.PARAM_STATE, state);
                        jsonObject.put(Const.PARAM_CITY, city);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    addressPresenter.addAddressData(jsonObject);
                }
            } else {
                String name = String.valueOf(edtTextName.getText());
                String flatNo = String.valueOf(edtTextFlatNo.getText());
                String landMark = String.valueOf(edtTextLandmark.getText());
                String contactInfo = String.valueOf(edtTextContactInformation.getText());
                String city = String.valueOf(edtTextCity.getText());
                String state = String.valueOf(edtTextState.getText());
                String country = String.valueOf(edtTextCountry.getText());

                if (Utils.isEmptyOrNull(name)) {
                    SnackNotify.showMessage("Please enter name.", relLayMain);
                } else {
                    jsonObject = new JSONObject();

                    try {
                        jsonObject.put(Const.APP_KEY, Const.APP_KEY_VALUE);
                        jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
                        jsonObject.put(Const.KEY_ADDRESS_ID, addressDetailModel.getId());
                        jsonObject.put(Const.KEY_NAME, name);
                        jsonObject.put(Const.PARAM_FLAT_OR_HOUSE, flatNo);
                        jsonObject.put(Const.PARAM_AREA_LOCALITY, flatNo);
                        jsonObject.put(Const.PARAM_LANDMARK, landMark);
                        jsonObject.put(Const.PARAM_LATITUDE, latitude);
                        jsonObject.put(Const.PARAM_LONGITUDE, longitude);
                        jsonObject.put(Const.PARAM_STREET, flatNo);
                        jsonObject.put(Const.PARAM_COUNTRY, country);
                        jsonObject.put(Const.PARAM_STATE, state);
                        jsonObject.put(Const.PARAM_CITY, city);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    addressPresenter.editAddressData(jsonObject);
                }
            }
        }
    }

    public  boolean validation()
    {

        if (latitude == 0 || longitude == 0)
        {
            AlertDialogHelper.showAlertDialog(this,"Please select any address");
            return  false;
        }






        return  true;
    }

    @Override
    public void getAddAddressSuccess(AddAddressModel.Data addAddressModel) {

        PrefUtil.putInt(this, Const.KEY_ADDRESS_ID, addAddressModel.getId());

        //LoginManager.getInstance().getUserData().setCurrentAddressId(addAddressModel.getId());

        finish();
    }

    @Override
    public void getAddAddressUnSucess(String message) {

    }

    @Override
    public void addAddressInternetError() {

    }

    @Override
    public void getAllAddressSuccess(ArrayList<AddressListModel.Datum> arrayListAddresses) {
        //not used
    }

    @Override
    public void getAllAddressUnSucess(String message) {
        //not used
    }

    @Override
    public void allAddressInternetError() {
        //not used
    }

    @Override
    public void deleteAddressSuccess(DeleteAddressModel.Data deleteAddressModel) {
        //not used
    }

    @Override
    public void deleteAddressUnSucess(String message) {
//not used
    }

    @Override
    public void deleteAddressInternetError() {
//not used
    }

    @Override
    public void updateCurrentAddressSuccess(UpdateCurrentAddressModel.Data deleteAddressModel) {
        //not used
    }

    @Override
    public void updateCurrentAddressUnSucess(String message) {
//not used
    }

    @Override
    public void updateCurrentAddressInternetError() {
//not used
    }

    @Override
    public void getAddressDetailSuccess(AddressDetailModel.Data addressDetailModel) {

        Progress.stop();
        this.addressDetailModel = addressDetailModel;


        edtTextName.setText(addressDetailModel.getName());
        edtTextFlatNo.setText(addressDetailModel.getHouseNo());
        edtTextLandmark.setText(addressDetailModel.getLandmark());
        edtTextContactInformation.setText(addressDetailModel.getAreaLocality());
        edtTextCity.setText(addressDetailModel.getCity());
        edtTextState.setText(addressDetailModel.getState());
        edtTextCountry.setText(addressDetailModel.getCountry());

        try {

            latitude = Double.parseDouble(addressDetailModel.getLatitude());
            longitude = Double.parseDouble(addressDetailModel.getLongitude());

            LatLng myLatLng = new LatLng(latitude, longitude);

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 14));



        }catch (Exception e)
        {
            e.printStackTrace();
        }

       // getCurrentLocation();
    }

    @Override
    public void getAddressDetailUnSucess(String message) {

    }

    @Override
    public void getAddressDetailInternetError() {

    }


    private  void checkService(Double lat, Double lang, final Result result)
    {

        CheckServiceAvailblityImpl checkServiceAvailblity = new CheckServiceAvailblityImpl(this, new CheckServiceAvailblityContractor.CheckServiceAvailbityView() {
            @Override
            public void checkServiceAaiblityDidFinshWithResult(boolean status) {

                if (status)
                {
                    if (result!=null)
                        result.resultDidFinish(true);
                }else
                {
                    AlertDialogHelper.showMessage(AddAddressActivity.this,"Service is not Available at this location");

                    if (result!=null)
                        result.resultDidFinish(false);
                }

            }

            @Override
            public void checkServiceAvaiblityNetworkError() {
                  AlertDialogHelper.showMessage(AddAddressActivity.this,getResources().getString(R.string.server_error));
                if (result!=null)
                    result.resultDidFinish(false);
            }
        });

        checkServiceAvailblity.checkServiceAvailablity(String.valueOf(lat),String.valueOf(lang));


    }




}


