package com.deliverymyorder.mvp.address.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.LocationProvider;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.adapter.CategoryAdapter;
import com.deliverymyorder.mvp.add_category.adapter.SubCategoryAdapter;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.address.AddAddressActivity;
import com.deliverymyorder.mvp.address.AddressListActivity;
import com.deliverymyorder.mvp.address.model.AddressListModel;
import com.deliverymyorder.mvp.login.LoginActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.Holder> {


    Activity activity;
    ArrayList<AddressListModel.Datum> datum;
    AddressListActivity addressListActivity;

    public AddressListAdapter(Activity activity, ArrayList<AddressListModel.Datum> datum, AddressListActivity addressListActivity) {
        this.activity = activity;
        this.datum = datum;
        this.addressListActivity = addressListActivity;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_address, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final AddressListModel.Datum result = datum.get(position);


        holder.txtViewName.setText(result.getName());
        holder.txtViewAreaLocality.setText(result.getAreaLocality());
        holder.txtViewLandmark.setText(result.getLandmark());
        // holder.txtViewPincode.setText(result.get());
        holder.txtViewCity.setText(result.getCity());
        holder.txtViewState.setText(result.getState());
        holder.txtViewCountry.setText(result.getCountry());

        //holder.radioDefaultAddress.setChecked(result.);

        holder.imgViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity,R.style.AlertDialogTheme);
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want to delete this address ?");
                alertDialog.setCancelable(false);
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {

                        addressListActivity.callDeleteAddressDataApi(result.getId());

                        dialog.dismiss();


                    }
                });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                // Showing Alert Message
                alertDialog.show();


            }
        });

        holder.imgViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, AddAddressActivity.class);
                intent.putExtra(ConstIntent.CONST_ADDRESS_ID, result.getId());
                intent.putExtra(ConstIntent.CONST_IS_COME_FOR_ADD, false);
                activity.startActivity(intent);

            }
        });


        holder.radioDefaultAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity,R.style.AlertDialogTheme);
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want to set this address as default ?");
                alertDialog.setCancelable(false);
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {


                        addressListActivity.callUpdateCurrentAddressDataApi(result.getId());

                        PrefUtil.putString(activity, Const.KEY_LATITUDE,String.valueOf(result.getLatitude()));
                        PrefUtil.putString(activity, Const.KEY_LONGITUDE, String.valueOf(result.getLongitude()));


                        Location loc = new Location("currnet ");
                        loc.setLatitude(result.getLatitude());
                        loc.setLongitude(result.getLongitude());

                        LocationProvider.getSharedInstacne().setmLocation(loc);
                        LocationProvider.getSharedInstacne().setShouldAutoUpdate(false);

                        dialog.dismiss();


                    }
                });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        holder.radioDefaultAddress.setChecked(false);
                    }
                });
                // Showing Alert Message
                alertDialog.show();


            }
        });


    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgViewEdit)
        ImageView imgViewEdit;

        @BindView(R.id.radioDefaultAddress)
        RadioButton radioDefaultAddress;

        @BindView(R.id.imgViewDelete)
        ImageView imgViewDelete;

        @BindView(R.id.txtViewName)
        TextView txtViewName;

        @BindView(R.id.txtViewAreaLocality)
        TextView txtViewAreaLocality;

        @BindView(R.id.txtViewLandmark)
        TextView txtViewLandmark;

        @BindView(R.id.txtViewPincode)
        TextView txtViewPincode;

        @BindView(R.id.txtViewCity)
        TextView txtViewCity;

        @BindView(R.id.txtViewState)
        TextView txtViewState;

        @BindView(R.id.txtViewCountry)
        TextView txtViewCountry;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}
