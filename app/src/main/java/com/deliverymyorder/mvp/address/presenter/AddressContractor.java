package com.deliverymyorder.mvp.address.presenter;

import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.address.model.AddAddressModel;
import com.deliverymyorder.mvp.address.model.AddressListModel;
import com.deliverymyorder.mvp.address.model.DeleteAddressModel;
import com.deliverymyorder.mvp.address.model.UpdateCurrentAddressModel;

import org.json.JSONObject;

import java.util.ArrayList;

public class AddressContractor {

    public interface AddressPresenter {

        void addAddressData(JSONObject jsonObject);

        void editAddressData(JSONObject jsonObject);

        void getAllAddressData();

        void deleteAddressData(int addressId);

        void makeDefaultAddressData(int addressId);
    }

    public interface AddressView {
        void getAddAddressSuccess(AddAddressModel.Data addAddressModel);

        void getAddAddressUnSucess(String message);

        void addAddressInternetError();


        void getAllAddressSuccess(ArrayList<AddressListModel.Datum> arrayListAddresses);

        void getAllAddressUnSucess(String message);

        void allAddressInternetError();

        void deleteAddressSuccess(DeleteAddressModel.Data deleteAddressModel);

        void deleteAddressUnSucess(String message);

        void deleteAddressInternetError();


        void updateCurrentAddressSuccess(UpdateCurrentAddressModel.Data deleteAddressModel);

        void updateCurrentAddressUnSucess(String message);

        void updateCurrentAddressInternetError();
    }
}
