package com.deliverymyorder.mvp.address.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.add_category.presenter.CategoryContractor;
import com.deliverymyorder.mvp.address.model.AddAddressModel;
import com.deliverymyorder.mvp.address.model.AddressListModel;
import com.deliverymyorder.mvp.address.model.DeleteAddressModel;
import com.deliverymyorder.mvp.address.model.UpdateCurrentAddressModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressPresenterImpl implements AddressContractor.AddressPresenter {

    Activity activity;
    AddressContractor.AddressView addressView;

    public AddressPresenterImpl(Activity activity, AddressContractor.AddressView addressView) {
        this.activity = activity;
        this.addressView = addressView;
    }

    @Override
    public void addAddressData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            callAddAddressDataApi(jsonObject);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            addressView.addAddressInternetError();

        }
    }

    @Override
    public void editAddressData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            callEditAddressDataApi(jsonObject);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            addressView.addAddressInternetError();

        }
    }

    @Override
    public void getAllAddressData() {
        try {
            ApiAdapter.getInstance(activity);
            callAllAddressDataApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            addressView.allAddressInternetError();

        }
    }

    @Override
    public void deleteAddressData(int addressId) {
        try {
            ApiAdapter.getInstance(activity);
            callDeleteAddressDataApi(addressId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            addressView.addAddressInternetError();

        }
    }

    @Override
    public void makeDefaultAddressData(int addressId) {
        try {
            ApiAdapter.getInstance(activity);
            callUpdateCurrentAddressDataApi(addressId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            addressView.updateCurrentAddressInternetError();

        }
    }


    private void callAddAddressDataApi(JSONObject jsonObject) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AddAddressModel> getResult = ApiAdapter.getApiService().getAddAddress("application/json", "no-cache", body);

        getResult.enqueue(new Callback<AddAddressModel>() {
            @Override
            public void onResponse(Call<AddAddressModel> call, Response<AddAddressModel> response) {

                Progress.stop();
                try {
                    AddAddressModel addAddressModel = response.body();

                    if (addAddressModel.getError() == 0) {
                        addressView.getAddAddressSuccess(addAddressModel.getData());
                    } else {
                        addressView.getAddAddressUnSucess(addAddressModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    addressView.getAddAddressUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<AddAddressModel> call, Throwable t) {
                Progress.stop();
                addressView.getAddAddressUnSucess(activity.getString(R.string.server_error));
            }
        });
    }


  private void callEditAddressDataApi(JSONObject jsonObject) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AddAddressModel> getResult = ApiAdapter.getApiService().getEditAddress("application/json", "no-cache", body);

        getResult.enqueue(new Callback<AddAddressModel>() {
            @Override
            public void onResponse(Call<AddAddressModel> call, Response<AddAddressModel> response) {

                Progress.stop();
                try {
                    AddAddressModel addAddressModel = response.body();

                    if (addAddressModel.getError() == 0) {
                        addressView.getAddAddressSuccess(addAddressModel.getData());
                    } else {
                        addressView.getAddAddressUnSucess(addAddressModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    addressView.getAddAddressUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<AddAddressModel> call, Throwable t) {
                Progress.stop();
                addressView.getAddAddressUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

    private void callAllAddressDataApi() {
        Progress.start(activity);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
         //   jsonObject.put(Const.KEY_CUSTOMER_ID, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AddressListModel> getResult = ApiAdapter.getApiService().getAddressList("application/json", "no-cache", body);

        getResult.enqueue(new Callback<AddressListModel>() {
            @Override
            public void onResponse(Call<AddressListModel> call, Response<AddressListModel> response) {

                Progress.stop();
                try {
                    AddressListModel addressListModel = response.body();

                    if (addressListModel.getError() == 0) {
                        addressView.getAllAddressSuccess(addressListModel.getData());
                    } else {
                        addressView.getAllAddressUnSucess(addressListModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    exp.printStackTrace();
                    addressView.getAllAddressUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<AddressListModel> call, Throwable t) {
                Progress.stop();
                t.printStackTrace();
                addressView.getAllAddressUnSucess(activity.getString(R.string.server_error));
            }
        });
    }


    private void callDeleteAddressDataApi(int addressId) {
        Progress.start(activity);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            //   jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_ADDRESS_ID, addressId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<DeleteAddressModel> getResult = ApiAdapter.getApiService().deleteAddress("application/json", "no-cache", body);

        getResult.enqueue(new Callback<DeleteAddressModel>() {
            @Override
            public void onResponse(Call<DeleteAddressModel> call, Response<DeleteAddressModel> response) {

                Progress.stop();
                try {
                    DeleteAddressModel deleteAddressModel = response.body();

                    if (deleteAddressModel.getError() == 0) {
                        addressView.deleteAddressSuccess(deleteAddressModel.getData());
                    } else {
                        addressView.deleteAddressUnSucess(deleteAddressModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    addressView.deleteAddressUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<DeleteAddressModel> call, Throwable t) {
                Progress.stop();
                addressView.deleteAddressUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

    private void callUpdateCurrentAddressDataApi(int addressId) {
        Progress.start(activity);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
           // jsonObject.put(Const.KEY_CUSTOMER_ID, 1);
            jsonObject.put(Const.KEY_ADDRESS_ID, addressId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<UpdateCurrentAddressModel> getResult = ApiAdapter.getApiService().updateCurrentAddress("application/json", "no-cache", body);

        getResult.enqueue(new Callback<UpdateCurrentAddressModel>() {
            @Override
            public void onResponse(Call<UpdateCurrentAddressModel> call, Response<UpdateCurrentAddressModel> response) {

                Progress.stop();
                try {
                    UpdateCurrentAddressModel updateCurrentAddressModel = response.body();

                    if (updateCurrentAddressModel.getError() == 0) {
                        addressView.updateCurrentAddressSuccess(updateCurrentAddressModel.getData());
                    } else {
                        addressView.updateCurrentAddressUnSucess(updateCurrentAddressModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    addressView.updateCurrentAddressUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateCurrentAddressModel> call, Throwable t) {
                Progress.stop();
                addressView.updateCurrentAddressUnSucess(activity.getString(R.string.server_error));
            }
        });
    }
}
