package com.deliverymyorder.mvp.address;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.address.adapter.AddressListAdapter;
import com.deliverymyorder.mvp.address.model.AddAddressModel;
import com.deliverymyorder.mvp.address.model.AddressListModel;
import com.deliverymyorder.mvp.address.model.DeleteAddressModel;
import com.deliverymyorder.mvp.address.model.UpdateCurrentAddressModel;
import com.deliverymyorder.mvp.address.presenter.AddressContractor;
import com.deliverymyorder.mvp.address.presenter.AddressPresenterImpl;
import com.deliverymyorder.mvp.common.MapRouteContractor;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressListActivity extends AppCompatActivity implements AddressContractor.AddressView {


    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.recyclerViewAddressList)
    RecyclerView recyclerViewAddressList;

    @BindView(R.id.txtViewAddNewAddress)
    TextView txtViewAddNewAddress;

    AddressPresenterImpl addressPresenter;
    AddressListAdapter addressListAdapter;

    int addressId;


    AddressChangeListner addressChangeListner;

    public void setAddressChangeListner(AddressChangeListner addressChangeListner) {
        this.addressChangeListner = addressChangeListner;
    }

    ArrayList<AddressListModel.Datum> arrayListAddresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);

        ButterKnife.bind(this);

        addressPresenter = new AddressPresenterImpl(this, this);



    }

    @Override
    protected void onStart() {
        super.onStart();
        getAllAddressListDataApi();
    }

    @OnClick(R.id.imgViewBack)
    public  void imgViewBack(View view)
    {
        finish();
    }

    private void manageRecyclerView() {
        addressListAdapter = new AddressListAdapter(this, arrayListAddresses, this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerViewAddressList.setLayoutManager(mLayoutManager);
        recyclerViewAddressList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAddressList.setAdapter(addressListAdapter);
    }

    private void getAllAddressListDataApi() {
        addressPresenter.getAllAddressData();
    }

    public void callDeleteAddressDataApi(int addressId) {

        this.addressId = addressId;
        addressPresenter.deleteAddressData(addressId);
    }

    public void callUpdateCurrentAddressDataApi(int addressId) {

        this.addressId = addressId;
        addressPresenter.makeDefaultAddressData(addressId);
    }


    @Override
    public void getAddAddressSuccess(AddAddressModel.Data addAddressModel) {

    }

    @Override
    public void getAddAddressUnSucess(String message) {
        //not used
    }

    @Override
    public void addAddressInternetError() {
        //not used
    }

    @Override
    public void getAllAddressSuccess(ArrayList<AddressListModel.Datum> arrayListAddresses) {

        this.arrayListAddresses = arrayListAddresses;
        manageRecyclerView();
    }

    @Override
    public void getAllAddressUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void allAddressInternetError() {

    }

    @Override
    public void deleteAddressSuccess(DeleteAddressModel.Data deleteAddressModel) {
        getAllAddressListDataApi();
    }

    @Override
    public void deleteAddressUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void deleteAddressInternetError() {

    }

    @Override
    public void updateCurrentAddressSuccess(UpdateCurrentAddressModel.Data deleteAddressModel) {


        PrefUtil.putInt(this, Const.KEY_ADDRESS_ID, deleteAddressModel.getCurrentAddressId());

        if (addressChangeListner!=null)
            addressChangeListner.addIsChanged();

        finish();
    }

    @Override
    public void updateCurrentAddressUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void updateCurrentAddressInternetError() {

    }

    @OnClick(R.id.txtViewAddNewAddress)
    public void txtViewAddNewAddress(){
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra(ConstIntent.CONST_IS_COME_FOR_ADD, true);
        startActivity(intent);

    }


    public  interface  AddressChangeListner
    {
        void  addIsChanged();
    }


}
