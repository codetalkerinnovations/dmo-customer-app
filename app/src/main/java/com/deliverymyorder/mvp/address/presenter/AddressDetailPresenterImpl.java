package com.deliverymyorder.mvp.address.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.model.UpdateDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.presenter.DeliveryTypeContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressDetailPresenterImpl implements AddressDetailContractor.AddressDetailPresenter {

    Activity activity;

    AddressDetailContractor.AddressDetailView addressDetailView;

    public AddressDetailPresenterImpl(Activity activity, AddressDetailContractor.AddressDetailView addressDetailView) {
        this.activity = activity;
        this.addressDetailView = addressDetailView;
    }


    @Override
    public void getAddressDetailData(int addressId) {
        try {
            ApiAdapter.getInstance(activity);
            callGetAddressDetailDataApi(addressId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            addressDetailView.getAddressDetailInternetError();
        }
    }


    private void callGetAddressDetailDataApi(int addressId) {
        Progress.start(activity);

        //int addressId = LoginManager.getInstance().getUserData().getCurrentAddressId();
        //int addressId = PrefUtil.getInt(activity,Const.KEY_ADDRESS_ID,0);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_ADDRESS_ID, addressId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AddressDetailModel> getResult = ApiAdapter.getApiService().getAddressDetailById("application/json", "no-cache", body);

        getResult.enqueue(new Callback<AddressDetailModel>() {
            @Override
            public void onResponse(Call<AddressDetailModel> call, Response<AddressDetailModel> response) {

                Progress.stop();
                try {
                    AddressDetailModel addressDetailModel = response.body();

                    if (addressDetailModel.getError() == 0) {
                        addressDetailView.getAddressDetailSuccess(addressDetailModel.getData());
                    } else {
                        addressDetailView.getAddressDetailUnSucess(addressDetailModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    addressDetailView.getAddressDetailUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<AddressDetailModel> call, Throwable t) {
                Progress.stop();
                addressDetailView.getAddressDetailUnSucess(activity.getString(R.string.server_error));
            }
        });
    }


}