package com.deliverymyorder.mvp.address.presenter;

import com.deliverymyorder.mvp.address.model.AddressDetailModel;

public class AddressDetailContractor {

    public interface AddressDetailPresenter {

        void getAddressDetailData(int addressId);
    }

    public interface AddressDetailView {
        void getAddressDetailSuccess(AddressDetailModel.Data addressDetailModel);

        void getAddressDetailUnSucess(String message);

        void getAddressDetailInternetError();

    }
}
