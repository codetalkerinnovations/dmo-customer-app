package com.deliverymyorder.mvp;

import android.Manifest;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.helpers.UploadPhotoHelper;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.custom_shop_order.AddBuinessContractor;
import com.deliverymyorder.mvp.custom_shop_order.AddBuisnessResponseModel;
import com.deliverymyorder.mvp.delivery_type.DeliveryTypeActivity;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailContractor;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailPresenterImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomMenuActivity extends AppCompatActivity   implements ShoppingDetailContractor.ShoppingDetailView, AddBuinessContractor.AddBuinesView {

//    @BindView(R.id.llayItemsChilds)
//    LinearLayout llayItemsChilds;
//
//    @BindView(R.id.txtViewQty)
//    TextView txtViewQty;
//
//    @BindView(R.id.edtTextItemName)
//    EditText edtTextItemName;
//
//    @BindView(R.id.edtTextItemDesc)
//    EditText edtTextItemDesc;
//
//    @BindView(R.id.imgViewCross)
//    ImageView imgViewCross;
//
//    @BindView(R.id.imgProduct)
//    ImageView imgProduct;


    ArrayList<View> arrayListView;

    String imagePath = null;
    File photo = null;
    Bitmap photoBitmap = null;
    Uri mImageUri;


    int shopHaveProducts = 0;

    private final int PERMISSION_CODE_REQUEST_CAMERA = 2906;
    private static int RESULT_LOAD_IMG = 2907;

    private int REQUEST_CAMERA = 0;

    ArrayList<CustomMenuModel> arrayListCustomMenu;


    int cutomOrderAllowed = 0;
    int buisnessId = 0;
    int merchantId = 0;
    String shopName;
    String address;
    int totalQty = 0;
    int totalPrice = 0;
    String searchKey;


    ShoppingDetailPresenterImpl shoppingDetailPresenter;

    @BindView(R.id.txtViewShopName)
    TextView txtViewShopName;

    @BindView(R.id.txtViewAddress)
    TextView txtViewAddress;


    @BindView(R.id.recylViewCustomProduct)
    RecyclerView recylViewCustomProduct;


    CustomOrderAdapter customOrderAdapter;


    int currentEditingPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_menu);

        ButterKnife.bind(this);

        arrayListView = new ArrayList<>();
        arrayListCustomMenu = new ArrayList<>();

        shoppingDetailPresenter = new ShoppingDetailPresenterImpl(this,this);

        if (getIntent() != null) {

            if (getIntent().hasExtra(ConstIntent.CONST_BUISNESS_ID)) {
                shopHaveProducts = getIntent().getIntExtra(ConstIntent.CONST_SHOP_HAS_PRODUCTS,0);
                buisnessId = getIntent().getIntExtra(ConstIntent.CONST_BUISNESS_ID, 0);
                merchantId = getIntent().getIntExtra(ConstIntent.CONST_MERCHANT_ID, 0);
                shopName = getIntent().getStringExtra(ConstIntent.CONST_SHOP_NAME);
                address = getIntent().getStringExtra(ConstIntent.CONST_ADDRESS);
                cutomOrderAllowed = getIntent().getIntExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, 0);

                try {
                    arrayListCustomMenu = getIntent().getParcelableArrayListExtra(ConstIntent.CONST_ADDED_CUSTOMER_ORDER);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

                if (arrayListCustomMenu != null)
                {

                }else
                {
                    arrayListCustomMenu = new ArrayList<>();
                }

            }
        }


        if ( arrayListCustomMenu.size() == 0)
        {
            arrayListCustomMenu.add(new CustomMenuModel());
        }


        txtViewShopName.setText(shopName);
        txtViewAddress.setText(address);
        manageRecyclerView();
    }


    public  void  manageRecyclerView()
    {

        recylViewCustomProduct.setLayoutManager(new LinearLayoutManager(this));

        customOrderAdapter = new CustomOrderAdapter(this,arrayListCustomMenu);
        customOrderAdapter.setCustomOrderInterface(new CustomOrderAdapter.CustomOrderInterface() {
            @Override
            public void selectImage(int position) {
                currentEditingPosition = position;
                imgViewCamera();
            }
        });

        recylViewCustomProduct.setItemAnimator(new DefaultItemAnimator());
        recylViewCustomProduct.setAdapter(customOrderAdapter);


    }

/*
    private  void addFiledsFromArray()
    {

        for (CustomMenuModel customMenuModel:arrayListCustomMenu) {

            final View view = LayoutInflater.from(this).inflate(R.layout.item_custom_create_own_item, null);

            ImageView imgViewMinus = (ImageView) view.findViewById(R.id.imgViewMinus);
            ImageView imgViewPlus = (ImageView) view.findViewById(R.id.imgViewPlus);
            ImageView imgViewCross = (ImageView) view.findViewById(R.id.imgViewCross);
            ImageView imgViewCamera = (ImageView) view.findViewById(R.id.imgViewCamera);

            ImageView imgProduct = (ImageView) view.findViewById(R.id.imgProduct);

            final EditText edtTextItemName = (EditText) view.findViewById(R.id.edtTextItemName);
            EditText edtTextItemDesc = (EditText) view.findViewById(R.id.edtTextItemDesc);
            final TextView txtViewQtyIn = (TextView) view.findViewById(R.id.txtViewQty);


            // set Values :
            edtTextItemName.setText(customMenuModel.getItemName());
            edtTextItemDesc.setText(customMenuModel.getItemDesc());
            // imgProduct.set


            imgViewMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String qty = txtViewQtyIn.getText().toString().trim();
                    int qtyInInt = Integer.parseInt(qty);

                    if (qtyInInt > 1) {
                        qtyInInt = qtyInInt - 1;
                        qty = String.valueOf(qtyInInt);
                        txtViewQtyIn.setText(qty);
                        edtTextItemName.setTag(qty);
                    } else {
                        AlertDialogHelper.showAlertDialog(CustomShopOrderActivity.this, "Minimus qty will be one.");
                    }
                }
            });

            imgViewCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectFromCamera();


                }
            });

            imgViewCross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrayListView.remove(view);
                    llayItemsChilds.removeView(view);
                }
            });

            imgViewPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String qty = txtViewQtyIn.getText().toString().trim();
                    int qtyInInt = Integer.parseInt(qty);

                    qtyInInt = qtyInInt + 1;
                    qty = String.valueOf(qtyInInt);
                    txtViewQtyIn.setText(qty);
                    edtTextItemName.setTag(qty);
                }
            });

            arrayListView.add(view);
            llayItemsChilds.addView(view);

        }
    }
*/

    @OnClick(R.id.txtViewAddress)
    public void txtViewAddress(View view)
    {
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//
//        try {
//            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
//        } catch (GooglePlayServicesRepairableException e) {
//            AlertDialogHelper.showAlertDialog(this,e.getLocalizedMessage());
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            AlertDialogHelper.showAlertDialog(this,e.getLocalizedMessage());
//            e.printStackTrace();
//        }
    }

    //li @OnClick(R.id.imgViewCamera)
    public void imgViewCamera(){


        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogTheme);
        builder.setTitle("");
        builder.setMessage("Please select image source");
        builder.setPositiveButton("Camera",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        selectFromCamera();
                        dialog.cancel();
                    }
                });

        builder.setNeutralButton("Cancel",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {

                    }
                });

        builder.setNegativeButton("Gallery",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        selectFromGallery();
                        dialog.cancel();
                    }
                });
        builder.create().show();


        //selectFromCamera();
    }

    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirm(){

        if (shopHaveProducts == 1) {
            if (validation()) {
                sendDataBackToMenuActivity();
            }
        }else {

            if (validation()) {
                createOrderAPI();
            }
        }
    }


    public void sendDataBackToMenuActivity(){
        Intent intent=new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstIntent.CONST_ARRAYLIST_CUSTOM_MENU,customOrderAdapter.getDatum());
        intent.putExtras(bundle);
        setResult(Const.KEY_CUSTOM_MENU_REQUEST_CODE,intent);
        finish();//finishing activity
    }
    public  boolean addBuissnessValidation()
    {
        if (Utils.isEmptyOrNull(txtViewShopName.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter shop name");
            return false;
        }

        if (Utils.isEmptyOrNull(address))
        {
            AlertDialogHelper.showAlertDialog(this,"Please locate your shop");
            return false;
        }



        return  true;
    }

    ///imgViewBack
    @OnClick(R.id.imgViewBack)
    public void imgViewBack(){
        finish();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        // requestCode (2908 for pick image from gallary), (2909 for camera) and 2910 is for selectedItemLocation permission

        switch (requestCode) {



            case PERMISSION_CODE_REQUEST_CAMERA: {

                int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA);

                int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

                int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);


                if ((permissionCheckCamera == 0) && (permissionCheckRead == 0) && (permissionCheckWrite == 0)) {
                    openCamera();
                } else {
                    //AlertDialogHelper.showMessage(DashboardActivity.this, "Please provide security permission from app setting.");
                }

                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {

                //------------------->>> 1st method to get image from gallery <<<-------------------------------

                Uri filePath = data.getData();

                //Log.e("filePath", "" + filePath);
                imagePath = Utils.getRealPathFromURI(filePath.toString(), this);

                if (customOrderAdapter != null)
                    customOrderAdapter.setImageAt(imagePath,currentEditingPosition);

                //Log.e("gallery image imagePath", "-------------->" + imagePath);
                //SnackNotify.showMessage(getString(R.string.title_successfully_upload), coordinatorLayout);

                setImage();
/*                arrayListAttachment.add(imagePathResized);


                String thumbnailImage = UplodPhotoHelper.generateThumbnailImage(imagePathResized, this);
                arrayListThumbnail.add(thumbnailImage);*/


            } else if (requestCode == REQUEST_CAMERA) {
                // taking photo using hardware camera
                onCaptureImageResult(data);

            }



        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }


    private void setImage() {

        File file = new File(imagePath);
        Log.e("SetImage", imagePath);

        if (customOrderAdapter!=null)
            customOrderAdapter.setImageAt(imagePath,currentEditingPosition);
    }

/*
    private void getAddressFromLatLongUpdated() {

        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                String address = addresses.get(0).getAddressLine(0);

                if (address==null)
                {
                    address = "unnamed place";
                }

                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();


                txtViewAddress.setText(address); //latitude + " , " + longitude);

                Log.e("Addr Lat", address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

*/

    private void onCaptureImageResult(Intent data) {
        String path = null;
        String absolutePath = null;
        float rotate = 0;

        ContentResolver cr = getContentResolver();
        cr.notifyChange(mImageUri, null);

        absolutePath = new File(mImageUri.getPath()).getAbsoluteFile().toString();
        path = new File(mImageUri.getPath()).toString();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            rotate = UploadPhotoHelper.getOrientation(photo.getPath());
        else
            rotate = UploadPhotoHelper.getOrientation(path);


        Bitmap bitmap = null;

        try {
            bitmap = UploadPhotoHelper.rotateImage(MediaStore.Images.Media.getBitmap(cr, mImageUri), rotate);
        } catch (Exception e) {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();

        }

        try {
            photoBitmap = UploadPhotoHelper.resizeImageForImageView(bitmap, rotate);

            //imgProduct.setImageBitmap(photoBitmap);

            uploadImage(photoBitmap);



        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
        }

    }

    private void uploadImage(Bitmap image) {
        if (photoBitmap != null) {

            imagePath = UploadPhotoHelper.getAbsolutePath(image, "_resize.JPEG");
            Log.e("imagePath",""+imagePath);
            Log.e("mImageUri",""+mImageUri);

            if (customOrderAdapter != null)
                customOrderAdapter.setImageAt(imagePath,currentEditingPosition);


        }
    }


    @OnClick(R.id.txtViewAddMore)
    public void txtViewAddMore() {

        if (customOrderAdapter!=null)
        {
            customOrderAdapter.addMore();
        }else
        {
            manageRecyclerView();
        }

        /*
        final View view = LayoutInflater.from(this).inflate(R.layout.item_custom_create_own_item, null);

        ImageView imgViewMinus = (ImageView) view.findViewById(R.id.imgViewMinus);
        ImageView imgViewPlus = (ImageView) view.findViewById(R.id.imgViewPlus);
        ImageView imgViewCross = (ImageView) view.findViewById(R.id.imgViewCross);
        ImageView imgViewCamera = (ImageView) view.findViewById(R.id.imgViewCamera);
        final EditText edtTextItemName = (EditText) view.findViewById(R.id.edtTextItemName);
        EditText edtTextItemDesc = (EditText) view.findViewById(R.id.edtTextItemDesc);
        final TextView txtViewQtyIn = (TextView)view.findViewById(R.id.txtViewQty);

        imgViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = txtViewQtyIn.getText().toString().trim();
                int qtyInInt = Integer.parseInt(qty);

                if (qtyInInt > 1) {
                    qtyInInt = qtyInInt - 1;
                    qty = String.valueOf(qtyInInt);
                    txtViewQtyIn.setText(qty);
                    edtTextItemName.setTag(qty);
                }else {
                    AlertDialogHelper.showAlertDialog(CustomShopOrderActivity.this,"Minimus qty will be one.");
                }
            }
        });

        imgViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFromCamera();


            }
        });

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayListView.remove(view);
                llayItemsChilds.removeView(view);
            }
        });

        imgViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = txtViewQtyIn.getText().toString().trim();
                int qtyInInt = Integer.parseInt(qty);

                qtyInInt = qtyInInt + 1;
                qty = String.valueOf(qtyInInt);
                txtViewQtyIn.setText(qty);
                edtTextItemName.setTag(qty);
            }
        });

        arrayListView.add(view);
        llayItemsChilds.addView(view);

        */
    }

//    @OnClick(R.id.imgViewMinus)
//    public void imgViewMinus() {
//
//        String qty = txtViewQty.getText().toString().trim();
//        int qtyInInt = Integer.parseInt(qty);
//
//        if (qtyInInt > 1) {
//            qtyInInt = qtyInInt - 1;
//            qty = String.valueOf(qtyInInt);
//            txtViewQty.setText(qty);
//        }else {
//            AlertDialogHelper.showAlertDialog(this,"Minimus qty will be one.");
//        }
//    }


//    @OnClick(R.id.imgViewPlus)
//    public void imgViewPlus() {
//
//        String qty = txtViewQty.getText().toString().trim();
//        int qtyInInt = Integer.parseInt(qty);
//
//        qtyInInt = qtyInInt + 1;
//        qty = String.valueOf(qtyInInt);
//        txtViewQty.setText(qty);
//
//    }

    private void selectFromCamera() {

        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if ((permissionCheckCamera == -1) || (permissionCheckRead == -1) || (permissionCheckWrite == -1)) {

                if (!Settings.System.canWrite(this)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_CODE_REQUEST_CAMERA);
                } else {

                    openCamera();
                }
            } else {
                openCamera();
            }
        } else {
            openCamera();
        }
    }


    private void selectFromGallery() {

        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if ((permissionCheckCamera == -1) || (permissionCheckRead == -1) || (permissionCheckWrite == -1)) {

                if (!Settings.System.canWrite(this)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_CODE_REQUEST_CAMERA);
                } else {

                    openGallery();
                }
            } else {

                openGallery();
            }
        } else {

            openGallery();
        }
    }

    private void openCamera() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
        } else {
            intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        }

        try {
            // place where to store camera taken picture
            photo = createTemporaryFile("picture", ".JPEG");
        } catch (Exception e) {
            e.printStackTrace();
            // Log.v("pic", "Can't create file to take picture! ");
        }
        mImageUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photo);


        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void openGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }




    public static File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }


//    public void getSingleLayoutData(){
//
//
//        String itemName = edtTextItemName.getText().toString().trim();
//        String itemDesc = edtTextItemDesc.getText().toString().trim();
//        String itemQty = txtViewQty.getText().toString().trim();
//
//        if(!Utils.isEmptyOrNull(itemName)){
//            CustomMenuModel customMenuModel = new CustomMenuModel();
//
//            customMenuModel.setItemDesc(itemDesc);
//            customMenuModel.setQty(itemQty);
//            customMenuModel.setItemName(itemName);
//            customMenuModel.setItemImage("");
//
//            arrayListCustomMenu.add(customMenuModel);
//        }else {
//            AlertDialogHelper.showAlertDialog(this,"Please fill item name in all box.");
//        }
//
//
//
//    }
//
//    public void getFormData(){
//
//        for( View view : arrayListView){
//
//            EditText edtTextItemName = (EditText) view.findViewById(R.id.edtTextItemName);
//            EditText edtTextItemDesc = (EditText) view.findViewById(R.id.edtTextItemDesc);
//
//            String itemName = edtTextItemName.getText().toString().trim();
//            String itemDesc = edtTextItemDesc.getText().toString().trim();
//            String itemQty = "1";
//            if(edtTextItemName.getTag() != null) {
//                itemQty = edtTextItemName.getTag().toString();
//            }
//
//            if(Utils.isEmptyOrNull(itemName)){
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        AlertDialogHelper.showAlertDialog(CustomShopOrderActivity.this,"Please fill item name in all box.");
//                    }
//                });
//
//            }else {
//
//                CustomMenuModel customMenuModel = new CustomMenuModel();
//
//                customMenuModel.setItemDesc(itemDesc);
//                customMenuModel.setQty(itemQty);
//                customMenuModel.setItemName(itemName);
//                customMenuModel.setItemImage("");
//
//                arrayListCustomMenu.add(customMenuModel);
//            }
//        }
//}



    private  void createOrderAPI()
    {

        Progress.start(this);


        new Thread(new Runnable() {
            @Override
            public void run() {

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();


        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();


        for (int i = 0; i < arrayListCustomMenu.size(); i++) {

            CustomMenuModel customMenuModel = arrayListCustomMenu.get(i);

            JSONObject jsonObjectItems = new JSONObject();
            try {

                jsonObjectItems.put(Const.PARAM_ID, 0);
                jsonObjectItems.put(Const.PARAM_NAME, customMenuModel.getItemName());
                jsonObjectItems.put(Const.PARAM_QTY, customMenuModel.getQty());
                jsonObjectItems.put(Const.PARAM_PRICE, 0);
                jsonObjectItems.put(Const.PARAM_DESCRIPTION, customMenuModel.getItemDesc());
                jsonObjectItems.put(Const.PARAM_IMAGE,Utils.getBase64StringFromImage(customMenuModel.getItemImage()));

                totalQty = totalQty + customMenuModel.getQty();

                  /*  String itemPrice = arrayListShopDetail.get(i).getItemPrice();

                    if (itemPrice.contains(".")) {
                        String[] priceArray = itemPrice.split(".");*/
                //  totalPrice = totalPrice + (arrayListShopDetail.get(i).getQty() * Integer.parseInt(priceArray[0]));
                //totalPrice = totalPrice + (arrayListShopDetail.get(i).getQty() * Integer.parseInt(arrayListShopDetail.get(i).getItemPrice().replace(".00", "")));


            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObjectItems);
        }


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_BUISNESS_ID, buisnessId);
            jsonObject.put(Const.KEY_MERCHANT_ID, merchantId);
            jsonObject.put(Const.KEY_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_TOTAL_ITEMS, totalQty);
            jsonObject.put(Const.PARAM_TOTAL_AMOUNT, totalPrice);
            jsonObject.put(Const.PARAM_ITEMS, jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (totalQty > 0) {
            shoppingDetailPresenter.getCreateorderDataApi(jsonObject);
        } else {
            //SnackNotify.showMessage("Please add atleast one product.", relLayMain);

            if (CustomMenuActivity.this!=null)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialogHelper.showAlertDialog(CustomMenuActivity.this,"Please add atleast one product.");
                    }
                });

        }


            }
        }).start();


    }





    public  String getBase64FromFile(String path)
    {
        Bitmap bmp = null;
        ByteArrayOutputStream baos = null;
        byte[] baat = null;
        String encodeString = null;
        try
        {
            bmp = BitmapFactory.decodeFile(path);
            baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            baat = baos.toByteArray();
            encodeString = Base64.encodeToString(baat, Base64.DEFAULT);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return encodeString;
    }


    @Override
    public void getShoppingDetailDataSuccess(ArrayList<ShopDetailModel.Datum> arrayListShopDetail) {

    }

    @Override
    public void getShoppingDetailDataUnSucess(String message) {

    }

    @Override
    public void ShoppingDetailDataInternetError() {

    }

    @Override
    public void getCreateOrderSuccess(CreateOrderResponseModel.Data createOrderResponseModel) {

        Intent intent = new Intent(this, DeliveryTypeActivity.class);
        intent.putExtra(ConstIntent.CONST_ORDER_ID, createOrderResponseModel.getId());
        startActivity(intent);
    }

    @Override
    public void getCreateOrderUnSucess(String message) {

    }

    @Override
    public void CreateOrderInternetError() {

    }

    @Override
    public void getAddBuinesSuccess(AddBuisnessResponseModel addBuisnessResponseModel) {

        if (addBuisnessResponseModel!= null)
        {
            buisnessId = addBuisnessResponseModel.getData().getId();

//            getSingleLayoutData();
//            getFormData();
            if (validation()) {
                createOrderAPI();
            }
        }

    }

    public boolean validation()
    {

        arrayListCustomMenu = customOrderAdapter.getDatum();

        for (CustomMenuModel customMenuModel:arrayListCustomMenu)
        {

            if(Utils.isEmptyOrNull(customMenuModel.getItemName()))
            {
                AlertDialogHelper.showMessage(this,"Please enter all item name");
                return  false;
            }

            if(Utils.isEmptyOrNull(customMenuModel.getItemDesc()))
            {
                AlertDialogHelper.showMessage(this,"Please enter all item description");
                return  false;
            }

            if (customMenuModel.getQty() == 0)
            {
                AlertDialogHelper.showMessage(this,"Quantity can't be zero");
                return  false;
            }

        }


        return  true;
    }

    @Override
    public void getAddBuinesUnSucess(String message) {

    }

    @Override
    public void addBuinesInternetError() {

    }


    // Converting Bitmap image to Base64.encode String type
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    // Converting File to Base64.encode String type using Method
    public String getStringFile(File f) {
        InputStream inputStream = null;
        String encodedFile= "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile =  output.toString();
        }
        catch (FileNotFoundException e1 ) {
            e1.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;
    }
}

