package com.deliverymyorder.mvp.show_current_capus;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.shop_listing.adapter.ShoppingListAdapter;
import com.deliverymyorder.mvp.show_current_capus.adapter.CampusAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CurrentCampusFragment extends DialogFragment implements CampusAdapter.CampusChangeListner {

    List<Campus> campusList;


    CampusAdapter.CampusChangeListner campusChangeListner;


    @BindView(R.id.recyclerViewCampusList)
    RecyclerView recyclerViewCampusList;

    @BindView(R.id.btnClosePopUp)
    Button btnClosePopUp;


    public CurrentCampusFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentCampusFragment newInstance(List<Campus> campusList) {
        CurrentCampusFragment fragment = new CurrentCampusFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("campus_list",new ArrayList<Parcelable>(campusList));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           campusList = getArguments().getParcelableArrayList("campus_list");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_current_campus, container, false);
        ButterKnife.bind(this,view);

        manageRecyclerView();
        return view;
    }


    private void manageRecyclerView() {

        CampusAdapter shoppingListAdapter = new CampusAdapter(getActivity(), campusList);
        shoppingListAdapter.setCampusChangeListner(this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewCampusList.setLayoutManager(mLayoutManager);
        recyclerViewCampusList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCampusList.setAdapter(shoppingListAdapter);
    }

    public CampusAdapter.CampusChangeListner getCampusChangeListner() {
        return campusChangeListner;
    }

    public void setCampusChangeListner(CampusAdapter.CampusChangeListner campusChangeListner) {
        this.campusChangeListner = campusChangeListner;
    }


    @OnClick(R.id.btnClosePopUp)
    public  void btnClosePopUp(View view)
    {
        this.dismiss();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void campusDidChangeStatus(Campus campus) {

        LoginManager.getInstance().setCurrentCampusSession(campus);

        if (campusChangeListner!=null)
        {
            campusChangeListner.campusDidChangeStatus(campus);
        }

        this.dismiss();
    }
}
