package com.deliverymyorder.mvp.show_current_capus.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.Campus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CampusAdapter extends RecyclerView.Adapter<CampusAdapter.Holder> {


    Activity activity;
    List<Campus>   campusList;
    String favBit;

    public CampusChangeListner getCampusChangeListner() {
        return campusChangeListner;
    }

    public void setCampusChangeListner(CampusChangeListner campusChangeListner) {
        this.campusChangeListner = campusChangeListner;
    }

    CampusChangeListner campusChangeListner;

    public CampusAdapter(Activity activity, List<Campus> datum) {
        this.activity = activity;
        this.campusList = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.campus_item, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final Campus result = campusList.get(position);


        holder.txtViewCampusTitle.setText(result.getTitle());

        holder.btnConfirmCampus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //PrefUtil.
                if (campusChangeListner!=null)
                    campusChangeListner.campusDidChangeStatus(result);

            }
        });

    }


    @Override
    public int getItemCount() {
        return campusList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {



        @BindView(R.id.txtViewCampusTitle)
        TextView txtViewCampusTitle;

        @BindView(R.id.btnConfirmCampus)
        Button btnConfirmCampus;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.setFontFace(txtViewCampusTitle, FontHelper.FontType.FONT_ROBOTO_LIGHT, activity);

            // setFont();
        }
    }


    public  interface  CampusChangeListner
    {
          void campusDidChangeStatus(Campus campus);
    }
}