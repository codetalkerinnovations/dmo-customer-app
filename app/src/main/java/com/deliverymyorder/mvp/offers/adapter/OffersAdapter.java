package com.deliverymyorder.mvp.offers.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.deliverymyorder.Common.helpers.RoundCornerTransformation;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.offers.OffersDetailActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.Holder> {


    Activity activity;
    ArrayList<HomePageDataModel.Data.Banner> datum;
    String favBit;

    public OffersAdapter(Activity activity, ArrayList<HomePageDataModel.Data.Banner> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.layout_item_banner, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final HomePageDataModel.Data.Banner result = datum.get(position);


        ///Picasso.with(activity).load(result.getImage()).transform(new RoundCornerTransformation()).into(holder.imageViewBanner);



        Picasso.with(activity).load(result.getImage())
                .into(holder.imageViewBanner, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) holder.imageViewBanner.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(activity.getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(10,10));
                        holder.imageViewBanner.setImageDrawable(imageDrawable);
                    }
                    @Override
                    public void onError() {
                        holder.imageViewBanner.setImageResource(R.mipmap.ic_img_placeholder);
                    }
                });


        holder.linLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, OffersDetailActivity.class);
                intent.putExtra(ConstIntent.CONST_COUPEN_CODE, result.getCouponCode());
                intent.putExtra(ConstIntent.CONST_COUPEN_IMAGE, result.getImage());
                intent.putExtra(ConstIntent.CONST_START_DATE, result.getValidFrom());
                intent.putExtra(ConstIntent.CONST_EXP_DATE, result.getValidTo());

                activity.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.linLayItem)
        LinearLayout linLayItem;

        @BindView(R.id.imageViewBanner)
        ImageView imageViewBanner;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}



