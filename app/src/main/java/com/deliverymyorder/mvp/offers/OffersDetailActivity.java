package com.deliverymyorder.mvp.offers;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deliverymyorder.Common.helpers.RoundCornerTransformation;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OffersDetailActivity extends AppCompatActivity {

    @BindView(R.id.imageViewBanner)
    ImageView imageViewBanner;

    @BindView(R.id.txtViewCoupenCodeTitle)
    TextView txtViewCoupenCodeTitle;

    @BindView(R.id.txtViewCoupenCode)
    TextView txtViewCoupenCode;

    String coupenCode;
    String startDate;
    String expDate;
    String coupenUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_detail);
        ButterKnife.bind(this);

        if (getIntent() != null) {

            coupenCode = getIntent().getStringExtra(ConstIntent.CONST_COUPEN_CODE);
            startDate = getIntent().getStringExtra(ConstIntent.CONST_START_DATE);
            expDate = getIntent().getStringExtra(ConstIntent.CONST_EXP_DATE);
            coupenUrl = getIntent().getStringExtra(ConstIntent.CONST_COUPEN_IMAGE);
        }

        txtViewCoupenCode.setText(coupenCode);
        //Picasso.with(this).load(coupenUrl).transform(new RoundCornerTransformation()).into(imageViewBanner);

        Picasso.with(this).load(coupenUrl)
                .into(imageViewBanner, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) imageViewBanner.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(15,15));
                        imageViewBanner.setImageDrawable(imageDrawable);
                    }
                    @Override
                    public void onError() {
                        imageViewBanner.setImageResource(R.mipmap.ic_img_placeholder);
                    }
                });

    }


    @OnClick(R.id.txtViewCoupenCode)
    public void txtViewCoupenCodeClicked() {

        String text = txtViewCoupenCode.getText().toString();
        ClipData myClip = ClipData.newPlainText("text", text);
        ClipboardManager myClipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
        myClipboard.setPrimaryClip(myClip);
        Toast.makeText(getApplicationContext(), "Coupen Code Copied",
                Toast.LENGTH_SHORT).show();

    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {

        finish();
    }
}
