package com.deliverymyorder.mvp.offers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;

import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.dashboard.presenter.HomePageContractor;
import com.deliverymyorder.mvp.dashboard.presenter.HomePagePresenterImpl;
import com.deliverymyorder.mvp.offers.adapter.OffersAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OffersActivity extends AppCompatActivity implements HomePageContractor.HomePageView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.recyclerViewCoupen)
    RecyclerView recyclerViewCoupen;


    HomePagePresenterImpl homePagePresenter;

    OffersAdapter offersAdapter;
    HomePageDataModel.Data homePageDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);

        ButterKnife.bind(this);

        homePagePresenter = new HomePagePresenterImpl(this, this);

        homePagePresenter.getHomePageData();
    }

    private void manageRecyclerView() {

        offersAdapter = new OffersAdapter(this, homePageDataModel.getBanner());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewCoupen.setLayoutManager(mLayoutManager);
        recyclerViewCoupen.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCoupen.setAdapter(offersAdapter);
    }


    @Override
    public void getHomePageDataSuccess(HomePageDataModel.Data arrayListHomePageDataModel) {
        this.homePageDataModel = arrayListHomePageDataModel;

        manageRecyclerView();
    }

    @Override
    public void getHomePageDataUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void homePageDataInternetError() {

    }

    @Override
    public void getSearchDataSuccess(ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel) {

        //Not Used
    }

    @Override
    public void getSearchDataUnSucess(String message) {
        //Not Used
    }

    @Override
    public void searchDataInternetError() {
        //Not Used
    }

    @Override
    public void getCampusApiDidFinishWith(Result result, List<Campus> campusList) {

    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBack(){

        finish();
    }
}
