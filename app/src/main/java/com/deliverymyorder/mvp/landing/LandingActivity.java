package com.deliverymyorder.mvp.landing;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.deliverymyorder.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LandingActivity extends AppCompatActivity {

    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

  @BindView(R.id.tab_layout)
  TabLayout tab_layout;


    int curentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        ButterKnife.bind(this);

        final IntroAdapter mCustomPagerAdapter = new IntroAdapter(this);
        viewPager.setAdapter(mCustomPagerAdapter);
        tab_layout.setupWithViewPager(viewPager, true);




        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                curentPage = position;
                Log.i("Pager", "second");

            }

            @Override
            public void onPageScrollStateChanged(int state) {

          }
        });
    }
}
