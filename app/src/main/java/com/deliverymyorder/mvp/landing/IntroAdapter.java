package com.deliverymyorder.mvp.landing;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.R;


/**
 * Created by mac on 12/05/18.
 */



public class IntroAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;

    int[] mResources = {
            R.mipmap.ic_tutorial,
            R.mipmap.ic_tutorial,
            R.mipmap.ic_tutorial,
    };


    public IntroAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.tutorial_scrolling_page, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imgScrollingPage);
        imageView.setImageResource(mResources[position]);




        switch (position)
        {

//            case 0:
//
//                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title1));
//                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description1));
//                imageBG.setVisibility(View.INVISIBLE);
//                break;

            case 0:
               /* imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title1));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description1));*/
                imageView.setImageResource(R.mipmap.ic_tutorial);

                break;

            case 1:
               /* imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title2));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description2));*/
                imageView.setImageResource(R.mipmap.ic_tutorial);
                break;

            case 2:
               /* imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title3));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description3));*/
                imageView.setImageResource(R.mipmap.ic_tutorial);
                break;

            default:
                /*imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title3));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description3));*/
                imageView.setImageResource(R.mipmap.ic_tutorial);
                break;
        }



        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }



    public int   getPagesCount()
    {

        return mResources.length;
    }

}
