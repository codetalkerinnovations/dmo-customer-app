package com.deliverymyorder.mvp.support;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.deliverymyorder.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SupportActivity extends AppCompatActivity {

    @BindView(R.id.linLayPhone)
    LinearLayout linLayPhone;

    @BindView(R.id.linLayEmail)
    LinearLayout linLayEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.linLayPhone)
    public void linLayPhoneClicked() {


        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if ((permissionCheckCamera == -1)) {

                if (!Settings.System.canWrite(this)) {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE
                    }, 2909);
                } else {
                    call();
                }
            } else {
                call();
            }
        } else {
            call();
        }

    }


    private void call() {

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8210347394"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }


    @OnClick(R.id.linLayEmail)
    public void linLayEmailClicked() {

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        String[] recipients = {"delivermyorders.in@gmail.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
        emailIntent.setType("message/rfc822");
        startActivity(emailIntent);
    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        finish();
    }
}
