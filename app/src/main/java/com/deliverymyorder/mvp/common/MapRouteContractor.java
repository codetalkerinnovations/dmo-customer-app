package com.deliverymyorder.mvp.common;

import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by Braintech on 28-02-2018.
 */


public interface MapRouteContractor {

    interface Presenter {
        void downloadUrlData(String url);
    }

    interface View {
        void downloadUrlSuccess(PolylineOptions lineOptions);
    }
}