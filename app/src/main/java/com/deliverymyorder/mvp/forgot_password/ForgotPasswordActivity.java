package com.deliverymyorder.mvp.forgot_password;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.forgot_password.presenter.ForgotPasswordContractor;
import com.deliverymyorder.mvp.forgot_password.presenter.ForgotPasswordPresenterImpl;
import com.deliverymyorder.mvp.otp.OtpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgotPasswordContractor.ForgotPasswordView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.edtTextPhonenNumber)
    AppCompatEditText edtTextPhonenNumber;

    @BindView(R.id.txtViewSubmit)
    TextView txtViewSubmit;


    ForgotPasswordPresenterImpl forgotPasswordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ButterKnife.bind(this);

        forgotPasswordPresenter = new ForgotPasswordPresenterImpl(this, this);
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        finish();
    }

    @OnClick(R.id.txtViewSubmit)
    public void txtViewSubmitClicked() {

        String phoneNumber = String.valueOf(edtTextPhonenNumber.getText());

        if (!Utils.isEmptyOrNull(phoneNumber)) {

            forgotPasswordPresenter.getForgotPasswordResult(phoneNumber);
        } else {

            SnackNotify.showMessage(getString(R.string.empty_phone_number), relLayMain);
        }
    }

    @Override
    public void onForgotPasswordSuccess(ForgotPasswordModel forgotPasswordModel) {

        Intent intent = new Intent(this, OtpActivity.class);
        intent.putExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER, String.valueOf(edtTextPhonenNumber.getText()));
        intent.putExtra(ConstIntent.CONST_PAGE_NAME, ConstIntent.CONST_FORGOT_PASSWORD_PAGE);
        startActivity(intent);

        finish();

    }

    @Override
    public void onForgotPasswordUnsuccess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onForgotPasswordInternetError() {

    }
}
