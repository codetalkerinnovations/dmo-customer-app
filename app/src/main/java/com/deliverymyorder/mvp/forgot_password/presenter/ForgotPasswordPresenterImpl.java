package com.deliverymyorder.mvp.forgot_password.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.forgot_password.ForgotPasswordModel;


import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordPresenterImpl implements ForgotPasswordContractor.ForgotPasswordPresenter {

    Activity activity;
    ForgotPasswordContractor.ForgotPasswordView forgotPasswordView;

    public ForgotPasswordPresenterImpl(Activity activity, ForgotPasswordContractor.ForgotPasswordView forgotPasswordView){
        this.activity = activity;
        this.forgotPasswordView = forgotPasswordView;
    }

    @Override
    public void getForgotPasswordResult(String phoneNumber) {

        try {
            ApiAdapter.getInstance(activity);
            callingForgotPasswordApi(phoneNumber);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            forgotPasswordView.onForgotPasswordInternetError();
        }
    }

    private void callingForgotPasswordApi(String phoneNumber) {
        Progress.start(activity);
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.KEY_PHONE_NUMBER, phoneNumber);
            jsonObject.put(Const.KEY_USER_TYPE, Const.VALUE_USER_TYPE);
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ForgotPasswordModel> getForgotPasswordResult = ApiAdapter.getApiService().getForgotPassword("application/json", "no-cache", body);

        getForgotPasswordResult.enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {

                Progress.stop();
                try {
                    ForgotPasswordModel forgotPasswordModel = response.body();


                    if (forgotPasswordModel.getError() == 0) {
                        forgotPasswordView.onForgotPasswordSuccess(forgotPasswordModel);
                    } else {
                        forgotPasswordView.onForgotPasswordUnsuccess(forgotPasswordModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    forgotPasswordView.onForgotPasswordUnsuccess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                Progress.stop();
                forgotPasswordView.onForgotPasswordUnsuccess(activity.getString(R.string.server_error));
            }
        });

    }
}
