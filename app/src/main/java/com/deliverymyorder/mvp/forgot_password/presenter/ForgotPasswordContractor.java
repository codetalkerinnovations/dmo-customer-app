package com.deliverymyorder.mvp.forgot_password.presenter;


import com.deliverymyorder.mvp.forgot_password.ForgotPasswordModel;

public interface ForgotPasswordContractor {

    interface ForgotPasswordPresenter{
        void getForgotPasswordResult(String phoneNumber);

    }

    interface ForgotPasswordView{
        void onForgotPasswordSuccess(ForgotPasswordModel forgotPasswordModel);
        void onForgotPasswordUnsuccess(String message);
        void onForgotPasswordInternetError();
    }

}
