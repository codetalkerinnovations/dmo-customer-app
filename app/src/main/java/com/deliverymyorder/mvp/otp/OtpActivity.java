package com.deliverymyorder.mvp.otp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.create_password.CreatePasswordActivity;
import com.deliverymyorder.mvp.otp.presenter.OtpContractor;
import com.deliverymyorder.mvp.otp.presenter.OtpPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtpActivity extends AppCompatActivity implements OtpContractor.OtpView {


    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.edtTextOtp1)
    EditText edtTextOtp1;

    @BindView(R.id.edtTextOtp2)
    EditText edtTextOtp2;

    @BindView(R.id.edtTextOtp3)
    EditText edtTextOtp3;

    @BindView(R.id.edtTextOtp4)
    EditText edtTextOtp4;

    @BindView(R.id.txtViewVerify)
    TextView txtViewVerify;

    @BindView(R.id.txtViewResendOtp)
    TextView txtViewResendOtp;

    OtpPresenterImpl otpPresenter;
    String otp;
    String mobileNumber;
    String pageName;

    Boolean resendOTPInitially = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        otpPresenter = new OtpPresenterImpl(this, this);

        edtTextOtp1.addTextChangedListener(new GenericTextWatcher(edtTextOtp1));
        edtTextOtp2.addTextChangedListener(new GenericTextWatcher(edtTextOtp2));
        edtTextOtp3.addTextChangedListener(new GenericTextWatcher(edtTextOtp3));
        edtTextOtp4.addTextChangedListener(new GenericTextWatcher(edtTextOtp4));

        if (getIntent().getStringExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER) != null) {

            mobileNumber = getIntent().getStringExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER);
            pageName = getIntent().getStringExtra(ConstIntent.CONST_PAGE_NAME);
            resendOTPInitially = getIntent().getBooleanExtra("shouldResend",false);
        }

        if ( resendOTPInitially)
        {
            otpPresenter.resendOtp(mobileNumber);
        }

    }

    @Override
    public void onOtpSuccess(OtpModel otpModel) {

        showMessage(otpModel);

    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.edtTextOtp1:
                    if (text.length() == 1) {
                        edtTextOtp2.requestFocus();
                    }else
                    {

                    }
                    break;
                case R.id.edtTextOtp2:
                    if (text.length() == 1)
                    {
                        edtTextOtp3.requestFocus();
                    }else
                    {
                        edtTextOtp1.requestFocus();
                    }
                    break;
                case R.id.edtTextOtp3:
                    if (text.length() == 1)
                    {
                        edtTextOtp4.requestFocus();
                    }else
                    {
                        edtTextOtp2.requestFocus();
                    }
                    break;
                case R.id.edtTextOtp4:
                    if (text.length() == 1)
                    {
                        Utils.hideKeyboardIfOpen(OtpActivity.this);
                    }else
                    {
                        edtTextOtp3.requestFocus();
                    }
                    break;
            }

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    private void showMessage(final OtpModel otpModel) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(otpModel.getMessage());

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                if (pageName.equals(ConstIntent.CONST_REGISTER_PAGE)) {

                    finish();
                } else {

                    Intent intent = new Intent(OtpActivity.this, CreatePasswordActivity.class);
                    intent.putExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER, otpModel.getData().getMobile());
                    startActivity(intent);

                    finish();
                }


            }
        });

        alertDialog.show();

    }

    @Override
    public void onOtpUnsuccess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onOtpInternetError() {

    }

    @Override
    public void onResendOtpSuccess(OtpModel otpModel) {
         showMessage(otpModel);
    }

    @Override
    public void onResendUnsuccess(String message) {

    }

    @Override
    public void onResendInternetError() {

    }


    @OnClick(R.id.txtViewVerify)
    public void txtViewVerifyClicked() {

        otp = String.valueOf(edtTextOtp1.getText()) + String.valueOf(edtTextOtp2.getText()) + String.valueOf(edtTextOtp3.getText()) + String.valueOf(edtTextOtp4.getText());

        if (otp.length() == 4) {

            otpPresenter.getOtpResult(otp, mobileNumber);
        }
    }

    @OnClick(R.id.txtViewResendOtp)
    public void txtViewResendOtpClicked() {
        otpPresenter.resendOtp(mobileNumber);
    }
}
