package com.deliverymyorder.mvp.otp.presenter;


import com.deliverymyorder.mvp.otp.OtpModel;

public interface OtpContractor {

    interface OtpPresenter{
        void getOtpResult(String otp, String mobileNo);
        void resendOtp(String mobileNo);

    }

    interface OtpView{
        void onOtpSuccess(OtpModel otpModel);
        void onOtpUnsuccess(String message);
        void onOtpInternetError();

        void onResendOtpSuccess(OtpModel otpModel);
        void onResendUnsuccess(String message);
        void onResendInternetError();
    }

}
