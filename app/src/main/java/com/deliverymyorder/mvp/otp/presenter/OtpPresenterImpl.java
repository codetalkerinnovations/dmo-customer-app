package com.deliverymyorder.mvp.otp.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.otp.OtpModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpPresenterImpl implements OtpContractor.OtpPresenter {

    Activity activity;
    OtpContractor.OtpView OtpView;

    public OtpPresenterImpl(Activity activity, OtpContractor.OtpView OtpView) {
        this.activity = activity;
        this.OtpView = OtpView;
    }

    @Override
    public void getOtpResult(String otp, String mobileNo) {

        try {
            ApiAdapter.getInstance(activity);
            callingOtpApi(otp, mobileNo);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            OtpView.onOtpInternetError();
        }
    }

    private void callingOtpApi(String otp, String mobileNo) {
        Progress.start(activity);
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_OTP, otp);
            jsonObject.put(Const.KEY_PHONE_NUMBER, mobileNo);
            jsonObject.put(Const.KEY_USER_TYPE, Const.VALUE_USER_TYPE);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<OtpModel> getOtpResult = ApiAdapter.getApiService().confirmOtp("application/json", "no-cache", body);

        getOtpResult.enqueue(new Callback<OtpModel>() {
            @Override
            public void onResponse(Call<OtpModel> call, Response<OtpModel> response) {

                Progress.stop();
                try {
                    OtpModel otpModel = response.body();


                    if (otpModel.getError() == 0) {
                        OtpView.onOtpSuccess(otpModel);
                    } else {
                        OtpView.onOtpUnsuccess(otpModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    OtpView.onOtpUnsuccess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<OtpModel> call, Throwable t) {
                Progress.stop();
                OtpView.onOtpUnsuccess(activity.getString(R.string.server_error));
            }
        });

    }

    // customer-resend-otp
    // mobile

    @Override
    public void resendOtp(String mobileNo) {

        try {
            ApiAdapter.getInstance(activity);
            callingResendOtpApi( mobileNo);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            OtpView.onOtpInternetError();
        }
    }

    private void callingResendOtpApi(String mobileNo) {
        Progress.start(activity);
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_PHONE_NUMBER, mobileNo);
            jsonObject.put(Const.KEY_USER_TYPE, Const.VALUE_USER_TYPE);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<OtpModel> getOtpResult = ApiAdapter.getApiService().customerResendOTP("application/json", "no-cache", body);

        getOtpResult.enqueue(new Callback<OtpModel>() {
            @Override
            public void onResponse(Call<OtpModel> call, Response<OtpModel> response) {

                Progress.stop();
                try {
                    OtpModel otpModel = response.body();


                    if (otpModel.getError() == 0) {
                        OtpView.onResendOtpSuccess(otpModel);
                    } else {
                        OtpView.onResendUnsuccess(otpModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    OtpView.onResendUnsuccess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<OtpModel> call, Throwable t) {
                Progress.stop();
                OtpView.onResendUnsuccess(activity.getString(R.string.server_error));
            }
        });

    }


}
