package com.deliverymyorder.mvp.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.AboutUsActivity;
import com.deliverymyorder.mvp.address.AddressListActivity;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.address.presenter.AddressDetailContractor;
import com.deliverymyorder.mvp.address.presenter.AddressDetailPresenterImpl;
import com.deliverymyorder.mvp.offers.OffersActivity;
import com.deliverymyorder.mvp.payment_activity.PayUPaymentActivity;
import com.deliverymyorder.mvp.referal.ReferalActivity;
import com.deliverymyorder.mvp.support.SupportActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileFragment extends Fragment implements AddressDetailContractor.AddressDetailView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewCustomerName)
    TextView txtViewCustomerName;

    @BindView(R.id.txtViewLogout)
    TextView txtViewLogout;

    @BindView(R.id.txtViewPhoneNumber)
    TextView txtViewPhoneNumber;

    @BindView(R.id.txtViewOffers)
    TextView txtViewOffers;

    @BindView(R.id.txtViewChangeAddress)
    TextView txtViewChangeAddress;

    @BindView(R.id.txtViewAddress)
    TextView txtViewAddress;

    @BindView(R.id.txtViewPaymentSetting)
    TextView txtViewPaymentSetting;

    @BindView(R.id.txtViewSupport)
    TextView txtViewSupport;

    @BindView(R.id.txtViewReferAndEarn)
    TextView txtViewReferAndEarn;

    @BindView(R.id.txtViewAboutUs)
    TextView txtViewAboutUs;

    @BindView(R.id.txtViewPrivacyPolicy)
    TextView txtViewPrivacyPolicy;

    @BindView(R.id.relLayAddress)
    RelativeLayout relLayAddress;

    @BindView(R.id.edtTextFlatNo)
    EditText edtTextFlatNo;

    @BindView(R.id.edtTextArea)
    EditText edtTextArea;

    @BindView(R.id.edtTextLandmark)
    EditText edtTextLandmark;

    AddressDetailPresenterImpl addressDetailPresenter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txtViewChangeAddress.setPaintFlags(txtViewChangeAddress.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        addressDetailPresenter = new AddressDetailPresenterImpl(getActivity(), this);

        txtViewCustomerName.setText(LoginManager.getInstance().getUserData().getFirstname() + " " + LoginManager.getInstance().getUserData().getLastname());
        txtViewPhoneNumber.setText(LoginManager.getInstance().getUserData().getMobile());


    }

    @Override
    public void onResume() {
        super.onResume();
        callGetAddressDetailDataApi();
    }

    private void callGetAddressDetailDataApi() {

        int addressId = PrefUtil.getInt(getActivity(), Const.KEY_ADDRESS_ID, 0);

        addressDetailPresenter.getAddressDetailData(addressId);
    }


    @Override
    public void getAddressDetailSuccess(AddressDetailModel.Data addressDetailModel) {

        edtTextFlatNo.setText(addressDetailModel.getHouseNo());
        edtTextArea.setText(addressDetailModel.getAreaLocality());
        edtTextLandmark.setText(addressDetailModel.getLandmark());
    }

    @Override
    public void getAddressDetailUnSucess(String message) {

    }

    @Override
    public void getAddressDetailInternetError() {

    }

    @OnClick(R.id.txtViewChangeAddress)
    public void txtViewChangeAddressClicked() {
        Intent intent = new Intent(getActivity(), AddressListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewReferAndEarn)
    public void txtViewReferAndEarnClicked() {
        Intent intent = new Intent(getActivity(), ReferalActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewLogout)
    public void txtViewLogoutClicked() {
        AlertDialogHelper.showMessageToLogout(getActivity());
    }


    @OnClick(R.id.txtViewOffers)
    public void txtViewOffersOnClick(View view) {
        Intent intent = new Intent(getActivity(), OffersActivity.class);
        startActivity(intent);
    }


 @OnClick(R.id.txtViewAboutUs)
    public void txtViewAboutUsOnClick(View view) {
        Intent intent = new Intent(getActivity(), AboutUsActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.txtViewSupport)
    public void txtViewSupportOnClick(View view) {
        Intent intent = new Intent(getActivity(), SupportActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.txtViewPrivacyPolicy)
    public void txtViewPrivacyPolicyOnClick(View view) {
        Intent intent = new Intent(getActivity(), AboutUsActivity.class);
        startActivity(intent);
    }

}
