package com.deliverymyorder.mvp.search_fragment.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.ApiResult;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.CampusListResponse;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.search_fragment.searchdata_model.SearchPageDataModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPagePresenterImpl implements SearchPageContractor.HomePagePresenter {

    Activity activity;
    SearchPageContractor.HomePageView homePageView;

    public SearchPagePresenterImpl(Activity activity, SearchPageContractor.HomePageView homePageView) {
        this.activity = activity;
        this.homePageView = homePageView;
    }


    @Override
    public void getSearchData(String searchKey) {
        try {
            ApiAdapter.getInstance(activity);
            callSearchDataApi(searchKey);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            homePageView.searchDataInternetError();

        }
    }




    private void callSearchDataApi(String searchKey) {
       // Progress.start(activity);

        int campusId = 0;

        JSONObject jsonObject = new JSONObject();

        String latitude = PrefUtil.getString(activity, Const.KEY_LATITUDE, "");
        String longitude = PrefUtil.getString(activity, Const.KEY_LONGITUDE, "");

        if (LoginManager.getInstance().getCurrentCampus() != null) {

            if (LoginManager.getInstance().getCurrentCampus().getId() != null) {
                campusId = LoginManager.getInstance().getCurrentCampus().getId();
            }
        }

        try {
            jsonObject.put(Const.KEY_CUSTOMER_ID,LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_LATITUDE, latitude);
            jsonObject.put(Const.KEY_LONGITUDE, longitude);
            jsonObject.put(Const.KEY, searchKey);
            jsonObject.put("category_data",1);
             jsonObject.put(Const.KEY_SHORT, 1);
           // jsonObject.put(Const.KEY_CATEGORY_ID, categoryId);
            if (campusId != 0) {
                //if (isInCampus) {
                    jsonObject.put(Const.KEY_CAPMUS_ID, campusId);
                      jsonObject.put(Const.KEY_IN_CAMPUS, false);
                //}
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<SearchPageDataModel> getCreatePasswordResult = ApiAdapter.getApiService().getSearchPageData("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<SearchPageDataModel>() {
            @Override
            public void onResponse(Call<SearchPageDataModel> call, Response<SearchPageDataModel> response) {

                Progress.stop();
                try {
                    SearchPageDataModel searchDataResponseModel = response.body();


                    if (searchDataResponseModel.getError() == 0) {
                        homePageView.getSearchDataSuccess(searchDataResponseModel.getData().getShopList(),searchDataResponseModel.getData().getCategory());
                    } else {
                        homePageView.getSearchDataUnSucess(searchDataResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    homePageView.getSearchDataUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<SearchPageDataModel> call, Throwable t) {
                Progress.stop();
                homePageView.getSearchDataUnSucess(activity.getString(R.string.server_error));
            }
        });

    }
}
