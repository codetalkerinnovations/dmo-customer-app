package com.deliverymyorder.mvp.search_fragment.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.RoundCornerTransformation;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.search_fragment.searchdata_model.SearchPageDataModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.Holder> {


    Activity activity;
    ArrayList<SearchPageDataModel.ShopList> datum;
    String favBit;


    public ShopAdapter(Activity activity, ArrayList<SearchPageDataModel.ShopList> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_near_you, parent, false);



        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final SearchPageDataModel.ShopList result = datum.get(position);


        holder.txtViewCategoryName.setText(result.getTitle());

        if(result.getIsFavorite()==0){

            holder.imgViewFavourite.setBackgroundResource(R.mipmap.ic_fav_off);
        }else{
            holder.imgViewFavourite.setBackgroundResource(R.mipmap.ic_fav_on);
        }


        if (Utils.isEmptyOrNull(result.getImage())) {
            //holder.relLayItem.setVisibility(View.GONE);
            holder.imgViewCategory.setScaleType(ImageView.ScaleType.CENTER);
            holder.imgViewCategory.setImageDrawable(activity.getResources().getDrawable(R.mipmap.ic_shop_placeholder));
        } else {
            holder.relLayItem.setVisibility(View.VISIBLE);

            try {
                Picasso.with(activity).load(result.getImage()).transform(new RoundCornerTransformation()).into(holder.imgViewCategory);

                holder.imgViewCategory.setScaleType(ImageView.ScaleType.FIT_XY);
            }catch (Exception e)
            {
                holder.imgViewCategory.setScaleType(ImageView.ScaleType.CENTER);

                holder.imgViewCategory.setImageDrawable(activity.getResources().getDrawable(R.mipmap.ic_shop_placeholder));
                e.printStackTrace();
            }
        }

        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, ShopDetailsActivity.class);
                intent.putExtra(ConstIntent.CONST_BUISNESS_ID, result.getId());
                intent.putExtra(ConstIntent.CONST_MERCHANT_ID, result.getMerchantId());
                intent.putExtra(ConstIntent.CONST_SHOP_NAME, result.getTitle());
                intent.putExtra(ConstIntent.CONST_ADDRESS, result.getAddress()== null ? "":result.getAddress());
                activity.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgViewCategory)
        ImageView imgViewCategory;

        @BindView(R.id.imgViewFavourite)
        ImageView imgViewFavourite;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}


