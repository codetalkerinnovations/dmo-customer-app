package com.deliverymyorder.mvp.search_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.deliverymyorder.Common.utility.DMOMerchat;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.search_fragment.adapter.CategoryAdapter;
import com.deliverymyorder.mvp.search_fragment.adapter.ShopAdapter;
import com.deliverymyorder.mvp.search_fragment.presenter.SearchPageContractor;
import com.deliverymyorder.mvp.search_fragment.presenter.SearchPagePresenterImpl;
import com.deliverymyorder.mvp.search_fragment.searchdata_model.SearchPageDataModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SearchFragment extends Fragment implements SearchPageContractor.HomePageView {





 @BindView(R.id.etSearch)
 EditText etSearch;

 @BindView(R.id.recyclerViewShop)
 RecyclerView recyclerViewShop;


 @BindView(R.id.recylCategory)
 RecyclerView recylCategory;



            TabLayout tabLayout;
            int currentPage = 0;
            Timer timer;
final long DELAY_MS = 3000;//delay in milliseconds before task is to be executed
final long PERIOD_MS = 6000; // time in milliseconds between successive task executions.

        ArrayList<Integer> bannerModel;
        ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel;

        //String searchKey;

        SearchPagePresenterImpl homePagePresenter;


        CategoryAdapter popularCategoryAdapter;
        ShopAdapter nearYouAdapter;


      //  SearchPageDataModel.Data homePageDataModel;



        List<SearchPageDataModel.ShopList> shopList;

        List<SearchPageDataModel.Category> categoryList;




    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
         //args.putParcelableArrayList("campus_list",new ArrayList<Parcelable>(campusList));
        fragment.setArguments(args);
        return fragment;
    }

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);



        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                homePagePresenter.getSearchData(etSearch.getText().toString());
            }
        });

        return view;
        }

@Override
public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        homePagePresenter = new SearchPagePresenterImpl(getActivity(), this);

        }




     @Override
     public void onResume() {
        super.onResume();


        }



private void manageRecyclerView() {

        int numberOfColumns = 4;
        popularCategoryAdapter = new CategoryAdapter(getActivity(), this.categoryList);


    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), numberOfColumns);
    recylCategory.setLayoutManager(mLayoutManager);
   // recylCategory.setItemAnimator(new DefaultItemAnimator());
    recylCategory.setAdapter(popularCategoryAdapter);
        }

private void manageRecyclerViewForNearShops() {


    if (this.shopList !=null && this.shopList.size() != 0)
    {
        ArrayList<SearchPageDataModel.ShopList> nearbyShops111 = new ArrayList<>();

        for (SearchPageDataModel.ShopList nearByShop:this.shopList)
        {
            if (DMOMerchat.merchant_id != nearByShop.getMerchantId() && DMOMerchat.old_merchant_id != nearByShop.getMerchantId())
            {
                nearbyShops111.add(nearByShop);
            }
        }
        shopList =  nearbyShops111;
    }

    nearYouAdapter = new ShopAdapter(getActivity(),new ArrayList<SearchPageDataModel.ShopList>(this.shopList));

    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
    mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
    recyclerViewShop.setLayoutManager(mLayoutManager);
    recyclerViewShop.setItemAnimator(new DefaultItemAnimator());
    recyclerViewShop.setAdapter(nearYouAdapter);
        }



    @Override
    public void getSearchDataSuccess(List<SearchPageDataModel.ShopList> shopListResult, List<SearchPageDataModel.Category> categoryListResult) {

    this.shopList = shopListResult;
    this.categoryList = categoryListResult;

        manageRecyclerView();
        manageRecyclerViewForNearShops();

    }

    @Override
    public void getSearchDataUnSucess(String message) {

    }

    @Override
    public void searchDataInternetError() {

    }


@OnClick(R.id.imgViewBack)
public  void imgViewBack(View view)
{
    getActivity().getSupportFragmentManager().popBackStack();
}

public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

    @Override
    public void onPageSelected(int position) {
        currentPage = position;
    }

    public final int getCurrentPage() {
        return currentPage;
    }
}



    private void callSearchDataApi() {

        Utils.hideKeyboardIfOpen(getActivity());

       // homePagePresenter.getSearchData(searchKey);
    }


}

