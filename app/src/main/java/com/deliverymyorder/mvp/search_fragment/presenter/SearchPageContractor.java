package com.deliverymyorder.mvp.search_fragment.presenter;

import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.search_fragment.searchdata_model.SearchPageDataModel;

import java.util.ArrayList;
import java.util.List;

public class SearchPageContractor {

    public interface HomePagePresenter {
        void getSearchData(String searchKey);

    }

    public interface HomePageView {

        void getSearchDataSuccess(List<SearchPageDataModel.ShopList> shopListResult, List<SearchPageDataModel.Category> categoryListResult);

        void getSearchDataUnSucess(String message);

        void searchDataInternetError();

    }
}
