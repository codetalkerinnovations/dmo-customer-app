package com.deliverymyorder.mvp.search_fragment.searchdata_model;

import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchPageDataModel {


    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Category {

        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("parent_id")
        @Expose
        private Integer parentId;
        @SerializedName("top")
        @Expose
        private Integer top;
        @SerializedName("column")
        @Expose
        private Integer column;
        @SerializedName("meta_title")
        @Expose
        private String metaTitle;
        @SerializedName("meta_description")
        @Expose
        private String metaDescription;
        @SerializedName("meta_keyword")
        @Expose
        private String metaKeyword;
        @SerializedName("sort_order")
        @Expose
        private Integer sortOrder;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public Integer getTop() {
            return top;
        }

        public void setTop(Integer top) {
            this.top = top;
        }

        public Integer getColumn() {
            return column;
        }

        public void setColumn(Integer column) {
            this.column = column;
        }

        public String getMetaTitle() {
            return metaTitle;
        }

        public void setMetaTitle(String metaTitle) {
            this.metaTitle = metaTitle;
        }

        public String getMetaDescription() {
            return metaDescription;
        }

        public void setMetaDescription(String metaDescription) {
            this.metaDescription = metaDescription;
        }

        public String getMetaKeyword() {
            return metaKeyword;
        }

        public void setMetaKeyword(String metaKeyword) {
            this.metaKeyword = metaKeyword;
        }

        public Integer getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(Integer sortOrder) {
            this.sortOrder = sortOrder;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class Data {

        @SerializedName("category")
        @Expose
        private List<Category> category = null;
        @SerializedName("shop_list")
        @Expose
        private List<ShopList> shopList = null;

        public List<Category> getCategory() {
            return category;
        }

        public void setCategory(List<Category> category) {
            this.category = category;
        }

        public List<ShopList> getShopList() {
            return shopList;
        }

        public void setShopList(List<ShopList> shopList) {
            this.shopList = shopList;
        }

    }

    public class ShopList {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("merchant_id")
        @Expose
        private Integer merchantId;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("custom_order_allowed")
        @Expose
        private Integer customOrderAllowed;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("distance")
        @Expose
        private Double distance;
        @SerializedName("popular_point")
        @Expose
        private Integer popularPoint;
        @SerializedName("rating_point")
        @Expose
        private Object ratingPoint;
        @SerializedName("isFavorite")
        @Expose
        private Integer isFavorite;

        @SerializedName("total_cost")
        @Expose
        private Float totalDeliveryCost = 0.0f;


        public Float getTotalDeliveryCost() {
            return totalDeliveryCost;
        }

        public void setTotalDeliveryCost(Float totalDeliveryCost) {
            this.totalDeliveryCost = totalDeliveryCost;
        }



        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(Integer merchantId) {
            this.merchantId = merchantId;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public Integer getCustomOrderAllowed() {
            return customOrderAllowed;
        }

        public void setCustomOrderAllowed(Integer customOrderAllowed) {
            this.customOrderAllowed = customOrderAllowed;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getDistance() {
            return distance;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

        public Integer getPopularPoint() {
            return popularPoint;
        }

        public void setPopularPoint(Integer popularPoint) {
            this.popularPoint = popularPoint;
        }

        public Object getRatingPoint() {
            return ratingPoint;
        }

        public void setRatingPoint(Object ratingPoint) {
            this.ratingPoint = ratingPoint;
        }

        public Integer getIsFavorite() {
            return isFavorite;
        }

        public void setIsFavorite(Integer isFavorite) {
            this.isFavorite = isFavorite;
        }

    }

}