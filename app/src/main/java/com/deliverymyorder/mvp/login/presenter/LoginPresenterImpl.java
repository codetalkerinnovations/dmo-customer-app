package com.deliverymyorder.mvp.login.presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.ApiService;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.login.LoginResponseModel;
import com.deliverymyorder.mvp.register.RegisterActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenterImpl implements LoginContractor.LoginPresenter {

    Activity activity;
    LoginContractor.LoginView loginView;

    private static final int RC_SIGN_IN = 1231;
    GoogleApiClient mGoogleApiClient;

    /*Social Login*/
    String socialLoginEmail = "";
    String socialLoginId = "0";
    String socialLoginName = "";

    public LoginPresenterImpl(Activity activity, LoginContractor.LoginView loginView) {
        this.activity = activity;
        this.loginView = loginView;
    }

    @Override
    public void verifyLoginData(String username, String password) {


        callingLoginApi(username, password);

      /*  try {
            ApiAdapter.(activity);
            callingLoginApi(username, password);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            loginView.getSimpleLoginInternetError();
        }*/
    }

    @Override
    public void loginWithFacebook(CallbackManager mCallbackManager) {

        Progress.start(activity);

        LoginManager.getInstance().logInWithReadPermissions((Activity) activity, Arrays.asList(new String[]{"email"}));

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResults) {

                        Progress.stop();

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResults.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {

                                        // Log.e("json", object.toString());
                                        // Application code

                                        String userEmail = "";
                                        String fullName = "";
                                        String userId = "";
                                        String first_name = "";
                                        String last_name = "";

                                        try {
                                            userEmail = object.getString("email");
                                            fullName = object.getString("name");
                                            userId = object.getString("id");
                                            first_name = object.getString("first_name");
                                            last_name = object.getString("last_name");
                                            //userId=object.getString();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        //log out user before login to server
                                        if (AccessToken.getCurrentAccessToken() != null) {
                                            LoginManager.getInstance().logOut();
                                        }

                                        //Call social Login API
                                            callSocialLoginMethod(fullName, userEmail, userId, first_name, last_name, "Facebook");


                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,first_name,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut();
                        }
                        Progress.stop();
                        Log.e("dd", "facebook login canceled");
                    }


                    @Override
                    public void onError(FacebookException e) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut();
                        }
                        e.printStackTrace();
                        Progress.stop();

                    }
                });
    }

    @Override
    public void loginWithGoogle(GoogleApiClient mGoogleApiClient) {
        this.mGoogleApiClient = mGoogleApiClient;
        signIn();
    }


    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((Activity) activity).startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]

    // [START signOut]
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }


    //Social Login
    private void callSocialLoginMethod(final String name, final String email, String ID, final String first_name, final String last_name, final String login_type) {

        Progress.start(activity);

        socialLoginEmail = email;
        socialLoginName = name;
        socialLoginId = ID;

        Log.e("Alam", first_name + " " + last_name + " " + name + " " + socialLoginEmail);


        String deviceId = Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.KEY_USER_NAME, "Social");
            jsonObject.put(Const.KEY_PASSWORD, "");
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_SOCIAL_ID, socialLoginId);
            jsonObject.put(Const.KEY_LOGIN_TYPE, login_type);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));


        Call<LoginResponseModel> callLogin = ApiAdapter.getApiService().userSimpleLogin("application/json", "no-cache", body);


        callLogin.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                Progress.stop();
                try {
                    LoginResponseModel loginResponseModel = response.body();
                    if (loginResponseModel.getError() == 0) {
                        loginView.getSimpleLoginSuccess(loginResponseModel);
                    } else if (loginResponseModel.getError() == 3) {

                        Intent intent = new Intent(activity, RegisterActivity.class);
                        intent.putExtra(ConstIntent.CONST_SOCIALLOGINID, socialLoginId);
                        intent.putExtra(ConstIntent.CONST_LOGIN_TYPE, login_type);
                        intent.putExtra(ConstIntent.CONST_FIRST_NAME, first_name);
                        intent.putExtra(ConstIntent.CONST_LAST_NAME, last_name);
                        intent.putExtra(ConstIntent.CONST_EMAIL, email);
                        activity.startActivity(intent);

                    } else if (loginResponseModel.getError() == 1) {
                        loginView.getSimpleLoginUnsuccess(loginResponseModel.getMessage());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    loginView.getSimpleLoginUnsuccess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Progress.stop();
                t.printStackTrace();
                loginView.getSimpleLoginUnsuccess(activity.getString(R.string.server_error));
            }
        });
    }

   /* private void handleSocketException(String apiTypeLogin,Throwable t) {
        if (t instanceof SocketTimeoutException ||t instanceof ConnectException) {
            AlertDialogHelper.showAlertToConnectionTimeout(this,activity,apiTypeLogin);
        }  else {
            mView.showAlert(context.getString(R.string.server_error));
        }
    }*/


    private void callingLoginApi(String email, String password) {

        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.KEY_USER_NAME, email);
            jsonObject.put(Const.KEY_PASSWORD, password);
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_SOCIAL_ID, "");
            jsonObject.put(Const.KEY_LOGIN_TYPE, "Normal");


        } catch (JSONException e) {
            e.printStackTrace();
        }
        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));


        Call<LoginResponseModel> callLogin = ApiAdapter.getApiService().userSimpleLogin("application/json", "no-cache", body);


        callLogin.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                Progress.stop();
                try {
                    LoginResponseModel loginResponseModel = response.body();
                    if (loginResponseModel.getError() == 0) {
                        loginView.getSimpleLoginSuccess(loginResponseModel);
                    } else if (loginResponseModel.getError() == 1) {
                        loginView.getSimpleLoginUnsuccess(loginResponseModel.getMessage());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    loginView.getSimpleLoginUnsuccess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Progress.stop();
                t.printStackTrace();
                loginView.getSimpleLoginUnsuccess(activity.getString(R.string.server_error));
            }
        });
    }

    //GOOGLE lOGIN
    public void handleSignInResult(GoogleSignInResult result) {
        //  Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            String email = "";
            String userID = "";
            String name = "";
            String first_name = "";
            String last_name = "";
            try {
                GoogleSignInAccount acct = result.getSignInAccount();
                email = acct.getEmail();
                name = acct.getDisplayName();
                userID = acct.getId();

                String[] parts = name.split("\\s+");
                Log.d("Length-->", "" + parts.length);
                if (parts.length == 2) {
                    first_name = parts[0];
                    last_name = parts[1];
                    Log.d("First-->", "" + first_name);
                    Log.d("Last-->", "" + last_name);

                } else if (parts.length == 3) {
                    first_name = parts[0];
                    last_name = parts[2];
                    Log.d("First-->", "" + first_name);
                    Log.d("Last-->", "" + last_name);
                }

            } catch (NullPointerException e) {

            }

            try {
                signOut();
                revokeAccess();
            } catch (IllegalStateException e) {
                clearApplicationData();
            } catch (RuntimeException e) {
                clearApplicationData();
            }

            //Call social Login API
            callSocialLoginMethod(name, email, userID, first_name, last_name, "Google");


        } else {

        }
    }

    public void clearApplicationData() {
        File cache = activity.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
