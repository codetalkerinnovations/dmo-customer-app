package com.deliverymyorder.mvp.login;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;
import com.deliverymyorder.mvp.forgot_password.ForgotPasswordActivity;
import com.deliverymyorder.mvp.login.presenter.LoginContractor;
import com.deliverymyorder.mvp.login.presenter.LoginPresenterImpl;
import com.deliverymyorder.mvp.otp.OtpActivity;
import com.deliverymyorder.mvp.register.RegisterActivity;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginContractor.LoginView, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.edtTextPhonenNumber)
    AppCompatEditText edtTextPhonenNumber;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.txtViewForgotPassword)
    TextView txtViewForgotPassword;

    @BindView(R.id.txtViewSignIn)
    TextView txtViewSignIn;

    @BindView(R.id.imgViewFacebook)
    ImageView imgViewFacebook;

    @BindView(R.id.imgViewEye)
    ImageView imgViewEye;

    @BindView(R.id.imgViewGoogle)
    ImageView imgViewGoogle;

    @BindView(R.id.txtViewRegisterNow)
    TextView txtViewRegisterNow;

    boolean isPasswordShow = false;

    CallbackManager mCallbackManager;

    //Google
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 1231;


    LoginPresenterImpl loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        // setFont();

        //Facebook
        AppEventsLogger.activateApp(getApplication());

        //Google
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();


        loginPresenter = new LoginPresenterImpl(this, this);




    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            loginPresenter.handleSignInResult(result);
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }


    @Override
    public void getSimpleLoginSuccess(final LoginResponseModel loginResponseModel) {

        PrefUtil.putInt(this, Const.KEY_ADDRESS_ID, loginResponseModel.getData().getCurrentAddressId());


        if (loginResponseModel.getData().getVerified() == 1) {

            LoginManager.getInstance().createLoginSession(loginResponseModel.getData());

            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);

            finish();


        }else
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Verify your account");
            alert.setMessage("Your Account is not verified yet , please verify your account and login again");
            alert.setIcon(android.R.drawable.ic_dialog_alert);
            alert.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
        intent.putExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER, String.valueOf(loginResponseModel.getData().getMobile()));
        intent.putExtra(ConstIntent.CONST_PAGE_NAME, ConstIntent.CONST_REGISTER_PAGE);
        intent.putExtra("shouldResend", true);
        startActivity(intent);

                    //Toast.makeText(inflater.getContext(), "Rejected", Toast.LENGTH_SHORT).show();
                } });


            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // finish();
                } });
            alert.show();

        }



//        Intent intent = new Intent(this, OtpActivity.class);
//        intent.putExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER, String.valueOf(edtTextPhonenNumber.getText()));
//        intent.putExtra(ConstIntent.CONST_PAGE_NAME, ConstIntent.CONST_REGISTER_PAGE);
//        startActivity(intent);

        //showMessage(loginResponseModel.getMessage());
    }

    @Override
    public void getSimpleLoginUnsuccess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getSimpleLoginInternetError() {

    }

    @Override
    public void getFacebookLoginSuccess(LoginResponseModel data) {

    }

    @Override
    public void getFacebookLoginUnsuccess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getFacebookLoginInternetError() {

    }


    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
          /*  Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);*/
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            //showProgressDialog();
            /* Progress.start(LoginActivity.this);*/
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    /*   Progress.stop();*/
                    loginPresenter.handleSignInResult(googleSignInResult);
                }
            });
        }
    }


    private void showMessage(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                startActivity(intent);

                finish();
            }
        });

        alertDialog.show();
    }

    @OnClick(R.id.txtViewRegisterNow)
    public void txtViewRegisterNowClicked() {

        Intent intent = new Intent(this, RegisterActivity.class);
        intent.putExtra(ConstIntent.CONST_SOCIALLOGINID, "");
        intent.putExtra(ConstIntent.CONST_LOGIN_TYPE, "Normal");
        startActivity(intent);
    }

    @OnClick(R.id.txtViewForgotPassword)
    public void txtViewForgotPasswordClicked() {

        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.txtViewSignIn)
    public void txtViewSignInClicked() {

        Utils.hideKeyboardIfOpen(this);

        String phoneNumber = String.valueOf(edtTextPhonenNumber.getText());
        String password = String.valueOf(edtTextPassword.getText());

        if (validate(phoneNumber, password)) {

            loginPresenter.verifyLoginData(phoneNumber, password);
        }
    }

    @OnClick(R.id.imgViewEye)
    public void imgViewEye() {
        if (isPasswordShow) {
            edtTextPassword.setTransformationMethod(new PasswordTransformationMethod());
            int length = edtTextPassword.getText().toString().length();
            edtTextPassword.setSelection(length);
            isPasswordShow = false;
            imgViewEye.setImageResource(R.mipmap.ic_pass_show_cross);
        } else {
            edtTextPassword.setTransformationMethod(null);
            int length = edtTextPassword.getText().toString().length();
            edtTextPassword.setSelection(length);
            isPasswordShow = true;
            imgViewEye.setImageResource(R.mipmap.ic_pass_show);
        }

    }


    @OnClick(R.id.imgViewFacebook)
    public void imgViewFacebookInClicked() {

        Utils.hideKeyboardIfOpen(this);

        try {
            ApiAdapter.getInstance(this);
            mCallbackManager = CallbackManager.Factory.create();
            loginPresenter.loginWithFacebook(mCallbackManager);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            SnackNotify.showMessage(getString(R.string.internet_error), relLayMain);
        }
    }

    @OnClick(R.id.imgViewGoogle)
    public void callGoogleLogin() {

        Utils.hideKeyboardIfOpen(this);

        try {
            ApiAdapter.getInstance(this);
            loginPresenter.loginWithGoogle(mGoogleApiClient);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            SnackNotify.showMessage(getString(R.string.internet_error), relLayMain);
        }
    }

    private boolean validate(String phoneNumber, String password) {

        if (Utils.isEmptyOrNull(phoneNumber)) {
            SnackNotify.showMessage(getString(R.string.empty_phone_number), relLayMain);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password), relLayMain);
            return false;
        }
        return true;
    }

    private void setFont() {
        FontHelper.setFontFace(txtViewForgotPassword, FontHelper.FontType.FONT_ROBOTO_REGULAR, this);
        FontHelper.setFontFace(txtViewSignIn, FontHelper.FontType.FONT_ROBOTO_REGULAR, this);
        FontHelper.setFontFace(txtViewRegisterNow, FontHelper.FontType.FONT_ROBOTO_REGULAR, this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
