package com.deliverymyorder.mvp.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponseModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Data {

        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("firstname")
        @Expose
        private String firstname;
        @SerializedName("lastname")
        @Expose
        private String lastname;
        @SerializedName("email")
        @Expose
        private String email;



        //is_verified
        @SerializedName("is_verified")
        @Expose
        private Integer isVerified;

        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("ip_address")
        @Expose
        private String ipAddress;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("otp_code")
        @Expose
        private String otpCode;
        @SerializedName("refer_code")
        @Expose
        private String referCode;
        @SerializedName("referer_id")
        @Expose
        private Integer refererId;
        @SerializedName("coupon_generate_status")
        @Expose
        private Integer couponGenerateStatus;
        @SerializedName("current_address_id")
        @Expose
        private Integer currentAddressId;
        @SerializedName("country_id")
        @Expose
        private int countryId;
        @SerializedName("state_id")
        @Expose
        private int stateId;
        @SerializedName("city_id")
        @Expose
        private int cityId;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;


        public Integer getVerified() {
            return isVerified;
        }

        public void setVerified(Integer verified) {
            isVerified = verified;
        }

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public void setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getOtpCode() {
            return otpCode;
        }

        public void setOtpCode(String otpCode) {
            this.otpCode = otpCode;
        }

        public String getReferCode() {
            return referCode;
        }

        public void setReferCode(String referCode) {
            this.referCode = referCode;
        }

        public Integer getRefererId() {
            return refererId;
        }

        public void setRefererId(Integer refererId) {
            this.refererId = refererId;
        }

        public Integer getCouponGenerateStatus() {
            return couponGenerateStatus;
        }

        public void setCouponGenerateStatus(Integer couponGenerateStatus) {
            this.couponGenerateStatus = couponGenerateStatus;
        }

        public Integer getCurrentAddressId() {
            return currentAddressId;
        }

        public void setCurrentAddressId(Integer currentAddressId) {
            this.currentAddressId = currentAddressId;
        }

        public int getCountryId() {
            return countryId;
        }

        public void setCountryId(int countryId) {
            this.countryId = countryId;
        }

        public int getStateId() {
            return stateId;
        }

        public void setStateId(int stateId) {
            this.stateId = stateId;
        }

        public int getCityId() {
            return cityId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }








}
