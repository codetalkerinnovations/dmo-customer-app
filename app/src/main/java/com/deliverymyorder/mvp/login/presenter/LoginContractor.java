package com.deliverymyorder.mvp.login.presenter;

import com.deliverymyorder.mvp.login.LoginResponseModel;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

public interface LoginContractor {

    interface LoginPresenter{

        void verifyLoginData(String userName, String password);

        void loginWithFacebook(CallbackManager mCallbackManager);

        void loginWithGoogle(GoogleApiClient mGoogleApiClient);

        void handleSignInResult(GoogleSignInResult result);
    }

    interface LoginView{

        void getSimpleLoginSuccess(LoginResponseModel data);
        void getSimpleLoginUnsuccess(String message);
        void getSimpleLoginInternetError();

        void getFacebookLoginSuccess(LoginResponseModel data);
        void getFacebookLoginUnsuccess(String message);
        void getFacebookLoginInternetError();
    }

}
