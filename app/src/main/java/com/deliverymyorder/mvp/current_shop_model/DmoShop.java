package com.deliverymyorder.mvp.current_shop_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DmoShop {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("rating")
    @Expose
    private Integer rating;

    //custom_order_allowed
    @SerializedName("custom_order_allowed")
    @Expose
    private Integer custom_order_allowed;

    @SerializedName("isFavorite")
    @Expose
    private Integer isFavorite;
    @SerializedName("distance")
    @Expose
    private Double distance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Integer isFavorite) {
        this.isFavorite = isFavorite;
    }


    public Integer getCustom_order_allowed() {

        if (custom_order_allowed == null)
        {
            custom_order_allowed = 0;
        }

        return custom_order_allowed;
    }

    public void setCustom_order_allowed(Integer custom_order_allowed) {
        this.custom_order_allowed = custom_order_allowed;
    }


}
