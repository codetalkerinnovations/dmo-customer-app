package com.deliverymyorder.mvp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailsActivity;
import com.deliverymyorder.mvp.shop_listing.adapter.ShoppingListAdapter;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomOrderAdapter  extends RecyclerView.Adapter<CustomOrderAdapter.Holder> {


    Activity activity;
    ArrayList<CustomMenuModel> datum;
    String favBit;

    CustomOrderInterface customOrderInterface;


    public CustomOrderAdapter(Activity activity, ArrayList<CustomMenuModel> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public CustomOrderAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_custom_create_own_item, parent, false);
        return new CustomOrderAdapter.Holder(view);

    }

    @Override
    public void onBindViewHolder(final CustomOrderAdapter.Holder holder, final int position) {

        final CustomMenuModel result = datum.get(position);

        holder.edtTextItemName.setText(result.getItemName());
        holder.edtTextItemDesc.setText(result.getItemDesc());
        holder.txtViewQty.setText(String.valueOf(result.getQty()));

        if (position == 0)
        {
            holder.imgViewCross.setVisibility(View.GONE);
        }else
        {
            holder.imgViewCross.setVisibility(View.VISIBLE);
        }


        if (result.getItemImage()!=null) {
            File imgFile = new File(result.getItemImage());

            if (imgFile.exists()) {

                //File file = new File(path);
                Picasso.with(activity).load(imgFile).placeholder(R.mipmap.ic_img_placeholder).into(holder.imgProduct);

//                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//                holder.imgProduct.setImageBitmap(myBitmap);
                holder.imgProduct.setVisibility(View.VISIBLE);
            } else {
                holder.imgProduct.setVisibility(View.GONE);
            }

        }else
        {
            holder.imgProduct.setVisibility(View.GONE);
        }

        holder.imgViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (customOrderInterface!=null)
                    customOrderInterface.selectImage(position);
            }
        });



        holder.imgViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (result.getQty()>1) {
                    result.setQty(result.getQty() - 1);
                    datum.set(position, result);
                    notifyItemChanged(position);
                }else
                {
                    datum.remove(position);
                    notifyDataSetChanged();
                }
            }
        });

        holder.imgViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    result.setQty(result.getQty() + 1);
                    datum.set(position, result);
                    notifyItemChanged(position);
            }
        });

        holder.imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum.remove(position);
                notifyDataSetChanged();
            }
        });



        holder.edtTextItemName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                result.setItemName(holder.edtTextItemName.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        holder.edtTextItemDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                result.setItemDesc(holder.edtTextItemDesc.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


//        holder.im

//        holder.txtViewShopName.setText(result.1);
//        holder.txtViewAddress.setText(result.getAddress());
//        // holder.txtViewDeliveryCharges.setText(result.getTitle());
//
//
//        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(activity, ShopDetailsActivity.class);
//                intent.putExtra(ConstIntent.CONST_BUISNESS_ID, result.getId());
//                intent.putExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, result.getCustom_order_allowed());
//                intent.putExtra(ConstIntent.CONST_MERCHANT_ID, result.getMerchant_id());
//                intent.putExtra(ConstIntent.CONST_SHOP_NAME, result.getTitle());
//                intent.putExtra(ConstIntent.CONST_ADDRESS, result.getAddress());
//                activity.startActivity(intent);
//
//            }
//        });

    }


    public  void  setImageAt(String strImage,int position)
    {
        CustomMenuModel customMenuModel = datum.get(position);
        customMenuModel.setItemImage(strImage);
        datum.set(position,customMenuModel);
        notifyDataSetChanged();
    }

    public  void addMore()
    {
        CustomMenuModel customMenuModel = new CustomMenuModel();
        datum.add(customMenuModel);
        notifyDataSetChanged();
    }


    public ArrayList<CustomMenuModel> getDatum() {
        return datum;
    }




    public void setCustomOrderInterface(CustomOrderInterface customOrderInterface) {
        this.customOrderInterface = customOrderInterface;
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.edtTextItemName)
        EditText edtTextItemName;

        @BindView(R.id.edtTextItemDesc)
        EditText edtTextItemDesc;

        @BindView(R.id.imgViewCamera)
        ImageView imgViewCamera;

        @BindView(R.id.imgViewMinus)
        ImageView imgViewMinus;

        @BindView(R.id.imgViewPlus)
        ImageView imgViewPlus;

        @BindView(R.id.txtViewQty)
        TextView txtViewQty;

        @BindView(R.id.imgViewCross)
        ImageView imgViewCross;

        @BindView(R.id.imgProduct)
        ImageView imgProduct;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.setFontFace(edtTextItemName, FontHelper.FontType.FONT_ROBOTO_LIGHT, activity);
            FontHelper.setFontFace(edtTextItemDesc, FontHelper.FontType.FONT_ROBOTO_LIGHT, activity);

            // setFont();
        }
    }


    public interface CustomOrderInterface
    {
        void selectImage(int position);
    }

}