package com.deliverymyorder.mvp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.GetLocation;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;
import com.deliverymyorder.mvp.dashboard.LocationFragment;
import com.deliverymyorder.mvp.intro_page.IntroActivity;
import com.deliverymyorder.mvp.login.LoginActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.vk.sdk.util.VKUtil;
//import com.vk.sdk.util.VKUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {

    int REQUEST_CHECK_SETTINGS = 1234;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);



        String[] fingerprints = VKUtil.getCertificateFingerprint(this, getPackageName());
       Log.e("SHA1", fingerprints[0]);


        ImageView imgBG = findViewById(R.id.imgBackground);
        ImageView imgSplashLogo = findViewById(R.id.imgSplashLogo);


        haskKey();
        //  imgBG.startAnimation(AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation) );

        imgSplashLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.zoom_in_bounc));

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        /*Intent i = new Intent(SplashActivity.this, CustomMenuActivity.class);
                        startActivity(i);
                        finish();*/

                        getLocationData();

                    }
                });

            }
        }).start();


    }


    public  boolean  checkIfFirstTime()
    {
        return true;
//
//        if (appGetFirstTimeRun() == 0)
//        {
//
//            return true;
//
//        }
//        else {
//
//               return  false;
//        }
    }

    private int appGetFirstTimeRun() {

        // 0: If this is the first time.
        // 1: It has started ever.
        // 2: It has started once, but not that version , ie it is an update.

        //Check if App Start First Time
        SharedPreferences appPreferences = getSharedPreferences("MyAPP", 0);
        int appCurrentBuildVersion = BuildConfig.VERSION_CODE;
        int appLastBuildVersion = appPreferences.getInt("app_first_time", 0);

        //Log.d("appPreferences", "app_first_time = " + appLastBuildVersion);

        if (appLastBuildVersion == appCurrentBuildVersion ) {
            return 1; //ya has iniciado la appp alguna vez

        } else {
            appPreferences.edit().putInt("app_first_time",
                    appCurrentBuildVersion).apply();
            if (appLastBuildVersion == 0) {
                return 0; //es la primera vez
            } else {
                return 2; //es una versión nueva
            }
        }
    }


    private void haskKey() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.deliverymyorder", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }


    public void getLocationData() {

        if (PrefUtil.getInt(SplashActivity.this, Const.LOCATION_PERMISSION, 0) != 1) {
            //  if (!Utility.isLocationEnabled(SplashActivity.this)) {
            LocationFragment dialogFragment = new LocationFragment();
            dialogFragment.show(getFragmentManager(), "LocationFragment");
        } else {
            checkRunTimePermission();
        }
    }

    public void checkRunTimePermission() {

        int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(permissionLocation == 0)) {
                if (!Settings.System.canWrite(SplashActivity.this)) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2909);
                } else {
                    //if can write
                    displayLocationSettingsRequest(this);
                }
            } else {
                //if permission is given
                displayLocationSettingsRequest(this);
            }
        } else {
            //for other version
            displayLocationSettingsRequest(this);
        }
    }


    private void displayLocationSettingsRequest(final Context context) {

        if (isGooglePlayServicesAvailable(this)) {
            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(10000 / 2);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            Log.e("", "All location settings are satisfied.");
                            navigateToNextActivity();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.e("", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result
                                // in onActivityResult().
                                status.startResolutionForResult((Activity) context, REQUEST_CHECK_SETTINGS);
                            } catch (Exception e) {
                                AlertDialogHelper.showAlertDialogToOpenLocationSetting(SplashActivity.this);
                                //LogUtil.d("PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            AlertDialogHelper.showAlertDialogToOpenLocationSetting(SplashActivity.this);

                            //  LogUtil.d("Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                }
            });
        } else {
            if (!Utils.isLocationEnabled(this)) {
                AlertDialogHelper.showAlertDialogToOpenLocationSetting(SplashActivity.this);
            } else {
                navigateToNextActivity();
            }
        }
    }

    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    public void navigateToNextActivity() {

        new GetLocation();
        GetLocation.getLocation(this);



        if (LoginManager.getInstance().isLoggedIn()) {
            goToActivities(DashboardActivity.class);
        } else {

            if (checkIfFirstTime())
            {
                final Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
                // intent.putExtra("dumbdata", Parcels.wrap(d
                // umbData));
                //startActivity(intent);
                Log.i("Splash","Called from Thread 2");
                startActivity(intent);
                finish();

                return;
            }

            goToActivities(LoginActivity.class);
        }

        //   goToActivities(LoginActivity.class);

    }

    public void goToActivities(Class activty) {
        Intent intent = new Intent(SplashActivity.this, activty);
        intent.putExtra("ISOPEN_HOME_FRAGMENT", true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionLocation == 0) {
            navigateToNextActivity();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case 1234:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //LogUtil.e("User agreed to make required location settings changes.");
                        navigateToNextActivity();
                        break;
                    case Activity.RESULT_CANCELED:
                        AlertDialogHelper.showDialogToClose(this);
                        // LogUtil.e("User chose not to make required location settings changes.");
                        break;
                }
                break;

            case 12345:

                if (Utils.isLocationEnabled(SplashActivity.this)) {
                    navigateToNextActivity();
                } else {
                    AlertDialogHelper.showDialogToClose(this);
                }
                break;
        }
    }


}