package com.deliverymyorder.mvp.add_category.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.CircleTransform;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.AddCategoryActivity;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.shop_listing.ShopListingActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCategoryAdapter extends RecyclerView.Adapter<AddCategoryAdapter.Holder> {

    Activity activity;
    ArrayList<CategoryModel.Datum> datum;
    String favBit;

    public AddCategoryAdapter(Activity activity, ArrayList<CategoryModel.Datum> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_category, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final CategoryModel.Datum result = datum.get(position);

        holder.txtViewCategoryName.setText(result.getName());

        Picasso.with(activity).load(result.getImage()).transform(new CircleTransform()).into(holder.imgViewCategory);

        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if (datum.get(position).isCategorySelected()) {
                    datum.get(position).setCategorySelected(false);
                    holder.imgViewCategory.setAlpha(1f);
                    holder.imgViewCategory.setBackgroundResource(R.drawable.drawable_round_corner);
                } else {
                    datum.get(position).setCategorySelected(true);
                    holder.imgViewCategory.setAlpha(.4f);
                    holder.imgViewCategory.setBackgroundResource(R.drawable.drawable_round_corner_selected_category);
                    ((AddCategoryActivity) activity).setCategoryId_Name(result.getName(), result.getCategoryId());
                }*/


                ((AddCategoryActivity) activity).setCategoryId_Name(result.getName(), result.getCategoryId());

                Intent intent = new Intent(activity, ShopListingActivity.class);
                intent.putExtra(ConstIntent.CONST_CATEGORY_ID, result.getCategoryId());
                intent.putExtra(ConstIntent.CONST_CATEGORY_NAME, result.getName());
                activity.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgViewCategory)
        ImageView imgViewCategory;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}

