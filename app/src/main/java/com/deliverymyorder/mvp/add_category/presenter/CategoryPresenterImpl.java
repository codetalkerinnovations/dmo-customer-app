package com.deliverymyorder.mvp.add_category.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryPresenterImpl implements CategoryContractor.CategoryPresenter {

    Activity activity;
    CategoryContractor.CategoryView categoryView;

    public CategoryPresenterImpl(Activity activity, CategoryContractor.CategoryView categoryView) {
        this.activity = activity;
        this.categoryView = categoryView;
    }

    @Override
    public void categoryData() {
        try {
            ApiAdapter.getInstance(activity);
            callGetCategoriesDataApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            categoryView.categoryInternetError();

        }
    }


    private void callGetCategoriesDataApi() {
        Progress.start(activity);

        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CategoryModel> getCreatePasswordResult = ApiAdapter.getApiService().getCategories("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {

                Progress.stop();
                try {
                    CategoryModel categoryModel = response.body();


                    if (categoryModel.getError() == 0) {
                        categoryView.getCategorySuccess(categoryModel.getData());
                    } else {
                        categoryView.getCategoryUnSucess(categoryModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    categoryView.getCategoryUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {
                Progress.stop();
                categoryView.getCategoryUnSucess(activity.getString(R.string.server_error));
            }
        });

    }

}
