package com.deliverymyorder.mvp.add_category.presenter;

import com.deliverymyorder.mvp.add_category.model.CategoryModel;

import java.util.ArrayList;

public class CategoryContractor {

    public interface CategoryPresenter {

        void categoryData();
    }

    public interface CategoryView {
        void getCategorySuccess(ArrayList<CategoryModel.Datum> categoryModel);

        void getCategoryUnSucess(String message);

        void categoryInternetError();
    }
}
