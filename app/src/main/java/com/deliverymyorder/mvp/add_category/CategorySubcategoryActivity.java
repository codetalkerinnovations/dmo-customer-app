package com.deliverymyorder.mvp.add_category;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.adapter.AddCategoryAdapter;
import com.deliverymyorder.mvp.add_category.adapter.CategoryAdapter;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.add_category.presenter.CategoryContractor;
import com.deliverymyorder.mvp.add_category.presenter.CategoryPresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategorySubcategoryActivity extends AppCompatActivity implements CategoryContractor.CategoryView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.recyclerViewCategoryList)
    RecyclerView recyclerViewCategoryList;

    CategoryPresenterImpl categoryPresenter;

    CategoryAdapter categoryAdapter;

    ArrayList<CategoryModel.Datum> arrayListCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_subcategory);
        ButterKnife.bind(this);
        categoryPresenter = new CategoryPresenterImpl(this, this);

        getCategoryListDataApi();
    }

    private void getCategoryListDataApi() {

        categoryPresenter.categoryData();
    }

    private void manageRecyclerView() {

        categoryAdapter = new CategoryAdapter(this, arrayListCategory);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerViewCategoryList.setLayoutManager(mLayoutManager);
        recyclerViewCategoryList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCategoryList.setAdapter(categoryAdapter);
    }

    @Override
    public void getCategorySuccess(ArrayList<CategoryModel.Datum> arrayListCategory) {
        this.arrayListCategory = arrayListCategory;
        manageRecyclerView();
    }

    @Override
    public void getCategoryUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void categoryInternetError() {

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {

        finish();
    }
}
