package com.deliverymyorder.mvp.add_category;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.adapter.AddCategoryAdapter;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.add_category.presenter.CategoryContractor;
import com.deliverymyorder.mvp.add_category.presenter.CategoryPresenterImpl;
import com.deliverymyorder.mvp.shop_listing.ShopListingActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCategoryActivity extends AppCompatActivity implements CategoryContractor.CategoryView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.imgViewNext)
    ImageView imgViewNext;

    @BindView(R.id.txtViewHeading)
    TextView txtViewHeading;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;


    @BindView(R.id.recyclerViewCategoryList)
    RecyclerView recyclerViewCategoryList;


    CategoryPresenterImpl categoryPresenter;

    AddCategoryAdapter addCategoryAdapter;

    ArrayList<CategoryModel.Datum> arrayListCategory;

    String categoryName = "";
    int categoryId = 0;

    int buisnessId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        ButterKnife.bind(this);

        categoryPresenter = new CategoryPresenterImpl(this, this);

        getCategoryListDataApi();

        setFont();
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewHeading, FontHelper.FontType.FONT_ROBOTO_MEDIUM, this);
        FontHelper.setFontFace(txtViewTitle, FontHelper.FontType.FONT_ROBOTO_MEDIUM, this);
    }

    private void getCategoryListDataApi() {

        categoryPresenter.categoryData();
    }

    private void manageRecyclerView() {

        addCategoryAdapter = new AddCategoryAdapter(this, arrayListCategory);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 4);
        recyclerViewCategoryList.setLayoutManager(mLayoutManager);
        recyclerViewCategoryList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCategoryList.setAdapter(addCategoryAdapter);
    }

    public void setCategoryId_Name(String categoryName, int categoryId) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }


    @Override
    public void getCategorySuccess(ArrayList<CategoryModel.Datum> arrayListCategory) {

        this.arrayListCategory = arrayListCategory;
        manageRecyclerView();
    }

    @Override
    public void getCategoryUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void categoryInternetError() {

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {

        finish();
    }


    @OnClick(R.id.imgViewNext)
    public void imgViewNext() {

        Intent intent = new Intent(this, ShopListingActivity.class);
        intent.putExtra(ConstIntent.CONST_CATEGORY_ID, categoryId);
        intent.putExtra(ConstIntent.CONST_CATEGORY_NAME, categoryName);
        startActivity(intent);
    }


}
