package com.deliverymyorder.mvp.add_category.adapter;

import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter  extends RecyclerView.Adapter<CategoryAdapter.Holder> {


    Activity activity;
    ArrayList<CategoryModel.Datum> datum;
    String favBit;

    public CategoryAdapter(Activity activity, ArrayList<CategoryModel.Datum> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_custom_category_name, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        final CategoryModel.Datum result = datum.get(position);


        if (result.getSubCategory().size()>0) {

            holder.txtViewCategoryName.setVisibility(View.VISIBLE);
            holder.recyclerSubCat.setVisibility(View.VISIBLE);

            holder.txtViewCategoryName.setText(result.getName());


            holder.recyclerSubCat.setLayoutManager(new GridLayoutManager(activity, 3));

            SubCategoryAdapter subCategoryAdapter = new SubCategoryAdapter(activity, datum.get(position).getSubCategory());
            holder.recyclerSubCat.setItemAnimator(new DefaultItemAnimator());
            holder.recyclerSubCat.setAdapter(subCategoryAdapter);
        } else {

            holder.txtViewCategoryName.setVisibility(View.GONE);
            holder.recyclerSubCat.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.recyclerSubCat)
        RecyclerView recyclerSubCat;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}
