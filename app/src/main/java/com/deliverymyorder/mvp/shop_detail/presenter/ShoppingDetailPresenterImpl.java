package com.deliverymyorder.mvp.shop_detail.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.dashboard.presenter.HomePageContractor;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShoppingDetailPresenterImpl implements ShoppingDetailContractor.ShoppingDetailPresenter {

    Activity activity;
    ShoppingDetailContractor.ShoppingDetailView shoppingDetailView;

    public ShoppingDetailPresenterImpl(Activity activity, ShoppingDetailContractor.ShoppingDetailView shoppingDetailView) {
        this.activity = activity;
        this.shoppingDetailView = shoppingDetailView;
    }

    @Override
    public void getShoppingDetailData(int buisnessId) {
        try {
            ApiAdapter.getInstance(activity);
            callGetProductsApi(buisnessId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            shoppingDetailView.ShoppingDetailDataInternetError();
        }
    }

    @Override
    public void getCreateorderDataApi(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            callCreateOrderApi(jsonObject);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            shoppingDetailView.CreateOrderInternetError();
        }
    }

    private void callGetProductsApi(int buisnessId) {
        Progress.start(activity);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_BUISNESS_ID, buisnessId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<ShopDetailModel> getCreatePasswordResult = ApiAdapter.getApiService().getItemsByShop("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<ShopDetailModel>() {
            @Override
            public void onResponse(Call<ShopDetailModel> call, Response<ShopDetailModel> response) {

                Progress.stop();
                try {
                    ShopDetailModel shopDetailModel = response.body();

                    if (shopDetailModel.getError() == 0) {
                        shoppingDetailView.getShoppingDetailDataSuccess(shopDetailModel.getData());
                    } else {
                        shoppingDetailView.getShoppingDetailDataUnSucess(shopDetailModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    shoppingDetailView.getShoppingDetailDataUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ShopDetailModel> call, Throwable t) {
                Progress.stop();
                shoppingDetailView.getShoppingDetailDataUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

    private void callCreateOrderApi(JSONObject jsonObject) {
        Progress.start(activity);


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<CreateOrderResponseModel> getCreatePasswordResult = ApiAdapter.getApiService().createOrder("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<CreateOrderResponseModel>() {
            @Override
            public void onResponse(Call<CreateOrderResponseModel> call, Response<CreateOrderResponseModel> response) {

                Progress.stop();
                try {
                    CreateOrderResponseModel createOrderResponseModel = response.body();

                    if (createOrderResponseModel.getError() == 0) {
                        shoppingDetailView.getCreateOrderSuccess(createOrderResponseModel.getData());
                    } else {
                        shoppingDetailView.getCreateOrderUnSucess(createOrderResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    shoppingDetailView.getCreateOrderUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CreateOrderResponseModel> call, Throwable t) {
                Progress.stop();
                shoppingDetailView.getCreateOrderUnSucess(activity.getString(R.string.server_error));
            }
        });
    }
}
