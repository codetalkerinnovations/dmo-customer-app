package com.deliverymyorder.mvp.shop_detail;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.CustomMenuActivity;
import com.deliverymyorder.mvp.CustomMenuModel;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.add_category.presenter.CategoryContractor;
import com.deliverymyorder.mvp.add_category.presenter.CategoryPresenterImpl;
import com.deliverymyorder.mvp.current_shop_model.DmoShop;
import com.deliverymyorder.mvp.delivery_type.DeliveryTypeActivity;
import com.deliverymyorder.mvp.shop_detail.adapter.CategoryMenuAdapter;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailContractor;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailPresenterImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShopDetailsActivity extends AppCompatActivity implements ShoppingDetailContractor.ShoppingDetailView, CategoryContractor.CategoryView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.relLaySearch)
    RelativeLayout relLaySearch;

    @BindView(R.id.edtTextSearchShop)
    EditText edtTextSearchShop;

    @BindView(R.id.imgViewSearchButton)
    ImageView imgViewSearchButton;


    @BindView(R.id.imgViewSearch)
    ImageView imgViewSearch;

    @BindView(R.id.txtViewShopName)
    TextView txtViewShopName;

    @BindView(R.id.txtViewAreaName)
    TextView txtViewAreaName;

    @BindView(R.id.txtCartSummary)
    TextView txtCartSummary;

    @BindView(R.id.txtViewCustomizationMenu)
    TextView txtViewCustomizationMenu;

    @BindView(R.id.recyclerViewProduct)
    RecyclerView recyclerViewProduct;

    @BindView(R.id.imgViewNext)
    Button imgViewNext;

    @BindView(R.id.recyclerViewCategory)
    RecyclerView recyclerViewCategory;


    ShoppingDetailPresenterImpl shoppingDetailPresenter;

    ShopDetailAdapter shopDetailAdapter;
    ArrayList<ShopDetailModel.Datum> arrayListShopDetail;
    ArrayList<ShopDetailModel.Datum> mArrayListShopDetail;

    JSONObject jsonObject;

    int cutomOrderAllowed = 0;

    int buisnessId = 0;
    int merchantId = 0;

    String shopName;
    String address;
    int totalQty = 0;
    int totalPrice = 0;
    String searchKey;


    CategoryPresenterImpl categoryPresenterImpl;
    ArrayList<CustomMenuModel> arrayListCustoMenu;

    ArrayList<CategoryModel.Datum> arryCategoryModel;

    DmoShop currentDmoShop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);
        ButterKnife.bind(this);


        arrayListCustoMenu = new ArrayList<>();
        shoppingDetailPresenter = new ShoppingDetailPresenterImpl(this, this);

        categoryPresenterImpl = new CategoryPresenterImpl(this, this);

        txtViewCustomizationMenu.setPaintFlags(txtViewCustomizationMenu.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        if (getIntent() != null) {

            merchantId = getIntent().getIntExtra(ConstIntent.CONST_MERCHANT_ID, 0);
            buisnessId = getIntent().getIntExtra(ConstIntent.CONST_BUISNESS_ID, 0);
            shopName = getIntent().getStringExtra(ConstIntent.CONST_SHOP_NAME);
            address = getIntent().getStringExtra(ConstIntent.CONST_ADDRESS);
            cutomOrderAllowed = getIntent().getIntExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, 0);

        }

       /* if (cutomOrderAllowed != 1) {
            txtViewCustomizationMenu.setVisibility(View.GONE);
        }*/


        getShoppingDetailDataApi();

        edtTextSearchShop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ShopDetailsActivity.this.filterDataBasedItemsBasedOnText(edtTextSearchShop.getText().toString());
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        imgViewSearch.setVisibility(View.VISIBLE);
        relLaySearch.setVisibility(View.GONE);


    }

    private void manageRecyclerView() {

        shopDetailAdapter = new ShopDetailAdapter(this, arrayListShopDetail);
        shopDetailAdapter.setCartItemsDidUpdateListner(new ShopDetailAdapter.CartItemsDidUpdateListner() {
            @Override
            public void cartItemsDidUpdate(ArrayList<ShopDetailModel.Datum> datum) {

                ShopDetailsActivity.this.arrayListShopDetail = datum;
                ShopDetailsActivity.this.updateCartSummaryLable();
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerViewProduct.setLayoutManager(mLayoutManager);
        recyclerViewProduct.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProduct.setAdapter(shopDetailAdapter);
    }


    private void getShoppingDetailDataApi() {

        shoppingDetailPresenter.getShoppingDetailData(buisnessId);
    }

    private void createOrderDataApi() {


        shoppingDetailPresenter.getCreateorderDataApi(jsonObject);
    }

    @Override
    public void getShoppingDetailDataSuccess(ArrayList<ShopDetailModel.Datum> arrayListShopDetail) {


        if (arrayListShopDetail.size() > 0) {
            this.mArrayListShopDetail = arrayListShopDetail;
            this.arrayListShopDetail = arrayListShopDetail;

            txtViewShopName.setText(shopName);
            txtViewAreaName.setText(address);
            manageRecyclerView();

            updateCartSummaryLable();

            categoryPresenterImpl.categoryData();
        } else {
            //  SnackNotify.showMessage("No item found", relLayMain);

            if (cutomOrderAllowed == 1) {
                Intent intent = new Intent(this, CustomMenuActivity.class);
                intent.putExtra(ConstIntent.CONST_BUISNESS_ID, buisnessId);
                intent.putExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, cutomOrderAllowed);
                intent.putExtra(ConstIntent.CONST_MERCHANT_ID, merchantId);
                intent.putExtra(ConstIntent.CONST_SHOP_NAME, shopName);
                intent.putExtra(ConstIntent.CONST_ADDRESS, address);
                intent.putExtra(ConstIntent.CONST_SHOP_HAS_PRODUCTS, 0);
                startActivity(intent);
                finish();
            }

        }


    }

    @Override
    public void getShoppingDetailDataUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void ShoppingDetailDataInternetError() {

    }

    @Override
    public void getCreateOrderSuccess(CreateOrderResponseModel.Data createOrderResponseModel) {

        Intent intent = new Intent(this, DeliveryTypeActivity.class);
        intent.putExtra(ConstIntent.CONST_ORDER_ID, createOrderResponseModel.getId());
        startActivity(intent);
    }

    @Override
    public void getCreateOrderUnSucess(String message) {

    }

    @Override
    public void CreateOrderInternetError() {

    }

    private void updateCartSummaryLable() {


        ArrayList<ShopDetailModel.Datum> arrayListShopDetailTemp = new ArrayList<>();

        if (arrayListShopDetail != null) {
            for (int i = 0; i < arrayListShopDetail.size(); i++) {

                if (arrayListShopDetail.get(i).isAddedForBuy()) {
                    arrayListShopDetailTemp.add(arrayListShopDetail.get(i));
                }
            }
        }

        int totaNumber = arrayListShopDetailTemp.size() + (arrayListCustoMenu != null ? arrayListCustoMenu.size() : 0);
        txtCartSummary.setText(totaNumber + " Items");
           /*

//        for (int i = 0; i < arrayListCustoMenu.size(); i++) {
//
//            CustomMenuModel customMenuModel = arrayListCustoMenu.get(i);
//
//        }

        //

        StringBuilder sb = new StringBuilder();



        if (arrayListShopDetailTemp!=null && arrayListShopDetailTemp.size() == 0)
        {


            if (arrayListShopDetailTemp.size() == 3 )
            {
                for  (int i=0;i<=1;i++)  {

                    ShopDetailModel.Datum  datum = arrayListShopDetailTemp.get(i);
                      sb.append(datum.getItemImage());
                }

                int totalCount = arrayListShopDetailTemp.size() + arrayListCustoMenu.size() - 2;

                txtCartSummary.setText(sb.toString()+" +"+totalCount +" more");

            }else
            {
               for (ShopDetailModel.Datum  datum : arrayListShopDetailTemp)
               {
                   sb.append(datum.getItemName());
               }
                txtCartSummary.setText(sb.toString());
            }

        }else
        {

            if (arrayListShopDetailTemp!=null && arrayListShopDetailTemp.size() == 0) {

            }

            int totalCount = arrayListShopDetailTemp.size() + arrayListCustoMenu.size() - 2;

            txtCartSummary.setText(sb.toString()+" +"+totalCount +" more");
        }


*/

    }

    @OnClick(R.id.imgViewNext)
    public void imgViewNext() {

        Progress.start(this);
        new Thread(new Runnable() {
            @Override
            public void run() {



        int customerId = LoginManager.getInstance().getUserData().getCustomerId();


        jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();


        for (int i = 0; i < arrayListShopDetail.size(); i++) {

            if (arrayListShopDetail.get(i).isAddedForBuy()) {

                JSONObject jsonObjectItems = new JSONObject();
                try {
                    jsonObjectItems.put(Const.PARAM_ID, arrayListShopDetail.get(i).getId());
                    jsonObjectItems.put(Const.PARAM_NAME, arrayListShopDetail.get(i).getItemName());
                    jsonObjectItems.put(Const.PARAM_QTY, arrayListShopDetail.get(i).getQty());
                    jsonObjectItems.put(Const.PARAM_PRICE, arrayListShopDetail.get(i).getItemPrice());
                    jsonObjectItems.put(Const.PARAM_DESCRIPTION, arrayListShopDetail.get(i).getItemDescription());


                    totalQty = totalQty + arrayListShopDetail.get(i).getQty();

                    totalPrice = totalPrice + (arrayListShopDetail.get(i).getQty() * Integer.parseInt(arrayListShopDetail.get(i).getItemPrice().replace(".00", "")));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(jsonObjectItems);
            }
        }


        for (int i = 0; i < arrayListCustoMenu.size(); i++) {

            CustomMenuModel customMenuModel = arrayListCustoMenu.get(i);

            JSONObject jsonObjectItems = new JSONObject();
            try {
                jsonObjectItems.put(Const.PARAM_ID, 0);
                jsonObjectItems.put(Const.PARAM_NAME, customMenuModel.getItemName());
                jsonObjectItems.put(Const.PARAM_QTY, customMenuModel.getQty());
                jsonObjectItems.put(Const.PARAM_PRICE, 0);
                jsonObjectItems.put(Const.PARAM_DESCRIPTION, customMenuModel.getItemDesc());
                jsonObjectItems.put(Const.PARAM_IMAGE,Utils.getBase64StringFromImage(customMenuModel.getItemImage()));

                totalQty = totalQty + customMenuModel.getQty();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObjectItems);
        }


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_BUISNESS_ID, buisnessId);
            jsonObject.put(Const.KEY_MERCHANT_ID, merchantId);
            jsonObject.put(Const.KEY_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_TOTAL_ITEMS, totalQty);
            jsonObject.put(Const.PARAM_TOTAL_AMOUNT, totalPrice);
            jsonObject.put(Const.PARAM_ITEMS, jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (totalQty > 0) {
            createOrderDataApi();
        } else {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SnackNotify.showMessage("Please add at least one product.", relLayMain);
                }
            });
        }


            }
        }).start();
    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {
        finish();
    }


    @OnClick(R.id.txtViewCustomizationMenu)
    public void txtViewCustomizationMenu() {

        // modified by shubham

//        arrayListCustoMenu

        Intent intent = new Intent(this, CustomMenuActivity.class);
        intent.putExtra(ConstIntent.CONST_BUISNESS_ID, buisnessId);
        intent.putExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, cutomOrderAllowed);
        intent.putExtra(ConstIntent.CONST_MERCHANT_ID, merchantId);
        intent.putExtra(ConstIntent.CONST_SHOP_NAME, shopName);
        intent.putExtra(ConstIntent.CONST_ADDRESS, address);
        intent.putExtra(ConstIntent.CONST_SHOP_HAS_PRODUCTS, 1);

        if (arrayListCustoMenu != null) {
            intent.putParcelableArrayListExtra(ConstIntent.CONST_ADDED_CUSTOMER_ORDER, arrayListCustoMenu);
        }

        startActivityForResult(intent, 1001);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 1001
        if (requestCode == Const.KEY_CUSTOM_MENU_REQUEST_CODE) {

            if (data != null) {
                // Bundle bundle = data.getExtras();
                if (data.getExtras() != null) {
                    arrayListCustoMenu = (ArrayList<CustomMenuModel>) data.getExtras().getSerializable(ConstIntent.CONST_ARRAYLIST_CUSTOM_MENU);

                    Log.e("array size ", "" + arrayListCustoMenu.size());
                }
            }

            updateCartSummaryLable();
        }
    }


    @OnClick(R.id.imgViewSearch)
    public void imgViewSearch() {

        imgViewSearch.setVisibility(View.GONE);
        relLaySearch.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.imgViewSearchButton)
    public void imgViewSearchButton() {

        searchKey = String.valueOf(edtTextSearchShop.getText());

        if (Utils.isEmptyOrNull(searchKey)) {

            SnackNotify.showMessage("Please enter shop name.", relLayMain);
        } else {

            callSearchDataApi();
        }
    }

    private void callSearchDataApi() {

        Utils.hideKeyboardIfOpen(this);

    }


    private void manageCategoryRecyclerView() {
        CategoryMenuAdapter subCategoryAdapter = new CategoryMenuAdapter(this, arryCategoryModel);

        subCategoryAdapter.setCategoryChangeListner(new CategoryMenuAdapter.CategoryChangeListner() {
            @Override
            public void categoryChangeListner(int selectedCategoryID) {
                ShopDetailsActivity.this.filterDataBasedItemsBasedOnCategory(selectedCategoryID);
            }
        });

        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(this);
        mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewCategory.setLayoutManager(mLayoutManager1);
        recyclerViewCategory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCategory.setAdapter(subCategoryAdapter);


    }

    private void filterDataBasedItemsBasedOnText(String searchKey) {

        if (searchKey == null || searchKey.length() == 0) {
            this.arrayListShopDetail = this.mArrayListShopDetail;
        } else {

            this.arrayListShopDetail = new ArrayList<>();

            for (ShopDetailModel.Datum datum : this.mArrayListShopDetail) {

                if (datum.getItemName() != null && datum.getItemName().contains(searchKey)) {
                    this.arrayListShopDetail.add(datum);
                }
            }

        }

        manageRecyclerView();
    }

    private void filterDataBasedItemsBasedOnCategory(int categpryID) {

        if (mArrayListShopDetail == null) {
            return;
        }

        if (categpryID == -1) {
            this.arrayListShopDetail = this.mArrayListShopDetail;
        } else {
            this.arrayListShopDetail = new ArrayList<>();

            for (ShopDetailModel.Datum datum : this.mArrayListShopDetail) {
                if (datum.getCategoryId() == categpryID) {
                    this.arrayListShopDetail.add(datum);
                }
            }

        }

        manageRecyclerView();
    }


    @Override
    public void getCategorySuccess(ArrayList<CategoryModel.Datum> categoryModel) {

        arryCategoryModel = categoryModel;

        //;
        manageCategoryRecyclerView();
    }


    public boolean isAvailableInItems(CategoryModel.Datum categoryModel) {
//        for (ShopDetailModel.Datum shop: arrayListShopDetail)
//        {
//            if (categoryModel.getCategoryId() == shop.ge)
//
//        }
//
        return true;
    }

    @Override
    public void getCategoryUnSucess(String message) {

    }

    @Override
    public void categoryInternetError() {

    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            recyclerViewCategory.setVisibility(View.GONE);
            //Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            recyclerViewCategory.setVisibility(View.VISIBLE);
            //Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
        }
    }
}
