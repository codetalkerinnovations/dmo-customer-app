package com.deliverymyorder.mvp.shop_detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ShopDetailModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Datum {



        //category_id
        @SerializedName("product_id")
        @Expose
        private Integer id;

        @SerializedName("category_id")
        @Expose
        private Integer categoryId;

        @SerializedName("item_name")
        @Expose
        private String itemName;
        @SerializedName("item_sku")
        @Expose
        private String itemSku;
        @SerializedName("item_image")
        @Expose
        private String itemImage;
        @SerializedName("item_price")
        @Expose
        private String itemPrice;
        @SerializedName("item_description")
        @Expose
        private String itemDescription;
        @SerializedName("is_featured")
        @Expose
        private Object isFeatured;

        private boolean isAddedForBuy;

        private int qty;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public String getItemImage() {
            return itemImage;
        }

        public void setItemImage(String itemImage) {
            this.itemImage = itemImage;
        }

        public String getItemPrice() {
            return itemPrice;
        }

        public void setItemPrice(String itemPrice) {
            this.itemPrice = itemPrice;
        }

        public String getItemDescription() {
            return itemDescription;
        }

        public void setItemDescription(String itemDescription) {
            this.itemDescription = itemDescription;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public Object getIsFeatured() {
            return isFeatured;
        }

        public void setIsFeatured(Object isFeatured) {
            this.isFeatured = isFeatured;
        }

        public boolean isAddedForBuy() {
            return isAddedForBuy;
        }

        public void setAddedForBuy(boolean addedForBuy) {
            isAddedForBuy = addedForBuy;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }
    }
}
