package com.deliverymyorder.mvp.shop_detail;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.shop_listing.adapter.ShoppingListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShopDetailAdapter extends RecyclerView.Adapter<ShopDetailAdapter.Holder> {


    Activity activity;
    ArrayList<ShopDetailModel.Datum> datum;
    String favBit;


    CartItemsDidUpdateListner cartItemsDidUpdateListner;

    public ShopDetailAdapter(Activity activity, ArrayList<ShopDetailModel.Datum> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_product, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final ShopDetailModel.Datum result = datum.get(position);


        holder.txtViewItemName.setText(result.getItemName());
        holder.txtViewItemPrice.setText("₹"+result.getItemPrice());
        // holder.txtViewDeliveryCharges.setText(result.getTitle());


        holder.txtViewAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.relLayAdd.setVisibility(View.GONE);
                holder.relLayItemCount.setVisibility(View.VISIBLE);

                datum.get(position).setAddedForBuy(true);
                datum.get(position).setQty(1);

                if (cartItemsDidUpdateListner!=null)
                    cartItemsDidUpdateListner.cartItemsDidUpdate(datum);

            }
        });


        holder.imgViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int quantity = result.getQty();
                int qty = quantity + 1;

                holder.txtViewQty.setText(String.valueOf(qty));

                datum.get(position).setAddedForBuy(true);
                datum.get(position).setQty(qty);

                if (cartItemsDidUpdateListner!=null)
                    cartItemsDidUpdateListner.cartItemsDidUpdate(datum);
            }
        });


        holder.imgViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int quantity = result.getQty();

                if (quantity > 1) {
                    int qty = quantity - 1;

                    holder.txtViewQty.setText(String.valueOf(qty));

                    datum.get(position).setAddedForBuy(true);
                    datum.get(position).setQty(qty);
                } else if (quantity == 1) {

                    datum.get(position).setAddedForBuy(false);

                    holder.relLayAdd.setVisibility(View.VISIBLE);
                    holder.relLayItemCount.setVisibility(View.GONE);


                    if (cartItemsDidUpdateListner!=null)
                        cartItemsDidUpdateListner.cartItemsDidUpdate(datum);
                }

            }
        });

    }

    public void setCartItemsDidUpdateListner(CartItemsDidUpdateListner cartItemsDidUpdateListner) {
        this.cartItemsDidUpdateListner = cartItemsDidUpdateListner;
    }



    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewItemName)
        TextView txtViewItemName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.txtViewItemPrice)
        TextView txtViewItemPrice;

        @BindView(R.id.txtViewAdd)
        TextView txtViewAdd;

        @BindView(R.id.relLayAdd)
        RelativeLayout relLayAdd;

        @BindView(R.id.relLayItemCount)
        RelativeLayout relLayItemCount;

        @BindView(R.id.imgViewMinus)
        ImageView imgViewMinus;

        @BindView(R.id.imgViewPlus)
        ImageView imgViewPlus;

        @BindView(R.id.txtViewQty)
        TextView txtViewQty;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }

    public  interface  CartItemsDidUpdateListner{
        void cartItemsDidUpdate(ArrayList<ShopDetailModel.Datum> datum);
    }
}