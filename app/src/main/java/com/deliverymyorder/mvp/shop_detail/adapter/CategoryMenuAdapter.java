package com.deliverymyorder.mvp.shop_detail.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.CircleTransform;
import com.deliverymyorder.Common.helpers.RoundCornerTransformation;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.adapter.CategoryAdapter;
import com.deliverymyorder.mvp.add_category.adapter.SubCategoryAdapter;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.shop_listing.ShopListingActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryMenuAdapter  extends RecyclerView.Adapter<CategoryMenuAdapter.Holder> {


    Activity activity;
    ArrayList<CategoryModel.Datum> datum;
    String favBit;

    CategoryChangeListner categoryChangeListner;


    int selectedCatID = -1;

    public CategoryMenuAdapter(Activity activity, ArrayList<CategoryModel.Datum> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_category_shop, parent, false);
        return new Holder(view);

    }


    public void setCategoryChangeListner(CategoryChangeListner categoryChangeListner) {
        this.categoryChangeListner = categoryChangeListner;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final CategoryModel.Datum result = datum.get(position);


        holder.txtViewCategoryName.setText(result.getName());

        Picasso.with(activity).load(result.getImage()).transform(new RoundCornerTransformation()).into(holder.imgViewCategory);


        if (result.getCategoryId() == selectedCatID)
        {
            holder.icSelectedCategory.setVisibility(View.VISIBLE);

        }else
        {
            holder.icSelectedCategory.setVisibility(View.GONE);
        }

        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (categoryChangeListner!=null) {

                    if (result.getCategoryId() == selectedCatID) {
                        selectedCatID = -1;
                    } else {
                        selectedCatID = result.getCategoryId();
                    }
                  categoryChangeListner.categoryChangeListner(selectedCatID);
                    notifyDataSetChanged();
                }

            }
        });


    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;


        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.imgViewCategory)
        ImageView imgViewCategory;

        @BindView(R.id.icSelectedCategory)
        ImageView icSelectedCategory;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }


    public  interface  CategoryChangeListner
    {
        void categoryChangeListner(int selectedCategoryID);
    }
}
