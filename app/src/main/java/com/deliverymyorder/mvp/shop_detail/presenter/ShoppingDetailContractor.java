package com.deliverymyorder.mvp.shop_detail.presenter;


import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public interface ShoppingDetailContractor {

    public interface ShoppingDetailPresenter {
        void getShoppingDetailData(int buisnessId);

        void getCreateorderDataApi(JSONObject jsonObject);

    }

    public interface ShoppingDetailView {
        void getShoppingDetailDataSuccess(ArrayList<ShopDetailModel.Datum> arrayListShopDetail);

        void getShoppingDetailDataUnSucess(String message);

        void ShoppingDetailDataInternetError();


        void getCreateOrderSuccess(CreateOrderResponseModel.Data createOrderResponseModel);

        void getCreateOrderUnSucess(String message);

        void CreateOrderInternetError();


    }
}
