package com.deliverymyorder.mvp.shop_detail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Braintech on 26-12-2018.
 */

public class CreateOrderResponseModel {


    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Data {

        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("merchant_id")
        @Expose
        private Integer merchantId;
        @SerializedName("business_id")
        @Expose
        private Integer businessId;
        @SerializedName("address_id")
        @Expose
        private Object addressId;
        @SerializedName("total_items")
        @Expose
        private Integer totalItems;
        @SerializedName("total_amount")
        @Expose
        private Integer totalAmount;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("invoice_no")
        @Expose
        private String invoiceNo;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("order_details")
        @Expose
        private ArrayList<OrderDetail> orderDetails = null;

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public Integer getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(Integer merchantId) {
            this.merchantId = merchantId;
        }

        public Integer getBusinessId() {
            return businessId;
        }

        public void setBusinessId(Integer businessId) {
            this.businessId = businessId;
        }

        public Object getAddressId() {
            return addressId;
        }

        public void setAddressId(Object addressId) {
            this.addressId = addressId;
        }

        public Integer getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(Integer totalItems) {
            this.totalItems = totalItems;
        }

        public Integer getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Integer totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public ArrayList<OrderDetail> getOrderDetails() {
            return orderDetails;
        }

        public void setOrderDetails(ArrayList<OrderDetail> orderDetails) {
            this.orderDetails = orderDetails;
        }


        public class OrderDetail {

            @SerializedName("order_id")
            @Expose
            private Integer orderId;
            @SerializedName("product_id")
            @Expose
            private String productId;
            @SerializedName("qty")
            @Expose
            private Integer qty;
            @SerializedName("product_price")
            @Expose
            private String productPrice;
            @SerializedName("total_amount")
            @Expose
            private Integer totalAmount;
            @SerializedName("delivered_status")
            @Expose
            private Integer deliveredStatus;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("name")
            @Expose
            private String name;

            public Integer getOrderId() {
                return orderId;
            }

            public void setOrderId(Integer orderId) {
                this.orderId = orderId;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public Integer getQty() {
                return qty;
            }

            public void setQty(Integer qty) {
                this.qty = qty;
            }

            public String getProductPrice() {
                return productPrice;
            }

            public void setProductPrice(String productPrice) {
                this.productPrice = productPrice;
            }

            public Integer getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(Integer totalAmount) {
                this.totalAmount = totalAmount;
            }

            public Integer getDeliveredStatus() {
                return deliveredStatus;
            }

            public void setDeliveredStatus(Integer deliveredStatus) {
                this.deliveredStatus = deliveredStatus;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

        }
    }
}
