package com.deliverymyorder.mvp.coupen.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.coupen.CoupenModel;
import com.deliverymyorder.mvp.order_summary.model.ApplyCoupenRes;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;
import com.deliverymyorder.mvp.track_order.presenter.TrackOrderContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoupenPresenterImpl implements CoupenContractor.CoupenPresenter{

    Activity activity;
    CoupenContractor.CoupenView coupenView;

    public CoupenPresenterImpl(Activity activity,CoupenContractor.CoupenView coupenView){
        this.activity = activity;
        this.coupenView = coupenView;
    }


    @Override
    public void callCoupenApi() {
        try {
            ApiAdapter.getInstance(activity);
            callCoupenDataApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            coupenView.onInternetErrorCoupenList();
        }
    }

    @Override
    public void updateCoupen(String coupenCode,int buissnesID) {
        try {
            ApiAdapter.getInstance(activity);
            updateCoupenDataApi(coupenCode,buissnesID);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            coupenView.onInternetErrorCoupenList();
        }
    }

    @Override
    public void cancelCoupen(String coupenCode,int buissnesID) {
        try {
            ApiAdapter.getInstance(activity);
            cancelCoupenDataApi(coupenCode,buissnesID);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            coupenView.onInternetErrorCoupenList();
        }
    }

    private void callCoupenDataApi() {

        Progress.start(activity);

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_USER_ID, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<CoupenModel> getCreatePasswordResult = ApiAdapter.getApiService().getCoupenList("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<CoupenModel>() {
            @Override
            public void onResponse(Call<CoupenModel> call, Response<CoupenModel> response) {

                Progress.stop();
                try {
                    CoupenModel coupenModel = response.body();

                    if (coupenModel.getError() == 0) {
                        coupenView.onSuccessCoupenList(coupenModel.getData());
                    } else {
                        coupenView.onUnSuccessCoupenList(coupenModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    coupenView.onUnSuccessCoupenList(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CoupenModel> call, Throwable t) {
                Progress.stop();
                coupenView.onUnSuccessCoupenList(activity.getString(R.string.server_error));
            }
        });
    }



    private void cancelCoupenDataApi(String coupenCode,int buissnesID) {

        Progress.start(activity);

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        int orderType = 0;
        int orderId = 0;

        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType =1;
            orderId = ((OrderHistoryModel.Data.Ongoing) obj).getId();
        }else if (obj instanceof PickAndDropOnGoingData)
        {
            orderType =2;
            orderId = ((PickAndDropOnGoingData) obj).getId();
        }


        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_USER_ID, customerId);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
            jsonObject.put(Const.PARAM_COUPEN_CODE, coupenCode);
            jsonObject.put(Const.KEY_BUISNESS_ID, buissnesID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<ApplyCoupenRes> getCreatePasswordResult = ApiAdapter.getApiService().removeCoupen("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<ApplyCoupenRes>() {
            @Override
            public void onResponse(Call<ApplyCoupenRes> call, Response<ApplyCoupenRes> response) {

                Progress.stop();
                try {
                    ApplyCoupenRes coupenModel = response.body();

                    if (coupenModel.getError() == 0) {
                        coupenView.onSuccessRemoveCoupen();
                    } else {
                        coupenView.onUnSuccessCoupenList(coupenModel.getReason());
                    }
                } catch (NullPointerException exp) {
                    //coupenView.onUnSuccessCoupenList(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ApplyCoupenRes> call, Throwable t) {
                Progress.stop();
                //coupenView.onUnSuccessCoupenList(activity.getString(R.string.server_error));
            }
        });
    }

    private void updateCoupenDataApi(String coupenCode,int buissnesID) {

        Progress.start(activity);

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        int orderType = 0;
        int orderId = 0;

        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType =1;
            orderId = ((OrderHistoryModel.Data.Ongoing) obj).getId();
        }else if (obj instanceof PickAndDropOnGoingData)
        {
            orderType =2;
            orderId = ((PickAndDropOnGoingData) obj).getId();
        }


        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_USER_ID, customerId);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
            jsonObject.put(Const.PARAM_COUPEN_CODE, coupenCode);
            jsonObject.put(Const.KEY_BUISNESS_ID, buissnesID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<ApplyCoupenRes> getCreatePasswordResult = ApiAdapter.getApiService().verifyCoupen("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<ApplyCoupenRes>() {
            @Override
            public void onResponse(Call<ApplyCoupenRes> call, Response<ApplyCoupenRes> response) {

                Progress.stop();
                try {
                    ApplyCoupenRes coupenModel = response.body();

                    if (coupenModel.getError() == 0) {
                        coupenView.onSuccessUpdateCoupen();
                    } else {
                        coupenView.onUnSuccessCoupenList(coupenModel.getReason());
                    }
                } catch (NullPointerException exp) {
                    //coupenView.onUnSuccessCoupenList(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ApplyCoupenRes> call, Throwable t) {
                Progress.stop();
                //coupenView.onUnSuccessCoupenList(activity.getString(R.string.server_error));
            }
        });
    }
}
