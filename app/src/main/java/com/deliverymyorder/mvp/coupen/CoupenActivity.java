package com.deliverymyorder.mvp.coupen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.coupen.adapter.CoupenAdapter;
import com.deliverymyorder.mvp.coupen.presenter.CoupenContractor;
import com.deliverymyorder.mvp.coupen.presenter.CoupenPresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CoupenActivity extends AppCompatActivity implements CoupenContractor.CoupenView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.recyclerViewCoupen)
    RecyclerView recyclerViewCoupen;

    @BindView(R.id.edtTextCoupen)
    EditText edtTextCoupen;

    @BindView(R.id.txtViewApply)
    TextView txtViewApply;

    CoupenPresenterImpl coupenPresenter;

    CoupenAdapter coupenAdapter;

    ArrayList<CoupenModel.Datum> arrayListCoupen;

    String coupenCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupen);
        ButterKnife.bind(this);

        coupenPresenter = new CoupenPresenterImpl(this, this);

        getCoupenListDataApi();
    }

    private void getCoupenListDataApi() {

        coupenPresenter.callCoupenApi();
    }

    private void manageRecyclerViewCoupenList() {

        coupenAdapter = new CoupenAdapter(this, arrayListCoupen);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerViewCoupen.setLayoutManager(mLayoutManager);
        recyclerViewCoupen.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCoupen.setAdapter(coupenAdapter);
    }

    @Override
    public void onSuccessCoupenList(ArrayList<CoupenModel.Datum> arrayListCoupen) {
        this.arrayListCoupen = arrayListCoupen;

        manageRecyclerViewCoupenList();
    }

    @Override
    public void onUnSuccessCoupenList(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onInternetErrorCoupenList() {

    }

    @Override
    public void onSuccessUpdateCoupen() {

    }

    @Override
    public void onUnSuccessUpdateCoupen(String message) {

    }


    @Override
    public void onInternetErrorUpdateCoupen() {

    }

    @Override
    public void onSuccessRemoveCoupen() {

    }

    public void callUpdateCoupenDataApi(String coupenCode) {
        this.coupenCode = coupenCode;
        coupenPresenter.updateCoupen(this.coupenCode,0);
    }


    @OnClick(R.id.txtViewApply)
    public void txtViewApplyClicked() {

        Utils.hideKeyboardIfOpen(this);

        coupenCode = String.valueOf(edtTextCoupen.getText());
        if (Utils.isEmptyOrNull(coupenCode)) {
            SnackNotify.showMessage("Please enter coupen code.", relLayMain);
        } else {
            callUpdateCoupenDataApi(coupenCode);
        }

    }
}
