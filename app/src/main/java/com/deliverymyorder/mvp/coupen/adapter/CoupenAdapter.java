package com.deliverymyorder.mvp.coupen.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.coupen.CoupenActivity;
import com.deliverymyorder.mvp.coupen.CoupenModel;
import com.deliverymyorder.mvp.track_order.TrackOrderFragment;
import com.deliverymyorder.mvp.track_order.TrackOrderMapActivity;
import com.deliverymyorder.mvp.track_order.adapter.OngoingOrderAdapter;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoupenAdapter extends RecyclerView.Adapter<CoupenAdapter.Holder> {


    Activity activity;
    ArrayList<CoupenModel.Datum> datum;

    public CoupenAdapter(Activity activity, ArrayList<CoupenModel.Datum> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_coupen, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final CoupenModel.Datum result = datum.get(position);

        holder.edtTextCoupen.setText(result.getCouponCode());

        holder.txtViewApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((CoupenActivity) activity).callUpdateCoupenDataApi(result.getCouponCode());

            }
        });


       /* holder.txtViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want to cancel this order ?");
                alertDialog.setCancelable(false);
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {

                        trackOrderFragment.getAllCancelreasonDataApi(result.getId());

                        dialog.dismiss();


                    }
                });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                // Showing Alert Message
                alertDialog.show();

            }
        });*/


    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.edtTextCoupen)
        EditText edtTextCoupen;

        @BindView(R.id.txtViewApply)
        TextView txtViewApply;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}