package com.deliverymyorder.mvp.coupen.presenter;


import com.deliverymyorder.mvp.coupen.CoupenModel;

import java.util.ArrayList;

public interface CoupenContractor {

    interface CoupenPresenter{

        void callCoupenApi();

        void updateCoupen(String coupenCode,int buissnesID);
        void cancelCoupen(String coupenCode,int buissnesID);

    }

    interface CoupenView{

        void onSuccessCoupenList(ArrayList<CoupenModel.Datum> arrayListCoupen);
        void onUnSuccessCoupenList(String message);
        void onInternetErrorCoupenList();

        void onSuccessUpdateCoupen();
        void onUnSuccessUpdateCoupen(String message);
        void onInternetErrorUpdateCoupen();


        void onSuccessRemoveCoupen();

    }
}
