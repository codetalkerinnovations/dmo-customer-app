package com.deliverymyorder.mvp.coupen;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CoupenModel {

        @SerializedName("error")
        @Expose
        private Integer error;
        @SerializedName("message")
        @Expose
        private String message;

    @SerializedName("reason")
    @Expose
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

         @SerializedName("data")
        @Expose
        private ArrayList<Datum> data = null;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Datum> getData() {
            return data;
        }

        public void setData(ArrayList<Datum> data) {
            this.data = data;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("coupon_code")
        @Expose
        private String couponCode;
        @SerializedName("valid_from")
        @Expose
        private String validFrom;
        @SerializedName("valid_to")
        @Expose
        private String validTo;
        @SerializedName("coupon_type")
        @Expose
        private Integer couponType;
        @SerializedName("c_type")
        @Expose
        private Integer cType;
        @SerializedName("c_value")
        @Expose
        private Integer cValue;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("business_id")
        @Expose
        private Object businessId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public String getValidFrom() {
            return validFrom;
        }

        public void setValidFrom(String validFrom) {
            this.validFrom = validFrom;
        }

        public String getValidTo() {
            return validTo;
        }

        public void setValidTo(String validTo) {
            this.validTo = validTo;
        }

        public Integer getCouponType() {
            return couponType;
        }

        public void setCouponType(Integer couponType) {
            this.couponType = couponType;
        }

        public Integer getCType() {
            return cType;
        }

        public void setCType(Integer cType) {
            this.cType = cType;
        }

        public Integer getCValue() {
            return cValue;
        }

        public void setCValue(Integer cValue) {
            this.cValue = cValue;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Object getBusinessId() {
            return businessId;
        }

        public void setBusinessId(Object businessId) {
            this.businessId = businessId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
