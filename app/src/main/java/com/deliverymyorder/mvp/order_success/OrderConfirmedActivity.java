package com.deliverymyorder.mvp.order_success;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.R;
import com.deliverymyorder.mvp.SplashActivity;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderConfirmedActivity extends AppCompatActivity {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.txtViewTrackYourOrder)
    TextView txtViewTrackYourOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmed);

        ButterKnife.bind(this);

        txtViewTrackYourOrder.setPaintFlags(txtViewTrackYourOrder.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @OnClick(R.id.txtViewTrackYourOrder)
    public void txtViewTrackYourOrderClicked() {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("ISOPEN_HOME_FRAGMENT", false);
        startActivity(intent);

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        Intent intent = new Intent(OrderConfirmedActivity.this, DashboardActivity.class);
        intent.putExtra("ISOPEN_HOME_FRAGMENT", true);
        startActivity(intent);
        finishAffinity();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(OrderConfirmedActivity.this, DashboardActivity.class);
        intent.putExtra("ISOPEN_HOME_FRAGMENT", true);
        startActivity(intent);
        finishAffinity();
    }
}
