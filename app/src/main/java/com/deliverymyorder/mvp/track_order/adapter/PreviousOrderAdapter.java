package com.deliverymyorder.mvp.track_order.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.feedback.FeedbackActivity;
import com.deliverymyorder.mvp.recipt.ReciptActivity;
import com.deliverymyorder.mvp.support.SupportActivity;
import com.deliverymyorder.mvp.track_order.TrackOrderFragment;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 26-12-2018.
 */

public class PreviousOrderAdapter extends RecyclerView.Adapter<PreviousOrderAdapter.Holder> {


    Activity activity;
//    ArrayList<OrderHistoryModel.Data.Previous> datum;

    ArrayList<Object> datum;

    String favBit;
    TrackOrderFragment trackOrderFragment;

    public PreviousOrderAdapter(Activity activity, ArrayList<Object> datum,TrackOrderFragment trackOrderFragment) {
        this.activity = activity;
        this.datum = datum;
        this.trackOrderFragment = trackOrderFragment;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_previous_order, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        Object obj = datum.get(position);

        if (obj instanceof OrderHistoryModel.Data.Previous) {

            final OrderHistoryModel.Data.Previous result = (OrderHistoryModel.Data.Previous)obj;

            if (result.getMerchantBuissness()!= null) {
                holder.txtViewMerchantName.setText(result.getMerchantBuissness().getTitle());
                holder.txtViewMerchantArea.setText(result.getMerchantBuissness().getAddress());
            }else
            {
                holder.txtViewMerchantName.setText("-NA-");
                holder.txtViewMerchantArea.setText("-NA-");
            }
            holder.txtViewOrderId.setText(result.getInvoiceNo());
            holder.txtViewDate.setText(Utils.getStringDateFromDate(activity, result.getCreateDate()));

            holder.txtViewTotal.setText("₹" + String.valueOf(result.getTotalAmount()));

            holder.recyclerViewItem.setVisibility(View.VISIBLE);


            holder.recyclerViewItem.setLayoutManager(new GridLayoutManager(activity, 1));
            PreviousOrderItemAdapter subCategoryAdapter = new PreviousOrderItemAdapter(activity, result.getOrderDetails());
            holder.recyclerViewItem.setItemAnimator(new DefaultItemAnimator());
            holder.recyclerViewItem.setAdapter(subCategoryAdapter);


            if (result.getStatus() == 11) {
                holder.txtViewReOrder.setEnabled(false);
                holder.txtViewRateOrder.setEnabled(false);
                holder.txtViewHelp.setEnabled(false);
                holder.txtViewRateOrder.setVisibility(View.GONE);
                holder.txtViewReciept.setEnabled(false);
                holder.txtViewReOrder.setText("cancelled");
                holder.txtViewReOrder.setTextColor(activity.getResources().getColor(R.color.color_red));
                holder.txtViewReOrder.setBackground(activity.getDrawable(R.drawable.drawable_round_coner_red));

            } else {
                holder.txtViewReOrder.setEnabled(true);
                holder.txtViewRateOrder.setEnabled(true);
                holder.txtViewHelp.setEnabled(true);
                holder.txtViewRateOrder.setVisibility(View.VISIBLE);
                holder.txtViewReciept.setEnabled(true);
                holder.txtViewReOrder.setText("Re-Order");
                holder.txtViewReOrder.setTextColor(activity.getResources().getColor(R.color.color_green));
                holder.txtViewReOrder.setBackground(activity.getDrawable(R.drawable.drawable_round_coner_green));

            }


            holder.txtViewRateOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, FeedbackActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_ID, result.getId());
                    intent.putExtra(ConstIntent.CONST_MERCHANT_ID, result.getMerchantId());
                    intent.putExtra(ConstIntent.CONST_BUISNESS_ID, result.getBusinessId());
                    intent.putExtra(ConstIntent.CONST_DELIVERY_PARTNER_ID, result.getDelivery_partner_id());
                    activity.startActivity(intent);

                }
            });

            holder.txtViewHelp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, SupportActivity.class);
                    activity.startActivity(intent);

                }
            });


            holder.txtViewReciept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, ReciptActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_DETAIL_MODEL, result.getOrderDetails());
                    activity.startActivity(intent);

                }
            });


            holder.txtViewReOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want to Re order this order ?");
                    alertDialog.setCancelable(false);
                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {

                            trackOrderFragment.callReOrderDataApi(result.getId(), 1);

                            dialog.dismiss();


                        }
                    });
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();


                }
            });
        }else if (obj instanceof PickAndDropOnGoingData)
        {

            final PickAndDropOnGoingData result = (PickAndDropOnGoingData) obj;

            holder.txtViewMerchantName.setText(result.getDropContactName());
            holder.txtViewMerchantArea.setText(result.getDrop_flat_number() + result.getDrop_city());
            holder.txtViewOrderId.setText(result.getInvoiceNo());
            holder.txtViewDate.setText(Utils.getStringDateFromDate(activity, result.getCreateDate()));

            holder.txtViewTotal.setText("₹" + String.valueOf(result.getTotalCost()));

            holder.recyclerViewItem.setVisibility(View.GONE);

//            holder.recyclerViewItem.setLayoutManager(new GridLayoutManager(activity, 1));
//            PreviousOrderItemAdapter subCategoryAdapter = new PreviousOrderItemAdapter(activity, result.getOrderDetails());
//            holder.recyclerViewItem.setItemAnimator(new DefaultItemAnimator());
//            holder.recyclerViewItem.setAdapter(subCategoryAdapter);


            if (result.getStatus() == 8) {
                holder.txtViewReOrder.setEnabled(false);
                holder.txtViewRateOrder.setEnabled(false);
                holder.txtViewHelp.setEnabled(false);
                holder.txtViewRateOrder.setVisibility(View.GONE);
                holder.txtViewReciept.setEnabled(false);
                holder.txtViewReOrder.setText("cancelled");
                holder.txtViewReOrder.setTextColor(activity.getResources().getColor(R.color.color_red));
                holder.txtViewReOrder.setBackground(activity.getDrawable(R.drawable.drawable_round_coner_red));

            } else {
                holder.txtViewReOrder.setEnabled(true);
                holder.txtViewRateOrder.setEnabled(true);
                holder.txtViewHelp.setEnabled(true);
                holder.txtViewRateOrder.setVisibility(View.VISIBLE);
                holder.txtViewReciept.setEnabled(true);
                holder.txtViewReOrder.setText("Re-Order");
                holder.txtViewReOrder.setTextColor(activity.getResources().getColor(R.color.color_green));
                holder.txtViewReOrder.setBackground(activity.getDrawable(R.drawable.drawable_round_coner_green));

            }

            holder.txtViewRateOrder.setVisibility(View.GONE);

//            holder.txtViewRateOrder.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = new Intent(activity, FeedbackActivity.class);
//                    intent.putExtra(ConstIntent.CONST_ORDER_ID, result.getId());
//                    intent.putExtra(ConstIntent.CONST_MERCHANT_ID, result.getMerchantId());
//                    intent.putExtra(ConstIntent.CONST_BUISNESS_ID, result.getBusinessId());
//                    intent.putExtra(ConstIntent.CONST_DELIVERY_PARTNER_ID, result.getDelivery_partner_id());
//                    activity.startActivity(intent);
//
//                }
//            });

            holder.txtViewHelp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, SupportActivity.class);
                    activity.startActivity(intent);

                }
            });

            holder.txtViewReciept.setVisibility(View.GONE);


//            holder.txtViewReciept.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = new Intent(activity, ReciptActivity.class);
//                    intent.putExtra(ConstIntent.CONST_ORDER_DETAIL_MODEL, result.getOrderDetails());
//                    activity.startActivity(intent);
//
//                }
//            });


            holder.txtViewReOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want to Re order this order ?");
                    alertDialog.setCancelable(false);
                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {

                            trackOrderFragment.callReOrderDataApi(result.getId(), 2);

                            dialog.dismiss();


                        }
                    });
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();


                }
            });


        }

    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.recyclerViewItem)
        RecyclerView recyclerViewItem;

        @BindView(R.id.txtViewMerchantName)
        TextView txtViewMerchantName;

        @BindView(R.id.txtViewReOrder)
        TextView txtViewReOrder;

        @BindView(R.id.txtViewMerchantArea)
        TextView txtViewMerchantArea;

        @BindView(R.id.txtViewOrderId)
        TextView txtViewOrderId;

        @BindView(R.id.txtViewRateOrder)
        TextView txtViewRateOrder;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;

        @BindView(R.id.txtViewReciept)
        TextView txtViewReciept;

        @BindView(R.id.txtViewHelp)
        TextView txtViewHelp;

        @BindView(R.id.txtViewTotal)
        TextView txtViewTotal;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}