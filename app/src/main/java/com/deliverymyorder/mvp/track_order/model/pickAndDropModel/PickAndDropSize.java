package com.deliverymyorder.mvp.track_order.model.pickAndDropModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickAndDropSize implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("length")
    @Expose
    private Integer length;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("charge")
    @Expose
    private Integer charge;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.length);
        dest.writeValue(this.height);
        dest.writeValue(this.width);
        dest.writeValue(this.status);
        dest.writeValue(this.charge);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public PickAndDropSize() {
    }

    protected PickAndDropSize(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.length = (Integer) in.readValue(Integer.class.getClassLoader());
        this.height = (Integer) in.readValue(Integer.class.getClassLoader());
        this.width = (Integer) in.readValue(Integer.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.charge = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt =  in.readString();
    }

    public static final Creator<PickAndDropSize> CREATOR = new Creator<PickAndDropSize>() {
        @Override
        public PickAndDropSize createFromParcel(Parcel source) {
            return new PickAndDropSize(source);
        }

        @Override
        public PickAndDropSize[] newArray(int size) {
            return new PickAndDropSize[size];
        }
    };
}