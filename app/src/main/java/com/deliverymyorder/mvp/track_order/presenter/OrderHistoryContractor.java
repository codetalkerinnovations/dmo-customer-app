package com.deliverymyorder.mvp.track_order.presenter;

import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.track_order.model.CancelOrderModel;
import com.deliverymyorder.mvp.track_order.model.CancelOrderReasonModel;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.ReOrderModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Braintech on 26-12-2018.
 */

public class OrderHistoryContractor {

    public interface OrderHistoryPresenter {
        void getOrderHistoryData();

        void getCancelOrderReasonsData();

        void reOrderData(int orderId);

        void updateCancelOrderReasonsData(int orderID, int cancelReasonId,int orderType);

    }

    public interface OrderHistoryView {
        void getOrderHistoryDataSuccess(OrderHistoryModel.Data orderHistoryModel);

        void getOrderHistoryDataUnSucess(String message);

        void OrderHistoryDataInternetError();

        void getCancelOrderReasonsSuccess(ArrayList<CancelOrderReasonModel.Datum> arrayListCancelOrderReasonModel);

        void getCancelOrderReasonsUnSucess(String message);

        void cancelOrderReasonsInternetError();

        void updateCancelOrderReasonsSuccess(CancelOrderModel.Data cancelOrderModel);

        void updateCancelOrderReasonsUnSucess(String message);

        void updateCancelOrderReasonsInternetError();


        void reOrderApiSuccess(ReOrderModel reOrderModel);

        void reOrderApiUnSucess(String message);

        void reOrderApiInternetError();
    }
}
