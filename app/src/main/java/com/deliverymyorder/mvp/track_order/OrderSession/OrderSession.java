package com.deliverymyorder.mvp.track_order.OrderSession;

import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

public class OrderSession {


    private Object currentOrder;

    private  OrderType orderType = OrderType.SIMPLE;

    private   static  OrderSession shared;

    private  static  int orderStatus = -1;

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public  int getOrderStatus() {
        return orderStatus;
    }

    public  void setOrderStatus(int orderStatus) {
        OrderSession.orderStatus = orderStatus;
    }

    private OrderSession()
    {
        super();
    }

    public  static  OrderSession getSharedInstance()
    {

        if (shared == null)
        {
            shared = new OrderSession();
        }
        return  shared;
    }


    public Object getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Object currentOrder) {

            if (currentOrder instanceof OrderHistoryModel.Data.Ongoing) {
                 this.orderType = OrderType.SIMPLE;
            }else if (currentOrder instanceof PickAndDropOnGoingData)
            {
                this.orderType = OrderType.PICK_AND_DROP;
            }

                this.currentOrder = currentOrder;
    }


    public enum OrderType { SIMPLE, PICK_AND_DROP }


}
