package com.deliverymyorder.mvp.track_order;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.deliverymyorder.Common.backgroundservices.BroadcastService;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.adapter.PickDropTrackOrderAdapter;
import com.deliverymyorder.mvp.track_order.adapter.TrackOrderAdapter;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;
import com.deliverymyorder.mvp.track_order.presenter.DirectionsJSONParser;
import com.deliverymyorder.mvp.track_order.presenter.TrackOrderContractor;
import com.deliverymyorder.mvp.track_order.presenter.TrackOrderDetailPresenterImpl;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.BroadcastReceiver;
import android.widget.TextView;
import android.widget.Toast;

public class TrackOrderMapActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, TrackOrderContractor.TrackOrderView {

/*
    @BindView(R.id.edtTextLatLong)
    EditText edtTextLatLong;

    @BindView(R.id.edtTextFlatNo)
    EditText edtTextFlatNo;

    @BindView(R.id.edtTextLandmark)
    EditText edtTextLandmark;*/


    @BindView(R.id.relLayMainTrackOrderMapActivity)
    RelativeLayout relLayMainTrackOrderMapActivity;


    @BindView(R.id.recyclerViewBottotmTrackOrder)
    RecyclerView recyclerViewBottotmTrackOrder;

    TrackOrderDetailModel.Data trackOrderDetailModel;


    TrackOrderAdapter trackOrderAdapter;
    PickDropTrackOrderAdapter pickDropTrackOrderAdapter;

    int orderType = 0;
    int orderStatus = -1;

    private GoogleMap mMap;

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private final static int PERMISSION_CODE__LOCATION = 2910;

    private LocationManager locationManager;

//    double latitude = 0.0;
//    double longitude = 0.0;
//
//    double i = 0.0;
//    double j = 0.0;

    boolean isFirstTime = true;
    //Marker markerName;

    boolean isAskedPermission = false;

    boolean isMarkerRotating = false;

//    String currentLatitude;
//    String currentLongitude;


    LatLng currentPoint;
    LatLng pickUpPoint;
    LatLng dropPoint;

    Handler handler;


    Marker markerPickupLocation;
    Marker markerDropLocation;
    Marker markerDeliveryBoy;

    Polyline deliveryBoyPath;


    TrackOrderDetailPresenterImpl trackOrderDetailPresenterImpl;

    int orderId;


    // --------------Variable for Background services

    private static final String TAG = "BroadcastTest";
    private Intent intent;

    private String strDeliveryBoyNumber= "";


    @BindView(R.id.btnMakeCall)
    Button btnMakeCall;

    @BindView(R.id.tvDeliveryPartnerName)
    TextView tvDeliveryPartnerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order_map);

        ButterKnife.bind(this);

        trackOrderDetailPresenterImpl = new TrackOrderDetailPresenterImpl(this, this);


        if (getIntent() != null) {
            orderId = getIntent().getIntExtra(ConstIntent.CONST_ORDER_ID, 0);
        }


        Object obj = OrderSession.getSharedInstance().getCurrentOrder();


        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            OrderHistoryModel.Data.Ongoing data = (OrderHistoryModel.Data.Ongoing) obj;

            if (data.getDeliveryBoy()!=null)
            {

                updateDeliveryBoyNumber(" " +data.getDeliveryBoy().getFirstname() +" "+data.getDeliveryBoy().getLastname());

                strDeliveryBoyNumber = data.getDeliveryBoy().getMobile();
            }else
            {
                btnMakeCall.setVisibility(View.GONE);
            }

        } else if (obj instanceof PickAndDropOnGoingData) {

            PickAndDropOnGoingData data = (PickAndDropOnGoingData) obj;

            if (data.getDeliveryBoy()!=null)
            {
                updateDeliveryBoyNumber(" " +data.getDeliveryBoy().getFirstname() +" "+data.getDeliveryBoy().getLastname());
                strDeliveryBoyNumber = data.getDeliveryBoy().getMobile();
            }else
            {
                btnMakeCall.setVisibility(View.GONE);
            }

        }else
        {
            btnMakeCall.setVisibility(View.GONE);
        }


//update orderid in share preference
        PrefUtil.putInt(this, Const.PARAM_ORDER_ID, orderId);


//----------Code  By Pramod------------>
        intent = new Intent(this, BroadcastService.class);

    }

    private  void updateDeliveryBoyNumber(String strDeliveryBoyName)
    {

        if (strDeliveryBoyName!=null)
        {
            tvDeliveryPartnerName.setText(strDeliveryBoyName);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();


        trackOrderDetailPresenterImpl.callTrackOrderApi(String.valueOf(orderId));


        // Code By Pramod Sinha---------->
        startService(intent);
        registerReceiver(broadcastReceiver, new IntentFilter(BroadcastService.BROADCAST_ACTION));

    }


    private void initializeMap() {

        // load map
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        // getCurrentLocation();

        mMap = googleMap;
        addOrUpdateLocationMarker();
/*
        if (markerName != null)
            markerName.remove();
        // mMap.clear();

        // Add a marker in Sydney and move the camera
        // LatLng TutorialsPoint = new LatLng(28.7041, 77.1025);


        LatLng sourcePoint = new LatLng(28.7041, 77.1025);
        LatLng destinationPoint = new LatLng(26.8467, 80.9462);


        MarkerOptions markerOptMyLoc = new MarkerOptions().position(sourcePoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_pickup_pin_maps)).title("Pick up Location");
        MarkerOptions markerOptMyLoc1 = new MarkerOptions().position(destinationPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_drop_pin_maps)).title("Drop Location");
        MarkerOptions markerOptMyLoc2 = new MarkerOptions().position(currentPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_motor_cycle)).title("Delivery boy");




        mMap.addMarker(markerOptMyLoc);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPoint));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));


        mMap.addMarker(markerOptMyLoc1);

        markerName = mMap.addMarker(markerOptMyLoc2);


        //markerName.remove();
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(destinationPoint));

        if (isFirstTime) {

            String url = getDirectionsUrl(sourcePoint, destinationPoint);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);

            isFirstTime = false;
        }

        */

    }

    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            isAskedPermission = false;

            alertEnableLocation(getString(R.string.title_gps), true);

            // return;
        } else if (!checkIsPermissionGranted()) {
            askLocationPermission();
        } else {
            getLocation();
        }
    }


    public void alertEnableLocation(String msg, final boolean isEnableLocation) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TrackOrderMapActivity.this,R.style.AlertDialogTheme);
        builder.setMessage(msg)
                .setCancelable(false)

                .setNegativeButton(getString(R.string.title_exit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })

                .setPositiveButton(getString(R.string.title_go_to_setting), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {

                        if (isEnableLocation) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        } else {
                            startActivity(new Intent(Settings.ACTION_APPLICATION_SETTINGS));
                        }

                        isAskedPermission = true;

                        dialog.dismiss();
                    }
                });
                /*.setNegativeButton(context.getString(R.string.title_no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        System.exit(0);
                    }
                });*/
        final AlertDialog alert = builder.create();
        alert.show();
    }


    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_CODE__LOCATION);
        }
    }


    private void getLocation() {
        // Progress.start(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling


            return;
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
    }

    private boolean checkIsPermissionGranted() {

        int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if ((permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        //Progress.stop();
        /*
        latitude = location.getLatitude();
        longitude = location.getLongitude();


        mMap.clear();


        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        Log.e("latitude", "latitude--" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);


            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //edtTextLatLong.setText("" + latitude + " , " + longitude);
                //edtTextFlatNo.setText(knownName);
                //edtTextLandmark.setText(address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
*/
        //  nestedScroll.fullScroll(View.FOCUS_DOWN);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        // requestCode (2908 for pick image from gallary), (2909 for camera) and 2910 is for selectedItemLocation permission

        switch (requestCode) {

            case PERMISSION_CODE__LOCATION: { // selectedItemLocation enable

                int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

                int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

                if ((permissionCheckLoc == 0) && (permissionCoarseLoc == 0)) {
                    getCurrentLocation();
                } else {
                    //SnackNotify.showMessage("Please provide security permission from aap setting.", coordinateLayout);
                    isAskedPermission = false;
                    alertEnableLocation(getString(R.string.title_app_setting), false);
                }
                return;
            }

        }
    }

    @Override
    public void onSuccessTrackOrderDetail(TrackOrderDetailModel.Data trackOrderDetailModel) {
        orderStatus =  trackOrderDetailModel.getOrderStatus();
        OrderSession.getSharedInstance().setOrderStatus(trackOrderDetailModel.getOrderStatus());
        this.trackOrderDetailModel = trackOrderDetailModel;
        initializeMap();
        manageRecyclerView();
    }

    @Override
    public void onUnSuccessTrackOrderDetail(String message) {
        SnackNotify.showMessage(message, relLayMainTrackOrderMapActivity);
    }

    @Override
    public void onInternetErrorTrackOrderDetail() {

    }

    private void manageRecyclerView() {


        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

         orderType = 0;
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType = 1;
        } else if (obj instanceof PickAndDropOnGoingData) {
            orderType = 2;
        }

        if (orderType == 1) {

            trackOrderAdapter = new TrackOrderAdapter(this, trackOrderDetailModel.getStatusData(), orderId);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recyclerViewBottotmTrackOrder.setLayoutManager(mLayoutManager);
            recyclerViewBottotmTrackOrder.setItemAnimator(new DefaultItemAnimator());
            recyclerViewBottotmTrackOrder.setAdapter(trackOrderAdapter);
        } else {
             pickDropTrackOrderAdapter = new PickDropTrackOrderAdapter(this, trackOrderDetailModel.getStatusData(), orderId);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recyclerViewBottotmTrackOrder.setLayoutManager(mLayoutManager);
            recyclerViewBottotmTrackOrder.setItemAnimator(new DefaultItemAnimator());
            recyclerViewBottotmTrackOrder.setAdapter(pickDropTrackOrderAdapter);

        }
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {

        if (handler != null) {
            handler.removeMessages(0);
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (handler != null) {
            handler.removeMessages(0);
        }

    }

    /*--Code to Draw Route path on gogle map-------------*/


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyD6qT8r_rVEL4Ls20tRayJV6Fi8Tv0FhEI";

        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {


            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            if (result == null) {
                return;
            }


            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLACK);
            }

            // Drawing polyline in the Google Map for the i-th route
            if (points != null && points.size() != 0) {

                if (deliveryBoyPath != null) {
                    deliveryBoyPath.remove();
                }

                //  rotate bike .


                if (points.size() >= 2) {
                    LatLng startPoint = points.get(0);
                    LatLng nextPoint = points.get(1);

                    double bearing = bearingBetweenLocations(startPoint, nextPoint);
                    rotateMarker(markerDeliveryBoy, (float) bearing);
                }


                deliveryBoyPath = mMap.addPolyline(lineOptions);
            }


            // String totaldistance=CalculationByDistance(sourcePoint,destinationPoint);

            //Log.d("Distance ",totaldistance);


        }
    }


    //---------------Code to Recive data From Background service--------------

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUI(intent);
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        stopService(intent);
    }

    private void updateUI(Intent intent) {
        String currentLatitude = intent.getStringExtra(ConstIntent.CONST_LATITUDE);
        String currentLongitude = intent.getStringExtra(ConstIntent.CONST_LONGITUDE);
        Log.d(TAG, currentLatitude);
        Log.d(TAG, currentLongitude);


        System.out.println("Counter-------->" + currentLatitude);
        System.out.println("Time-------->" + currentLongitude);


        if ((currentLatitude != null && currentLongitude != null) &&
                (!currentLongitude.equalsIgnoreCase("0") && !currentLatitude.equalsIgnoreCase("0"))) {

            try {

                // LatLng oldLatLang = currentPoint;

                Double lat = Double.parseDouble(currentLatitude);
                Double lang = Double.parseDouble(currentLongitude);
                currentPoint = new LatLng(lat, lang);

                // rotate marker
//                double rotation = bearingBetweenLocations(oldLatLang,currentPoint);
//                rotateMarker(markerDeliveryBoy, (float) rotation);


                addOrUpdateLocationMarker();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        if (orderStatus != OrderSession.getSharedInstance().getOrderStatus())
        {
            trackOrderDetailPresenterImpl.callTrackOrderApi(String.valueOf(orderId));
        }

        /*
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

        if (orderType == 1)
        {
           trackOrderAdapter.notifyDataSetChanged();
        }else if (orderType == 2)
        {
           pickDropTrackOrderAdapter.notifyDataSetChanged();
        }

           }
        });
        */
    }


    public void addOrUpdateLocationMarker() {

        if (mMap != null && trackOrderDetailModel != null) {


            if (markerPickupLocation == null || markerDropLocation == null) {

                try {
                    pickUpPoint = new LatLng(Double.parseDouble(trackOrderDetailModel.getPickup().getLatitude()), Double.parseDouble(trackOrderDetailModel.getPickup().getLongitude()));
                    dropPoint = new LatLng(trackOrderDetailModel.getDrop().getLatitude(), trackOrderDetailModel.getDrop().getLongitude());



                    Object obj = OrderSession.getSharedInstance().getCurrentOrder();

                    String pickUp = "Pick up location";
                    String pickUpSnipit  = "";

                    String dropLocation = "Drop location";
                    String dropSnipit = "";

                    if (obj instanceof OrderHistoryModel.Data.Ongoing) {
                        OrderHistoryModel.Data.Ongoing data = (OrderHistoryModel.Data.Ongoing) obj;
                        pickUp = data.getMerchantBuissness().getTitle();
                        pickUpSnipit = data.getMerchantBuissness().getAddress();

                        dropLocation = data.getShippingAddress().getName();
                        dropSnipit = data.getShippingAddress().getStreet();

                    } else if (obj instanceof PickAndDropOnGoingData) {

                        PickAndDropOnGoingData data = (PickAndDropOnGoingData) obj;
                        pickUp = data.getPickupContactName();
                        pickUpSnipit = data.getPickupContactNo();

                        dropLocation = data.getDropContactName();
                        dropSnipit = data.getDropContactNo();
                    }


                        MarkerOptions markerOptMyLoc = new MarkerOptions().position(pickUpPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_pickup_pin_maps)).title(pickUp).snippet(pickUpSnipit);
                    MarkerOptions markerOptMyLoc1 = new MarkerOptions().position(dropPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_drop_pin_maps)).title(dropLocation).snippet(dropSnipit);


                    markerPickupLocation = mMap.addMarker(markerOptMyLoc);
                    markerDropLocation = mMap.addMarker(markerOptMyLoc1);

//                  mMap.moveCamera(CameraUpdateFactory.newLatLng(dropPoint));
//                  mMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f));


                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dropPoint, 15.0f));


                    // Pickup and drop location Path
                    String url = getDirectionsUrl(pickUpPoint, dropPoint);
                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            if (markerDeliveryBoy == null && currentPoint != null) {
                MarkerOptions markerOptMyLoc2 = new MarkerOptions().position(currentPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_dlp_bike)).title("Delivery boy");
                markerDeliveryBoy = mMap.addMarker(markerOptMyLoc2);
            }


            if (currentPoint != null) {

                markerDeliveryBoy.setPosition(currentPoint);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPoint, 17.0f));

                try {

                    if (trackOrderDetailModel.getStatusData() != null && trackOrderDetailModel.getStatusData().size() >= 3) {

                        TrackOrderDetailModel.Data.StatusDatum status = trackOrderDetailModel.getStatusData().get(3);

                        if (status.getStatus() == 1) {
                            // draw from delivery boy to drop location
                            String url = getDirectionsUrl(currentPoint, dropPoint);
                            DownloadTask downloadTask = new DownloadTask();
                            downloadTask.execute(url);

                        } else {
                            // draw from delivery boy to DmoShop path
                            String url = getDirectionsUrl(currentPoint, pickUpPoint);
                            DownloadTask downloadTask = new DownloadTask();
                            downloadTask.execute(url);

                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            // }

            //markerName.remove();
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(destinationPoint));

            // if (isFirstTime) {


            //   isFirstTime = false;
            // }

        }
    }


    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }


    private void rotateMarker(final Marker marker, final float toRotation) {

        if (marker == null) {
            return;
        }

        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }


    //------------------  DMO Call Method

    private void callToNumber(String phone) {

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));

        if (ContextCompat.checkSelfPermission(TrackOrderMapActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(TrackOrderMapActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 1234);
        } else {
            startActivity(callIntent);
        }

//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        startActivity(callIntent);
    }

    @OnClick(R.id.btnMakeCall)
    public void btnMakeCall(View view) {
        callToNumber(strDeliveryBoyNumber);
//        Object obj = OrderSession.getSharedInstance().getCurrentOrder();
//
//
//        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
//            OrderHistoryModel.Data.Ongoing data = (OrderHistoryModel.Data.Ongoing) obj;
//
//            if (data.getDeliveryBoy()!=null)
//            {
//                callToNumber(strDeliveryBoyNumber);
//                //updateDeliveryBoyNumber(" " +data.getDeliveryBoy().getFirstname() +" "+data.getDeliveryBoy().getLastname());
//            }
//
//        } else if (obj instanceof PickAndDropOnGoingData) {
//
//            PickAndDropOnGoingData data = (PickAndDropOnGoingData) obj;
//
//            if (data.getDeliveryBoy()!=null)
//            {
//                callToNumber(strDeliveryBoyNumber);// data.getDeliveryBoy().getMobile());
//                //updateDeliveryBoyNumber(" " +data.getDeliveryBoy().getFirstname() +" "+data.getDeliveryBoy().getLastname());
//            }
//
//        }
    }

    @OnClick(R.id.btnDMOCall)
    public void btnDMOCall(View view) {
        openCustomerWhatsapp(getResources().getString(R.string.support_phone_number));
    }

    @OnClick(R.id.btnDeliveryPartner)
    public void btnDeliveryPartner(View view) {
        if (strDeliveryBoyNumber!=null && strDeliveryBoyNumber.length() !=0) {
            openCustomerWhatsapp(strDeliveryBoyNumber);
        }
    }


    public void emailSupport() {

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        String[] recipients = {getResources().getString(R.string.support_email)};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
        emailIntent.setType("message/rfc822");
        startActivity(emailIntent);
    }


    public void openCustomerWhatsapp(String phone) {

        if (phone == null && phone.length() == 0)
        {
            return;
        }

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        String invoiceID = "";
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            invoiceID = ((OrderHistoryModel.Data.Ongoing) obj).getInvoiceNo();
        } else if (obj instanceof PickAndDropOnGoingData) {
            invoiceID =  ((PickAndDropOnGoingData) obj).getInvoiceNo();  //"DMO" + ((PickAndDropOnGoingData) obj).getId();
        }


        PackageManager packageManager = getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);

        phone = "91" + phone;

        String message = "*DMO Customer* \n------------\n *OrderID* : #" + invoiceID + "\n\n";

        try {
            String url = "https://api.whatsapp.com/send?phone=" + phone + "&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i);
            }
            else{
                Toast.makeText(this, "No WhatsApp in this device.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}


//package com.deliverymyorder.mvp.track_order;
//
//import android.Manifest;
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.location.Address;
//import android.location.Geocoder;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.AsyncTask;
//import android.os.Build;
//import android.os.Handler;
//import android.os.SystemClock;
//import android.provider.Settings;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.animation.BounceInterpolator;
//import android.view.animation.Interpolator;
//import android.widget.EditText;
//import android.widget.RelativeLayout;
//
//import com.deliverymyorder.Common.backgroundservices.BroadcastService;
//import com.deliverymyorder.Common.requestresponse.ConstIntent;
//import com.deliverymyorder.Common.utility.SnackNotify;
//import com.deliverymyorder.R;
//import com.deliverymyorder.mvp.track_order.adapter.TrackOrderAdapter;
//import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;
//import com.deliverymyorder.mvp.track_order.presenter.DirectionsJSONParser;
//import com.deliverymyorder.mvp.track_order.presenter.TrackOrderContractor;
//import com.deliverymyorder.mvp.track_order.presenter.TrackOrderDetailPresenterImpl;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapView;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.PolylineOptions;
//
//import org.json.JSONObject;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//import android.content.BroadcastReceiver;
//
//public class TrackOrderMapActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, TrackOrderContractor.TrackOrderView {
//
///*
//    @BindView(R.id.edtTextLatLong)
//    EditText edtTextLatLong;
//
//    @BindView(R.id.edtTextFlatNo)
//    EditText edtTextFlatNo;
//
//    @BindView(R.id.edtTextLandmark)
//    EditText edtTextLandmark;*/
//
//
//    @BindView(R.id.relLayMainTrackOrderMapActivity)
//    RelativeLayout relLayMainTrackOrderMapActivity;
//
//
//    @BindView(R.id.recyclerViewBottotmTrackOrder)
//    RecyclerView recyclerViewBottotmTrackOrder;
//
//    TrackOrderDetailModel.Data trackOrderDetailModel;
//
//
//    private GoogleMap mMap;
//
//    private final static int ALL_PERMISSIONS_RESULT = 101;
//    private final static int PERMISSION_CODE__LOCATION = 2910;
//
//    private LocationManager locationManager;
//
////    double latitude = 0.0;
////    double longitude = 0.0;
//
//    double i = 0.0;
//    double j = 0.0;
//
//    boolean isFirstTime = true;
//    Marker markerName;
//
//    boolean isAskedPermission = false;
//
//    String currentLatitude;
//    String currentLongitude;
//
//
//    LatLng currentPoint;
//    Handler handler;
//
//
//    TrackOrderDetailPresenterImpl trackOrderDetailPresenterImpl;
//
//    int orderId;
//
//
//    // --------------Variable for Background services
//
//    private static final String TAG = "BroadcastTest";
//    private Intent intent;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_track_order_map);
//
//        ButterKnife.bind(this);
//
//        trackOrderDetailPresenterImpl = new TrackOrderDetailPresenterImpl(this, this);
//
//
//        if (getIntent() != null) {
//            orderId = getIntent().getIntExtra(ConstIntent.CONST_ORDER_ID, 0);
//        }
//
//        trackOrderDetailPresenterImpl.callTrackOrderApi(String.valueOf(orderId));
//
//
////----------Code  By Pramod------------>
//        intent = new Intent(this, BroadcastService.class);
//
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//
//        trackOrderDetailPresenterImpl.callTrackOrderApi(String.valueOf(orderId));
//
//
//// Code By Pramod Sinha---------->
//        startService(intent);
//        registerReceiver(broadcastReceiver, new IntentFilter(BroadcastService.BROADCAST_ACTION));
//
//    }
//
//
//    private void initializeMap() {
//
//        // load map
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.mapView);
//        mapFragment.getMapAsync(this);
//    }
//
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        // getCurrentLocation();
//
//        mMap = googleMap;
//
//
//        if (markerName != null)
//            markerName.remove();
//        // mMap.clear();
//
//        // Add a marker in Sydney and move the camera
//        // LatLng TutorialsPoint = new LatLng(28.7041, 77.1025);
//
//
//        LatLng sourcePoint = new LatLng(28.7041, 77.1025);
//        LatLng destinationPoint = new LatLng(26.8467, 80.9462);
//
//
//        MarkerOptions markerOptMyLoc = new MarkerOptions().position(sourcePoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_pickup_pin_maps)).title("Pick up Location");
//        MarkerOptions markerOptMyLoc1 = new MarkerOptions().position(destinationPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_drop_pin_maps)).title("Drop Location");
//        MarkerOptions markerOptMyLoc2 = new MarkerOptions().position(currentPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_motor_cycle)).title("Current Location");
//        mMap.addMarker(markerOptMyLoc);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPoint));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
//
//
//        mMap.addMarker(markerOptMyLoc1);
//
//        markerName = mMap.addMarker(markerOptMyLoc2);
//
//
//        //markerName.remove();
//        //mMap.moveCamera(CameraUpdateFactory.newLatLng(destinationPoint));
//
//        if (isFirstTime) {
//
//            String url = getDirectionsUrl(sourcePoint, destinationPoint);
//            DownloadTask downloadTask = new DownloadTask();
//            downloadTask.execute(url);
//
//            isFirstTime = false;
//        }
//
//    }
//
//    @SuppressLint("MissingPermission")
//    private void getCurrentLocation() {
//
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//
//            isAskedPermission = false;
//
//            alertEnableLocation(getString(R.string.title_gps), true);
//
//            // return;
//        } else if (!checkIsPermissionGranted()) {
//            askLocationPermission();
//        } else {
//            getLocation();
//        }
//    }
//
//
//    public void alertEnableLocation(String msg, final boolean isEnableLocation) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(TrackOrderMapActivity.this);
//        builder.setMessage(msg)
//                .setCancelable(false)
//
//                .setNegativeButton(getString(R.string.title_exit), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        finishAffinity();
//                    }
//                })
//
//                .setPositiveButton(getString(R.string.title_go_to_setting), new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, final int id) {
//
//                        if (isEnableLocation) {
//                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                        } else {
//                            startActivity(new Intent(Settings.ACTION_APPLICATION_SETTINGS));
//                        }
//
//                        isAskedPermission = true;
//
//                        dialog.dismiss();
//                    }
//                });
//                /*.setNegativeButton(context.getString(R.string.title_no), new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, final int id) {
//                        System.exit(0);
//                    }
//                });*/
//        final AlertDialog alert = builder.create();
//        alert.show();
//    }
//
//
//    private void askLocationPermission() {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
//                    Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_CODE__LOCATION);
//        }
//    }
//
//
//    private void getLocation() {
//        // Progress.start(this);
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//
//
//            return;
//        }
//
//        if (networkLocationEnabled || gpsLocationEnabled) {
//
//            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
//                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 2000, this);
//                locationManager
//                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 2000, this);
//                locationManager
//                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            }
//        }
//    }
//
//    private boolean checkIsPermissionGranted() {
//
//        int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION);
//
//        int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_COARSE_LOCATION);
//
//        if ((permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED)) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//
//    @Override
//    public void onLocationChanged(Location location) {
//        //Progress.stop();
//
//        /*
//        latitude = location.getLatitude();
//        longitude = location.getLongitude();
//
//
//        mMap.clear();
//
//
//        Geocoder geocoder;
//        List<Address> addresses;
//        geocoder = new Geocoder(this, Locale.getDefault());
//
//        latitude = location.getLatitude();
//        longitude = location.getLongitude();
//
//        Log.e("latitude", "latitude--" + latitude);
//
//        try {
//            Log.e("latitude", "inside latitude--" + latitude);
//            addresses = geocoder.getFromLocation(latitude, longitude, 1);
//
//
//            if (addresses != null && addresses.size() > 0) {
//                String address = addresses.get(0).getAddressLine(0);
//                String city = addresses.get(0).getLocality();
//                String state = addresses.get(0).getAdminArea();
//                String country = addresses.get(0).getCountryName();
//                String postalCode = addresses.get(0).getPostalCode();
//                String knownName = addresses.get(0).getFeatureName();
//
//                //edtTextLatLong.setText("" + latitude + " , " + longitude);
//                //edtTextFlatNo.setText(knownName);
//                //edtTextLandmark.setText(address);
//            }
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//*/
//        //  nestedScroll.fullScroll(View.FOCUS_DOWN);
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//
//        // requestCode (2908 for pick image from gallary), (2909 for camera) and 2910 is for selectedItemLocation permission
//
//        switch (requestCode) {
//
//            case PERMISSION_CODE__LOCATION: { // selectedItemLocation enable
//
//                int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.ACCESS_FINE_LOCATION);
//
//                int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.ACCESS_COARSE_LOCATION);
//
//                if ((permissionCheckLoc == 0) && (permissionCoarseLoc == 0)) {
//                    getCurrentLocation();
//                } else {
//                    //SnackNotify.showMessage("Please provide security permission from aap setting.", coordinateLayout);
//                    isAskedPermission = false;
//                    alertEnableLocation(getString(R.string.title_app_setting), false);
//                }
//                return;
//            }
//
//        }
//    }
//
//    @Override
//    public void onSuccessTrackOrderDetail(TrackOrderDetailModel.Data trackOrderDetailModel) {
//        this.trackOrderDetailModel = trackOrderDetailModel;
//
//
//        currentPoint = new LatLng(28.7041 + i, 77.1025 + j);
//
//        i = i + .9;
//        j = j + .9;
//
//
//        markerName.setPosition(currentPoint);
//
//        //initializeMap();
//        manageRecyclerView();
//    }
//
//    @Override
//    public void onUnSuccessTrackOrderDetail(String message) {
//        SnackNotify.showMessage(message, relLayMainTrackOrderMapActivity);
//    }
//
//    @Override
//    public void onInternetErrorTrackOrderDetail() {
//
//    }
//
//    private void manageRecyclerView() {
//
//        TrackOrderAdapter trackOrderAdapter = new TrackOrderAdapter(this, trackOrderDetailModel.getStatusData(), orderId);
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
//        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        recyclerViewBottotmTrackOrder.setLayoutManager(mLayoutManager);
//        recyclerViewBottotmTrackOrder.setItemAnimator(new DefaultItemAnimator());
//        recyclerViewBottotmTrackOrder.setAdapter(trackOrderAdapter);
//    }
//
//    @OnClick(R.id.imgViewBack)
//    public void imgViewBack() {
//
//        if (handler != null) {
//            handler.removeMessages(0);
//        }
//
//        finish();
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
//        if (handler != null) {
//            handler.removeMessages(0);
//        }
//
//    }
//
//    /*--Code to Draw Route path on gogle map-------------*/
//
//
//    private String getDirectionsUrl(LatLng origin, LatLng dest) {
//
//        // Origin of route
//        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
//
//        // Destination of route
//        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
//
//        // Sensor enabled
//        String sensor = "sensor=false";
//
//        // Building the parameters to the web service
//        String parameters = str_origin + "&" + str_dest + "&" + sensor;
//
//        // Output format
//        String output = "json";
//
//        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyD6qT8r_rVEL4Ls20tRayJV6Fi8Tv0FhEI";
//
//        return url;
//    }
//
//
//    private String downloadUrl(String strUrl) throws IOException {
//        String data = "";
//        InputStream iStream = null;
//        HttpURLConnection urlConnection = null;
//        try {
//            URL url = new URL(strUrl);
//
//            urlConnection = (HttpURLConnection) url.openConnection();
//
//            // Connecting to url
//            urlConnection.connect();
//
//            // Reading data from url
//            iStream = urlConnection.getInputStream();
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
//
//            StringBuffer sb = new StringBuffer();
//
//            String line = "";
//            while ((line = br.readLine()) != null) {
//                sb.append(line);
//            }
//
//            data = sb.toString();
//
//            br.close();
//
//        } catch (Exception e) {
//            Log.d("downloading url", e.toString());
//        } finally {
//            iStream.close();
//            urlConnection.disconnect();
//        }
//        return data;
//    }
//
//
//    // Fetches data from url passed
//    private class DownloadTask extends AsyncTask<String, Void, String> {
//
//        // Downloading data in non-ui thread
//        @Override
//        protected String doInBackground(String... url) {
//
//            // For storing data from web service
//            String data = "";
//
//            try {
//                // Fetching the data from web service
//                data = downloadUrl(url[0]);
//            } catch (Exception e) {
//                Log.d("Background Task", e.toString());
//            }
//            return data;
//        }
//
//        // Executes in UI thread, after the execution of
//        // doInBackground()
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//
//            ParserTask parserTask = new ParserTask();
//
//            // Invokes the thread for parsing the JSON data
//            parserTask.execute(result);
//        }
//    }
//
//
//    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
//
//        @Override
//        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
//
//            JSONObject jObject;
//            List<List<HashMap<String, String>>> routes = null;
//
//            try {
//                jObject = new JSONObject(jsonData[0]);
//                DirectionsJSONParser parser = new DirectionsJSONParser();
//
//                // Starts parsing data
//                routes = parser.parse(jObject);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return routes;
//        }
//
//        @Override
//        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//            ArrayList<LatLng> points = null;
//            PolylineOptions lineOptions = null;
//            MarkerOptions markerOptions = new MarkerOptions();
//
//            // Traversing through all the routes
//            for (int i = 0; i < result.size(); i++) {
//                points = new ArrayList<LatLng>();
//                lineOptions = new PolylineOptions();
//
//                // Fetching i-th route
//                List<HashMap<String, String>> path = result.get(i);
//
//                // Fetching all the points in i-th route
//                for (int j = 0; j < path.size(); j++) {
//                    HashMap<String, String> point = path.get(j);
//
//                    double lat = Double.parseDouble(point.get("lat"));
//                    double lng = Double.parseDouble(point.get("lng"));
//                    LatLng position = new LatLng(lat, lng);
//
//                    points.add(position);
//                }
//
//                // Adding all the points in the route to LineOptions
//                lineOptions.addAll(points);
//                lineOptions.width(10);
//                lineOptions.color(Color.RED);
//            }
//
//            // Drawing polyline in the Google Map for the i-th route
//            if (points.size() != 0) mMap.addPolyline(lineOptions);
//
//
//            // String totaldistance=CalculationByDistance(sourcePoint,destinationPoint);
//
//            //Log.d("Distance ",totaldistance);
//
//
//        }
//    }
//
//
//    //---------------Code to Recive data From Background service--------------
//
//    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            updateUI(intent);
//        }
//    };
//
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        unregisterReceiver(broadcastReceiver);
//        stopService(intent);
//    }
//
//    private void updateUI(Intent intent) {
//        currentLatitude = intent.getStringExtra(ConstIntent.CONST_LATITUDE);
//        currentLongitude = intent.getStringExtra(ConstIntent.CONST_LONGITUDE);
//        Log.d(TAG, currentLatitude);
//        Log.d(TAG, currentLongitude);
//
//
//        System.out.println("Counter-------->" + currentLatitude);
//        System.out.println("Time-------->" + currentLongitude);
//
//        currentPoint = new LatLng(28.7041 + i, 77.1025 + j);
//
//        i = i + .5;
//        j = j + .5;
//
//
//        initializeMap();
//    }
//}






