package com.deliverymyorder.mvp.track_order.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.feedback.FeedbackActivity;
import com.deliverymyorder.mvp.recipt.ReciptActivity;
import com.deliverymyorder.mvp.support.SupportActivity;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviousOrderItemAdapter extends RecyclerView.Adapter<PreviousOrderItemAdapter.Holder> {


    Activity activity;
    ArrayList<OrderHistoryModel.Data.Previous.OrderDetail> datum;
    String favBit;

    public PreviousOrderItemAdapter(Activity activity, ArrayList<OrderHistoryModel.Data.Previous.OrderDetail> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_oreviousorder_subitem, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final OrderHistoryModel.Data.Previous.OrderDetail result = datum.get(position);

if (result.getProduct()!=null) {
    holder.txtViewItemName.setText(String.valueOf(result.getProduct().getName()));
}else
{
    holder.txtViewItemName.setText(String.valueOf(result.getOrderId()));
}
        holder.txtViewQty.setText(String.valueOf(result.getQty()));
        holder.txtViewPrice.setText(  "₹"+String.valueOf(result.getProductPrice()));



    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        LinearLayout relLayItem;

        @BindView(R.id.txtViewItemName)
        TextView txtViewItemName;

        @BindView(R.id.txtViewQty)
        TextView txtViewQty;

        @BindView(R.id.txtViewPrice)
        TextView txtViewPrice;




        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}