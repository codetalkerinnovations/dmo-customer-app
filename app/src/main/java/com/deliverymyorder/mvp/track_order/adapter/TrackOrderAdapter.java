package com.deliverymyorder.mvp.track_order.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.order_summary.OrderSummaryActivity;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrackOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Activity activity;
    ArrayList<TrackOrderDetailModel.Data.StatusDatum> datum;
    String favBit;
    int orderId;

    int orderStatus = -1;

    public TrackOrderAdapter(Activity activity, ArrayList<TrackOrderDetailModel.Data.StatusDatum> datum, int orderId) {
        this.activity = activity;
        this.datum = datum;
        this.orderId = orderId;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 1) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_track_order_for_selected, parent, false);
            return new BigViewHolder(view);
        } else {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_track_order, parent, false);
            return new SmallViewHolder(view);
        }

    }




    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        final TrackOrderDetailModel.Data.StatusDatum result = datum.get(position);

//        int order_status = OrderSession.getSharedInstance().getOrderStatus();
//
//        BigViewHolder holder = (BigViewHolder) viewHolder;
//
//
//        holder.txtViewCategoryName.setText(result.getText());
//
//
//
//        if (order_status == 0)
//    {
//
//
//        switch (position)
//        {
//
//            case 0:
//
//                // Delivery Partner assigned and on way to pickup location
//
//                holder.txtViewTitle.setVisibility(View.GONE);
//
//                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//
//                holder.txtViewOtp.setVisibility(View.GONE);
//
//                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_in_process_);
//
//                holder.btnStatusAction.setVisibility(View.GONE);
//
//                break;
//
//            case 1:
//
//                // Reached pickup location
//
//                holder.txtViewTitle.setVisibility(View.GONE);
//
//                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//
//                holder.txtViewOtp.setVisibility(View.GONE);
//
//                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon);
//
//                holder.btnStatusAction.setVisibility(View.GONE);
//
//                break;
//
//            case 2:
//
//                // Confirm Order
//
//                holder.txtViewTitle.setVisibility(View.VISIBLE);
//
//                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//
//                holder.txtViewOtp.setVisibility(View.GONE);
//
//                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon);
//
//                holder.btnStatusAction.setVisibility(View.GONE);
//
//                break;
//
//            case 3:
//
//                //    Pay Bill Option  : Delivery partner on the way to drop location
//
//                holder.txtViewTitle.setVisibility(View.VISIBLE);
//
//                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//
//                holder.txtViewOtp.setVisibility(View.GONE);
//
//                //holder.txtViewOtp.setText("- - - -");
//
//                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon);
//
//                holder.btnStatusAction.setVisibility(View.GONE);
//
//                holder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                        intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
//                        activity.startActivity(intent);
//                    }
//                });
//
//                break;
//
//
//            case 4:
//
//                // OTP Page
//
//                holder.txtViewTitle.setVisibility(View.VISIBLE);
//
//                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//
//                holder.txtViewOtp.setVisibility(View.VISIBLE);
//
//                holder.txtViewOtp.setText("- - - -");
//
//                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon);
//
//                holder.btnStatusAction.setVisibility(View.GONE);
//
//                holder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                        intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
//                        activity.startActivity(intent);
//                    }
//                });
//
//                break;
//
//            case 5:
//
//                // Delivered
//
//                holder.txtViewTitle.setVisibility(View.GONE);
//
//                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//
//                holder.txtViewOtp.setVisibility(View.GONE);
//
//                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon);
//
//                holder.btnStatusAction.setVisibility(View.GONE);
//
//
//                break;
//        }
//
//
//    }else if (order_status == 1)
//       {
//          /*
//         switch (position)
//         {
//
//             case 0:
//
//
//                 holder.txtViewTitle.setVisibility(View.GONE);
//                 holder.txtViewOtp.setVisibility(View.GONE);
//                 holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//                 holder.txtViewCategoryName.setText(result.getText());
//                 holder.txtViewOtp.setText(result.getOtp());
//
//                 // Order status Image
//                 switch (result.getStatus())
//                 {
//                     case 0: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon); break;
//                     case 1: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_in_process_); break;
//                     case 2: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done); break;
//                 }
//
//
//
//                 if (result.getOtp().equals("- - - -")) {
//                     holder.txtViewTitle.setVisibility(View.GONE);
//                     holder.txtViewOtp.setVisibility(View.GONE);
//                     holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//                     holder.txtViewCategoryName.setText(result.getText());
//                     holder.txtViewOtp.setText(result.getOtp());
//                 }else
//                 {
//                     holder.txtViewCategoryName.setText("OTP");
//                     holder.txtViewOtp.setVisibility(View.VISIBLE);
//                     holder.txtViewOtp.setText(result.getOtp());
//                     holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done);
//                 }
//
//                 if (position == 4) {
//                     holder.txtViewOtp.setVisibility(View.VISIBLE);
//                 } else {
//                     holder.txtViewOtp.setVisibility(View.GONE);
//                 }
//
//                 if (position > 2 && position < 5) {
//
//                     holder.txtViewTitle.setVisibility(View.VISIBLE);
//                 } else {
//
//                     holder.txtViewTitle.setVisibility(View.GONE);
//                 }
//
//                 if (result.getStatus() >= 1 && position == 2) {
//                     holder.txtViewButton.setText("Confirm");
//                     holder.txtViewButton.setVisibility(View.VISIBLE);
//                 } else {
//                     holder.txtViewButton.setVisibility(View.GONE);
//                 }
//
//
//                 if (result.getIsShowPayOption() == 1 && position == 3) {
//                     holder.txtViewButton.setText("Pay");
//                     holder.txtViewButton.setVisibility(View.VISIBLE);
//                 } else {
//                     holder.txtViewButton.setVisibility(View.GONE);
//                 }
//
//
//
//
//                 holder.relLayItem.setOnClickListener(new View.OnClickListener() {
//                     @Override
//                     public void onClick(View view) {
//                         if (position == 0) {
//
//                             Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                             intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                             intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 3);
//                             activity.startActivity(intent);
//                         }
//                     }
//                 });
//
//                 holder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
//                     @Override
//                     public void onClick(View view) {
//                         Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                         intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                         intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
//                         activity.startActivity(intent);
//                     }
//                 });
//
//
//                 holder.txtViewButton.setOnClickListener(new View.OnClickListener() {
//                     @Override
//                     public void onClick(View view) {
//                         Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                         intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//
//                         if (position == 2) {
//                             intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 1);
//                         } else {
//                             intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 2);
//                         }
//                         activity.startActivity(intent);
//                     }
//                 });
//
//           break;
//
//             case 1:
//
//
//
//                 break;
//
//             case 2:
//
//
//
//                 break;
//
//             case 3:
//
//
//
//                 break;
//
//
//             case 4:
//
//
//
//                 break;
//
//             case 5:
//
//
//
//                 break;
//         }
//
//         */
//
//
//       } else if (order_status == 2)
//       {
//           // Delivery Partner Reached to pickup location
//
//
//       }else if (order_status == 3)
//       {
//           //Confirm Order
//
//
//       }else if (order_status == 4)
//       {
//
//
//
//       }else if (order_status == 5)
//       {
//
//       }else if (order_status == 6)
//       {
//
//       }else if (order_status == 7)
//       {
//
//       }else if (order_status == 8)
//       {
//
//       }else if (order_status == 9)
//       {
//
//       }
//        else if (order_status == 10)
//        {
//
//        }
//


        //THis is End 💛
       /*

        // Order status Image
                 switch (result.getStatus())
                 {
                     case 0: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon); break;
                     case 1: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_in_process_); break;
                     case 2: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done); break;
                 }



                 if (result.getOtp().equals("- - - -")) {
                     holder.txtViewTitle.setVisibility(View.GONE);
                     holder.txtViewOtp.setVisibility(View.GONE);
                     holder.txtViewCategoryName.setVisibility(View.VISIBLE);
                     holder.txtViewCategoryName.setText(result.getText());
                     holder.txtViewOtp.setText(result.getOtp());
                 }else
                 {
                     holder.txtViewCategoryName.setText("OTP");
                     holder.txtViewOtp.setVisibility(View.VISIBLE);
                     holder.txtViewOtp.setText(result.getOtp());
                     holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done);
                 }

                 if (position == 4) {
                     holder.txtViewOtp.setVisibility(View.VISIBLE);
                 } else {
                     holder.txtViewOtp.setVisibility(View.GONE);
                 }

                 if (position > 2 && position < 5) {

                     holder.txtViewTitle.setVisibility(View.VISIBLE);
                 } else {

                     holder.txtViewTitle.setVisibility(View.GONE);
                 }

                 if (result.getStatus() >= 1 && position == 2) {
                     holder.txtViewButton.setText("Confirm");
                     holder.txtViewButton.setVisibility(View.VISIBLE);
                 } else {
                     holder.txtViewButton.setVisibility(View.GONE);
                 }


                 if (result.getIsShowPayOption() == 1 && position == 3) {
                     holder.txtViewButton.setText("Pay");
                     holder.txtViewButton.setVisibility(View.VISIBLE);
                 } else {
                     holder.txtViewButton.setVisibility(View.GONE);
                 }




                 holder.relLayItem.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         if (position == 0) {

                             Intent intent = new Intent(activity, OrderSummaryActivity.class);
                             intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
                             intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 3);
                             activity.startActivity(intent);
                         }
                     }
                 });

                 holder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         Intent intent = new Intent(activity, OrderSummaryActivity.class);
                         intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
                         intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
                         activity.startActivity(intent);
                     }
                 });


                 holder.txtViewButton.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         Intent intent = new Intent(activity, OrderSummaryActivity.class);
                         intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);

                         if (position == 2) {
                             intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 1);
                         } else {
                             intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 2);
                         }
                         activity.startActivity(intent);
                     }
                 });


        */



//        if (result.getStatus() == 1) {
//            BigViewHolder holder = (BigViewHolder) viewHolder;
//
//            // Order status Image
//            switch (result.getStatus())
//            {
//                case 0: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon); break;
//                case 1: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_in_process_); break;
//                case 2: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done); break;
//            }
//
//            if (result.getOtp().equals("- - - -")) {
//                holder.txtViewTitleBig.setVisibility(View.GONE);
//                holder.txtViewOtpBig.setVisibility(View.GONE);
//                holder.txtViewCategoryNameBig.setVisibility(View.VISIBLE);
//                holder.txtViewCategoryNameBig.setText(result.getText());
//                holder.txtViewOtpBig.setText(result.getOtp());
//            }
//
//            if (position > 2 && position < 5) {
//
//                holder.txtViewTitleBig.setVisibility(View.VISIBLE);
//            } else {
//
//                holder.txtViewTitleBig.setVisibility(View.GONE);
//
//            }
//
//
//            if (position == 4) {
//                holder.txtViewOtpBig.setVisibility(View.VISIBLE);
//            } else {
//                holder.txtViewOtpBig.setVisibility(View.GONE);
//            }
//
//
//            if (result.getStatus() >= 1 && position == 2) {
//                holder.txtViewButtonBig.setText("Confirm");
//                holder.txtViewButtonBig.setVisibility(View.VISIBLE);
//            } else {
//                holder.txtViewButtonBig.setVisibility(View.GONE);
//            }
//
//            if (result.getIsShowPayOption() == 1 && position == 3) {
//                holder.txtViewButtonBig.setText("Pay");
//                holder.txtViewButtonBig.setVisibility(View.VISIBLE);
//            } else {
//                holder.txtViewButtonBig.setVisibility(View.GONE);
//            }
//
//            /* else {
//                holder.txtViewTitleBig.setVisibility(View.VISIBLE);
//                holder.txtViewOtpBig.setVisibility(View.VISIBLE);
//                holder.txtViewCategoryNameBig.setVisibility(View.VISIBLE);
//                holder.txtViewTitleBig.setText(result.getText());
//                holder.txtViewOtpBig.setText(result.getOtp().toString());
//
//            }*/
//
//            holder.relLayItemBig.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    if (position == 0) {
//
//                        Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                        intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 3);
//                        activity.startActivity(intent);
//                    }
//
//                }
//            });
//
//            holder.txtViewTitleBig.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                    intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
//                    activity.startActivity(intent);
//
//                }
//            });
//
//            holder.txtViewButtonBig.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                    if (position == 2) {
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 1);
//                    } else {
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 2);
//                    }
//                    activity.startActivity(intent);
//
//                }
//            });
//
//
//        } else {
//            SmallViewHolder holder = (SmallViewHolder) viewHolder;
//
//            // Order status Image
//            switch (result.getStatus())
//            {
//                case 0: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon); break;
//                case 1: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_in_process_); break;
//                case 2: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done); break;
//            }
//
//
//
//            if (result.getOtp().equals("- - - -")) {
//                holder.txtViewTitle.setVisibility(View.GONE);
//                holder.txtViewOtp.setVisibility(View.GONE);
//                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
//                holder.txtViewCategoryName.setText(result.getText());
//                holder.txtViewOtp.setText(result.getOtp());
//            }else
//            {
//                holder.txtViewCategoryName.setText("OTP");
//                holder.txtViewOtp.setVisibility(View.VISIBLE);
//                holder.txtViewOtp.setText(result.getOtp());
//                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done);
//            }
//
//            if (position == 4) {
//                holder.txtViewOtp.setVisibility(View.VISIBLE);
//            } else {
//                holder.txtViewOtp.setVisibility(View.GONE);
//            }
//
//            if (position > 2 && position < 5) {
//
//                holder.txtViewTitle.setVisibility(View.VISIBLE);
//            } else {
//
//                holder.txtViewTitle.setVisibility(View.GONE);
//            }
//
//            if (result.getStatus() >= 1 && position == 2) {
//                holder.txtViewButton.setText("Confirm");
//                holder.txtViewButton.setVisibility(View.VISIBLE);
//            } else {
//                holder.txtViewButton.setVisibility(View.GONE);
//            }
//
//
//            if (result.getIsShowPayOption() == 1 && position == 3) {
//                holder.txtViewButton.setText("Pay");
//                holder.txtViewButton.setVisibility(View.VISIBLE);
//            } else {
//                holder.txtViewButton.setVisibility(View.GONE);
//            }
//
//
//
//
//            holder.relLayItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (position == 0) {
//
//                        Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                        intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 3);
//                        activity.startActivity(intent);
//                    }
//                }
//            });
//
//            holder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//                    intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
//                    activity.startActivity(intent);
//                }
//            });
//
//
//            holder.txtViewButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
//                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
//
//                    if (position == 2) {
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 1);
//                    } else {
//                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 2);
//                    }
//                    activity.startActivity(intent);
//                }
//            });
//
//
//        }










        if (result.getStatus() == 1) {
            BigViewHolder holder = (BigViewHolder) viewHolder;

            // Order status Image
            switch (result.getStatus())
            {
                case 0: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon); break;
                case 1: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_in_process_); break;
                case 2: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done); break;
            }

            if (result.getOtp().equals("- - - -")) {
                holder.txtViewTitle.setVisibility(View.GONE);
                holder.txtViewOtp.setVisibility(View.GONE);
                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
                holder.txtViewCategoryName.setText(result.getText());
                holder.txtViewOtp.setText(result.getOtp());
            }

            if (position > 2 && position < 5) {

                holder.txtViewTitle.setVisibility(View.VISIBLE);
            } else {

                holder.txtViewTitle.setVisibility(View.GONE);

            }


            if (position == 4) {
                holder.txtViewOtp.setVisibility(View.VISIBLE);
            } else {
                holder.txtViewOtp.setVisibility(View.GONE);
            }


            if (result.getStatus() >= 1 && position == 2) {
                holder.btnStatusAction.setText("Confirm");
                holder.btnStatusAction.setVisibility(View.VISIBLE);
            } else {
                holder.btnStatusAction.setVisibility(View.GONE);
            }

            if (result.getIsShowPayOption() == 1 && position == 3) {
                holder.btnStatusAction.setText("Pay");
                holder.btnStatusAction.setVisibility(View.VISIBLE);
            } else {
                holder.btnStatusAction.setVisibility(View.GONE);
            }

            /* else {
                holder.txtViewTitleBig.setVisibility(View.VISIBLE);
                holder.txtViewOtpBig.setVisibility(View.VISIBLE);
                holder.txtViewCategoryNameBig.setVisibility(View.VISIBLE);
                holder.txtViewTitleBig.setText(result.getText());
                holder.txtViewOtpBig.setText(result.getOtp().toString());

            }*/

            holder.relLayItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (position == 2) {

                        Intent intent = new Intent(activity, OrderSummaryActivity.class);
                        intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 3);
                        activity.startActivity(intent);
                    }

                }
            });

            holder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
                    intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
                    activity.startActivity(intent);

                }
            });

            holder.btnStatusAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
                    if (position == 2) {
                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 1);
                    } else {
                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 2);
                    }
                    activity.startActivity(intent);

                }
            });


        } else {
            SmallViewHolder holder = (SmallViewHolder) viewHolder;

            // Order status Image
            switch (result.getStatus())
            {
                case 0: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_pending_icon); break;
                case 1: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_in_process_); break;
                case 2: holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done); break;
            }



            if (result.getOtp().equals("- - - -")) {
                holder.txtViewTitle.setVisibility(View.GONE);
                holder.txtViewOtp.setVisibility(View.GONE);
                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
                holder.txtViewCategoryName.setText(result.getText());
                holder.txtViewOtp.setText(result.getOtp());
            }else
            {
                holder.txtViewCategoryName.setText("OTP");
                holder.txtViewOtp.setVisibility(View.VISIBLE);
                holder.txtViewOtp.setText(result.getOtp());
                holder.imgOrderStatus.setImageResource(R.mipmap.ic_step_done);
            }

            if (position == 4) {
                holder.txtViewOtp.setVisibility(View.VISIBLE);
            } else {
                holder.txtViewOtp.setVisibility(View.GONE);
            }

            if (position > 2 && position < 5) {

                holder.txtViewTitle.setVisibility(View.VISIBLE);
            } else {

                holder.txtViewTitle.setVisibility(View.GONE);
            }

            if (result.getStatus() >= 1 && position == 2) {
                holder.btnStatusAction.setText("Confirm");
                holder.btnStatusAction.setVisibility(View.VISIBLE);
            } else {
                holder.btnStatusAction.setVisibility(View.GONE);
            }


            if (result.getIsShowPayOption() == 1 && position == 3) {
                holder.btnStatusAction.setText("Pay");
                holder.btnStatusAction.setVisibility(View.VISIBLE);
            } else {
                holder.btnStatusAction.setVisibility(View.GONE);
            }



            /*else {
                holder.txtViewTitle.setVisibility(View.VISIBLE);
                holder.txtViewOtp.setVisibility(View.VISIBLE);
                holder.txtViewCategoryName.setVisibility(View.VISIBLE);
                holder.txtViewTitle.setText(result.getText());
                holder.txtViewOtp.setText(result.getOtp().toString());

            }*/


            holder.relLayItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position == 0) {

                        Intent intent = new Intent(activity, OrderSummaryActivity.class);
                        intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 3);
                        activity.startActivity(intent);
                    }
                }
            });

            holder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);
                    intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 0);
                    activity.startActivity(intent);
                }
            });


            holder.btnStatusAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, OrderSummaryActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_ID, orderId);

                    if (position == 2) {
                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 1);
                    } else {
                        intent.putExtra(ConstIntent.CONST_ISCOMEFROM_YOUR_BILL, 2);
                    }
                    activity.startActivity(intent);
                }
            });


        }

    }

    @Override
    public int getItemCount() {
        return datum.size();
    }


    public class SmallViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;


        @BindView(R.id.txtViewOtp)
        TextView txtViewOtp;

        @BindView(R.id.txtViewTitle)
        TextView txtViewTitle;

        @BindView(R.id.txtViewButton)
        TextView btnStatusAction;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgOrderStatus)
        ImageView imgOrderStatus;


        public SmallViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }


    public class BigViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;


        @BindView(R.id.txtViewOtp)
        TextView txtViewOtp;

        @BindView(R.id.txtViewTitle)
        TextView txtViewTitle;

        @BindView(R.id.txtViewButton)
        TextView btnStatusAction;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgOrderStatus)
        ImageView imgOrderStatus;



        public BigViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        TrackOrderDetailModel.Data.StatusDatum data = datum.get(position);

        if (data.getStatus() == 1) {
            return 1;
        } else {
            return 0;
        }


        /*
        int order_status = OrderSession.getSharedInstance().getOrderStatus();



        if (order_status == 0)
        {

            if (position == 0)
            {
                return 1;
            }else
            {
                return  0;
            }

        }else if (order_status == 1)
        {

            if (position == 0)
            {
                return 1;
            }else
            {
                return  0;
            }

        } else if (order_status == 2)
        {
            // Delivery Partner Reached to pickup location
            if (position == 1)
            {
                return 1;
            }else
            {
                return  0;
            }

        }else if (order_status == 3)
        {
            //Confirm Order
            if (position == 2)
            {
                return 1;
            }else
            {
                return  0;
            }

        }else if (order_status == 4)
        {



        }else if (order_status == 5)
        {

        }else if (order_status == 6)
        {

        }else if (order_status == 7)
        {

        }else if (order_status == 8)
        {

        }else if (order_status == 9)
        {

        }
        else if (order_status == 10)
        {

        }



        return 0;
        */

    }
}


