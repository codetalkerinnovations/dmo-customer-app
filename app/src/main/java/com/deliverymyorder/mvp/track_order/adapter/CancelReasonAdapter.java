package com.deliverymyorder.mvp.track_order.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.R;
import com.deliverymyorder.mvp.track_order.TrackOrderFragment;
import com.deliverymyorder.mvp.track_order.model.CancelOrderReasonModel;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CancelReasonAdapter extends RecyclerView.Adapter<CancelReasonAdapter.Holder> {


    Activity activity;
    ArrayList<CancelOrderReasonModel.Datum> datum;
    String favBit;
    TrackOrderFragment trackOrderFragment;

    public CancelReasonAdapter(Activity activity, ArrayList<CancelOrderReasonModel.Datum> datum, TrackOrderFragment trackOrderFragment) {
        this.activity = activity;
        this.datum = datum;
        this.trackOrderFragment = trackOrderFragment;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_cancel_reason_order, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final CancelOrderReasonModel.Datum result = datum.get(position);

        holder.txtViewTitle.setText(result.getTitle());

        if (result.isSelected()) {

            holder.radioButtonSelectedCancelReason.setChecked(true);
        } else {

            holder.radioButtonSelectedCancelReason.setChecked(false);
        }



        holder.radioButtonSelectedCancelReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < datum.size(); i++) {

                    datum.get(i).setSelected(false);
                }

                datum.get(position).setSelected(true);

                trackOrderFragment.selectedCancelReason(result.getId());
                notifyDataSetChanged();




            }
        });

    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.radioButtonSelectedCancelReason)
        RadioButton radioButtonSelectedCancelReason;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.txtViewTitle)
        TextView txtViewTitle;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}