package com.deliverymyorder.mvp.track_order.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryBoy implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("service_status")
    @Expose
    private Integer serviceStatus;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("otp_code")
    @Expose
    private String otpCode;
    @SerializedName("vehicle_number")
    @Expose
    private String vehicleNumber;
    @SerializedName("vehicle_licence_number")
    @Expose
    private String vehicleLicenceNumber;
    @SerializedName("partner_type")
    @Expose
    private String partnerType;
    @SerializedName("service_country_id")
    @Expose
    private Integer serviceCountryId;
    @SerializedName("service_state_id")
    @Expose
    private Integer serviceStateId;
    @SerializedName("service_city_id")
    @Expose
    private Integer serviceCityId;
    @SerializedName("sevice_latitude")
    @Expose
    private Double seviceLatitude;
    @SerializedName("sevice_longitude")
    @Expose
    private Double seviceLongitude;
    @SerializedName("campus_status")
    @Expose
    private Integer campusStatus;
    @SerializedName("service_time_from")
    @Expose
    private String serviceTimeFrom;
    @SerializedName("service_time_to")
    @Expose
    private String serviceTimeTo;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(Integer serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public Object getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleLicenceNumber() {
        return vehicleLicenceNumber;
    }

    public void setVehicleLicenceNumber(String vehicleLicenceNumber) {
        this.vehicleLicenceNumber = vehicleLicenceNumber;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Integer getServiceCountryId() {
        return serviceCountryId;
    }

    public void setServiceCountryId(Integer serviceCountryId) {
        this.serviceCountryId = serviceCountryId;
    }

    public Integer getServiceStateId() {
        return serviceStateId;
    }

    public void setServiceStateId(Integer serviceStateId) {
        this.serviceStateId = serviceStateId;
    }

    public Integer getServiceCityId() {
        return serviceCityId;
    }

    public void setServiceCityId(Integer serviceCityId) {
        this.serviceCityId = serviceCityId;
    }

    public Object getSeviceLatitude() {
        return seviceLatitude;
    }

    public void setSeviceLatitude(Double seviceLatitude) {
        this.seviceLatitude = seviceLatitude;
    }

    public Object getSeviceLongitude() {
        return seviceLongitude;
    }

    public void setSeviceLongitude(Double seviceLongitude) {
        this.seviceLongitude = seviceLongitude;
    }

    public Integer getCampusStatus() {
        return campusStatus;
    }

    public void setCampusStatus(Integer campusStatus) {
        this.campusStatus = campusStatus;
    }

    public String getServiceTimeFrom() {
        return serviceTimeFrom;
    }

    public void setServiceTimeFrom(String serviceTimeFrom) {
        this.serviceTimeFrom = serviceTimeFrom;
    }

    public String getServiceTimeTo() {
        return serviceTimeTo;
    }

    public void setServiceTimeTo(String serviceTimeTo) {
        this.serviceTimeTo = serviceTimeTo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.firstname);
        dest.writeString(this.lastname);
        dest.writeString(this.mobile);
        dest.writeString(this.email);
        dest.writeValue(this.status);
        dest.writeValue(this.serviceStatus);
        dest.writeString(this.profileImage);
        dest.writeString(this.otpCode);
        dest.writeString(this.vehicleNumber);
        dest.writeString(this.vehicleLicenceNumber);
        dest.writeString(this.partnerType);
        dest.writeValue(this.serviceCountryId);
        dest.writeValue(this.serviceStateId);
        dest.writeValue(this.serviceCityId);
        dest.writeDouble(this.seviceLatitude);
        dest.writeDouble(this.seviceLongitude);
        dest.writeValue(this.campusStatus);
        dest.writeString(this.serviceTimeFrom);
        dest.writeString(this.serviceTimeTo);
        dest.writeString(this.address);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public DeliveryBoy() {
    }

    protected DeliveryBoy(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.firstname = in.readString();
        this.lastname = in.readString();
        this.mobile = in.readString();
        this.email = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.serviceStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.profileImage = in.readParcelable(Object.class.getClassLoader());
        this.otpCode = in.readString();
        this.vehicleNumber = in.readString();
        this.vehicleLicenceNumber = in.readString();
        this.partnerType = in.readString();
        this.serviceCountryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.serviceStateId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.serviceCityId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.seviceLatitude = in.readParcelable(Double.class.getClassLoader());
        this.seviceLongitude = in.readParcelable(Double.class.getClassLoader());
        this.campusStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.serviceTimeFrom = in.readString();
        this.serviceTimeTo = in.readString();
        this.address = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Parcelable.Creator<DeliveryBoy> CREATOR = new Parcelable.Creator<DeliveryBoy>() {
        @Override
        public DeliveryBoy createFromParcel(Parcel source) {
            return new DeliveryBoy(source);
        }

        @Override
        public DeliveryBoy[] newArray(int size) {
            return new DeliveryBoy[size];
        }
    };
}
