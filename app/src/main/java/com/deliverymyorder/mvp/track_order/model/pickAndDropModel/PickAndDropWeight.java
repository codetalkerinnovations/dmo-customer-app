package com.deliverymyorder.mvp.track_order.model.pickAndDropModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickAndDropWeight implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("w_from")
    @Expose
    private Integer wFrom;
    @SerializedName("w_to")
    @Expose
    private Integer wTo;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("charge")
    @Expose
    private Integer charge;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWFrom() {
        return wFrom;
    }

    public void setWFrom(Integer wFrom) {
        this.wFrom = wFrom;
    }

    public Integer getWTo() {
        return wTo;
    }

    public void setWTo(Integer wTo) {
        this.wTo = wTo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.wFrom);
        dest.writeValue(this.wTo);
        dest.writeValue(this.status);
        dest.writeValue(this.charge);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public PickAndDropWeight() {
    }

    protected PickAndDropWeight(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.wFrom = (Integer) in.readValue(Integer.class.getClassLoader());
        this.wTo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.charge = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Creator<PickAndDropWeight> CREATOR = new Creator<PickAndDropWeight>() {
        @Override
        public PickAndDropWeight createFromParcel(Parcel source) {
            return new PickAndDropWeight(source);
        }

        @Override
        public PickAndDropWeight[] newArray(int size) {
            return new PickAndDropWeight[size];
        }
    };
}