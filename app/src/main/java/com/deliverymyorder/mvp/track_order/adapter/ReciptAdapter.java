package com.deliverymyorder.mvp.track_order.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReciptAdapter  extends RecyclerView.Adapter<ReciptAdapter.Holder> {


    Activity activity;
    ArrayList<OrderHistoryModel.Data.Previous.OrderDetail> datum;
    String favBit;

    public ReciptAdapter(Activity activity, ArrayList<OrderHistoryModel.Data.Previous.OrderDetail> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_recipt, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final OrderHistoryModel.Data.Previous.OrderDetail result = datum.get(position);


        holder.txtViewItemTitle.setText(String.valueOf(result.getProductId()));
        holder.txtViewItemPrice.setText(String.valueOf(result.getProductPrice()));
        //holder.txtViewPrice.setText(String.valueOf(result.getProductPrice()));



    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.txtViewItemTitle)
        TextView txtViewItemTitle;

        @BindView(R.id.txtViewItemPrice)
        TextView txtViewItemPrice;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
             setFont();
        }

        private void setFont() {
            FontHelper.setFontFace(txtViewItemTitle, FontHelper.FontType.FONT_REGULAR, activity);
            FontHelper.setFontFace(txtViewItemPrice, FontHelper.FontType.FONT_REGULAR, activity);
        }

    }



}