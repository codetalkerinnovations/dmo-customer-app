package com.deliverymyorder.mvp.track_order.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackOrderDetailPresenterImpl implements TrackOrderContractor.TrackOrderPresenter {

    Activity activity;
    TrackOrderContractor.TrackOrderView trackOrderView;

    public TrackOrderDetailPresenterImpl(Activity activity,TrackOrderContractor.TrackOrderView trackOrderView){
        this.activity = activity;
        this.trackOrderView = trackOrderView;
    }


    @Override
    public void callTrackOrderApi(String orderId) {
        try {
            ApiAdapter.getInstance(activity);
            callTracOrderDetailApi(orderId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            trackOrderView.onInternetErrorTrackOrderDetail();
        }
    }

    private void callTracOrderDetailApi(String orderId) {

       // Progress.start(activity);

        //int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        int orderType = 1;
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType =1;
        }else if (obj instanceof PickAndDropOnGoingData)
        {
            orderType =2;
        }

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<TrackOrderDetailModel> getCreatePasswordResult = ApiAdapter.getApiService().getTrackOrderDetails("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<TrackOrderDetailModel>() {
            @Override
            public void onResponse(Call<TrackOrderDetailModel> call, Response<TrackOrderDetailModel> response) {

                //Progress.stop();
                try {
                    TrackOrderDetailModel trackOrderDetailModel = response.body();

                    if (trackOrderDetailModel.getError() == 0) {
                        trackOrderView.onSuccessTrackOrderDetail(trackOrderDetailModel.getData());
                    } else {
                        trackOrderView.onUnSuccessTrackOrderDetail(trackOrderDetailModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    trackOrderView.onUnSuccessTrackOrderDetail(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<TrackOrderDetailModel> call, Throwable t) {
              //  Progress.stop();
                trackOrderView.onUnSuccessTrackOrderDetail(activity.getString(R.string.server_error));
            }
        });



    }
}
