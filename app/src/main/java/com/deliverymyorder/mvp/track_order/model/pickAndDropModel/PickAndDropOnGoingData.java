package com.deliverymyorder.mvp.track_order.model.pickAndDropModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.deliverymyorder.mvp.track_order.model.DeliveryBoy;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PickAndDropOnGoingData implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("pickup_contact_name")
    @Expose
    private String pickupContactName;
    @SerializedName("pickup_contact_no")
    @Expose
    private String pickupContactNo;
    @SerializedName("pickup_lat")
    @Expose
    private Double pickupLat;
    @SerializedName("pickup_long")
    @Expose
    private Double pickupLong;
    @SerializedName("drop_contact_name")
    @Expose
    private String dropContactName;
    @SerializedName("drop_contact_no")
    @Expose
    private String dropContactNo;
    @SerializedName("drop_lat")
    @Expose
    private Double dropLat;
    @SerializedName("drop_long")
    @Expose
    private Double dropLong;
    @SerializedName("product_type_id")
    @Expose
    private Integer productTypeId;
    @SerializedName("weight_id")
    @Expose
    private Integer weightId;
    @SerializedName("size_id")
    @Expose
    private Integer sizeId;
    @SerializedName("total_cost")
    @Expose
    private Double totalCost;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("delivery_partner_id")
    @Expose
    private Integer deliveryPartnerId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("pick_and_drop_product_type")
    @Expose
    private PickAndDropProductType pickAndDropProductType;
    @SerializedName("pick_and_drop_size")
    @Expose
    private PickAndDropSize pickAndDropSize;
    @SerializedName("pick_and_drop_weight")
    @Expose
    private PickAndDropWeight pickAndDropWeight;

    @SerializedName("delivery_partner")
    @Expose
    private DeliveryBoy deliveryBoy;
    @SerializedName("pickup_flat_number")
    @Expose
    private String pickup_flat_number;

    @SerializedName("pickup_landmark")
    @Expose
    private String pickup_landmark;

    @SerializedName("pickup_city")
    @Expose
    private String pickup_city;

    @SerializedName("pickup_state")
    @Expose
    private String pickup_state;

    @SerializedName("pickup_country")
    @Expose
    private String pickup_country;


    @SerializedName("drop_flat_number")
    @Expose
    private String drop_flat_number;

    @SerializedName("drop_landmark")
    @Expose
    private String drop_landmark;

    @SerializedName("drop_city")
    @Expose
    private String drop_city;

    @SerializedName("drop_state")
    @Expose
    private String drop_state;

    @SerializedName("drop_country")
    @Expose
    private String drop_country;

    public String getPickup_flat_number() {
        return pickup_flat_number;
    }

    public void setPickup_flat_number(String pickup_flat_number) {
        this.pickup_flat_number = pickup_flat_number;
    }

    public String getPickup_landmark() {
        return pickup_landmark;
    }

    public void setPickup_landmark(String pickup_landmark) {
        this.pickup_landmark = pickup_landmark;
    }

    public String getPickup_city() {
        return pickup_city;
    }

    public void setPickup_city(String pickup_city) {
        this.pickup_city = pickup_city;
    }

    public String getPickup_state() {
        return pickup_state;
    }

    public void setPickup_state(String pickup_state) {
        this.pickup_state = pickup_state;
    }

    public String getPickup_country() {
        return pickup_country;
    }

    public void setPickup_country(String pickup_country) {
        this.pickup_country = pickup_country;
    }

    public String getDrop_flat_number() {
        return drop_flat_number;
    }

    public void setDrop_flat_number(String drop_flat_number) {
        this.drop_flat_number = drop_flat_number;
    }

    public String getDrop_landmark() {
        return drop_landmark;
    }

    public void setDrop_landmark(String drop_landmark) {
        this.drop_landmark = drop_landmark;
    }

    public String getDrop_city() {
        return drop_city;
    }

    public void setDrop_city(String drop_city) {
        this.drop_city = drop_city;
    }

    public String getDrop_state() {
        return drop_state;
    }

    public void setDrop_state(String drop_state) {
        this.drop_state = drop_state;
    }

    public String getDrop_country() {
        return drop_country;
    }

    public void setDrop_country(String drop_country) {
        this.drop_country = drop_country;
    }

    @SerializedName("invoice_no")
    @Expose
    private String invoiceNo;

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public DeliveryBoy getDeliveryBoy() {
        return deliveryBoy;
    }

    public void setDeliveryBoy(DeliveryBoy deliveryBoy) {
        this.deliveryBoy = deliveryBoy;
    }

    public Date getCreateDate()
    {

        Date convertedDate = new Date();
        String stringDateFormat = createdAt; //2019-01-03 21:32:41
        SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            convertedDate = (Date) formatter.parse(stringDateFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedDate;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getPickupContactName() {
        return pickupContactName;
    }

    public void setPickupContactName(String pickupContactName) {
        this.pickupContactName = pickupContactName;
    }

    public String getPickupContactNo() {
        return pickupContactNo;
    }

    public void setPickupContactNo(String pickupContactNo) {
        this.pickupContactNo = pickupContactNo;
    }

    public Double getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(Double pickupLat) {
        this.pickupLat = pickupLat;
    }

    public Double getPickupLong() {
        return pickupLong;
    }

    public void setPickupLong(Double pickupLong) {
        this.pickupLong = pickupLong;
    }

    public String getDropContactName() {
        return dropContactName;
    }

    public void setDropContactName(String dropContactName) {
        this.dropContactName = dropContactName;
    }

    public String getDropContactNo() {
        return dropContactNo;
    }

    public void setDropContactNo(String dropContactNo) {
        this.dropContactNo = dropContactNo;
    }

    public Double getDropLat() {
        return dropLat;
    }

    public void setDropLat(Double dropLat) {
        this.dropLat = dropLat;
    }

    public Double getDropLong() {
        return dropLong;
    }

    public void setDropLong(Double dropLong) {
        this.dropLong = dropLong;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getWeightId() {
        return weightId;
    }

    public void setWeightId(Integer weightId) {
        this.weightId = weightId;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDeliveryPartnerId() {
        return deliveryPartnerId;
    }

    public void setDeliveryPartnerId(Integer deliveryPartnerId) {
        this.deliveryPartnerId = deliveryPartnerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PickAndDropProductType getPickAndDropProductType() {
        return pickAndDropProductType;
    }

    public void setPickAndDropProductType(PickAndDropProductType pickAndDropProductType) {
        this.pickAndDropProductType = pickAndDropProductType;
    }

    public PickAndDropSize getPickAndDropSize() {
        return pickAndDropSize;
    }

    public void setPickAndDropSize(PickAndDropSize pickAndDropSize) {
        this.pickAndDropSize = pickAndDropSize;
    }

    public PickAndDropWeight getPickAndDropWeight() {
        return pickAndDropWeight;
    }

    public void setPickAndDropWeight(PickAndDropWeight pickAndDropWeight) {
        this.pickAndDropWeight = pickAndDropWeight;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.customerId);
        dest.writeString(this.pickupContactName);
        dest.writeString(this.pickupContactNo);
        dest.writeValue(this.pickupLat);
        dest.writeValue(this.pickupLong);
        dest.writeString(this.dropContactName);
        dest.writeString(this.dropContactNo);
        dest.writeValue(this.dropLat);
        dest.writeValue(this.dropLong);
        dest.writeValue(this.productTypeId);
        dest.writeValue(this.weightId);
        dest.writeValue(this.sizeId);
        dest.writeValue(this.totalCost);
        dest.writeValue(this.status);
        dest.writeValue(this.deliveryPartnerId);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeParcelable(this.pickAndDropProductType, flags);
        dest.writeParcelable(this.pickAndDropSize, flags);
        dest.writeParcelable(this.pickAndDropWeight, flags);
    }

    public PickAndDropOnGoingData() {
    }

    protected PickAndDropOnGoingData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.customerId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pickupContactName = in.readString();
        this.pickupContactNo = in.readString();
        this.pickupLat = (Double) in.readValue(Double.class.getClassLoader());
        this.pickupLong = (Double) in.readValue(Double.class.getClassLoader());
        this.dropContactName = in.readString();
        this.dropContactNo = in.readString();
        this.dropLat = (Double) in.readValue(Double.class.getClassLoader());
        this.dropLong = (Double) in.readValue(Double.class.getClassLoader());
        this.productTypeId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.weightId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sizeId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalCost = (Double) in.readValue(Double.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.deliveryPartnerId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.pickAndDropProductType = in.readParcelable(PickAndDropProductType.class.getClassLoader());
        this.pickAndDropSize = in.readParcelable(PickAndDropSize.class.getClassLoader());
        this.pickAndDropWeight = in.readParcelable(PickAndDropWeight.class.getClassLoader());
    }

    public static final Creator<PickAndDropOnGoingData> CREATOR = new Creator<PickAndDropOnGoingData>() {
        @Override
        public PickAndDropOnGoingData createFromParcel(Parcel source) {
            return new PickAndDropOnGoingData(source);
        }

        @Override
        public PickAndDropOnGoingData[] newArray(int size) {
            return new PickAndDropOnGoingData[size];
        }
    };
}
