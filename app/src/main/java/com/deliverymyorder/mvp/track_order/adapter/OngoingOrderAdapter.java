package com.deliverymyorder.mvp.track_order.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.TrackOrderFragment;
import com.deliverymyorder.mvp.track_order.TrackOrderMapActivity;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 26-12-2018.
 */

public class OngoingOrderAdapter extends RecyclerView.Adapter<OngoingOrderAdapter.Holder> {


    Activity activity;
    //ArrayList<OrderHistoryModel.Data.Ongoing> datum;

    ArrayList<Object> onGoingOrdersData = null;

    String favBit;
    TrackOrderFragment trackOrderFragment;

    public OngoingOrderAdapter(Activity activity, ArrayList<Object> datum, TrackOrderFragment trackOrderFragment) {
        this.activity = activity;
        this.onGoingOrdersData = datum;
        this.trackOrderFragment = trackOrderFragment;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_ongoing_orders, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        Object object = onGoingOrdersData.get(position);

        if (object instanceof OrderHistoryModel.Data.Ongoing) {

            final OrderHistoryModel.Data.Ongoing result =   (OrderHistoryModel.Data.Ongoing)object; //datum.get(position);

            if (result.getStatus() > 3)
            {
                holder.txtViewCancel.setVisibility(View.GONE);
            }else
            {
                holder.txtViewCancel.setVisibility(View.VISIBLE);
            }

            holder.txtViewOrderId.setText(result.getInvoiceNo());

            if (result.getMerchantBuissness()!=null) {
                holder.txtViewMerchantName.setText(result.getMerchantBuissness().getTitle());
//                holder.txtViewMerchantName.setText(result.getMerchant().getFirstname() + " " + result.getMerchant().getLastname());
                holder.txtViewMerchantArea.setText(result.getMerchantBuissness().getAddress());

            }else
            {
                holder.txtViewMerchantName.setText("-NA-");
                holder.txtViewMerchantArea.setText("-NA-");
//                holder.txtViewOrderId.setText("-NA-");
            }

            holder.txtViewDate.setText(Utils.getStringDateFromDate(activity,result.getCreateDate())); //result.getUpdatedAt());

            holder.txtViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity,R.style.AlertDialogTheme);
                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want to cancel this order ?");
                    alertDialog.setCancelable(false);
                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {

                            trackOrderFragment.getAllCancelreasonDataApi(result.getId(),1);

                            dialog.dismiss();


                        }
                    });
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();

                }
            });


            holder.txtViewTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    OrderSession.getSharedInstance().setCurrentOrder(result);

                    Intent intent = new Intent(activity, TrackOrderMapActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_ID, result.getId());
                    activity.startActivity(intent);
                }
            });
        }else if (object instanceof PickAndDropOnGoingData)
        {

            final PickAndDropOnGoingData result =   (PickAndDropOnGoingData)object; //datum.get(position);

            holder.txtViewMerchantName.setText(result.getDropContactName());
            holder.txtViewMerchantArea.setText(result.getDropContactNo());
            holder.txtViewOrderId.setText(result.getInvoiceNo());

            //holder.txtViewDate.setText(result.getCreatedAt());
            holder.txtViewDate.setText(Utils.getStringDateFromDate(activity,result.getCreateDate())); //result.getUpdatedAt());


            holder.txtViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity,R.style.AlertDialogTheme);
                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want to cancel this order ?");
                    alertDialog.setCancelable(false);
                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {

                            trackOrderFragment.getAllCancelreasonDataApi(result.getId(),2);

                            dialog.dismiss();


                        }
                    });
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();

                }
            });


            holder.txtViewTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    OrderSession.getSharedInstance().setCurrentOrder(result);

                    Intent intent = new Intent(activity, TrackOrderMapActivity.class);
                    intent.putExtra(ConstIntent.CONST_ORDER_ID, result.getId());
                    activity.startActivity(intent);
                }
            });

        }

    }


    @Override
    public int getItemCount() {
        if (onGoingOrdersData!=null) {
            return onGoingOrdersData.size();
        }else
        {
            return  0;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewMerchantName)
        TextView txtViewMerchantName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.txtViewMerchantArea)
        TextView txtViewMerchantArea;

        @BindView(R.id.txtViewOrderId)
        TextView txtViewOrderId;

        @BindView(R.id.txtViewTrack)
        TextView txtViewTrack;

        @BindView(R.id.txtViewCancel)
        TextView txtViewCancel;

        @BindView(R.id.txtViewDate)
        TextView txtViewDate;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}