package com.deliverymyorder.mvp.track_order.model.pickAndDropModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PickAndDropProductType implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("charge")
    @Expose
    private Integer charge;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeString(this.img);
        dest.writeValue(this.status);
        dest.writeValue(this.charge);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public PickAndDropProductType() {
    }

    protected PickAndDropProductType(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.img = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.charge = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Creator<PickAndDropProductType> CREATOR = new Creator<PickAndDropProductType>() {
        @Override
        public PickAndDropProductType createFromParcel(Parcel source) {
            return new PickAndDropProductType(source);
        }

        @Override
        public PickAndDropProductType[] newArray(int size) {
            return new PickAndDropProductType[size];
        }
    };
}