package com.deliverymyorder.mvp.track_order.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TrackOrderDetailModel {

        @SerializedName("error")
        @Expose
        private Integer error;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    public class Data {

        @SerializedName("current")
        @Expose
        private Current current;
        @SerializedName("pickup")
        @Expose
        private Pickup pickup;

        //order_status
        @SerializedName("order_status")
        @Expose
        private int orderStatus;

        @SerializedName("drop")
        @Expose
        private Drop drop;
        @SerializedName("status_data")
        @Expose
        private ArrayList<StatusDatum> statusData = null;


        public int getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(int orderStatus) {
            this.orderStatus = orderStatus;
        }


        public Current getCurrent() {
            return current;
        }

        public void setCurrent(Current current) {
            this.current = current;
        }

        public Pickup getPickup() {
            return pickup;
        }

        public void setPickup(Pickup pickup) {
            this.pickup = pickup;
        }

        public Drop getDrop() {
            return drop;
        }

        public void setDrop(Drop drop) {
            this.drop = drop;
        }

        public ArrayList<StatusDatum> getStatusData() {
            return statusData;
        }

        public void setStatusData(ArrayList<StatusDatum> statusData) {
            this.statusData = statusData;
        }




        public class Current {

            @SerializedName("latitude")
            @Expose
            private String latitude;
            @SerializedName("longitude")
            @Expose
            private String longitude;

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

        }


        public class Drop {

            @SerializedName("latitude")
            @Expose
            private Double latitude;
            @SerializedName("longitude")
            @Expose
            private Double longitude;

            public Double getLatitude() {
                return latitude;
            }

            public void setLatitude(Double latitude) {
                this.latitude = latitude;
            }

            public Double getLongitude() {
                return longitude;
            }

            public void setLongitude(Double longitude) {
                this.longitude = longitude;
            }

        }


        public class Pickup {

            @SerializedName("latitude")
            @Expose
            private String latitude;
            @SerializedName("longitude")
            @Expose
            private String longitude;

            public String getLatitude() {
                return latitude;
            }



            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

        }


        public class StatusDatum {

            @SerializedName("text")
            @Expose
            private String text;
            @SerializedName("isShowPayOption")
            @Expose
            private Integer isShowPayOption;
            @SerializedName("otp")
            @Expose
            private String otp;
            @SerializedName("status")
            @Expose
            private Integer status;

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public Integer getIsShowPayOption() {
                return isShowPayOption;
            }

            public void setIsShowPayOption(Integer isShowPayOption) {
                this.isShowPayOption = isShowPayOption;
            }

            public String getOtp() {
                return otp;
            }

            public void setOtp(String otp) {
                this.otp = otp;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

        }
    }

}