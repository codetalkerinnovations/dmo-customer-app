package com.deliverymyorder.mvp.track_order.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.coupen.CoupenModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailContractor;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.CancelOrderModel;
import com.deliverymyorder.mvp.track_order.model.CancelOrderReasonModel;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.ReOrderModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 26-12-2018.
 */

public class OrderHistoryPresenterImpl implements OrderHistoryContractor.OrderHistoryPresenter {

    Activity activity;
    OrderHistoryContractor.OrderHistoryView orderHistoryView;

    public OrderHistoryPresenterImpl(Activity activity, OrderHistoryContractor.OrderHistoryView orderHistoryView) {
        this.activity = activity;
        this.orderHistoryView = orderHistoryView;
    }

    @Override
    public void getOrderHistoryData() {
        try {
            ApiAdapter.getInstance(activity);
            callGetOrderHistoryApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            orderHistoryView.OrderHistoryDataInternetError();
        }
    }

    @Override
    public void getCancelOrderReasonsData() {
        try {
            ApiAdapter.getInstance(activity);
            callGetOrderCancelReasonApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            orderHistoryView.cancelOrderReasonsInternetError();
        }
    }

    @Override
    public void reOrderData(int orderId) {
        try {
            ApiAdapter.getInstance(activity);
            reOrderDataApi(orderId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            orderHistoryView.reOrderApiInternetError();
        }
    }

    @Override
    public void updateCancelOrderReasonsData(int orderID, int cancelReasonId,int orderType) {
        try {
            ApiAdapter.getInstance(activity);
            callUpdateOrderCancelReasonApi( orderID,  cancelReasonId,orderType);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            orderHistoryView.updateCancelOrderReasonsInternetError();
        }
    }


    private void callGetOrderHistoryApi() {
        Progress.start(activity);

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, customerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<OrderHistoryModel> getCreatePasswordResult = ApiAdapter.getApiService().getOrderHistory("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<OrderHistoryModel>() {
            @Override
            public void onResponse(Call<OrderHistoryModel> call, Response<OrderHistoryModel> response) {

                Progress.stop();
                try {
                    OrderHistoryModel orderHistoryModel = response.body();

                    if (orderHistoryModel.getError() == 0) {
                        orderHistoryView.getOrderHistoryDataSuccess(orderHistoryModel.getData());
                    } else {
                        orderHistoryView.getOrderHistoryDataUnSucess(orderHistoryModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    orderHistoryView.getOrderHistoryDataUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<OrderHistoryModel> call, Throwable t) {
                Progress.stop();
                orderHistoryView.getOrderHistoryDataUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

    private void callUpdateOrderCancelReasonApi(int orderID, int cancelReasonId,int orderType) {
        Progress.start(activity);

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

     //   int orderType = 0;
//        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
//            orderType = 1;
//        }else if (obj instanceof PickAndDropOnGoingData)
//        {
//            orderType =2;
//        }



        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_ORDER_ID, orderID);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
            jsonObject.put(Const.PARAM_CANCEL_REASON_ID, cancelReasonId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<CancelOrderModel> getCreatePasswordResult = ApiAdapter.getApiService().updateOrderCancelReason("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<CancelOrderModel>() {
            @Override
            public void onResponse(Call<CancelOrderModel> call, Response<CancelOrderModel> response) {

                Progress.stop();
                try {
                    CancelOrderModel cancelOrderModel = response.body();

                    if (cancelOrderModel.getError() == 0) {
                        orderHistoryView.updateCancelOrderReasonsSuccess(cancelOrderModel.getData());
                    } else {
                        orderHistoryView.updateCancelOrderReasonsUnSucess(cancelOrderModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    orderHistoryView.updateCancelOrderReasonsUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CancelOrderModel> call, Throwable t) {
                Progress.stop();
                orderHistoryView.updateCancelOrderReasonsUnSucess(activity.getString(R.string.server_error));
            }
        });
    }


    private void reOrderDataApi(int orderID) {
        Progress.start(activity);

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        int orderType = 0;
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType = 1;
        } else if (obj instanceof PickAndDropOnGoingData) {
            orderType = 2;
        }

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_ORDER_ID, orderID);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<ReOrderModel> getCreatePasswordResult = ApiAdapter.getApiService().reOrder("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<ReOrderModel>() {
            @Override
            public void onResponse(Call<ReOrderModel> call, Response<ReOrderModel> response) {

                Progress.stop();
                try {
                    ReOrderModel reOrderModel = response.body();

                    if (reOrderModel.getError() == 0) {
                        orderHistoryView.reOrderApiSuccess(reOrderModel);
                    } else {
                        orderHistoryView.reOrderApiUnSucess(reOrderModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    orderHistoryView.reOrderApiUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ReOrderModel> call, Throwable t) {
                Progress.stop();
                orderHistoryView.reOrderApiUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

    private void callGetOrderCancelReasonApi() {
        Progress.start(activity);

        int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<CancelOrderReasonModel> getCreatePasswordResult = ApiAdapter.getApiService().getOrderCancelReason("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<CancelOrderReasonModel>() {
            @Override
            public void onResponse(Call<CancelOrderReasonModel> call, Response<CancelOrderReasonModel> response) {

                Progress.stop();
                try {
                    CancelOrderReasonModel cancelOrderReasonModel = response.body();

                    if (cancelOrderReasonModel.getError() == 0) {
                        orderHistoryView.getCancelOrderReasonsSuccess(cancelOrderReasonModel.getData());
                    } else {
                        orderHistoryView.getCancelOrderReasonsUnSucess(cancelOrderReasonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    exp.printStackTrace();
                    orderHistoryView.getCancelOrderReasonsUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CancelOrderReasonModel> call, Throwable t) {
                Progress.stop();
                orderHistoryView.getCancelOrderReasonsUnSucess(activity.getString(R.string.server_error));
            }
        });
    }
}
