package com.deliverymyorder.mvp.track_order.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MerchantBuissnessModel {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("merchant_id")
    @Expose
    private Integer merchantId;
    @SerializedName("campus_id")
    @Expose
    private Object campusId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("gstin")
    @Expose
    private Object gstin;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("work_hours_from")
    @Expose
    private Object workHoursFrom;
    @SerializedName("work_hours_to")
    @Expose
    private Object workHoursTo;
    @SerializedName("country_id")
    @Expose
    private Integer countryId;
    @SerializedName("state_id")
    @Expose
    private Integer stateId;
    @SerializedName("city_id")
    @Expose
    private Integer cityId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("custom_order_allowed")
    @Expose
    private Integer customOrderAllowed;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Object getCampusId() {
        return campusId;
    }

    public void setCampusId(Object campusId) {
        this.campusId = campusId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getGstin() {
        return gstin;
    }

    public void setGstin(Object gstin) {
        this.gstin = gstin;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getWorkHoursFrom() {
        return workHoursFrom;
    }

    public void setWorkHoursFrom(Object workHoursFrom) {
        this.workHoursFrom = workHoursFrom;
    }

    public Object getWorkHoursTo() {
        return workHoursTo;
    }

    public void setWorkHoursTo(Object workHoursTo) {
        this.workHoursTo = workHoursTo;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getCustomOrderAllowed() {
        return customOrderAllowed;
    }

    public void setCustomOrderAllowed(Integer customOrderAllowed) {
        this.customOrderAllowed = customOrderAllowed;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
