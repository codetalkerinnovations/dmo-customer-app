package com.deliverymyorder.mvp.track_order.presenter;

import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;

public interface TrackOrderContractor {

    interface TrackOrderPresenter{

        void callTrackOrderApi(String orderId);

    }

    interface TrackOrderView{

        void onSuccessTrackOrderDetail(TrackOrderDetailModel.Data trackOrderDetailModel);
        void onUnSuccessTrackOrderDetail(String message);
        void onInternetErrorTrackOrderDetail();

    }



}
