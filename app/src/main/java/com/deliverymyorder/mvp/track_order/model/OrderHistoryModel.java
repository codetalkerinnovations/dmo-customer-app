package com.deliverymyorder.mvp.track_order.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;
import android.util.Log;

import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Braintech on 26-12-2018.
 */

public class OrderHistoryModel implements Serializable {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Data implements Serializable{

        @SerializedName("ongoing")
        @Expose
        private ArrayList<Ongoing> ongoing = null;
        @SerializedName("previous")
        @Expose
        private ArrayList<Previous> previous = null;
        @SerializedName("canceled")
        @Expose
        private ArrayList<Canceled> canceled = null;


        // ongoing_PickAndDropOrders
        @SerializedName("ongoing_PickAndDropOrders")
        @Expose
        private ArrayList<PickAndDropOnGoingData> pickAndDropOnGoing = null;

        @SerializedName("previous_PickAndDropOrders")
        @Expose
        private ArrayList<PickAndDropOnGoingData> pickAndDropOnPrevious= null;

        public ArrayList<PickAndDropOnGoingData> getPickAndDropOnPrevious() {
            return pickAndDropOnPrevious;
        }

        public void setPickAndDropOnPrevious(ArrayList<PickAndDropOnGoingData> pickAndDropOnPrevious) {
            this.pickAndDropOnPrevious = pickAndDropOnPrevious;
        }

        public ArrayList<PickAndDropOnGoingData> getPickAndDropOnGoing() {
            return pickAndDropOnGoing;
        }

        public void setPickAndDropOnGoing(ArrayList<PickAndDropOnGoingData> pickAndDropOnGoing) {
            this.pickAndDropOnGoing = pickAndDropOnGoing;
        }

        public ArrayList<Ongoing> getOngoing() {
            return ongoing;
        }

        public void setOngoing(ArrayList<Ongoing> ongoing) {
            this.ongoing = ongoing;
        }

        public ArrayList<Previous> getPrevious() {
            return previous;
        }

        public void setPrevious(ArrayList<Previous> previous) {
            this.previous = previous;
        }

        public ArrayList<Canceled> getCanceled() {
            return canceled;
        }

        public void setCanceled(ArrayList<Canceled> canceled) {
            this.canceled = canceled;
        }


        public class Canceled implements Serializable {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("customer_id")
            @Expose
            private Integer customerId;
            @SerializedName("merchant_id")
            @Expose
            private Integer merchantId;
            @SerializedName("business_id")
            @Expose
            private Object businessId;
            @SerializedName("address_id")
            @Expose
            private Integer addressId;
            @SerializedName("invoice_no")
            @Expose
            private String invoiceNo;
            @SerializedName("total_items")
            @Expose
            private Integer totalItems;
            @SerializedName("total_amount")
            @Expose
            private Integer totalAmount;
            @SerializedName("status")
            @Expose
            private Integer status;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("shipping_address")
            @Expose
            private ShippingAddress__ shippingAddress;
            @SerializedName("merchant")
            @Expose
            private Merchant__ merchant;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getCustomerId() {
                return customerId;
            }

            public void setCustomerId(Integer customerId) {
                this.customerId = customerId;
            }

            public Integer getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(Integer merchantId) {
                this.merchantId = merchantId;
            }

            public Object getBusinessId() {
                return businessId;
            }

            public void setBusinessId(Object businessId) {
                this.businessId = businessId;
            }

            public Integer getAddressId() {
                return addressId;
            }

            public void setAddressId(Integer addressId) {
                this.addressId = addressId;
            }

            public String getInvoiceNo() {
                return invoiceNo;
            }

            public void setInvoiceNo(String invoiceNo) {
                this.invoiceNo = invoiceNo;
            }

            public Integer getTotalItems() {
                return totalItems;
            }

            public void setTotalItems(Integer totalItems) {
                this.totalItems = totalItems;
            }

            public Integer getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(Integer totalAmount) {
                this.totalAmount = totalAmount;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public ShippingAddress__ getShippingAddress() {
                return shippingAddress;
            }

            public void setShippingAddress(ShippingAddress__ shippingAddress) {
                this.shippingAddress = shippingAddress;
            }

            public Merchant__ getMerchant() {
                return merchant;
            }

            public void setMerchant(Merchant__ merchant) {
                this.merchant = merchant;
            }

            public class Merchant__ implements Serializable{

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("unique_id")
                @Expose
                private String uniqueId;
                @SerializedName("firstname")
                @Expose
                private String firstname;
                @SerializedName("lastname")
                @Expose
                private String lastname;
                @SerializedName("mobile")
                @Expose
                private String mobile;
                @SerializedName("email")
                @Expose
                private String email;
                @SerializedName("facebook_auth")
                @Expose
                private Integer facebookAuth;
                @SerializedName("facebook_id")
                @Expose
                private Object facebookId;
                @SerializedName("country_id")
                @Expose
                private Integer countryId;
                @SerializedName("state_id")
                @Expose
                private Integer stateId;
                @SerializedName("city_id")
                @Expose
                private Integer cityId;
                @SerializedName("address")
                @Expose
                private String address;
                @SerializedName("otp_code")
                @Expose
                private Object otpCode;
                @SerializedName("status")
                @Expose
                private Integer status;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getUniqueId() {
                    return uniqueId;
                }

                public void setUniqueId(String uniqueId) {
                    this.uniqueId = uniqueId;
                }

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getMobile() {
                    return mobile;
                }

                public void setMobile(String mobile) {
                    this.mobile = mobile;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public Integer getFacebookAuth() {
                    return facebookAuth;
                }

                public void setFacebookAuth(Integer facebookAuth) {
                    this.facebookAuth = facebookAuth;
                }

                public Object getFacebookId() {
                    return facebookId;
                }

                public void setFacebookId(Object facebookId) {
                    this.facebookId = facebookId;
                }

                public Integer getCountryId() {
                    return countryId;
                }

                public void setCountryId(Integer countryId) {
                    this.countryId = countryId;
                }

                public Integer getStateId() {
                    return stateId;
                }

                public void setStateId(Integer stateId) {
                    this.stateId = stateId;
                }

                public Integer getCityId() {
                    return cityId;
                }

                public void setCityId(Integer cityId) {
                    this.cityId = cityId;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public Object getOtpCode() {
                    return otpCode;
                }

                public void setOtpCode(Object otpCode) {
                    this.otpCode = otpCode;
                }

                public Integer getStatus() {
                    return status;
                }

                public void setStatus(Integer status) {
                    this.status = status;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

            }


            public class ShippingAddress__ implements Serializable{

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("customer_id")
                @Expose
                private Integer customerId;
                @SerializedName("name")
                @Expose
                private String name;
                @SerializedName("street")
                @Expose
                private String street;
                @SerializedName("area_locality")
                @Expose
                private String areaLocality;
                @SerializedName("landmark")
                @Expose
                private String landmark;
                @SerializedName("country_id")
                @Expose
                private Integer countryId;
                @SerializedName("state_id")
                @Expose
                private Integer stateId;
                @SerializedName("city_id")
                @Expose
                private Integer cityId;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public Integer getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(Integer customerId) {
                    this.customerId = customerId;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getStreet() {
                    return street;
                }

                public void setStreet(String street) {
                    this.street = street;
                }

                public String getAreaLocality() {
                    return areaLocality;
                }

                public void setAreaLocality(String areaLocality) {
                    this.areaLocality = areaLocality;
                }

                public String getLandmark() {
                    return landmark;
                }

                public void setLandmark(String landmark) {
                    this.landmark = landmark;
                }

                public Integer getCountryId() {
                    return countryId;
                }

                public void setCountryId(Integer countryId) {
                    this.countryId = countryId;
                }

                public Integer getStateId() {
                    return stateId;
                }

                public void setStateId(Integer stateId) {
                    this.stateId = stateId;
                }

                public Integer getCityId() {
                    return cityId;
                }

                public void setCityId(Integer cityId) {
                    this.cityId = cityId;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

            }


        }


        public class Ongoing implements Serializable{

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("customer_id")
            @Expose
            private Integer customerId;
            @SerializedName("merchant_id")
            @Expose
            private Integer merchantId;
            @SerializedName("business_id")
            @Expose
            private Object businessId;
            @SerializedName("address_id")
            @Expose
            private Integer addressId;
            @SerializedName("invoice_no")
            @Expose
            private String invoiceNo;
            @SerializedName("total_items")
            @Expose
            private Integer totalItems;
            @SerializedName("total_amount")
            @Expose
            private Double totalAmount;
            @SerializedName("status")
            @Expose
            private Integer status;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("shipping_address")
            @Expose
            private ShippingAddress shippingAddress;
            @SerializedName("merchant")
            @Expose
            private Merchant merchant;

            @SerializedName("delivery_partner")
            @Expose
            private DeliveryBoy deliveryBoy;

            @SerializedName("merchant_business")
            @Expose
            private MerchantBuissnessModel merchantBuissness;

            public MerchantBuissnessModel getMerchantBuissness() {
                return merchantBuissness;
            }

            public void setMerchantBuissness(MerchantBuissnessModel merchantBuissness) {
                this.merchantBuissness = merchantBuissness;
            }


            //MerchantBuissnessModel

            public DeliveryBoy getDeliveryBoy() {
                return deliveryBoy;
            }

            public void setDeliveryBoy(DeliveryBoy deliveryBoy) {
                this.deliveryBoy = deliveryBoy;
            }

            public Date getCreateDate()
            {

                Date convertedDate = new Date();
                String stringDateFormat = createdAt; //2019-01-03 21:32:41
                SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                try {
                     convertedDate = (Date) formatter.parse(stringDateFormat);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                return convertedDate;
            }



            private  long getDateInMillis() {

                    long dateInMillis = 0;

                    Date date = getCreateDate();
                    dateInMillis = date.getTime();
                    return dateInMillis;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getCustomerId() {
                return customerId;
            }

            public void setCustomerId(Integer customerId) {
                this.customerId = customerId;
            }

            public Integer getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(Integer merchantId) {
                this.merchantId = merchantId;
            }

            public Object getBusinessId() {
                return businessId;
            }

            public void setBusinessId(Object businessId) {
                this.businessId = businessId;
            }

            public Integer getAddressId() {
                return addressId;
            }

            public void setAddressId(Integer addressId) {
                this.addressId = addressId;
            }

            public String getInvoiceNo() {
                return invoiceNo;
            }

            public void setInvoiceNo(String invoiceNo) {
                this.invoiceNo = invoiceNo;
            }

            public Integer getTotalItems() {
                return totalItems;
            }

            public void setTotalItems(Integer totalItems) {
                this.totalItems = totalItems;
            }

            public Double getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(Double totalAmount) {
                this.totalAmount = totalAmount;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public ShippingAddress getShippingAddress() {
                return shippingAddress;
            }

            public void setShippingAddress(ShippingAddress shippingAddress) {
                this.shippingAddress = shippingAddress;
            }

            public Merchant getMerchant() {
                return merchant;
            }

            public void setMerchant(Merchant merchant) {
                this.merchant = merchant;
            }

            public class Merchant implements Serializable {

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("unique_id")
                @Expose
                private String uniqueId;
                @SerializedName("firstname")
                @Expose
                private String firstname;
                @SerializedName("lastname")
                @Expose
                private String lastname;
                @SerializedName("mobile")
                @Expose
                private String mobile;
                @SerializedName("email")
                @Expose
                private String email;
                @SerializedName("facebook_auth")
                @Expose
                private Integer facebookAuth;
                @SerializedName("facebook_id")
                @Expose
                private Object facebookId;
                @SerializedName("country_id")
                @Expose
                private Integer countryId;
                @SerializedName("state_id")
                @Expose
                private Integer stateId;
                @SerializedName("city_id")
                @Expose
                private Integer cityId;
                @SerializedName("address")
                @Expose
                private String address;
                @SerializedName("otp_code")
                @Expose
                private Object otpCode;
                @SerializedName("status")
                @Expose
                private Integer status;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getUniqueId() {
                    return uniqueId;
                }

                public void setUniqueId(String uniqueId) {
                    this.uniqueId = uniqueId;
                }

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getMobile() {
                    return mobile;
                }

                public void setMobile(String mobile) {
                    this.mobile = mobile;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public Integer getFacebookAuth() {
                    return facebookAuth;
                }

                public void setFacebookAuth(Integer facebookAuth) {
                    this.facebookAuth = facebookAuth;
                }

                public Object getFacebookId() {
                    return facebookId;
                }

                public void setFacebookId(Object facebookId) {
                    this.facebookId = facebookId;
                }

                public Integer getCountryId() {
                    return countryId;
                }

                public void setCountryId(Integer countryId) {
                    this.countryId = countryId;
                }

                public Integer getStateId() {
                    return stateId;
                }

                public void setStateId(Integer stateId) {
                    this.stateId = stateId;
                }

                public Integer getCityId() {
                    return cityId;
                }

                public void setCityId(Integer cityId) {
                    this.cityId = cityId;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public Object getOtpCode() {
                    return otpCode;
                }

                public void setOtpCode(Object otpCode) {
                    this.otpCode = otpCode;
                }

                public Integer getStatus() {
                    return status;
                }

                public void setStatus(Integer status) {
                    this.status = status;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

            }

            public class ShippingAddress implements Serializable {

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("customer_id")
                @Expose
                private Integer customerId;
                @SerializedName("name")
                @Expose
                private String name;
                @SerializedName("street")
                @Expose
                private String street;
                @SerializedName("area_locality")
                @Expose
                private String areaLocality;
                @SerializedName("landmark")
                @Expose
                private String landmark;
                @SerializedName("country_id")
                @Expose
                private Integer countryId;
                @SerializedName("state_id")
                @Expose
                private Integer stateId;
                @SerializedName("city_id")
                @Expose
                private Integer cityId;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public Integer getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(Integer customerId) {
                    this.customerId = customerId;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getStreet() {
                    return street;
                }

                public void setStreet(String street) {
                    this.street = street;
                }

                public String getAreaLocality() {
                    return areaLocality;
                }

                public void setAreaLocality(String areaLocality) {
                    this.areaLocality = areaLocality;
                }

                public String getLandmark() {
                    return landmark;
                }

                public void setLandmark(String landmark) {
                    this.landmark = landmark;
                }

                public Integer getCountryId() {
                    return countryId;
                }

                public void setCountryId(Integer countryId) {
                    this.countryId = countryId;
                }

                public Integer getStateId() {
                    return stateId;
                }

                public void setStateId(Integer stateId) {
                    this.stateId = stateId;
                }

                public Integer getCityId() {
                    return cityId;
                }

                public void setCityId(Integer cityId) {
                    this.cityId = cityId;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

            }

        }

        public class Previous implements Serializable {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("customer_id")
            @Expose
            private Integer customerId;
            @SerializedName("merchant_id")
            @Expose
            private Integer merchantId;
            @SerializedName("delivery_partner_id")
            @Expose
            private Integer delivery_partner_id;
            @SerializedName("business_id")
            @Expose
            private String businessId;
            @SerializedName("address_id")
            @Expose
            private Integer addressId;
            @SerializedName("invoice_no")
            @Expose
            private String invoiceNo;
            @SerializedName("total_items")
            @Expose
            private Integer totalItems;
            @SerializedName("total_amount")
            @Expose
            private Double totalAmount;
            @SerializedName("status")
            @Expose
            private Integer status;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("shipping_address")
            @Expose
            private ShippingAddress_ shippingAddress;
            @SerializedName("merchant")
            @Expose
            private Merchant_ merchant;

            @SerializedName("order_details")
            @Expose
            private ArrayList<OrderDetail> orderDetails = null;

            @SerializedName("merchant_business")
            @Expose
            private MerchantBuissnessModel merchantBuissness;

            public MerchantBuissnessModel getMerchantBuissness() {
                return merchantBuissness;
            }

            public void setMerchantBuissness(MerchantBuissnessModel merchantBuissness) {
                this.merchantBuissness = merchantBuissness;
            }

            public Date getCreateDate()
            {

                Date convertedDate = new Date();
                String stringDateFormat = createdAt; //2019-01-03 21:32:41
                SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                try {
                    convertedDate = (Date) formatter.parse(stringDateFormat);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                return convertedDate;
            }


            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getCustomerId() {
                return customerId;
            }

            public void setCustomerId(Integer customerId) {
                this.customerId = customerId;
            }

            public Integer getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(Integer merchantId) {
                this.merchantId = merchantId;
            }

            public String getBusinessId() {
                return businessId;
            }

            public void setBusinessId(String businessId) {
                this.businessId = businessId;
            }

            public Integer getAddressId() {
                return addressId;
            }

            public void setAddressId(Integer addressId) {
                this.addressId = addressId;
            }

            public String getInvoiceNo() {
                return invoiceNo;
            }

            public void setInvoiceNo(String invoiceNo) {
                this.invoiceNo = invoiceNo;
            }

            public Integer getTotalItems() {
                return totalItems;
            }

            public void setTotalItems(Integer totalItems) {
                this.totalItems = totalItems;
            }

            public Double getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(Double totalAmount) {
                this.totalAmount = totalAmount;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public ShippingAddress_ getShippingAddress() {
                return shippingAddress;
            }

            public void setShippingAddress(ShippingAddress_ shippingAddress) {
                this.shippingAddress = shippingAddress;
            }

            public Merchant_ getMerchant() {
                return merchant;
            }

            public void setMerchant(Merchant_ merchant) {
                this.merchant = merchant;
            }

            public Integer getDelivery_partner_id() {
                return delivery_partner_id;
            }

            public void setDelivery_partner_id(Integer delivery_partner_id) {
                this.delivery_partner_id = delivery_partner_id;
            }
            public ArrayList<OrderDetail> getOrderDetails() {
                return orderDetails;
            }

            public void setOrderDetails(ArrayList<OrderDetail> orderDetails) {
                this.orderDetails = orderDetails;
            }

            public class Merchant_  implements Serializable{

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("unique_id")
                @Expose
                private String uniqueId;
                @SerializedName("firstname")
                @Expose
                private String firstname;
                @SerializedName("lastname")
                @Expose
                private String lastname;
                @SerializedName("mobile")
                @Expose
                private String mobile;
                @SerializedName("email")
                @Expose
                private String email;
                @SerializedName("facebook_auth")
                @Expose
                private Integer facebookAuth;
                @SerializedName("facebook_id")
                @Expose
                private Object facebookId;
                @SerializedName("country_id")
                @Expose
                private Integer countryId;
                @SerializedName("state_id")
                @Expose
                private Integer stateId;
                @SerializedName("city_id")
                @Expose
                private Integer cityId;
                @SerializedName("address")
                @Expose
                private String address;
                @SerializedName("otp_code")
                @Expose
                private Object otpCode;
                @SerializedName("status")
                @Expose
                private Integer status;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getUniqueId() {
                    return uniqueId;
                }

                public void setUniqueId(String uniqueId) {
                    this.uniqueId = uniqueId;
                }

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getMobile() {
                    return mobile;
                }

                public void setMobile(String mobile) {
                    this.mobile = mobile;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public Integer getFacebookAuth() {
                    return facebookAuth;
                }

                public void setFacebookAuth(Integer facebookAuth) {
                    this.facebookAuth = facebookAuth;
                }

                public Object getFacebookId() {
                    return facebookId;
                }

                public void setFacebookId(Object facebookId) {
                    this.facebookId = facebookId;
                }

                public Integer getCountryId() {
                    return countryId;
                }

                public void setCountryId(Integer countryId) {
                    this.countryId = countryId;
                }

                public Integer getStateId() {
                    return stateId;
                }

                public void setStateId(Integer stateId) {
                    this.stateId = stateId;
                }

                public Integer getCityId() {
                    return cityId;
                }

                public void setCityId(Integer cityId) {
                    this.cityId = cityId;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public Object getOtpCode() {
                    return otpCode;
                }

                public void setOtpCode(Object otpCode) {
                    this.otpCode = otpCode;
                }

                public Integer getStatus() {
                    return status;
                }

                public void setStatus(Integer status) {
                    this.status = status;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

            }

            public class ShippingAddress_ implements Serializable {

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("customer_id")
                @Expose
                private Integer customerId;
                @SerializedName("name")
                @Expose
                private String name;
                @SerializedName("street")
                @Expose
                private String street;
                @SerializedName("area_locality")
                @Expose
                private String areaLocality;
                @SerializedName("landmark")
                @Expose
                private String landmark;
                @SerializedName("country_id")
                @Expose
                private Integer countryId;
                @SerializedName("state_id")
                @Expose
                private Integer stateId;
                @SerializedName("city_id")
                @Expose
                private Integer cityId;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public Integer getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(Integer customerId) {
                    this.customerId = customerId;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getStreet() {
                    return street;
                }

                public void setStreet(String street) {
                    this.street = street;
                }

                public String getAreaLocality() {
                    return areaLocality;
                }

                public void setAreaLocality(String areaLocality) {
                    this.areaLocality = areaLocality;
                }

                public String getLandmark() {
                    return landmark;
                }

                public void setLandmark(String landmark) {
                    this.landmark = landmark;
                }

                public Integer getCountryId() {
                    return countryId;
                }

                public void setCountryId(Integer countryId) {
                    this.countryId = countryId;
                }

                public Integer getStateId() {
                    return stateId;
                }

                public void setStateId(Integer stateId) {
                    this.stateId = stateId;
                }

                public Integer getCityId() {
                    return cityId;
                }

                public void setCityId(Integer cityId) {
                    this.cityId = cityId;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

            }

            public class OrderDetail implements Serializable{

                @SerializedName("id")
                @Expose
                private Integer id;
                @SerializedName("order_id")
                @Expose
                private Integer orderId;
                @SerializedName("product_id")
                @Expose
                private Integer productId;
                @SerializedName("qty")
                @Expose
                private Integer qty;
                @SerializedName("product_price")
                @Expose
                private Double productPrice;
                @SerializedName("total_amount")
                @Expose
                private String totalAmount;
                @SerializedName("delivered_status")
                @Expose
                private Integer deliveredStatus;
                @SerializedName("created_at")
                @Expose
                private String createdAt;
                @SerializedName("updated_at")
                @Expose
                private String updatedAt;

                @SerializedName("products")
                @Expose
                private  OrderProduct product;

                public OrderProduct getProduct() {
                    return product;
                }

                public void setProduct(OrderProduct product) {
                    this.product = product;
                }

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public Integer getOrderId() {
                    return orderId;
                }

                public void setOrderId(Integer orderId) {
                    this.orderId = orderId;
                }

                public Integer getProductId() {
                    return productId;
                }

                public void setProductId(Integer productId) {
                    this.productId = productId;
                }

                public Integer getQty() {
                    return qty;
                }

                public void setQty(Integer qty) {
                    this.qty = qty;
                }

                public Double getProductPrice() {
                    return productPrice;
                }

                public void setProductPrice(Double productPrice) {
                    this.productPrice = productPrice;
                }

                public String getTotalAmount() {
                    return totalAmount;
                }

                public void setTotalAmount(String totalAmount) {
                    this.totalAmount = totalAmount;
                }

                public Integer getDeliveredStatus() {
                    return deliveredStatus;
                }

                public void setDeliveredStatus(Integer deliveredStatus) {
                    this.deliveredStatus = deliveredStatus;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

            }

        }


    }


}
