package com.deliverymyorder.mvp.track_order;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_listing.adapter.ShoppingListAdapter;
import com.deliverymyorder.mvp.track_order.adapter.CancelReasonAdapter;
import com.deliverymyorder.mvp.track_order.adapter.OngoingOrderAdapter;
import com.deliverymyorder.mvp.track_order.adapter.PreviousOrderAdapter;
import com.deliverymyorder.mvp.track_order.model.CancelOrderModel;
import com.deliverymyorder.mvp.track_order.model.CancelOrderReasonModel;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.ReOrderModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;
import com.deliverymyorder.mvp.track_order.presenter.OrderHistoryContractor;
import com.deliverymyorder.mvp.track_order.presenter.OrderHistoryPresenterImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TrackOrderFragment extends Fragment implements OrderHistoryContractor.OrderHistoryView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.linLayOngoingOrder)
    LinearLayout linLayOngoingOrder;

    @BindView(R.id.linLayPreviousOrder)
    LinearLayout linLayPreviousOrder;

    @BindView(R.id.viewLineOngoingOrder)
    View viewLineOngoingOrder;

    @BindView(R.id.viewLinePreviousOrder)
    View viewLinePreviousOrder;

    @BindView(R.id.recyclerViewOngoingOrder)
    RecyclerView recyclerViewOngoingOrder;

    @BindView(R.id.recyclerViewPreviousOrder)
    RecyclerView recyclerViewPreviousOrder;


    OngoingOrderAdapter ongoingOrderAdapter;
    PreviousOrderAdapter previousOrderAdapter;
    CancelReasonAdapter cancelReasonAdapter;

    ArrayList<OrderHistoryModel.Data.Ongoing> arrayListOnGoingOrder;
    ArrayList<PickAndDropOnGoingData> arrayListOnGoingPickAndDroOrder;

    ArrayList<OrderHistoryModel.Data.Previous> arrayListPreviousOrder;
    ArrayList<PickAndDropOnGoingData> arrayListPrivousPickAndDroOrder;


    OrderHistoryPresenterImpl orderHistoryPresenter;
    int selectedCancelReasonId;
    int orderId;
    int orderType;

    Dialog dialog;

    ArrayList<CancelOrderReasonModel.Datum> arrayListCancelOrderReasonModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_track_order, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        orderHistoryPresenter = new OrderHistoryPresenterImpl(getActivity(), this);



    }

    @Override
    public void onStart() {
        super.onStart();
        getOrderHistoryDataApi();
    }

    public void getAllCancelreasonDataApi(int orderId, int orderType) {
        this.orderId = orderId;
        this.orderType = orderType;
        orderHistoryPresenter.getCancelOrderReasonsData();
    }


    public void updateCancelReasonDataApi() {
        orderHistoryPresenter.updateCancelOrderReasonsData(orderId, selectedCancelReasonId,orderType);
    }


    public void callReOrderDataApi(int orderId, int orderType) {
        this.orderId = orderId;
        this.orderType = orderType;

        orderHistoryPresenter.reOrderData(orderId);
    }


    public void selectedCancelReason(int selectedCancelReasonId) {
        this.selectedCancelReasonId = selectedCancelReasonId;
    }


    private void manageRecyclerViewOngoingOrder() {

        ArrayList<Object> ongingOrderList = new ArrayList<>();
        ongingOrderList.addAll(arrayListOnGoingOrder);
        ongingOrderList.addAll(arrayListOnGoingPickAndDroOrder);

//
        Collections.sort(ongingOrderList, new Comparator() {

            public int compare(Object o2, Object o1) {

                Date date2 = null;
                Date date1 = null;

                if (o1 instanceof OrderHistoryModel.Data.Ongoing)
                {
                    OrderHistoryModel.Data.Ongoing model = (OrderHistoryModel.Data.Ongoing) o1;
                    date1 = model.getCreateDate();

                }else if (o1 instanceof PickAndDropOnGoingData)
                {
                    PickAndDropOnGoingData model = (PickAndDropOnGoingData) o1;
                    date1 = model.getCreateDate();
                }


                // Obje 2
                if (o2 instanceof OrderHistoryModel.Data.Ongoing)
                {
                    OrderHistoryModel.Data.Ongoing model = (OrderHistoryModel.Data.Ongoing) o2;
                    date2 = model.getCreateDate();

                }else if (o2 instanceof PickAndDropOnGoingData)
                {
                    PickAndDropOnGoingData model = (PickAndDropOnGoingData) o2;
                    date2 = model.getCreateDate();
                }



//                String x1 =  o1.Date;
//                String x2 =  o2.Date;

                return  date1.compareTo(date2);

            }
        });


        // Short by Date
//        Collections.sort(ongingOrderList, new Comparator() {
//            public int compare(MyObject o1, MyObject o2) {
//                return o1.getDateTime().compareTo(o2.getDateTime());
//            }
//        });



        ongoingOrderAdapter = new OngoingOrderAdapter(getActivity(), ongingOrderList, this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewOngoingOrder.setLayoutManager(mLayoutManager);
        recyclerViewOngoingOrder.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOngoingOrder.setAdapter(ongoingOrderAdapter);
    }

    private void manageRecyclerViewPreviousorder() {

        ArrayList<Object> ongingOrderList = new ArrayList<>();
        ongingOrderList.addAll(arrayListPreviousOrder);
        ongingOrderList.addAll(arrayListPrivousPickAndDroOrder);

//
        Collections.sort(ongingOrderList, new Comparator() {

            public int compare(Object o2, Object o1) {

                Date date2 = null;
                Date date1 = null;

                if (o1 instanceof OrderHistoryModel.Data.Previous)
                {
                    OrderHistoryModel.Data.Previous model = (OrderHistoryModel.Data.Previous) o1;
                    date1 = model.getCreateDate();

                }else if (o1 instanceof PickAndDropOnGoingData)
                {
                    PickAndDropOnGoingData model = (PickAndDropOnGoingData) o1;
                    date1 = model.getCreateDate();
                }


                // Obje 2
                if (o2 instanceof OrderHistoryModel.Data.Previous)
                {
                    OrderHistoryModel.Data.Previous model = (OrderHistoryModel.Data.Previous) o2;
                    date2 = model.getCreateDate();

                }else if (o2 instanceof PickAndDropOnGoingData)
                {
                    PickAndDropOnGoingData model = (PickAndDropOnGoingData) o2;
                    date2 = model.getCreateDate();
                }

                return  date1.compareTo(date2);

            }
        });

        previousOrderAdapter = new PreviousOrderAdapter(getActivity(), ongingOrderList,this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewPreviousOrder.setLayoutManager(mLayoutManager);
        recyclerViewPreviousOrder.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPreviousOrder.setAdapter(previousOrderAdapter);
    }

    private void getOrderHistoryDataApi() {
        orderHistoryPresenter.getOrderHistoryData();
    }

    @Override
    public void getOrderHistoryDataSuccess(OrderHistoryModel.Data orderHistoryModel) {

        arrayListOnGoingOrder = orderHistoryModel.getOngoing();
        arrayListPreviousOrder = orderHistoryModel.getPrevious();

        if (arrayListPreviousOrder!=null)
        {
            Collections.reverse(arrayListPreviousOrder);
        }

        arrayListOnGoingPickAndDroOrder = orderHistoryModel.getPickAndDropOnGoing();
        arrayListPrivousPickAndDroOrder= orderHistoryModel.getPickAndDropOnPrevious();
        manageRecyclerViewOngoingOrder();
        manageRecyclerViewPreviousorder();


    }

    public void openDialog() {
        dialog = new Dialog(getActivity()); // Context, this, etc.
        dialog.setContentView(R.layout.dialog_order_cancel_reason);
        dialog.setTitle("Why canceling order ?");

        RelativeLayout relLayItem = (RelativeLayout) dialog.findViewById(R.id.relLayItem);
        RecyclerView recyclerViewCancelReason = (RecyclerView) dialog.findViewById(R.id.recyclerViewCancelReason);
        TextView txtViewContinue = (TextView) dialog.findViewById(R.id.txtViewContinue);

        cancelReasonAdapter = new CancelReasonAdapter(getActivity(), arrayListCancelOrderReasonModel, this);

        recyclerViewCancelReason.setLayoutManager(new LinearLayoutManager(getActivity()));
        // RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        //abcd.setLayoutManager(mLayoutManager);
        recyclerViewCancelReason.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCancelReason.setAdapter(cancelReasonAdapter);


        txtViewContinue.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                updateCancelReasonDataApi();
                // dialog.dismiss();
            }
        });


        dialog.show();
    }


    @Override
    public void getOrderHistoryDataUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void OrderHistoryDataInternetError() {

    }

    @Override
    public void getCancelOrderReasonsSuccess(ArrayList<CancelOrderReasonModel.Datum> arrayListCancelOrderReasonModel) {

        this.arrayListCancelOrderReasonModel = arrayListCancelOrderReasonModel;
        openDialog();
    }

    @Override
    public void getCancelOrderReasonsUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void cancelOrderReasonsInternetError() {

    }

    @Override
    public void updateCancelOrderReasonsSuccess(CancelOrderModel.Data cancelOrderModel) {

        dialog.dismiss();

        getOrderHistoryDataApi();
    }

    @Override
    public void updateCancelOrderReasonsUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void updateCancelOrderReasonsInternetError() {

    }

    @Override
    public void reOrderApiSuccess(ReOrderModel reOrderModel) {

        SnackNotify.showMessage(reOrderModel.getMessage(),relLayMain);
    }

    @Override
    public void reOrderApiUnSucess(String message) {
        SnackNotify.showMessage(message,relLayMain);
    }

    @Override
    public void reOrderApiInternetError() {

    }

    @OnClick(R.id.linLayOngoingOrder)
    public void linLayOngoingOrderClicked() {

        viewLineOngoingOrder.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_yellow));
        viewLinePreviousOrder.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.themeBackgroundColor));

        recyclerViewOngoingOrder.setVisibility(View.VISIBLE);
        recyclerViewPreviousOrder.setVisibility(View.GONE);
    }


    @OnClick(R.id.linLayPreviousOrder)
    public void llinLayPreviousOrderClicked() {

        viewLineOngoingOrder.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.themeBackgroundColor));
        viewLinePreviousOrder.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_yellow));

        recyclerViewOngoingOrder.setVisibility(View.GONE);
        recyclerViewPreviousOrder.setVisibility(View.VISIBLE);
    }
}
