package com.deliverymyorder.mvp.delivery_type;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.adapter.AddCategoryAdapter;
import com.deliverymyorder.mvp.address.AddAddressActivity;
import com.deliverymyorder.mvp.address.AddressListActivity;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.address.presenter.AddressDetailContractor;
import com.deliverymyorder.mvp.address.presenter.AddressDetailPresenterImpl;
import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.model.UpdateDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.presenter.DeliveryTypeContractor;
import com.deliverymyorder.mvp.delivery_type.presenter.DeliveryTypePresenterImpl;
import com.deliverymyorder.mvp.order_success.OrderConfirmedActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeliveryTypeActivity extends AppCompatActivity implements DeliveryTypeContractor.DeliveryTypeView, AddressDetailContractor.AddressDetailView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.recylerViewDeliveryType)
    RecyclerView recylerViewDeliveryType;

    @BindView(R.id.imgViewEdit)
    ImageView imgViewEdit;

    @BindView(R.id.imgViewAdd)
    ImageView imgViewAdd;

    @BindView(R.id.txtViewConfirm)
    TextView txtViewConfirm;

    @BindView(R.id.txtViewDeliveryTo)
    TextView txtViewDeliveryTo;

    DeliveryTypePresenterImpl deliveryTypePresenter;
    AddressDetailPresenterImpl addressDetailPresenter;
    ArrayList<GetDeliveryTypeModel.Datum> arrayListGetDeliveryTypeModel;
    DeliveryTypeAdapter deliveryTypeAdapter;

    int deliveryTypeId;
    boolean isSubDeliveryTypeSelected;
    int orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_type);
        ButterKnife.bind(this);

        deliveryTypePresenter = new DeliveryTypePresenterImpl(this, this);
        addressDetailPresenter = new AddressDetailPresenterImpl(this, this);

        if (getIntent() != null) {

            orderId = getIntent().getIntExtra(ConstIntent.CONST_ORDER_ID, 0);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        callGetAddressDetailDataApi();
    }

    private void callGetAddressDetailDataApi() {

        int addressId = PrefUtil.getInt(this, Const.KEY_ADDRESS_ID,0);

        addressDetailPresenter.getAddressDetailData(addressId);
    }

    private void callGetDeliveryTypesDataApi() {

        deliveryTypePresenter.getDeliveryTypeData(orderId);
    }

    private void manageRecyclerView() {

        deliveryTypeAdapter = new DeliveryTypeAdapter(this, arrayListGetDeliveryTypeModel);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recylerViewDeliveryType.setLayoutManager(mLayoutManager);
        recylerViewDeliveryType.setItemAnimator(new DefaultItemAnimator());
        recylerViewDeliveryType.setAdapter(deliveryTypeAdapter);
    }

    public void setDeliverytypeId(int deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }

    public void setDeliverytypeSubId(boolean isSubDeliveryTypeSelected) {
        this.isSubDeliveryTypeSelected = isSubDeliveryTypeSelected;
    }

    @Override
    public void getDeliveryTypeSuccess(ArrayList<GetDeliveryTypeModel.Datum> arrayListGetDeliveryTypeModel) {

        this.arrayListGetDeliveryTypeModel = arrayListGetDeliveryTypeModel;
        manageRecyclerView();
    }

    @Override
    public void getDeliveryTypeUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getDeliveryTypeInternetError() {

    }

    @Override
    public void saveDeliveryTypeSuccess(UpdateDeliveryTypeModel.Data updateDeliveryTypeModel) {
        Intent intent = new Intent(this, OrderConfirmedActivity.class);
        startActivity(intent);
    }

    @Override
    public void saveDeliveryTypeUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void saveDeliveryTypeInternetError() {

    }

    @Override
    public void getAddressDetailSuccess(AddressDetailModel.Data addressDetailModel) {

        if (addressDetailModel != null) {

            txtViewConfirm.setEnabled(true);
            txtViewConfirm.setAlpha(1);

            txtViewDeliveryTo.setText(addressDetailModel.getName() + " , " + addressDetailModel.getAreaLocality() + " , " + addressDetailModel.getCity());
        } else {

            txtViewConfirm.setEnabled(false);
            txtViewConfirm.setAlpha(.5f);
        }

        callGetDeliveryTypesDataApi();
    }

    @Override
    public void getAddressDetailUnSucess(String message) {
        txtViewConfirm.setEnabled(false);
        txtViewConfirm.setAlpha(.5f);

        callGetDeliveryTypesDataApi();
    }

    @Override
    public void getAddressDetailInternetError() {

    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        finish();
    }

    @OnClick(R.id.imgViewEdit)
    public void imgViewEditClicked() {
        Intent intent = new Intent(this, AddressListActivity.class);
       // intent.putExtra(ConstIntent.CONST_IS_COME_FOR_ADD, false);
        startActivity(intent);
    }

    @OnClick(R.id.imgViewAdd)
    public void imgViewAddClicked() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra(ConstIntent.CONST_IS_COME_FOR_ADD, true);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirmClicked() {


       // if(isSubDeliveryTypeSelected) {

            deliveryTypePresenter.saveDeliveryTypeData(orderId, deliveryTypeId);
//        }
//        else{
//
//            SnackNotify.showMessage("Please confirm the condition.",relLayMain);
//        }
    }
}
