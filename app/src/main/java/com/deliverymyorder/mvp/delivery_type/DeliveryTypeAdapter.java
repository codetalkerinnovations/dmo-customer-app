package com.deliverymyorder.mvp.delivery_type;

import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.application.DeliverMyOrderApplication;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.adapter.CategoryAdapter;
import com.deliverymyorder.mvp.add_category.adapter.SubCategoryAdapter;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 28-12-2018.
 */

public class DeliveryTypeAdapter extends RecyclerView.Adapter<DeliveryTypeAdapter.Holder> {


    Activity activity;
    ArrayList<GetDeliveryTypeModel.Datum> datum;
    String favBit;

    public DeliveryTypeAdapter(Activity activity, ArrayList<GetDeliveryTypeModel.Datum> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_delivery_type, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final GetDeliveryTypeModel.Datum result = datum.get(position);


        holder.txtViewDeliveryType.setText(result.getTitle());
        holder.txtViewDeliveryPrice.setText("( Rs " + result.getTotalCost() + " )");



        switch (position)
        {


            case 0:

                if (result.isSelected()) {
                holder.radioButtonDeliveryType.setChecked(true);
                holder.radioDeliverySubType.setVisibility(View.VISIBLE);
                holder.txtDeliverySubType.setVisibility(View.VISIBLE);

            } else {
                    holder.radioButtonDeliveryType.setChecked(false);
                    holder.radioDeliverySubType.setVisibility(View.GONE);
                    holder.txtDeliverySubType.setVisibility(View.GONE);

            }

            break;

            case 1:

                Campus campus = LoginManager.getInstance().getCurrentCampus();
                if (campus!=null) {
                    if (result.isSelected()) {
                        holder.radioDeliverySubType.setChecked(true);
                        holder.radioDeliverySubType.setVisibility(View.VISIBLE);
                        holder.txtDeliverySubType.setVisibility(View.VISIBLE);

                    } else {
                        holder.radioButtonDeliveryType.setChecked(false);

                        holder.radioDeliverySubType.setVisibility(View.GONE);
                        holder.txtDeliverySubType.setVisibility(View.GONE);
                    }
                }else
                {
//                    holder.radioDeliverySubType.setChecked(false);

                    holder.radioDeliverySubType.setVisibility(View.GONE);
                    holder.txtDeliverySubType.setVisibility(View.GONE);

                    if (result.isSelected()) {
                        holder.radioDeliverySubType.setChecked(true);

                    }else
                    {
                        holder.radioButtonDeliveryType.setChecked(false);
                    }
                }

                break;
        }


        if (result.isSubTypeSelected()) {
            holder.radioDeliverySubType.setChecked(true);
        } else {
            holder.radioDeliverySubType.setChecked(false);
        }

        if (position == 0)
        {
            holder.txtDeliverySubType.setText(" i'll be at home, deliver anytime today");
        }else if (position == 1)
        {
            holder.txtDeliverySubType.setText(" Want to share the fair, join wait list");
            holder.txtDeliverySubType.setVisibility(View.GONE);
            holder.radioDeliverySubType.setVisibility(View.GONE);

        }else if (position == 2)
        {
            holder.txtDeliverySubType.setText(" Want to share the fair, join wait list");
            holder.txtDeliverySubType.setVisibility(View.GONE);
            holder.radioDeliverySubType.setVisibility(View.GONE);
            holder.relLayItem.setAlpha(.5f);
            holder.relLayItem.setEnabled(false);
            holder.radioDeliverySubType.setEnabled(false);
            holder.radioButtonDeliveryType.setEnabled(false);
        }

        holder.radioButtonDeliveryType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (int i = 0; i < datum.size(); i++) {

                    datum.get(i).setSelected(false);
                }

                datum.get(position).setSelected(true);



                ((DeliveryTypeActivity) activity).setDeliverytypeId(result.getId());

                if(position==0) {
                    ((DeliveryTypeActivity) activity).setDeliverytypeSubId(false);
                }else{
                    ((DeliveryTypeActivity) activity).setDeliverytypeSubId(true);
                }

                notifyDataSetChanged();

            }
        });

        holder.radioDeliverySubType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (int i = 0; i < datum.size(); i++) {

                    datum.get(i).setSubTypeSelected(false);
                }

                datum.get(position).setSubTypeSelected(true);


                ((DeliveryTypeActivity) activity).setDeliverytypeSubId(true);

                notifyDataSetChanged();

            }
        });



        // Temo HIdden

        holder.radioDeliverySubType.setVisibility(View.GONE);
        holder.txtDeliverySubType.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.radioButtonDeliveryType)
        RadioButton radioButtonDeliveryType;

        @BindView(R.id.txtViewDeliveryType)
        TextView txtViewDeliveryType;

        @BindView(R.id.txtDeliverySubType)
        TextView txtDeliverySubType;

        @BindView(R.id.txtViewDeliveryPrice)
        TextView txtViewDeliveryPrice;


        @BindView(R.id.radioDeliverySubType)
        RadioButton radioDeliverySubType ;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}
