package com.deliverymyorder.mvp.delivery_type.presenter;

import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.model.UpdateDeliveryTypeModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Braintech on 28-12-2018.
 */

public class DeliveryTypeContractor {

    public interface DeliveryTypePresenter {

        void getDeliveryTypeData();

        void getDeliveryTypeData(int orderId);

        void saveDeliveryTypeData(int orderId, int deliveryTypeId);
    }

    public interface DeliveryTypeView {
        void getDeliveryTypeSuccess(ArrayList<GetDeliveryTypeModel.Datum> arrayListGetDeliveryTypeModel);

        void getDeliveryTypeUnSucess(String message);

        void getDeliveryTypeInternetError();

        void saveDeliveryTypeSuccess(UpdateDeliveryTypeModel.Data updateDeliveryTypeModel);

        void saveDeliveryTypeUnSucess(String message);

        void saveDeliveryTypeInternetError();
    }
}
