package com.deliverymyorder.mvp.delivery_type.presenter;

import android.app.Activity;
import android.util.Log;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.address.presenter.AddressContractor;
import com.deliverymyorder.mvp.address.presenter.AddressDetailContractor;
import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.model.UpdateDeliveryTypeModel;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 28-12-2018.
 */

public class DeliveryTypePresenterImpl implements DeliveryTypeContractor.DeliveryTypePresenter {

    Activity activity;
    DeliveryTypeContractor.DeliveryTypeView deliveryTypeView;

    public DeliveryTypePresenterImpl(Activity activity, DeliveryTypeContractor.DeliveryTypeView deliveryTypeView) {
        this.activity = activity;
        this.deliveryTypeView = deliveryTypeView;
    }


    @Override
    public void getDeliveryTypeData() {
        try {
            ApiAdapter.getInstance(activity);
            callGetDeliveryDataApi(0);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            deliveryTypeView.getDeliveryTypeInternetError();

        }
    }

    @Override
    public void getDeliveryTypeData(int orderId) {
        try {
            ApiAdapter.getInstance(activity);
            callGetDeliveryDataApi(orderId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            deliveryTypeView.getDeliveryTypeInternetError();

        }
    }

    @Override
    public void saveDeliveryTypeData(int orderId, int deliveryTypeId) {

        try {
            ApiAdapter.getInstance(activity);
            callSaveDeliveryDataApi(orderId, deliveryTypeId);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            deliveryTypeView.saveDeliveryTypeInternetError();
        }
    }


    private void callGetDeliveryDataApi(int orderId) {
        Progress.start(activity);

        int addressId= PrefUtil.getInt(activity,Const.KEY_ADDRESS_ID,0);

        if (addressId == 0)
        {
            deliveryTypeView.getDeliveryTypeUnSucess("Please select delivery address");
            Progress.stop();
            return;
        }


        int campusId = 0;

        Boolean isInCampus = true;

        if (LoginManager.getInstance().getCurrentCampus() != null) {

            if (LoginManager.getInstance().getCurrentCampus().getId() != null) {
                campusId = LoginManager.getInstance().getCurrentCampus().getId();
                isInCampus = LoginManager.getInstance().isInCampus();
            }
        }




        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_ADDRESS_ID, addressId);

            if (orderId!=0) {
                jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            }

            if (campusId != 0) {

                jsonObject.put(Const.KEY_IN_CAMPUS, isInCampus == true ? 1:0);
                jsonObject.put(Const.KEY_CAPMUS_ID, campusId);

            }

            jsonObject.put(Const.PARAM_ORDER_TYPE,"1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<GetDeliveryTypeModel> getResult = ApiAdapter.getApiService().getDeliveryTypes("application/json", "no-cache", body);

        getResult.enqueue(new Callback<GetDeliveryTypeModel>() {
            @Override
            public void onResponse(Call<GetDeliveryTypeModel> call, Response<GetDeliveryTypeModel> response) {

                Progress.stop();
                try {
                    GetDeliveryTypeModel getDeliveryTypeModel = response.body();

                    if (getDeliveryTypeModel.getError() == 0) {
                        deliveryTypeView.getDeliveryTypeSuccess(getDeliveryTypeModel.getData());
                    } else {
                        deliveryTypeView.getDeliveryTypeUnSucess(getDeliveryTypeModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    deliveryTypeView.getDeliveryTypeUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<GetDeliveryTypeModel> call, Throwable t) {
                Progress.stop();
                deliveryTypeView.getDeliveryTypeUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

    private void callSaveDeliveryDataApi(int orderId, int deliveryTypeId) {
        Progress.start(activity);

        int addressId= PrefUtil.getInt(activity,Const.KEY_ADDRESS_ID,0);

        JSONObject jsonObject = new JSONObject();

       // Object obj = OrderSession.getSharedInstance().getCurrentOrder();

//        int orderType = 0;
//        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
//            orderType =1;
//        }else if (obj instanceof PickAndDropOnGoingData)
//        {
//            orderType =2;
//        }

        int campusId = 0;

        Boolean isInCampus = true;

        if (LoginManager.getInstance().getCurrentCampus() != null) {

            if (LoginManager.getInstance().getCurrentCampus().getId() != null) {
                campusId = LoginManager.getInstance().getCurrentCampus().getId();
                isInCampus = LoginManager.getInstance().isInCampus();
            }
        }


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            jsonObject.put(Const.PARAM_DELIVERY_TYPE_ID, deliveryTypeId);
            jsonObject.put(Const.KEY_ADDRESS_ID, addressId);
            jsonObject.put(Const.PARAM_ORDER_TYPE,"1");

            if (campusId != 0) {

                jsonObject.put(Const.KEY_IN_CAMPUS, isInCampus == true ? 1:0);
                jsonObject.put(Const.KEY_CAPMUS_ID, campusId);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<UpdateDeliveryTypeModel> getResult = ApiAdapter.getApiService().updateDeliveryTypes("application/json", "no-cache", body);

        getResult.enqueue(new Callback<UpdateDeliveryTypeModel>() {
            @Override
            public void onResponse(Call<UpdateDeliveryTypeModel> call, Response<UpdateDeliveryTypeModel> response) {

                Progress.stop();
                try {
                    UpdateDeliveryTypeModel updateDeliveryTypeModel = response.body();

                    if (updateDeliveryTypeModel.getError() == 0) {
                        deliveryTypeView.saveDeliveryTypeSuccess(updateDeliveryTypeModel.getData());
                    } else {
                        deliveryTypeView.saveDeliveryTypeUnSucess(updateDeliveryTypeModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    deliveryTypeView.saveDeliveryTypeUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateDeliveryTypeModel> call, Throwable t) {
                Progress.stop();
                deliveryTypeView.saveDeliveryTypeUnSucess(activity.getString(R.string.server_error));
            }
        });
    }



}