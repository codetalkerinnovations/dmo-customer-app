package com.deliverymyorder.mvp.recipt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.track_order.adapter.ReciptAdapter;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReciptActivity extends AppCompatActivity {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.recyclerViewItems)
    RecyclerView recyclerViewItems;

    @BindView(R.id.relLayDeliveryCharge)
    RelativeLayout relLayDeliveryCharge;

    @BindView(R.id.txtViewDeliveryChargeTitle)
    TextView txtViewDeliveryChargeTitle;

    @BindView(R.id.txtViewDeliveryCharge)
    TextView txtViewDeliveryCharge;

    @BindView(R.id.relLayDiscount)
    RelativeLayout relLayDiscount;

    @BindView(R.id.txtViewDiscountTitle)
    TextView txtViewDiscountTitle;

    @BindView(R.id.txtViewDiscount)
    TextView txtViewDiscount;

    @BindView(R.id.relLayExtraDiscount)
    RelativeLayout relLayExtraDiscount;

    @BindView(R.id.txtViewExtraDiscountTitle)
    TextView txtViewExtraDiscountTitle;

    @BindView(R.id.txtViewExtraDiscount)
    TextView txtViewExtraDiscount;

    @BindView(R.id.relLayTotal)
    RelativeLayout relLayTotal;

    @BindView(R.id.txtViewTotalTitle)
    TextView txtViewTotalTitle;

    @BindView(R.id.txtViewTotal)
    TextView txtViewTotal;


    ArrayList<OrderHistoryModel.Data.Previous.OrderDetail> arrayListOrderDetail;

    ReciptAdapter reciptAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipt);
        ButterKnife.bind(this);

        setFont();

        if (getIntent() != null) {
            arrayListOrderDetail = (ArrayList<OrderHistoryModel.Data.Previous.OrderDetail>) getIntent().getSerializableExtra(ConstIntent.CONST_ORDER_DETAIL_MODEL);

            manageRecyclerViewOngoingOrder();

            txtViewTotal.setText(arrayListOrderDetail.get(0).getTotalAmount());
        }
    }

    private void setFont() {
        FontHelper.setFontFace(txtViewDeliveryChargeTitle, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewDeliveryCharge, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewDiscountTitle, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewDiscount, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewExtraDiscountTitle, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewExtraDiscount, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewTotalTitle, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewTotal, FontHelper.FontType.FONT_REGULAR, this);
    }

    private void manageRecyclerViewOngoingOrder() {

        reciptAdapter = new ReciptAdapter(this, arrayListOrderDetail);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerViewItems.setLayoutManager(mLayoutManager);
        recyclerViewItems.setItemAnimator(new DefaultItemAnimator());
        recyclerViewItems.setAdapter(reciptAdapter);
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {
        finish();
    }
}
