package com.deliverymyorder.mvp.service_not_aviailble_page;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.login.presenter.LoginContractor;
import com.deliverymyorder.mvp.service_not_aviailble_page.model.CheckServiceModel;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckServiceAvailblityImpl implements CheckServiceAvailblityContractor.CheckServiceAvailblityPresenter {


    Activity activity;
    CheckServiceAvailblityContractor.CheckServiceAvailbityView checkServiceAvailbityView;

    private static final int RC_SIGN_IN = 9001;
    GoogleApiClient mGoogleApiClient;

    public CheckServiceAvailblityImpl(Activity activity, CheckServiceAvailblityContractor.CheckServiceAvailbityView checkServiceAvailbityView) {
        this.activity = activity;
        this.checkServiceAvailbityView = checkServiceAvailbityView;
    }

    @Override
    public void checkServiceAvailablity(String latitude, String longitude) {
        try {
            ApiAdapter.getInstance(activity);
            checkService(latitude,longitude);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            checkServiceAvailbityView.checkServiceAvaiblityNetworkError();

        }
    }

    private void checkService(String latitude, String longitude) {
       // Progress.start(activity);

        int campusId = 0;
//        String latitude = PrefUtil.getString(activity, Const.KEY_LATITUDE, "");
//        String longitude = PrefUtil.getString(activity, Const.KEY_LONGITUDE, "");


        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_LATITUDE, latitude);
            jsonObject.put(Const.KEY_LONGITUDE, longitude);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CheckServiceModel> getCreatePasswordResult = ApiAdapter.getApiService().checkServiceAvailblity("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<CheckServiceModel>() {
            @Override
            public void onResponse(Call<CheckServiceModel> call, Response<CheckServiceModel> response) {

               /// Progress.stop();
                try {
                    CheckServiceModel homePageDataModel = response.body();


                    if (homePageDataModel.getError() == 0) {
                        checkServiceAvailbityView.checkServiceAaiblityDidFinshWithResult(true);
                    } else {

                        checkServiceAvailbityView.checkServiceAaiblityDidFinshWithResult(false);
                    }
                } catch (NullPointerException exp) {

                    checkServiceAvailbityView.checkServiceAaiblityDidFinshWithResult(false);
                }
            }

            @Override
            public void onFailure(Call<CheckServiceModel> call, Throwable t) {
                Progress.stop();
                checkServiceAvailbityView.checkServiceAvaiblityNetworkError();
            }
        });

    }

}
