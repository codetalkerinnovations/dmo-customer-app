package com.deliverymyorder.mvp.service_not_aviailble_page;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LocationProvider;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.address.AddressListActivity;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;
import com.deliverymyorder.mvp.dashboard.fragment.DashboardFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceNotAvailableActivity extends AppCompatActivity implements CheckServiceAvailblityContractor.CheckServiceAvailbityView {


    CheckServiceAvailblityImpl checkServiceAvailblityImpl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_not_available2);

        ButterKnife.bind(this);

        checkServiceAvailblityImpl = new CheckServiceAvailblityImpl(this,this);


    }

    @Override
    protected void onRestart() {
        super.onRestart();


        try {
            String latitude = PrefUtil.getString(this, Const.KEY_LATITUDE, "");
            String longitude = PrefUtil.getString(this, Const.KEY_LONGITUDE, "");

            if (latitude != null && longitude != null && latitude.length() != 0 && longitude.length() != 0) {
                checkServiceAvailblityImpl.checkServiceAvailablity(latitude, longitude);
            } else {

                LocationProvider.getSharedInstacne().getLocation(this, new LocationProvider.LocationResult() {
                    @Override
                    public void gotLocation(Location location) {
                        checkServiceAvailblityImpl.checkServiceAvailablity(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                    }
                });
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            AlertDialogHelper.showMessage(this,"Something went wrong");
        }

    }

    @OnClick(R.id.btnRetry)
    public  void btnRetry(View view)
    {


        Intent i = new Intent(this,AddressListActivity.class);
        startActivity(i);
//        try {
//            String latitude = PrefUtil.getString(this, Const.KEY_LATITUDE, "");
//            String longitude = PrefUtil.getString(this, Const.KEY_LONGITUDE, "");
//
//            if (latitude != null && longitude != null && latitude.length() != 0 && longitude.length() != 0) {
//                checkServiceAvailblityImpl.checkServiceAvailablity(latitude, longitude);
//            } else {
//
//                LocationProvider.getSharedInstacne().getLocation(this, new LocationProvider.LocationResult() {
//                    @Override
//                    public void gotLocation(Location location) {
//                        checkServiceAvailblityImpl.checkServiceAvailablity(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
//                    }
//                });
//            }
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//            AlertDialogHelper.showMessage(this,"Something went wrong");
//        }
    }



    @Override
    public void checkServiceAaiblityDidFinshWithResult(boolean status) {

        try {

            if (status) {

                Intent i = new Intent(this,DashboardActivity.class);
                startActivity(i);
                finish();

            } else {
                AlertDialogHelper.showMessage(this, "Service not available");
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void checkServiceAvaiblityNetworkError() {
        AlertDialogHelper.showMessage(this,getResources().getString(R.string.server_error));
    }

}
