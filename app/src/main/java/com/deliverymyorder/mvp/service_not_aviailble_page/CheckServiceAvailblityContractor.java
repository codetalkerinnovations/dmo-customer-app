package com.deliverymyorder.mvp.service_not_aviailble_page;

public interface CheckServiceAvailblityContractor {

    interface  CheckServiceAvailblityPresenter
    {
        void  checkServiceAvailablity(String latitude, String longitude);
    }

    interface  CheckServiceAvailbityView
    {
        void  checkServiceAaiblityDidFinshWithResult(boolean status);
        void  checkServiceAvaiblityNetworkError();
    }

}
