package com.deliverymyorder.mvp.service_not_aviailble_page;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LocationProvider;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;
import com.deliverymyorder.mvp.dashboard.fragment.DashboardFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class ServiceNotAvailableFragment extends Fragment implements CheckServiceAvailblityContractor.CheckServiceAvailbityView {


    CheckServiceAvailblityImpl checkServiceAvailblityImpl;

    public ServiceNotAvailableFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ServiceNotAvailableFragment newInstance() {
        ServiceNotAvailableFragment fragment = new ServiceNotAvailableFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        checkServiceAvailblityImpl = new CheckServiceAvailblityImpl(getActivity(),this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_not_available, container, false);
        ButterKnife.bind(this,view);


        return  view;
    }



    @OnClick(R.id.btnRetry)
     public  void btnRetry(View view)
    {

        try {
            String latitude = PrefUtil.getString(getActivity(), Const.KEY_LATITUDE, "");
            String longitude = PrefUtil.getString(getActivity(), Const.KEY_LONGITUDE, "");

            if (latitude != null && longitude != null && latitude.length() != 0 && longitude.length() != 0) {
                checkServiceAvailblityImpl.checkServiceAvailablity(latitude, longitude);
            } else {

                LocationProvider.getSharedInstacne().getLocation(getActivity(), new LocationProvider.LocationResult() {
                    @Override
                    public void gotLocation(Location location) {
                        checkServiceAvailblityImpl.checkServiceAvailablity(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                    }
                });
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            AlertDialogHelper.showMessage(getActivity(),"Something went wrong");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void checkServiceAaiblityDidFinshWithResult(boolean status) {

    }

    @Override
    public void checkServiceAvaiblityNetworkError() {

    }
}
