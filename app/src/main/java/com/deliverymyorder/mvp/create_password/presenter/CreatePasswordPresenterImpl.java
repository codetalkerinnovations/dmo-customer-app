package com.deliverymyorder.mvp.create_password.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.create_password.CreatePasswordModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePasswordPresenterImpl implements CreatePasswordContractor.CreatePasswordPresenter {

    Activity activity;
    CreatePasswordContractor.CreatePasswordView createPasswordView;

    public CreatePasswordPresenterImpl(Activity activity, CreatePasswordContractor.CreatePasswordView createPasswordView) {
        this.activity = activity;
        this.createPasswordView = createPasswordView;
    }

    @Override
    public void getCreatePasswordResult(String phoneNumber, String password, String confirmPassword) {

        try {
            ApiAdapter.getInstance(activity);
            callingCreatePasswordApi(phoneNumber, password, confirmPassword);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            createPasswordView.onCreatePasswordInternetError();
        }
    }

    private void callingCreatePasswordApi(String phoneNumber, String password, String confirmPassword) {
        Progress.start(activity);
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_USER_TYPE, Const.VALUE_USER_TYPE);
            jsonObject.put(Const.KEY_PHONE_NUMBER, phoneNumber);
            jsonObject.put(Const.KEY_PASSWORD, password);
            jsonObject.put(Const.KEY_CONFIRM_PASSWORD, confirmPassword);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CreatePasswordModel> getCreatePasswordResult = ApiAdapter.getApiService().createPassword("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<CreatePasswordModel>() {
            @Override
            public void onResponse(Call<CreatePasswordModel> call, Response<CreatePasswordModel> response) {

                Progress.stop();
                try {
                    CreatePasswordModel createPasswordModel = response.body();


                    if (createPasswordModel.getError() == 0) {
                        createPasswordView.onCreatePasswordSuccess(createPasswordModel);
                    } else {
                        createPasswordView.onCreatePasswordUnsuccess(createPasswordModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    createPasswordView.onCreatePasswordUnsuccess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CreatePasswordModel> call, Throwable t) {
                Progress.stop();
                createPasswordView.onCreatePasswordUnsuccess(activity.getString(R.string.server_error));
            }
        });

    }
}
