package com.deliverymyorder.mvp.create_password;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.create_password.presenter.CreatePasswordContractor;
import com.deliverymyorder.mvp.create_password.presenter.CreatePasswordPresenterImpl;
import com.deliverymyorder.mvp.login.LoginActivity;
import com.deliverymyorder.mvp.otp.OtpActivity;
import com.deliverymyorder.mvp.otp.OtpModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreatePasswordActivity extends AppCompatActivity implements CreatePasswordContractor.CreatePasswordView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.edtTextConfirmPassword)
    AppCompatEditText edtTextConfirmPassword;

    @BindView(R.id.txtViewSubmit)
    TextView txtViewSubmit;

    CreatePasswordPresenterImpl createPasswordPresenter;

    String mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);

        ButterKnife.bind(this);

        createPasswordPresenter = new CreatePasswordPresenterImpl(this, this);

        if (getIntent().getStringExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER) != null) {

            mobileNumber = getIntent().getStringExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER);
        }
    }

    @Override
    public void onCreatePasswordSuccess(CreatePasswordModel createPasswordModel) {

        showMessage(createPasswordModel.getMessage());
    }

    private void showMessage(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                Intent intent = new Intent(CreatePasswordActivity.this, LoginActivity.class);
                startActivity(intent);

                finish();


            }
        });

        alertDialog.show();

    }


    @Override
    public void onCreatePasswordUnsuccess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void onCreatePasswordInternetError() {

    }

    @OnClick(R.id.txtViewSubmit)
    public void txtViewSubmitClicked() {

        String newPassword = String.valueOf(edtTextPassword.getText());
        String confirmPassword = String.valueOf(edtTextConfirmPassword.getText());
        createPasswordPresenter.getCreatePasswordResult(mobileNumber, newPassword, confirmPassword);
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        String newPassword = String.valueOf(edtTextPassword.getText());
        String confirmPassword = String.valueOf(edtTextConfirmPassword.getText());
        createPasswordPresenter.getCreatePasswordResult(mobileNumber, newPassword, confirmPassword);
    }
}
