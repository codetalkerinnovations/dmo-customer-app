package com.deliverymyorder.mvp.create_password.presenter;


import com.deliverymyorder.mvp.create_password.CreatePasswordModel;

public interface CreatePasswordContractor {

    interface CreatePasswordPresenter {
        void getCreatePasswordResult(String phoneNumber, String password, String confirmPassword);

    }

    interface CreatePasswordView {
        void onCreatePasswordSuccess(CreatePasswordModel createPasswordModel);

        void onCreatePasswordUnsuccess(String message);

        void onCreatePasswordInternetError();
    }

}
