package com.deliverymyorder.mvp.referal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.widget.RelativeLayout;

import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReferalActivity extends AppCompatActivity {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.edtTextCoupen)
    AppCompatEditText edtTextCoupen;

    @BindView(R.id.relLayShare)
    RelativeLayout relLayShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referal);
        ButterKnife.bind(this);

        edtTextCoupen.setText(LoginManager.getInstance().getUserData().getReferCode());
    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        finish();
    }

    @OnClick(R.id.relLayShare)
    public void relLayShareClicked() {

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        // share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, "Referal code: " + LoginManager.getInstance().getUserData().getReferCode());

        startActivity(Intent.createChooser(share, "Share link!"));
    }
}
