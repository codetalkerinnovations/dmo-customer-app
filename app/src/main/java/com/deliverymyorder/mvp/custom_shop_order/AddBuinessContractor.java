package com.deliverymyorder.mvp.custom_shop_order;


import java.util.List;

public class AddBuinessContractor {

    public interface AddBuinessPresenter {
        
        void addBuisnessData(String merchant_id,String shopName, String gstin, String countryId, String stateId, String cityId, String area, String phoneNumber, String imagePath, String campusID, String acceptCustomOrder, String latitude, String longitude);

    }
    
    public interface AddBuinesView {
        void getAddBuinesSuccess(AddBuisnessResponseModel addBuisnessResponseModel);
        
        void getAddBuinesUnSucess(String message);
        
        void addBuinesInternetError();

    }
}
