package com.deliverymyorder.mvp.custom_shop_order;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;


import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBuisnessPresenterImpl implements AddBuinessContractor.AddBuinessPresenter {

    Activity activity;
    AddBuinessContractor.AddBuinesView addBuinesView;

    public AddBuisnessPresenterImpl(Activity activity, AddBuinessContractor.AddBuinesView addBuinesView) {
        this.activity = activity;
        this.addBuinesView = addBuinesView;
    }

    @Override
    public void addBuisnessData(String merchant_id,String shopName, String gstin, String countryId, String stateId, String cityId, String area,String phoneNumber, String imagePath,String campusID, String acceptCustomOrder, String latitude, String longitude) {
        try {
            ApiAdapter.getInstance(activity);
            callingAddBuisnessDataApi(merchant_id,shopName, gstin, countryId, stateId, cityId, area, phoneNumber,imagePath,campusID,acceptCustomOrder,latitude,longitude);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            addBuinesView.addBuinesInternetError();
        }

    }


    private void callingAddBuisnessDataApi(String merchant_id,String shopName, String gstin, String countryId, String stateId, String cityId, String area,String phoneNumber, String imagePath,String campusID, String acceptCustomOrder,String latitude, String longitude) {

        RequestBody requestFile;

        File fileDoc = null;
        MultipartBody.Part bodyImage = null;

        if (!Utils.isEmptyOrNull(imagePath)) {

            fileDoc = new File(imagePath);

            String mimeType = Utils.getMimeType(imagePath);

            if (Utils.isEmptyOrNull(mimeType))
                mimeType = "image/*";

            if (imagePath == null) {
                requestFile = RequestBody.create(MediaType.parse(mimeType), "");
            } else {
                requestFile = RequestBody.create(MediaType.parse(mimeType), fileDoc);
            }

            if (!Utils.isEmptyOrNull(imagePath)) {
                bodyImage = MultipartBody.Part.createFormData(Const.PARAM_IMAGE, fileDoc.getName(), requestFile);
            } else
                bodyImage = MultipartBody.Part.createFormData(Const.PARAM_IMAGE, "image.png", requestFile);

        }

        Progress.start(activity);

//        String latitude = PrefUtil.getString(activity, Const.KEY_LATITUDE, "");
//        String longitude = PrefUtil.getString(activity, Const.KEY_LONGITUDE, "");

        RequestBody accessToken = RequestBody.create(MediaType.parse("text/plain"), Const.VALUE_ACCESS_TOKEN);
        RequestBody merchantId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(merchant_id));
        RequestBody shopname = RequestBody.create(MediaType.parse("text/plain"), shopName);
        RequestBody gStIn = RequestBody.create(MediaType.parse("text/plain"), gstin);
        RequestBody countryIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(countryId));
        RequestBody stateIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(stateId));
        RequestBody cityIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(cityId));
        RequestBody phoneNumberBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(phoneNumber));
        RequestBody areaBody = RequestBody.create(MediaType.parse("text/plain"), area);
        RequestBody latitudeBody = RequestBody.create(MediaType.parse("text/plain"), latitude);
        RequestBody longitudeBody = RequestBody.create(MediaType.parse("text/plain"), longitude);

        RequestBody campusIDRequest = RequestBody.create(MediaType.parse("text/plain"), campusID);

        RequestBody acceptCusomtOrderRequest = RequestBody.create(MediaType.parse("text/plain"), acceptCustomOrder);

        Call<AddBuisnessResponseModel> getCreatePasswordResult = ApiAdapter.getApiService().addBuisness(accessToken, merchantId, shopname, gStIn, countryIdBody, stateIdBody, cityIdBody, areaBody,phoneNumberBody,latitudeBody,longitudeBody,campusIDRequest,acceptCusomtOrderRequest, bodyImage);

        getCreatePasswordResult.enqueue(new Callback<AddBuisnessResponseModel>() {
            @Override
            public void onResponse(Call<AddBuisnessResponseModel> call, Response<AddBuisnessResponseModel> response) {

                Progress.stop();
                try {
                    AddBuisnessResponseModel addBuisnessResponseModel = response.body();


                    if (addBuisnessResponseModel.getError() == 0) {
                        addBuinesView.getAddBuinesSuccess(addBuisnessResponseModel);
                    } else {
                        addBuinesView.getAddBuinesUnSucess(addBuisnessResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    addBuinesView.getAddBuinesUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<AddBuisnessResponseModel> call, Throwable t) {
                Progress.stop();
                addBuinesView.getAddBuinesUnSucess(activity.getString(R.string.server_error));
            }
        });

    }


}