package com.deliverymyorder.mvp.custom_shop_order;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.UploadPhotoHelper;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.DMOMerchat;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.CustomMenuModel;
import com.deliverymyorder.mvp.delivery_type.DeliveryTypeActivity;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailContractor;
import com.deliverymyorder.mvp.shop_detail.presenter.ShoppingDetailPresenterImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomShopOrderActivity extends AppCompatActivity  implements ShoppingDetailContractor.ShoppingDetailView {

    @BindView(R.id.llayItemsChilds)
    LinearLayout llayItemsChilds;

    @BindView(R.id.txtViewQty)
    TextView txtViewQty;

    @BindView(R.id.edtTextItemName)
    EditText edtTextItemName;

    @BindView(R.id.edtTextItemDesc)
    EditText edtTextItemDesc;

    @BindView(R.id.imgViewCross)
    ImageView imgViewCross;

    @BindView(R.id.imgProduct)
    ImageView imgProduct;


    ArrayList<View> arrayListView;

    String imagePath = null;
    File photo = null;
    Bitmap photoBitmap = null;
    Uri mImageUri;


    int shopHaveProducts = 0;

    private final int PERMISSION_CODE_REQUEST_CAMERA = 2906;

    

    private int REQUEST_CAMERA = 0;

    ArrayList<CustomMenuModel> arrayListCustomMenu;


    int cutomOrderAllowed = 0;
    int buisnessId = 0;
    int merchantId = 0;
    String shopName;
    String address;
    int totalQty = 0;
    int totalPrice = 0;
    String searchKey;


    ShoppingDetailPresenterImpl shoppingDetailPresenter;

    @BindView(R.id.txtViewShopName)
    TextView txtViewShopName;

    @BindView(R.id.txtViewAddress)
    TextView txtViewAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_shop_order);

        ButterKnife.bind(this);
        imgViewCross.setVisibility(View.GONE);
        arrayListView = new ArrayList<>();
        arrayListCustomMenu = new ArrayList<>();

        shoppingDetailPresenter = new ShoppingDetailPresenterImpl(this,this);

        if (getIntent() != null) {

            if (getIntent().hasExtra(ConstIntent.CONST_BUISNESS_ID)) {
                shopHaveProducts = 2;  //getIntent().getIntExtra(ConstIntent.CONST_SHOP_HAS_PRODUCTS,0);
                buisnessId =  0; //getIntent().getIntExtra(ConstIntent.CONST_BUISNESS_ID, 0);
                merchantId = DMOMerchat.merchant_id;  //getIntent().getIntExtra(ConstIntent.CONST_MERCHANT_ID, 0);
                shopName =   "";     //getIntent().getStringExtra(ConstIntent.CONST_SHOP_NAME);
                address =   ""; //getIntent().getStringExtra(ConstIntent.CONST_ADDRESS);
                cutomOrderAllowed =  1;//getIntent().getIntExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, 0);

//                try {
//                    arrayListCustomMenu = getIntent().getParcelableArrayListExtra(ConstIntent.CONST_ADDED_CUSTOMER_ORDER);
//                }catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//
//                if (arrayListCustomMenu != null)
//                {
//                    addFiledsFromArray();
//                }else
//                {
//                    arrayListCustomMenu = new ArrayList<>();
//                }


                arrayListCustomMenu = new ArrayList<>();

            }
        }


        txtViewShopName.setText(shopName);
        txtViewAddress.setText(address);

    }


    private  void addFiledsFromArray()
    {

        for (CustomMenuModel customMenuModel:arrayListCustomMenu) {

            final View view = LayoutInflater.from(this).inflate(R.layout.item_custom_create_own_item, null);

            ImageView imgViewMinus = (ImageView) view.findViewById(R.id.imgViewMinus);
            ImageView imgViewPlus = (ImageView) view.findViewById(R.id.imgViewPlus);
            ImageView imgViewCross = (ImageView) view.findViewById(R.id.imgViewCross);
            ImageView imgViewCamera = (ImageView) view.findViewById(R.id.imgViewCamera);

            ImageView imgProduct = (ImageView) view.findViewById(R.id.imgProduct);

            final EditText edtTextItemName = (EditText) view.findViewById(R.id.edtTextItemName);
            EditText edtTextItemDesc = (EditText) view.findViewById(R.id.edtTextItemDesc);
            final TextView txtViewQtyIn = (TextView) view.findViewById(R.id.txtViewQty);


            // set Values :
            edtTextItemName.setText(customMenuModel.getItemName());
            edtTextItemDesc.setText(customMenuModel.getItemDesc());
            // imgProduct.set


            imgViewMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String qty = txtViewQtyIn.getText().toString().trim();
                    int qtyInInt = Integer.parseInt(qty);

                    if (qtyInInt > 1) {
                        qtyInInt = qtyInInt - 1;
                        qty = String.valueOf(qtyInInt);
                        txtViewQtyIn.setText(qty);
                        edtTextItemName.setTag(qty);
                    } else {
                        AlertDialogHelper.showAlertDialog(CustomShopOrderActivity.this, "Minimus qty will be one.");
                    }
                }
            });

            imgViewCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectFromCamera();


                }
            });

            imgViewCross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrayListView.remove(view);
                    llayItemsChilds.removeView(view);
                }
            });

            imgViewPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String qty = txtViewQtyIn.getText().toString().trim();
                    int qtyInInt = Integer.parseInt(qty);

                    qtyInInt = qtyInInt + 1;
                    qty = String.valueOf(qtyInInt);
                    txtViewQtyIn.setText(qty);
                    edtTextItemName.setTag(qty);
                }
            });

            arrayListView.add(view);
            llayItemsChilds.addView(view);

        }
    }

    @OnClick(R.id.imgViewCamera)
    public void imgViewCamera(){
        selectFromCamera();
    }

    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirm(){
        getSingleLayoutData();
        getFormData();

        if (shopHaveProducts == 1) {
            sendDataBackToMenuActivity();
        }else
        {
            createOrderAPI();
        }
    }

    ///imgViewBack
    @OnClick(R.id.imgViewBack)
    public void imgViewBack(){
        finish();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        // requestCode (2908 for pick image from gallary), (2909 for camera) and 2910 is for selectedItemLocation permission

        switch (requestCode) {



            case PERMISSION_CODE_REQUEST_CAMERA: {

                int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA);

                int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

                int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);


                if ((permissionCheckCamera == 0) && (permissionCheckRead == 0) && (permissionCheckWrite == 0)) {
                    openCamera();
                } else {
                    //AlertDialogHelper.showMessage(DashboardActivity.this, "Please provide security permission from app setting.");
                }

                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == REQUEST_CAMERA) {
                // taking photo using hardware camera
                onCaptureImageResult(data);


            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    private void onCaptureImageResult(Intent data) {
        String path = null;
        String absolutePath = null;
        float rotate = 0;

        ContentResolver cr = getContentResolver();
        cr.notifyChange(mImageUri, null);

        absolutePath = new File(mImageUri.getPath()).getAbsoluteFile().toString();
        path = new File(mImageUri.getPath()).toString();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            rotate = UploadPhotoHelper.getOrientation(photo.getPath());
        else
            rotate = UploadPhotoHelper.getOrientation(path);


        Bitmap bitmap = null;

        try {
            bitmap = UploadPhotoHelper.rotateImage(MediaStore.Images.Media.getBitmap(cr, mImageUri), rotate);
        } catch (Exception e) {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();

        }

        try {
            photoBitmap = UploadPhotoHelper.resizeImageForImageView(bitmap, rotate);

            imgProduct.setImageBitmap(photoBitmap);

            //uploadImage(photoBitmap);



        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
        }

    }

    private void uploadImage(Bitmap image) {
        if (photoBitmap != null) {

            imagePath = UploadPhotoHelper.getAbsolutePath(image, "_resize.JPEG");
            Log.e("imagePath",""+imagePath);
            Log.e("mImageUri",""+mImageUri);


        }
    }


    @OnClick(R.id.txtViewAddMore)
    public void txtViewAddMore() {

        final View view = LayoutInflater.from(this).inflate(R.layout.item_custom_create_own_item, null);

        ImageView imgViewMinus = (ImageView) view.findViewById(R.id.imgViewMinus);
        ImageView imgViewPlus = (ImageView) view.findViewById(R.id.imgViewPlus);
        ImageView imgViewCross = (ImageView) view.findViewById(R.id.imgViewCross);
        ImageView imgViewCamera = (ImageView) view.findViewById(R.id.imgViewCamera);
        final EditText edtTextItemName = (EditText) view.findViewById(R.id.edtTextItemName);
        EditText edtTextItemDesc = (EditText) view.findViewById(R.id.edtTextItemDesc);
        final TextView txtViewQtyIn = (TextView)view.findViewById(R.id.txtViewQty);

        imgViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = txtViewQtyIn.getText().toString().trim();
                int qtyInInt = Integer.parseInt(qty);

                if (qtyInInt > 1) {
                    qtyInInt = qtyInInt - 1;
                    qty = String.valueOf(qtyInInt);
                    txtViewQtyIn.setText(qty);
                    edtTextItemName.setTag(qty);
                }else {
                    AlertDialogHelper.showAlertDialog(CustomShopOrderActivity.this,"Minimus qty will be one.");
                }
            }
        });

        imgViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFromCamera();


            }
        });

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayListView.remove(view);
                llayItemsChilds.removeView(view);
            }
        });

        imgViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = txtViewQtyIn.getText().toString().trim();
                int qtyInInt = Integer.parseInt(qty);

                qtyInInt = qtyInInt + 1;
                qty = String.valueOf(qtyInInt);
                txtViewQtyIn.setText(qty);
                edtTextItemName.setTag(qty);
            }
        });

        arrayListView.add(view);
        llayItemsChilds.addView(view);
    }

    @OnClick(R.id.imgViewMinus)
    public void imgViewMinus() {

        String qty = txtViewQty.getText().toString().trim();
        int qtyInInt = Integer.parseInt(qty);

        if (qtyInInt > 1) {
            qtyInInt = qtyInInt - 1;
            qty = String.valueOf(qtyInInt);
            txtViewQty.setText(qty);
        }else {
            AlertDialogHelper.showAlertDialog(this,"Minimus qty will be one.");
        }
    }


    @OnClick(R.id.imgViewPlus)
    public void imgViewPlus() {

        String qty = txtViewQty.getText().toString().trim();
        int qtyInInt = Integer.parseInt(qty);

        qtyInInt = qtyInInt + 1;
        qty = String.valueOf(qtyInInt);
        txtViewQty.setText(qty);

    }

    private void selectFromCamera() {

        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionCheckRead = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if ((permissionCheckCamera == -1) || (permissionCheckRead == -1) || (permissionCheckWrite == -1)) {

                if (!Settings.System.canWrite(this)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_CODE_REQUEST_CAMERA);
                } else {

                    openCamera();
                }
            } else {
                openCamera();
            }
        } else {
            openCamera();
        }
    }

    private void openCamera() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
        } else {
            intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        }

        try {
            // place where to store camera taken picture
            photo = createTemporaryFile("picture", ".JPEG");
        } catch (Exception e) {
            e.printStackTrace();
            // Log.v("pic", "Can't create file to take picture! ");
        }
        mImageUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photo);


        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    public static File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }


    public void getSingleLayoutData(){


        String itemName = edtTextItemName.getText().toString().trim();
        String itemDesc = edtTextItemDesc.getText().toString().trim();
        String itemQty = txtViewQty.getText().toString().trim();

        if(!Utils.isEmptyOrNull(itemName)){
            CustomMenuModel customMenuModel = new CustomMenuModel();

            customMenuModel.setItemDesc(itemDesc);
            customMenuModel.setQty(Integer.parseInt(itemQty));
            customMenuModel.setItemName(itemName);
            customMenuModel.setItemImage("");

            arrayListCustomMenu.add(customMenuModel);
        }else {
            AlertDialogHelper.showAlertDialog(this,"Please fill item name in all box.");
        }



    }

    public void getFormData(){

        for( View view : arrayListView){

            EditText edtTextItemName = (EditText) view.findViewById(R.id.edtTextItemName);
            EditText edtTextItemDesc = (EditText) view.findViewById(R.id.edtTextItemDesc);

            String itemName = edtTextItemName.getText().toString().trim();
            String itemDesc = edtTextItemDesc.getText().toString().trim();
            String itemQty = "1";
            if(edtTextItemName.getTag() != null) {
                itemQty = edtTextItemName.getTag().toString();
            }

            if(Utils.isEmptyOrNull(itemName)){

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialogHelper.showAlertDialog(CustomShopOrderActivity.this,"Please fill item name in all box.");
                    }
                });

            }else {

                CustomMenuModel customMenuModel = new CustomMenuModel();

                customMenuModel.setItemDesc(itemDesc);
                customMenuModel.setQty(Integer.parseInt(itemQty));
                customMenuModel.setItemName(itemName);
                customMenuModel.setItemImage("");

                arrayListCustomMenu.add(customMenuModel);
            }
        }






    }


    public void sendDataBackToMenuActivity(){
        Intent intent=new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstIntent.CONST_ARRAYLIST_CUSTOM_MENU,arrayListCustomMenu);
        intent.putExtras(bundle);
        setResult(Const.KEY_CUSTOM_MENU_REQUEST_CODE,intent);
        finish();//finishing activity
    }


    private  void createOrderAPI()
    {
        int customerId = LoginManager.getInstance().getUserData().getCustomerId();


        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();


        for (int i = 0; i < arrayListCustomMenu.size(); i++) {

            CustomMenuModel customMenuModel = arrayListCustomMenu.get(i);

            JSONObject jsonObjectItems = new JSONObject();
            try {
                jsonObjectItems.put(Const.PARAM_ID, 0);
                jsonObjectItems.put(Const.PARAM_NAME, customMenuModel.getItemName());
                jsonObjectItems.put(Const.PARAM_QTY, customMenuModel.getQty());
                jsonObjectItems.put(Const.PARAM_PRICE, 0);
                jsonObjectItems.put(Const.PARAM_DESCRIPTION, customMenuModel.getItemDesc());

                totalQty = totalQty + customMenuModel.getQty();

                  /*  String itemPrice = arrayListShopDetail.get(i).getItemPrice();

                    if (itemPrice.contains(".")) {
                        String[] priceArray = itemPrice.split(".");*/
                //  totalPrice = totalPrice + (arrayListShopDetail.get(i).getQty() * Integer.parseInt(priceArray[0]));
                //totalPrice = totalPrice + (arrayListShopDetail.get(i).getQty() * Integer.parseInt(arrayListShopDetail.get(i).getItemPrice().replace(".00", "")));


            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObjectItems);
        }


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_BUISNESS_ID, buisnessId);
            jsonObject.put(Const.KEY_MERCHANT_ID, merchantId);
            jsonObject.put(Const.KEY_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_TOTAL_ITEMS, totalQty);
            jsonObject.put(Const.PARAM_TOTAL_AMOUNT, totalPrice);
            jsonObject.put(Const.PARAM_ITEMS, jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (totalQty > 0) {
            shoppingDetailPresenter.getCreateorderDataApi(jsonObject);
        } else {
            //SnackNotify.showMessage("Please add atleast one product.", relLayMain);
            AlertDialogHelper.showAlertDialog(this,"Please add atleast one product.");
        }

    }

    @Override
    public void getShoppingDetailDataSuccess(ArrayList<ShopDetailModel.Datum> arrayListShopDetail) {

    }

    @Override
    public void getShoppingDetailDataUnSucess(String message) {

    }

    @Override
    public void ShoppingDetailDataInternetError() {

    }

    @Override
    public void getCreateOrderSuccess(CreateOrderResponseModel.Data createOrderResponseModel) {

        Intent intent = new Intent(this, DeliveryTypeActivity.class);
        intent.putExtra(ConstIntent.CONST_ORDER_ID, createOrderResponseModel.getId());
        startActivity(intent);


//        Intent intent = new Intent(this, DeliveryTypeActivity.class);
//        intent.putExtra(ConstIntent.CONST_ORDER_ID, createOrderResponseModel.getId());
//        startActivity(intent);
    }

    @Override
    public void getCreateOrderUnSucess(String message) {

    }

    @Override
    public void CreateOrderInternetError() {

    }
}