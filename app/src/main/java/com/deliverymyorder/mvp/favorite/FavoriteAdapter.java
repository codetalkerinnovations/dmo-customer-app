package com.deliverymyorder.mvp.favorite;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.RoundCornerTransformation;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.favorite.model.FavoriteListModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.Holder> {


    Activity activity;
    ArrayList<FavoriteListModel.Datum> datum;
    String favBit;
    FavouriteFragment favouriteFragment;


    public FavoriteAdapter(Activity activity, ArrayList<FavoriteListModel.Datum> datum,FavouriteFragment favouriteFragment) {
        this.activity = activity;
        this.datum = datum;
        this.favouriteFragment = favouriteFragment;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_near_you, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final FavoriteListModel.Datum result = datum.get(position);

if (result.getMerchantBusiness() !=null) {
    holder.txtViewCategoryName.setText(result.getMerchantBusiness().getTitle());

    if (Utils.isEmptyOrNull(result.getMerchantBusiness().getImage())) {
        // holder.relLayItem.setVisibility(View.GONE);
    } else {
        //holder.relLayItem.setVisibility(View.VISIBLE);
        Picasso.with(activity).load(result.getMerchantBusiness().getImage()).transform(new RoundCornerTransformation()).into(holder.imgViewCategory);
    }

    holder.relLayItem.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Intent intent = new Intent(activity, ShopDetailsActivity.class);
            intent.putExtra(ConstIntent.CONST_BUISNESS_ID, result.getBusinessId());
            intent.putExtra(ConstIntent.CONST_MERCHANT_ID, result.getMerchantBusiness().getId());
            intent.putExtra(ConstIntent.CONST_SHOP_NAME, result.getMerchantBusiness().getTitle());
            intent.putExtra(ConstIntent.CONST_ADDRESS, result.getMerchantBusiness().getAddress());
            activity.startActivity(intent);

        }
    });


}

        holder.imgViewFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (favouriteFragment != null) {
                    favouriteFragment.callUpdateFavoriteStatusDataApi(result.getBusinessId(), 0);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgViewCategory)
        ImageView imgViewCategory;

        @BindView(R.id.imgViewFavourite)
        ImageView imgViewFavourite;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}



