package com.deliverymyorder.mvp.favorite.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.favorite.model.FavoriteListModel;
import com.deliverymyorder.mvp.favorite.model.UpdateFavoriteResponseModel;
import com.deliverymyorder.mvp.feedback.FeedbackResponseModel;
import com.deliverymyorder.mvp.feedback.presenter.FeedbackContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritePresenterImpl implements FavoriteContractor.FavoritePresenter {

    Activity activity;
    FavoriteContractor.FavoriteView favoriteView;

    public FavoritePresenterImpl(Activity activity, FavoriteContractor.FavoriteView favoriteView) {
        this.activity = activity;
        this.favoriteView = favoriteView;
    }

    @Override
    public void getFavoriteData() {
        try {
            ApiAdapter.getInstance(activity);
            callGetFavoriteDataApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            favoriteView.getFavoriteInternetError();

        }
    }

    @Override
    public void updateFavoriteStatus(int buisnessId, int favStatus) {
        try {
            ApiAdapter.getInstance(activity);
            callUpdateFavoriteDataApi(buisnessId, favStatus);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            favoriteView.updateFavoriteInternetError();

        }
    }


    private void callGetFavoriteDataApi() {
        Progress.start(activity);


        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_FAVORITE_STATUS, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<FavoriteListModel> getResult = ApiAdapter.getApiService().getFavoriteList("application/json", "no-cache", body);

        getResult.enqueue(new Callback<FavoriteListModel>() {
            @Override
            public void onResponse(Call<FavoriteListModel> call, Response<FavoriteListModel> response) {

                Progress.stop();
                try {
                    FavoriteListModel favoriteListModel = response.body();

                    if (favoriteListModel.getError() == 0) {
                        favoriteView.getFavoriteSuccess(favoriteListModel.getData());
                    } else {
                        favoriteView.getFavoriteUnSucess(favoriteListModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    favoriteView.getFavoriteUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<FavoriteListModel> call, Throwable t) {
                Progress.stop();
                favoriteView.getFavoriteUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

    private void callUpdateFavoriteDataApi(int buisnessId, int favStatus) {
        Progress.start(activity);


        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_BUISNESS_ID, buisnessId);
            jsonObject.put(Const.KEY_FAVORITE_STATUS, favStatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<UpdateFavoriteResponseModel> getResult = ApiAdapter.getApiService().updateFavorite("application/json", "no-cache", body);

        getResult.enqueue(new Callback<UpdateFavoriteResponseModel>() {
            @Override
            public void onResponse(Call<UpdateFavoriteResponseModel> call, Response<UpdateFavoriteResponseModel> response) {

                Progress.stop();
                try {
                    UpdateFavoriteResponseModel updateFavoriteResponseModel = response.body();

                    if (updateFavoriteResponseModel.getError() == 0) {
                        favoriteView.updateFavoriteSuccess(updateFavoriteResponseModel.getData());
                    } else {
                        favoriteView.updateFavoriteUnSucess(updateFavoriteResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    favoriteView.updateFavoriteUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateFavoriteResponseModel> call, Throwable t) {
                Progress.stop();
                favoriteView.updateFavoriteUnSucess(activity.getString(R.string.server_error));
            }
        });
    }


}

