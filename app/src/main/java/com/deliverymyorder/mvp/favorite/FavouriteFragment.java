package com.deliverymyorder.mvp.favorite;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.adapter.NearYouAdapter;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.dashboard.presenter.HomePageContractor;
import com.deliverymyorder.mvp.dashboard.presenter.HomePagePresenterImpl;
import com.deliverymyorder.mvp.favorite.model.FavoriteListModel;
import com.deliverymyorder.mvp.favorite.model.UpdateFavoriteResponseModel;
import com.deliverymyorder.mvp.favorite.presenter.FavoriteContractor;
import com.deliverymyorder.mvp.favorite.presenter.FavoritePresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteFragment extends Fragment implements FavoriteContractor.FavoriteView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.recyclerViewFavourite)
    RecyclerView recyclerViewFavourite;

    //SearchPagePresenterImpl homePagePresenter;
    FavoritePresenterImpl favoritePresenter;

    FavoriteAdapter favoriteAdapter;

    int favStatus;
    int buisnessId;


    Context mContext;

    HomePageDataModel.Data arrayListHomePageDataModel;
    ArrayList<FavoriteListModel.Datum> arrayListFavorite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        favoritePresenter = new FavoritePresenterImpl(getActivity(), this);

        getFavoriteListtData();


    }

    private void getFavoriteListtData() {

        favoritePresenter.getFavoriteData();
    }

    public void callUpdateFavoriteStatusDataApi(int buisnessId, int favStatus) {

        this.buisnessId = buisnessId;
        this.favStatus = favStatus;

        favoritePresenter.updateFavoriteStatus(buisnessId, favStatus);
    }


    private void manageRecyclerViewForNearShops() {

        favoriteAdapter = new FavoriteAdapter(getActivity(), arrayListFavorite,this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewFavourite.setLayoutManager(mLayoutManager);
        recyclerViewFavourite.setItemAnimator(new DefaultItemAnimator());
        recyclerViewFavourite.setAdapter(favoriteAdapter);
    }

    @Override
    public void getFavoriteSuccess(ArrayList<FavoriteListModel.Datum> arrayListFavorite) {

        this.arrayListFavorite = arrayListFavorite;

        if (this.arrayListFavorite == null || this.arrayListFavorite.size() == 0)
        {
            AlertDialogHelper.showAlertDialog(getActivity(),"You've not added any shop in favorite list");
        }

        manageRecyclerViewForNearShops();
    }

    @Override
    public void getFavoriteUnSucess(String message) {

    }

    @Override
    public void getFavoriteInternetError() {

    }

    @Override
    public void updateFavoriteSuccess(UpdateFavoriteResponseModel.Data updateFavoriteResponseModel) {
      getFavoriteListtData();
    }

    @Override
    public void updateFavoriteUnSucess(String message) {

    }

    @Override
    public void updateFavoriteInternetError() {

    }
}
