package com.deliverymyorder.mvp.favorite.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FavoriteListModel {


    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("business_id")
        @Expose
        private Integer businessId;
        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("favorite_status")
        @Expose
        private Integer favoriteStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        @SerializedName("merchant_business")
        @Expose
        private MerchantBusiness merchantBusiness;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getBusinessId() {
            return businessId;
        }

        public void setBusinessId(Integer businessId) {
            this.businessId = businessId;
        }

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public Integer getFavoriteStatus() {
            return favoriteStatus;
        }

        public void setFavoriteStatus(Integer favoriteStatus) {
            this.favoriteStatus = favoriteStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public MerchantBusiness getMerchantBusiness() {
            return merchantBusiness;
        }

        public void setMerchantBusiness(MerchantBusiness merchantBusiness) {
            this.merchantBusiness = merchantBusiness;
        }

        public class MerchantBusiness {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("merchant_id")
            @Expose
            private Integer merchantId;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("description")
            @Expose
            private Object description;
            @SerializedName("gstin")
            @Expose
            private String gstin;
            @SerializedName("status")
            @Expose
            private Integer status;
            @SerializedName("work_hours_from")
            @Expose
            private String workHoursFrom;
            @SerializedName("work_hours_to")
            @Expose
            private String workHoursTo;
            @SerializedName("country_id")
            @Expose
            private Integer countryId;
            @SerializedName("state_id")
            @Expose
            private Integer stateId;
            @SerializedName("city_id")
            @Expose
            private Integer cityId;
            @SerializedName("phone")
            @Expose
            private String phone;
            @SerializedName("latitude")
            @Expose
            private String latitude;
            @SerializedName("longitude")
            @Expose
            private String longitude;
            @SerializedName("address")
            @Expose
            private String address;
            @SerializedName("image")
            @Expose
            private String image;
            @SerializedName("rating")
            @Expose
            private Integer rating;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(Integer merchantId) {
                this.merchantId = merchantId;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public Object getDescription() {
                return description;
            }

            public void setDescription(Object description) {
                this.description = description;
            }

            public String getGstin() {
                return gstin;
            }

            public void setGstin(String gstin) {
                this.gstin = gstin;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public String getWorkHoursFrom() {
                return workHoursFrom;
            }

            public void setWorkHoursFrom(String workHoursFrom) {
                this.workHoursFrom = workHoursFrom;
            }

            public String getWorkHoursTo() {
                return workHoursTo;
            }

            public void setWorkHoursTo(String workHoursTo) {
                this.workHoursTo = workHoursTo;
            }

            public Integer getCountryId() {
                return countryId;
            }

            public void setCountryId(Integer countryId) {
                this.countryId = countryId;
            }

            public Integer getStateId() {
                return stateId;
            }

            public void setStateId(Integer stateId) {
                this.stateId = stateId;
            }

            public Integer getCityId() {
                return cityId;
            }

            public void setCityId(Integer cityId) {
                this.cityId = cityId;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Integer getRating() {
                return rating;
            }

            public void setRating(Integer rating) {
                this.rating = rating;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

        }
    }

}
