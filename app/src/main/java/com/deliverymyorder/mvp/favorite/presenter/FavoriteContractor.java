package com.deliverymyorder.mvp.favorite.presenter;


import com.deliverymyorder.mvp.favorite.model.FavoriteListModel;
import com.deliverymyorder.mvp.favorite.model.UpdateFavoriteResponseModel;

import org.json.JSONObject;

import java.util.ArrayList;

public class FavoriteContractor {

    public interface FavoritePresenter {

        void getFavoriteData();

        void updateFavoriteStatus(int buisnessId, int favStatus);

    }

    public interface FavoriteView {
        void getFavoriteSuccess(ArrayList<FavoriteListModel.Datum> arrayListFavourite);

        void getFavoriteUnSucess(String message);

        void getFavoriteInternetError();

        void updateFavoriteSuccess(UpdateFavoriteResponseModel.Data upDateFavoriteModel);

        void updateFavoriteUnSucess(String message);

        void updateFavoriteInternetError();

    }
}
