package com.deliverymyorder.mvp.favorite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.adapter.NearYouAdapter;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.dashboard.presenter.HomePageContractor;
import com.deliverymyorder.mvp.dashboard.presenter.HomePagePresenterImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteActivity extends AppCompatActivity implements HomePageContractor.HomePageView {

    @BindView(R.id.relLayMain)
    RecyclerView relLayMain;

    @BindView(R.id.recyclerViewFavourite)
    RecyclerView recyclerViewFavourite;

    HomePagePresenterImpl homePagePresenter;

    NearYouAdapter nearYouAdapter;

    HomePageDataModel.Data arrayListHomePageDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ButterKnife.bind(this);

        homePagePresenter = new HomePagePresenterImpl(this, this);

        getFavoriteListtData();

    }

    private void getFavoriteListtData() {

        homePagePresenter.getHomePageData();
    }

    @Override
    public void getHomePageDataSuccess(HomePageDataModel.Data arrayListHomePageDataModel) {

        this.arrayListHomePageDataModel=arrayListHomePageDataModel;




        manageRecyclerViewForNearShops();
    }

    @Override
    public void getHomePageDataUnSucess(String message) {

    }

    @Override
    public void homePageDataInternetError() {

    }

    @Override
    public void getSearchDataSuccess(ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel) {
        //Not Used
    }

    @Override
    public void getSearchDataUnSucess(String message) {
        //Not Used
    }

    @Override
    public void searchDataInternetError() {
        //Not Used
    }

    @Override
    public void getCampusApiDidFinishWith(Result result, List<Campus> campusList) {

    }


    private void manageRecyclerViewForNearShops() {

       // nearYouAdapter = new NearYouAdapter(this, arrayListHomePageDataModel.getNearByShops(),null);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewFavourite.setLayoutManager(mLayoutManager);
        recyclerViewFavourite.setItemAnimator(new DefaultItemAnimator());
        recyclerViewFavourite.setAdapter(nearYouAdapter);
    }
}
