package com.deliverymyorder.mvp.dashboard.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.ApiResult;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.CampusListResponse;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePagePresenterImpl implements HomePageContractor.HomePagePresenter {

    Activity activity;
    HomePageContractor.HomePageView homePageView;

    public HomePagePresenterImpl(Activity activity, HomePageContractor.HomePageView homePageView) {
        this.activity = activity;
        this.homePageView = homePageView;
    }

    @Override
    public void getHomePageData() {
        try {
            ApiAdapter.getInstance(activity);
            callGetCategoriesDataApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            homePageView.homePageDataInternetError();

        }
    }

    @Override
    public void getSearchData(String searchKey, String categoryId, String subCategoryId, int filterType, boolean isInCampus) {
        try {
            ApiAdapter.getInstance(activity);
            callSearchDataApi(searchKey, categoryId,subCategoryId, filterType, isInCampus);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            homePageView.searchDataInternetError();

        }
    }

    @Override
    public void getCampusList() {
        try {
            ApiAdapter.getInstance(activity);
            callCampusListDataApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }

            Result result = new Result(ApiResult.FAILED, "Please check your internet connection");
            homePageView.getCampusApiDidFinishWith(result, null);

        }
    }


    private void callGetCategoriesDataApi() {
        Progress.start(activity);

       // int campusId = 0;
        String latitude = PrefUtil.getString(activity, Const.KEY_LATITUDE, "");
        String longitude = PrefUtil.getString(activity, Const.KEY_LONGITUDE, "");

//        if (LoginManager.getInstance().getCurrentCampus() != null) {
//
//            if (LoginManager.getInstance().getCurrentCampus().getId() != null) {
//                campusId = LoginManager.getInstance().getCurrentCampus().getId();
//            }
//        }



        int campusId = 0;

        if (LoginManager.getInstance().getCurrentCampus() != null) {

            if (LoginManager.getInstance().getCurrentCampus().getId() != null) {
                campusId = LoginManager.getInstance().getCurrentCampus().getId();
            }
        }


        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_LATITUDE, latitude);
            jsonObject.put(Const.KEY_LONGITUDE, longitude);
            if (campusId != 0)
                jsonObject.put(Const.KEY_CAPMUS_ID, campusId);
                jsonObject.put(Const.KEY_IN_CAMPUS, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<HomePageDataModel> getCreatePasswordResult = ApiAdapter.getApiService().getHomeData("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<HomePageDataModel>() {
            @Override
            public void onResponse(Call<HomePageDataModel> call, Response<HomePageDataModel> response) {

                Progress.stop();
                try {
                    HomePageDataModel homePageDataModel = response.body();


                    if (homePageDataModel.getError() == 0) {
                        homePageView.getHomePageDataSuccess(homePageDataModel.getData());
                    } else {
                        homePageView.getHomePageDataUnSucess(homePageDataModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    homePageView.getHomePageDataUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<HomePageDataModel> call, Throwable t) {
                Progress.stop();
                homePageView.getHomePageDataUnSucess(activity.getString(R.string.server_error));
            }
        });

    }


    private void callCampusListDataApi() {
        Progress.start(activity);

        String latitude = PrefUtil.getString(activity, Const.KEY_LATITUDE, "");
        String longitude = PrefUtil.getString(activity, Const.KEY_LONGITUDE, "");

        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_LATITUDE, latitude);
            jsonObject.put(Const.KEY_LONGITUDE, longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CampusListResponse> getCreatePasswordResult = ApiAdapter.getApiService().getCampusListData("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<CampusListResponse>() {
            @Override
            public void onResponse(Call<CampusListResponse> call, Response<CampusListResponse> response) {

                Progress.stop();
                try {
                    CampusListResponse homePageDataModel = response.body();


                    if (homePageDataModel.getError() == 0) {
                        Result result = new Result(ApiResult.SUCCESS);
                        homePageView.getCampusApiDidFinishWith(result, homePageDataModel.getData());
                    } else {

                        Result result = new Result(ApiResult.FAILED, homePageDataModel.getMessage());
                        homePageView.getCampusApiDidFinishWith(result, null);
                        // homePageView.getHomePageDataUnSucess(homePageDataModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    Result result = new Result(ApiResult.FAILED, activity.getString(R.string.server_error));
                    homePageView.getCampusApiDidFinishWith(result, null);
                    // homePageView.getHomePageDataUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CampusListResponse> call, Throwable t) {
                Progress.stop();
                Result result = new Result(ApiResult.FAILED, activity.getString(R.string.server_error));
                homePageView.getCampusApiDidFinishWith(result, null);
            }
        });

    }


    private void callSearchDataApi(String searchKey, String categoryId,String subCategoryId, int filterType, boolean isInCampus) {
        Progress.start(activity);

        int campusId = 0;

        JSONObject jsonObject = new JSONObject();

        String latitude = PrefUtil.getString(activity, Const.KEY_LATITUDE, "");
        String longitude = PrefUtil.getString(activity, Const.KEY_LONGITUDE, "");

        if (LoginManager.getInstance().getCurrentCampus() != null) {

            if (LoginManager.getInstance().getCurrentCampus().getId() != null) {
                campusId = LoginManager.getInstance().getCurrentCampus().getId();
            }
        }

        try {
            jsonObject.put(Const.KEY_CUSTOMER_ID,LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_LATITUDE, latitude);
            jsonObject.put(Const.KEY_LONGITUDE, longitude);
            jsonObject.put(Const.KEY, searchKey);
            jsonObject.put(Const.KEY_SHORT, filterType);

           if (subCategoryId.equalsIgnoreCase( "0")) {
               if (categoryId.length() != 0)
                   jsonObject.put(Const.KEY_CATEGORY_ID, categoryId);
           }else
           {
               if (subCategoryId.length() != 0)
               jsonObject.put(Const.KEY_SUBCATEGORY_ID, subCategoryId);
           }


            if (campusId != 0) {

                jsonObject.put(Const.KEY_IN_CAMPUS, isInCampus == true ? 1:0);
                jsonObject.put(Const.KEY_CAPMUS_ID, campusId);

//                if (isInCampus) {
//                    jsonObject.put(Const.KEY_CAPMUS_ID, campusId);
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<SearchDataResponseModel> getCreatePasswordResult = ApiAdapter.getApiService().getSearchData("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<SearchDataResponseModel>() {
            @Override
            public void onResponse(Call<SearchDataResponseModel> call, Response<SearchDataResponseModel> response) {

                Progress.stop();
                try {
                    SearchDataResponseModel searchDataResponseModel = response.body();


                    if (searchDataResponseModel.getError() == 0) {
                        homePageView.getSearchDataSuccess(searchDataResponseModel.getData());
                    } else {
                        homePageView.getSearchDataUnSucess(searchDataResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    homePageView.getSearchDataUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<SearchDataResponseModel> call, Throwable t) {
                Progress.stop();
                homePageView.getSearchDataUnSucess(activity.getString(R.string.server_error));
            }
        });

    }
}
