package com.deliverymyorder.mvp.dashboard.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.CircleTransform;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.CustomShopOrderActivity;
import com.deliverymyorder.mvp.add_category.CategorySubcategoryActivity;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.shop_listing.ShopListingActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopularCategoryAdapter extends RecyclerView.Adapter<PopularCategoryAdapter.Holder> {


    Activity activity;
    ArrayList<HomePageDataModel.Data.PopularCategory> datum;
    String favBit;

    public PopularCategoryAdapter(Activity activity, ArrayList<HomePageDataModel.Data.PopularCategory> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_category, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        if (position < datum.size()) {
            final HomePageDataModel.Data.PopularCategory result = datum.get(position);


            holder.txtViewCategoryName.setText(result.getName());

            if (Utils.isEmptyOrNull(result.getImage())) {
                holder.txtViewCategoryName.setText("Tell your needs");
                Picasso.with(activity).load(R.mipmap.ic_custom).transform(new CircleTransform()).into(holder.imgViewCategory);
            } else {
                Picasso.with(activity).load(result.getImage()).transform(new CircleTransform()).into(holder.imgViewCategory);
            }

            holder.relLayItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!Utils.isEmptyOrNull(result.getName())) {


                        Intent intent = new Intent(activity, ShopListingActivity.class);
                        intent.putExtra(ConstIntent.CONST_CATEGORY_ID, result.getCategoryId());
                        intent.putExtra(ConstIntent.CONST_CATEGORY_NAME, result.getName());
                         activity.startActivity(intent);
                    } else {

                        Intent i = new Intent(activity,CustomShopOrderActivity.class);
                        activity.startActivity(i);
                    }

                }
            });

        }else
        {

            holder.txtViewCategoryName.setText("More");
            holder.imgViewCategory.setImageDrawable(activity.getResources().getDrawable(R.mipmap.ic_cat_more));

//            if (Utils.isEmptyOrNull(result.getImage())) {
//                Picasso.with(activity).load(R.mipmap.ic_cat_more).transform(new CircleTransform()).into(holder.imgViewCategory);
//            } else {
//                Picasso.with(activity).load(result.getImage()).transform(new CircleTransform()).into(holder.imgViewCategory);
//            }

            holder.relLayItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    Intent intent = new Intent(activity, CategorySubcategoryActivity.class);
                    activity.startActivity(intent);
                }
            });

        }

    }


    @Override
    public int getItemCount() {
        return datum.size() + 1;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgViewCategory)
        ImageView imgViewCategory;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}


