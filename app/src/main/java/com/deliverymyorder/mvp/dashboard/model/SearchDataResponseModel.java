package com.deliverymyorder.mvp.dashboard.model;

import com.deliverymyorder.Common.utility.DMOMerchat;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Braintech on 24-12-2018.
 */

public class SearchDataResponseModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {

            if (this.data !=null && this.data.size() != 0)
            {
                ArrayList<Datum> nearbyShops111 = new ArrayList<>();

                for (Datum nearByShop:this.data)
                {
                    if (DMOMerchat.merchant_id != nearByShop.getMerchant_id() && DMOMerchat.old_merchant_id != nearByShop.getMerchant_id())
                    {
                        nearbyShops111.add(nearByShop);
                    }
                }
                return  nearbyShops111;
            }

        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("merchant_id")
        @Expose
        private Integer merchant_id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("distance")
        @Expose
        private Double distance;

        @SerializedName("custom_order_allowed")
        @Expose
        private Integer custom_order_allowed;

        @SerializedName("total_cost")
        @Expose
        private Float totalDeliveryCost = 0.0f;

        public Float getTotalDeliveryCost() {

            if (totalDeliveryCost== null)
            {
                this.totalDeliveryCost = 0.0f;
            }
            return totalDeliveryCost;
        }

        public void setTotalDeliveryCost(Float totalDeliveryCost) {
            this.totalDeliveryCost = totalDeliveryCost;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getDistance() {
            return distance;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

        public Integer getMerchant_id() {
            return merchant_id;
        }

        public void setMerchant_id(Integer merchant_id) {
            this.merchant_id = merchant_id;
        }

        public Integer getCustom_order_allowed() {
            return custom_order_allowed;
        }

        public void setCustom_order_allowed(Integer custom_order_allowed) {
            this.custom_order_allowed = custom_order_allowed;
        }
    }

}
