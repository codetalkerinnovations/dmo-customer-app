package com.deliverymyorder.mvp.dashboard;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.SplashActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Braintech on 17-04-2018.
 */

public class LocationFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_location, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);


    }
    /****************************** ButterKnife Click *****************************/

    @OnClick(R.id.imgCross)
    public void closeDialog() {
        AlertDialogHelper.showDialogToClose(getActivity());
        getDialog().dismiss();
    }

    @OnClick(R.id.llayAllow)
    public void allowLocation() {
        PrefUtil.putInt(getActivity(), Const.LOCATION_PERMISSION, 1);
        ((SplashActivity) getActivity()).checkRunTimePermission();
        getDialog().dismiss();
    }

    @OnClick(R.id.llayDontAllow)
    public void dontAllowLocation() {
        PrefUtil.putInt(getActivity(), Const.LOCATION_PERMISSION, 2);
        AlertDialogHelper.showDialogToClose(getActivity());
        getDialog().dismiss();
    }


}