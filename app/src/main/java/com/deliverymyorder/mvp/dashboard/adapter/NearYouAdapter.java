package com.deliverymyorder.mvp.dashboard.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.RoundCornerTransformation;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.fragment.DashboardFragment;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.favorite.FavouriteFragment;
import com.deliverymyorder.mvp.shop_detail.ShopDetailsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NearYouAdapter extends RecyclerView.Adapter<NearYouAdapter.Holder> {


    Activity activity;
    ArrayList<HomePageDataModel.Data.NearByShop> datum;
    String favBit;

    DashboardFragment dashboardFragment;
    FavouriteFragment favouriteFragment;

    public NearYouAdapter(Activity activity, ArrayList<HomePageDataModel.Data.NearByShop> datum, DashboardFragment dashboardFragment) {
        this.activity = activity;
        this.datum = datum;
        this.dashboardFragment = dashboardFragment;
    }

    public NearYouAdapter(Activity activity, ArrayList<HomePageDataModel.Data.NearByShop> datum, FavouriteFragment favouriteFragment) {
        this.activity = activity;
        this.datum = datum;
        this.favouriteFragment = favouriteFragment;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_near_you, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final HomePageDataModel.Data.NearByShop result = datum.get(position);


        holder.txtViewCategoryName.setText(result.getTitle());

        if(result.getIsFavorite()==0){

            holder.imgViewFavourite.setBackgroundResource(R.mipmap.ic_fav_off);
        }else{
            holder.imgViewFavourite.setBackgroundResource(R.mipmap.ic_fav_on);
        }


        if (Utils.isEmptyOrNull(result.getImage())) {
            //holder.relLayItem.setVisibility(View.GONE);
            holder.imgViewCategory.setScaleType(ImageView.ScaleType.CENTER);
            holder.imgViewCategory.setImageDrawable(activity.getResources().getDrawable(R.mipmap.ic_shop_placeholder));
        } else {
            holder.relLayItem.setVisibility(View.VISIBLE);

            try {
                Picasso.with(activity).load(result.getImage()).transform(new RoundCornerTransformation()).into(holder.imgViewCategory);

//                Picasso.with(activity).load(result.getImage())
//                        .into(holder.imgViewCategory, new Callback() {
//                            @Override
//                            public void onSuccess() {
//                                Bitmap imageBitmap = ((BitmapDrawable) holder.imgViewCategory.getDrawable()).getBitmap();
//                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(activity.getResources(), imageBitmap);
//                                imageDrawable.setCircular(true);
//                                imageDrawable.setCornerRadius(Math.max(10,10));
//                                holder.imgViewCategory.setImageDrawable(imageDrawable);
//                            }
//                            @Override
//                            public void onError() {
//                                holder.imgViewCategory.setImageResource(R.mipmap.ic_img_placeholder);
//                            }
//                        });

                holder.imgViewCategory.setScaleType(ImageView.ScaleType.FIT_XY);
            }catch (Exception e)
            {
                holder.imgViewCategory.setScaleType(ImageView.ScaleType.CENTER);

                holder.imgViewCategory.setImageDrawable(activity.getResources().getDrawable(R.mipmap.ic_shop_placeholder));
                e.printStackTrace();
            }
        }

        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int buissnessId = result.getId();

                Intent intent = new Intent(activity, ShopDetailsActivity.class);
                intent.putExtra(ConstIntent.CONST_BUISNESS_ID,buissnessId );
                intent.putExtra(ConstIntent.CONST_MERCHANT_ID, result.getMerchantId());
                intent.putExtra(ConstIntent.CONST_SHOP_NAME, result.getTitle());
                intent.putExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, result.getCustom_order_allowed());
                intent.putExtra(ConstIntent.CONST_ADDRESS, "");
                activity.startActivity(intent);

            }
        });

        holder.imgViewFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (dashboardFragment != null) {

                    if (result.getIsFavorite() == 0) {
                        dashboardFragment.callUpdateFavoriteStatusDataApi(result.getId(), 1);
                    } else {
                        dashboardFragment.callUpdateFavoriteStatusDataApi(result.getId(), 0);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgViewCategory)
        ImageView imgViewCategory;

        @BindView(R.id.imgViewFavourite)
        ImageView imgViewFavourite;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}


