package com.deliverymyorder.mvp.dashboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Parcelable;
import android.service.autofill.Transformation;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.deliverymyorder.Common.helpers.RoundCornerTransformation;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.offers.OffersDetailActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends android.support.v4.view.PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<HomePageDataModel.Data.Banner> mResources;



    public ImageAdapter(Context mContext, ArrayList<HomePageDataModel.Data.Banner> mResources) {
        this.mResources = mResources;
        this.mContext = mContext;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.layout_item_banner, container, false);


        //Picasso.with(activity).load(url).transform(transformation).into(imageView);
      final  ImageView imageViewBanner = (ImageView) itemView.findViewById(R.id.imageViewBanner);
        LinearLayout linLayItem = (LinearLayout) itemView.findViewById(R.id.linLayItem);

        try {
        container.addView(itemView, position);


        imageViewBanner.setTag(position);


            Picasso.with(mContext).load(mResources.get(position).getImage())
                    .into(imageViewBanner, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) imageViewBanner.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(10,10));
                            imageViewBanner.setImageDrawable(imageDrawable);
                        }
                        @Override
                        public void onError() {
                            imageViewBanner.setImageResource(R.mipmap.ic_img_placeholder);
                        }
                    });

            //final Transformation transformation = new RoundCornerTransformation();
//            Picasso.with(mContext)
//                    .load(mResources
//                            .get(position)
//                            .getImage()).placeholder(mContext.getDrawable(R.mipmap.ic_img_placeholder)).into(imageViewBanner);
                // transform(new RoundCornerTransformation()).into(imageViewBanner);
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
            //this.
        }

        linLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, OffersDetailActivity.class);
                intent.putExtra(ConstIntent.CONST_COUPEN_CODE, mResources.get(position).getCouponCode());
                intent.putExtra(ConstIntent.CONST_COUPEN_IMAGE, mResources.get(position).getImage());
                intent.putExtra(ConstIntent.CONST_START_DATE, mResources.get(position).getValidFrom());
                intent.putExtra(ConstIntent.CONST_EXP_DATE, mResources.get(position).getValidTo());

                mContext.startActivity(intent);

            }
        });

        return itemView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }





}
