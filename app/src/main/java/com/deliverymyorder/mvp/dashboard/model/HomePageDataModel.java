package com.deliverymyorder.mvp.dashboard.model;

import com.deliverymyorder.Common.utility.DMOMerchat;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HomePageDataModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Data {

        @SerializedName("banner")
        @Expose
        private ArrayList<Banner> banner = null;
        @SerializedName("popular_category")
        @Expose
        private ArrayList<PopularCategory> popularCategory = null;
        @SerializedName("nearByShops")
        @Expose
        private ArrayList<NearByShop> nearByShops = null;


        public ArrayList<PopularCategory> getPopularCategory() {
            return popularCategory;
        }

        public void setPopularCategory(ArrayList<PopularCategory> popularCategory) {
            this.popularCategory = popularCategory;
        }

        public ArrayList<NearByShop> getNearByShops() {

            if (this.nearByShops !=null && this.nearByShops.size() != 0)
            {
                ArrayList<NearByShop> nearbyShops111 = new ArrayList<>();

                for (NearByShop nearByShop:this.nearByShops)
                {
                    if (DMOMerchat.merchant_id != nearByShop.getMerchantId() && DMOMerchat.old_merchant_id != nearByShop.getMerchantId())
                    {
                        nearbyShops111.add(nearByShop);
                    }
                }
                return  nearbyShops111;
            }
            return nearByShops;
        }

        public void setNearByShops(ArrayList<NearByShop> nearByShops) {
            this.nearByShops = nearByShops;
        }

        public ArrayList<Banner> getBanner() {
            return banner;
        }

        public void setBanner(ArrayList<Banner> banner) {
            this.banner = banner;
        }


        public class Banner {

            @SerializedName("coupon_code")
            @Expose
            private String couponCode;
            @SerializedName("valid_from")
            @Expose
            private String validFrom;
            @SerializedName("valid_to")
            @Expose
            private String validTo;
            @SerializedName("image")
            @Expose
            private String image;

            public String getCouponCode() {
                return couponCode;
            }

            public void setCouponCode(String couponCode) {
                this.couponCode = couponCode;
            }

            public String getValidFrom() {
                return validFrom;
            }

            public void setValidFrom(String validFrom) {
                this.validFrom = validFrom;
            }

            public String getValidTo() {
                return validTo;
            }

            public void setValidTo(String validTo) {
                this.validTo = validTo;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

        }

        public class NearByShop {

            @SerializedName("id")
            @Expose
            private Integer id;



            //merchant_id
            @SerializedName("merchant_id")
            @Expose
            private Integer merchantId;


            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("image")
            @Expose
            private String image;
            @SerializedName("rating")
            @Expose
            private Integer rating;

            //custom_order_allowed
            @SerializedName("custom_order_allowed")
            @Expose
            private Integer custom_order_allowed;

            @SerializedName("isFavorite")
            @Expose
            private Integer isFavorite;
            @SerializedName("distance")
            @Expose
            private Double distance;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Integer getRating() {
                return rating;
            }

            public void setRating(Integer rating) {
                this.rating = rating;
            }

            public Double getDistance() {
                return distance;
            }

            public void setDistance(Double distance) {
                this.distance = distance;
            }

            public Integer getIsFavorite() {
                return isFavorite;
            }

            public void setIsFavorite(Integer isFavorite) {
                this.isFavorite = isFavorite;
            }


            public Integer getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(Integer merchantId) {
                this.merchantId = merchantId;
            }

            public Integer getCustom_order_allowed() {

                if (custom_order_allowed == null)
                {
                    custom_order_allowed = 0;
                }

                return custom_order_allowed;
            }

            public void setCustom_order_allowed(Integer custom_order_allowed) {
                this.custom_order_allowed = custom_order_allowed;
            }

        }

        public class PopularCategory {

            @SerializedName("category_id")
            @Expose
            private Integer categoryId;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("image")
            @Expose
            private String image;
            @SerializedName("parent_id")
            @Expose
            private Integer parentId;
            @SerializedName("top")
            @Expose
            private Integer top;
            @SerializedName("column")
            @Expose
            private Integer column;
            @SerializedName("meta_title")
            @Expose
            private String metaTitle;
            @SerializedName("meta_description")
            @Expose
            private String metaDescription;
            @SerializedName("meta_keyword")
            @Expose
            private String metaKeyword;
            @SerializedName("sort_order")
            @Expose
            private Integer sortOrder;
            @SerializedName("status")
            @Expose
            private Integer status;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("more")
            @Expose
            private Integer more;

            public Integer getCategoryId() {
                return categoryId;
            }

            public void setCategoryId(Integer categoryId) {
                this.categoryId = categoryId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Integer getParentId() {
                return parentId;
            }

            public void setParentId(Integer parentId) {
                this.parentId = parentId;
            }

            public Integer getTop() {
                return top;
            }

            public void setTop(Integer top) {
                this.top = top;
            }

            public Integer getColumn() {
                return column;
            }

            public void setColumn(Integer column) {
                this.column = column;
            }

            public String getMetaTitle() {
                return metaTitle;
            }

            public void setMetaTitle(String metaTitle) {
                this.metaTitle = metaTitle;
            }

            public String getMetaDescription() {
                return metaDescription;
            }

            public void setMetaDescription(String metaDescription) {
                this.metaDescription = metaDescription;
            }

            public String getMetaKeyword() {
                return metaKeyword;
            }

            public void setMetaKeyword(String metaKeyword) {
                this.metaKeyword = metaKeyword;
            }

            public Integer getSortOrder() {
                return sortOrder;
            }

            public void setSortOrder(Integer sortOrder) {
                this.sortOrder = sortOrder;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public Integer getMore() {
                return more;
            }

            public void setMore(Integer more) {
                this.more = more;
            }

        }
    }
}