package com.deliverymyorder.mvp.dashboard;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.LocationProvider;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.fragment.DashboardFragment;
import com.deliverymyorder.mvp.favorite.FavouriteFragment;
import com.deliverymyorder.mvp.login.LoginActivity;
import com.deliverymyorder.mvp.profile.ProfileFragment;
import com.deliverymyorder.mvp.track_order.TrackOrderFragment;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;


    /*bottom tab*/
    @BindView(R.id.imgViewHome)
    ImageView imgViewHome;

    @BindView(R.id.imgViewTracking)
    ImageView imgViewTracking;

    @BindView(R.id.imgViewfav)
    ImageView imgViewfav;

    @BindView(R.id.imgViewProfile)
    ImageView imgViewProfile;


    /* *//*toolbar*//*
    @BindView(R.id.relLayToolbar)
    RelativeLayout relLayToolbar;

    @BindView(R.id.imgViewMenu)
    ImageView imgViewMenu;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.frameCart)
    FrameLayout frameCart;

    @BindView(R.id.txtViewCount)
    TextView txtViewCount;*/


    @BindView(R.id.txtViewLogout)
    TextView txtViewLogout;


    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ButterKnife.bind(this);

        checkReadWritePermimssion();

        fragmentManager = getSupportFragmentManager();



        if (getIntent() != null) {

            if (getIntent().getBooleanExtra("ISOPEN_HOME_FRAGMENT", true)) {
                DashboardFragment dashboardFragment = new DashboardFragment();

                imgViewTracking.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
                imgViewHome.setColorFilter(ContextCompat.getColor(this,R.color.colorButton));
                imgViewfav.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
                imgViewProfile.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));

                addDashboardFragment(dashboardFragment, getString(R.string.tag_dashboard));
            } else {

                TrackOrderFragment trackOrderFragment = new TrackOrderFragment();

                imgViewTracking.setColorFilter(ContextCompat.getColor(this,R.color.colorButton));
                imgViewHome.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
                imgViewfav.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
                imgViewProfile.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));

                addDashboardFragment(trackOrderFragment, getString(R.string.tag_tracking));
            }
        }

/*

        imgViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                } else {
                    drawer_layout.openDrawer(GravityCompat.START);
                }
            }
        });
*/



    }



    public  void  checkReadWritePermimssion()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("Dashboard","Permission is granted");
                //File write logic here
                return;
            }else
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 4433);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            loginPresenter.handleSignInResult(result);
//        } else {
//            mCallbackManager.onActivityResult(requestCode, resultCode, data);
//        }

    }

    @Override
    protected void onStart() {
        super.onStart();



    }




    private void addDashboardFragment(Fragment fragment, String tag) {


        Utils.hideKeyboardIfOpen(this);
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.rlyFragmentLayout, fragment, tag);
        transaction.commitAllowingStateLoss();

    }

    public void replaceFragment(Fragment fragment, String tag) {
        if (!Utils.isVisibleOrNull(fragment)) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.rlyFragmentLayout, fragment, tag);
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    private void showMessage() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);

        alertDialog.setMessage("Are you sure you want to logout ?");

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(intent);

                finishAffinity();

            }
        });

        alertDialog.show();

    }

    @Override
    public void onBackPressed() {

      /*  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.relLayMain);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {*/
        Fragment dashboardFragment = (DashboardFragment) fragmentManager.findFragmentByTag(getString(R.string.nav_dashboard));
        if (fragmentManager.getBackStackEntryCount() > 1) {
            if (Utils.isVisibleOrNull(dashboardFragment)) {

                SnackNotify.showSnakeBarForBackPress(relLayMain);

            } else {
                fragmentManager.popBackStack();
            }
        } else {
            SnackNotify.showSnakeBarForBackPress(relLayMain);
        }

    }

    public void drawerClose() {

       /* if (relLayMain.isDrawerOpen(GravityCompat.START)) {
            relLayMain.closeDrawer(GravityCompat.START);
        }*/
    }


    @OnClick(R.id.imgViewHome)
    public void imgViewHomeClicked() {

        imgViewTracking.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewHome.setColorFilter(ContextCompat.getColor(this,R.color.colorButton));
        imgViewfav.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewProfile.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));


        DashboardFragment dashboardFragment = new DashboardFragment();

        replaceFragment(dashboardFragment, getString(R.string.tag_dashboard));

    }

    @OnClick(R.id.imgViewProfile)
    public void imgViewProfileClicked() {

        imgViewTracking.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewHome.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewfav.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewProfile.setColorFilter(ContextCompat.getColor(this,R.color.colorButton));


        ProfileFragment profileFragment = new ProfileFragment();

        replaceFragment(profileFragment, getString(R.string.tag_profile));

    }


    @OnClick(R.id.imgViewTracking)
    public void imgViewTrackingClicked() {

        imgViewTracking.setColorFilter(ContextCompat.getColor(this,R.color.colorButton));
        imgViewHome.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewfav.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewProfile.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));

        TrackOrderFragment trackOrderFragment = new TrackOrderFragment();

        replaceFragment(trackOrderFragment, getString(R.string.tag_tracking));

    }

    @OnClick(R.id.imgViewfav)
    public void imgViewfavClicked() {

        imgViewTracking.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewHome.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));
        imgViewfav.setColorFilter(ContextCompat.getColor(this,R.color.colorButton));
        imgViewProfile.setColorFilter(ContextCompat.getColor(this,R.color.color_gray));

        FavouriteFragment favouriteFragment = new FavouriteFragment();

        replaceFragment(favouriteFragment, getString(R.string.tag_favorite));

    }



}
