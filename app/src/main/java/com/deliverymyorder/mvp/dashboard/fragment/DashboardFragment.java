package com.deliverymyorder.mvp.dashboard.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.LocationProvider;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.ViewPagerCustomDuration;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.ApiResult;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.AddCategoryActivity;
import com.deliverymyorder.mvp.address.AddressListActivity;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.address.presenter.AddressDetailContractor;
import com.deliverymyorder.mvp.address.presenter.AddressDetailPresenterImpl;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;
import com.deliverymyorder.mvp.dashboard.adapter.ImageAdapter;
import com.deliverymyorder.mvp.dashboard.adapter.NearYouAdapter;
import com.deliverymyorder.mvp.dashboard.adapter.PopularCategoryAdapter;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.dashboard.presenter.HomePageContractor;
import com.deliverymyorder.mvp.dashboard.presenter.HomePagePresenterImpl;
import com.deliverymyorder.mvp.dashboard.slider.PagerContainer;
import com.deliverymyorder.mvp.favorite.model.FavoriteListModel;
import com.deliverymyorder.mvp.favorite.model.UpdateFavoriteResponseModel;
import com.deliverymyorder.mvp.favorite.presenter.FavoriteContractor;
import com.deliverymyorder.mvp.favorite.presenter.FavoritePresenterImpl;
import com.deliverymyorder.mvp.pick_drop.WhereToDeliverActivity;
import com.deliverymyorder.mvp.search_fragment.SearchFragment;
import com.deliverymyorder.mvp.service_not_aviailble_page.CheckServiceAvailblityContractor;
import com.deliverymyorder.mvp.service_not_aviailble_page.CheckServiceAvailblityImpl;
import com.deliverymyorder.mvp.service_not_aviailble_page.ServiceNotAvailableActivity;
import com.deliverymyorder.mvp.show_current_capus.CurrentCampusFragment;
import com.deliverymyorder.mvp.show_current_capus.adapter.CampusAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DashboardFragment extends Fragment implements HomePageContractor.HomePageView, FavoriteContractor.FavoriteView, AddressDetailContractor.AddressDetailView, CheckServiceAvailblityContractor.CheckServiceAvailbityView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.relLaySearch)
    RelativeLayout relLaySearch;

    @BindView(R.id.imgViewSearch)
    ImageView imgViewSearch;

    @BindView(R.id.edtTextSearchShop)
    EditText edtTextSearchShop;

    @BindView(R.id.imgViewSearchButton)
    ImageView imgViewSearchButton;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.txtViewOrderNow)
    TextView txtViewOrderNow;

    @BindView(R.id.txtViewPickDrop)
    TextView txtViewPickDrop;

    @BindView(R.id.pager_container)
    PagerContainer pager_container;

    @BindView(R.id.recyclerViewPopularCategories)
    RecyclerView recyclerViewPopularCategories;


    @BindView(R.id.recyclerViewNearBy)
    RecyclerView recyclerViewNearBy;

    @BindView(R.id.txtViewDeliveringTo)
    TextView txtViewDeliveringTo;

    @BindView(R.id.txtViewCurrentCampus)
    TextView txtViewCurrentCampus;


    TabLayout tabLayout;
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 3000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 6000; // time in milliseconds between successive task executions.

    int NUM_PAGES = 0;

    ArrayList<Integer> bannerModel;
    ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel;

    String searchKey;

    HomePagePresenterImpl homePagePresenter;
    FavoritePresenterImpl favoritePresenter;


    PopularCategoryAdapter popularCategoryAdapter;
    NearYouAdapter nearYouAdapter;
    HomePageDataModel.Data homePageDataModel;

    AddressDetailPresenterImpl addressDetailPresenter;


    int favStatus;
    int buisnessId;

    AddressDetailModel.Data mAddressDetailModel;

    CheckServiceAvailblityImpl checkServiceAvailblityImpl;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);


        Campus campus = LoginManager.getInstance().getCurrentCampus();
        if (campus != null) {
            txtViewCurrentCampus.setVisibility(View.VISIBLE);
            txtViewCurrentCampus.setText("Current campus :" + campus.getTitle());
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        homePagePresenter = new HomePagePresenterImpl(getActivity(), this);
        favoritePresenter = new FavoritePresenterImpl(getActivity(), this);
        addressDetailPresenter = new AddressDetailPresenterImpl(getActivity(), this);
        checkServiceAvailblityImpl = new CheckServiceAvailblityImpl(getActivity(),this);


        homePagePresenter.getCampusList();

        //bottomNavigation();

        setFont();

        bannerModel = new ArrayList<>();

        bannerModel.add(R.drawable.ic_banner_one);
        bannerModel.add(R.drawable.ic_banner_two);
        bannerModel.add(R.drawable.ic_banner_three);
        bannerModel.add(R.drawable.ic_banner_four);

        viewPager.setOffscreenPageLimit(3);


        //Check if Location has changed


        String strLatitude = PrefUtil.getString(getActivity(), Const.KEY_LATITUDE, "");
        String strLongitude = PrefUtil.getString(getActivity(), Const.KEY_LONGITUDE, "");

        checkServiceAvailblityImpl.checkServiceAvailablity(strLatitude, strLongitude);


//        LocationProvider.getSharedInstacne().getLocation(getActivity(), new LocationProvider.LocationResult() {
//            @Override
//            public void gotLocation(Location location) {
//
//
//
//                //  if (strLatitude!=null && strLatitude!=null && strLatitude.length()!= 0  && strLongitude.length() != 0) {
//
//                Location currentSavedLoc = new Location("current");
//                try {
//                    currentSavedLoc.setLatitude(Double.parseDouble(strLatitude));
//                    currentSavedLoc.setLatitude(Double.parseDouble(strLongitude));
//
//                }catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//
//                float disatance = location.distanceTo(currentSavedLoc);
//
//                if (disatance >= 300) {
//                    checkServiceAvailblityImpl.checkServiceAvailablity(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
//                    getAddressFromLatLongUpdated(location.getLatitude(), location.getLongitude());
//                }
//
//            }
//        });


    }

    @Override
    public void onStart() {
        super.onStart();
        callHomePageDataApi();
    }

    @OnClick(R.id.imgSearchView)
    public  void imgSearchView(View view)
    {
        try{

            DashboardActivity dashboardActivity = (DashboardActivity)getActivity();

            SearchFragment searchFragment = SearchFragment.newInstance();
            dashboardActivity.replaceFragment(searchFragment,"SearchFragment");

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void callHomePageDataApi() {
        homePagePresenter.getHomePageData();
    }

    @Override
    public void onResume() {
        super.onResume();

        int addressId = PrefUtil.getInt(getActivity(), Const.KEY_ADDRESS_ID, 0);
        addressDetailPresenter.getAddressDetailData(addressId);
        imgViewSearch.setVisibility(View.GONE);
        relLaySearch.setVisibility(View.GONE);
    }


    public void callUpdateFavoriteStatusDataApi(int buisnessId, int favStatus) {

        this.buisnessId = buisnessId;
        this.favStatus = favStatus;

        favoritePresenter.updateFavoriteStatus(buisnessId, favStatus);
    }

    private void setFont() {

        //FontHelper.setFontFace(txtViewDeliveringToTitle, FontHelper.FontType.FONT_ROBOTO_REGULAR, this);
    }

    private void manageRecyclerView() {

        popularCategoryAdapter = new PopularCategoryAdapter(getActivity(), homePageDataModel.getPopularCategory());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewPopularCategories.setLayoutManager(mLayoutManager);
        recyclerViewPopularCategories.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPopularCategories.setAdapter(popularCategoryAdapter);
    }

    private void manageRecyclerViewForNearShops() {

        nearYouAdapter = new NearYouAdapter(getActivity(), homePageDataModel.getNearByShops(), this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewNearBy.setLayoutManager(mLayoutManager);
        recyclerViewNearBy.setItemAnimator(new DefaultItemAnimator());
        recyclerViewNearBy.setAdapter(nearYouAdapter);
    }


    @Override
    public void getHomePageDataSuccess(HomePageDataModel.Data homePageDataModel) {

        this.homePageDataModel = homePageDataModel;

        init();
        manageRecyclerView();
        manageRecyclerViewForNearShops();
    }


    @OnClick(R.id.txtViewCurrentCampus)
    public void txtViewCurrentCampus(View view) {

        Campus campus = LoginManager.getInstance().getCurrentCampus();
        if (campus != null) {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

            alertDialog.setTitle("Current Campus");

            alertDialog.setMessage("You are currently under " + campus.getTitle() + " campus , to order from outside of this campus, click on  remove");

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Remove campus", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    // Remove campus
                    LoginManager.getInstance().clearCurrentCampus();

                    txtViewCurrentCampus.setVisibility(View.GONE);
                    txtViewCurrentCampus.setText("");

                }
            });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    //...

                }
            });

            alertDialog.show();
        }

    }

    @Override
    public void getHomePageDataUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void homePageDataInternetError() {

    }

    @Override
    public void getSearchDataSuccess(ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel) {

        this.arrayListSearchDataResponseModel = arrayListSearchDataResponseModel;
    }

    @Override
    public void getSearchDataUnSucess(String message) {

    }

    @Override
    public void searchDataInternetError() {

    }

    @Override
    public void getCampusApiDidFinishWith(Result result, List<Campus> campusList) {

        try {

            if (campusList == null || campusList.size() == 0) {
                return;
            }

            if (result.getResult() == ApiResult.SUCCESS) {

                // check if current campus is inside
                Campus currentCampus = LoginManager.getInstance().getCurrentCampus();
                if (currentCampus != null) {

                    for (Campus campus : campusList) {
                        if (currentCampus.getId() == campus.getId()) {
                            return;
                        }

                    }

                }

                LoginManager.getInstance().clearCurrentCampus();
                txtViewCurrentCampus.setVisibility(View.GONE);

                DashboardActivity dashboardActivity = (DashboardActivity) getActivity();
                CurrentCampusFragment currentCampusFragment = CurrentCampusFragment.newInstance(campusList);
                currentCampusFragment.show(getChildFragmentManager(), "CurrentCampusFragment");

                currentCampusFragment.setCampusChangeListner(new CampusAdapter.CampusChangeListner() {
                    @Override
                    public void campusDidChangeStatus(Campus campus) {

                        txtViewCurrentCampus.setVisibility(View.VISIBLE);
                        txtViewCurrentCampus.setText("Current campus :" + campus.getTitle());
                    }
                });
                // dashboardActivity.replaceFragment(currentCampusFragment,"CurrentCampusFragment");
            } else {
                LoginManager.getInstance().clearCurrentCampus();

                txtViewCurrentCampus.setVisibility(View.GONE);

                // Don't do anything if any error
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    private void init() {


        final ViewPagerCustomDuration pager = (ViewPagerCustomDuration) pager_container.getViewPager();
        pager.setScrollDurationFactor(5);
        // final  ViewPager pager =

        ImageAdapter adapter = new ImageAdapter(getActivity(), homePageDataModel.getBanner());
        pager.setAdapter(adapter);


        //Necessary or the pager will only have one extra page to show
        // make this at least however many pages you can see


        //shubham

        //pager.setOffscreenPageLimit(adapter.getCount());
        //A little space between pages
        pager.setPageMargin(70);
        /// pager.setPageTransformer(true, new com.deliverymyorder.mvp.dashboard.slider.ZoomOutPageTransformer(true));
        // clipping on the pager for its children.
        pager.setClipChildren(true);


        pager.addOnPageChangeListener(new DetailOnPageChangeListener());

         NUM_PAGES =  homePageDataModel.getBanner().size();// bannerModel.size();
        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {

            public void run() {

                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0;
                    pager.setScrollDurationFactor(0);

                } else {
                    pager.setScrollDurationFactor(5);
                    currentPage = currentPage + 1;

                }

               if (currentPage < NUM_PAGES - 1) {
                   pager.setCurrentItem(currentPage, true);
               }else
               {
                   currentPage = 0;
               }
            }
        };


        if (timer!=null)
        {
            timer.cancel();
        }

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

    }

    @Override
    public void getFavoriteSuccess(ArrayList<FavoriteListModel.Datum> arrayListFavorite) {

        //Not Used
    }


    @Override
    public void getFavoriteUnSucess(String message) {
//Not Used
    }

    @Override
    public void getFavoriteInternetError() {
//Not Used
    }

    @Override
    public void updateFavoriteSuccess(UpdateFavoriteResponseModel.Data updateFavoriteResponseModel) {

        callHomePageDataApi();
    }

    @Override
    public void updateFavoriteUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void updateFavoriteInternetError() {

    }

    @Override
    public void getAddressDetailSuccess(AddressDetailModel.Data addressDetailModel) {
        mAddressDetailModel = addressDetailModel;
        if (addressDetailModel != null) {
            txtViewDeliveringTo.setText(addressDetailModel.getHouseNo() + ", " + addressDetailModel.getCity());

            /*


            */


        } else {
            txtViewDeliveringTo.setText("Unnamed location");

            String latitude = PrefUtil.getString(getActivity(), Const.KEY_LATITUDE, "");
             String longitude = PrefUtil.getString(getActivity(), Const.KEY_LONGITUDE, "");

            if (latitude!=null && longitude!=null && latitude.length()!= 0  && longitude.length() != 0) {
                checkServiceAvailblityImpl.checkServiceAvailablity(latitude, longitude);
            }else
            {

                LocationProvider.getSharedInstacne().getLocation(getActivity(), new LocationProvider.LocationResult() {
                    @Override
                    public void gotLocation(Location location) {

                        PrefUtil.putString(getActivity(), Const.KEY_LATITUDE,String.valueOf(location.getLatitude()));
                        PrefUtil.putString(getActivity(), Const.KEY_LONGITUDE, String.valueOf(location.getLongitude()));
                        checkServiceAvailblityImpl.checkServiceAvailablity(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));

                       getAddressFromLatLongUpdated(location.getLatitude(),location.getLongitude());
                    }
                });
            }


//            LocationProvider.getSharedInstacne().getLocation(getActivity(), new LocationProvider.LocationResult() {
//                @Override
//                public void gotLocation(Location location) {
//                    //getAddressFromLatLongUpdated(location.getLatitude(),location.getLongitude());
//                }
//            });


        }
    }

    @Override
    public void getAddressDetailUnSucess(String message) {
        txtViewDeliveringTo.setText("Unnamed location");

        LocationProvider.getSharedInstacne().getLocation(getActivity(), new LocationProvider.LocationResult() {
            @Override
            public void gotLocation(Location location) {
                getAddressFromLatLongUpdated(location.getLatitude(),location.getLongitude());
            }
        });
    }


    private void getAddressFromLatLongUpdated(Double latitude,Double longitude) {

        String address;

        Geocoder geocoder;

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
           List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {

                String locality = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();


                address =  locality+ "," +city; //addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()


                if (address == null)
                {
                    address = "unnamed";
                }else
                {
                    txtViewDeliveringTo.setText(address);
                }

                Log.e("Addr Lat", address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getAddressDetailInternetError() {
        txtViewDeliveringTo.setText("Unnamed location");
    }

    @OnClick(R.id.btnChangeAddress)
    public void btnChangeAddress() {
        Intent intent = new Intent(getActivity(), AddressListActivity.class);
        startActivity(intent);
    }


    @Override
    public void checkServiceAaiblityDidFinshWithResult(boolean status) {
        if (!status)
        {
//
            if (getActivity()!=null) {
                Intent i = new Intent(getActivity(), ServiceNotAvailableActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        }
    }

    @Override
    public void checkServiceAvaiblityNetworkError() {

    }


    public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            currentPage = position;
        }

        public final int getCurrentPage() {
            return currentPage;
        }
    }

    @OnClick(R.id.imgViewSearch)
    public void imgViewSearch() {

        imgViewSearch.setVisibility(View.GONE);
        relLaySearch.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.imgViewSearchButton)
    public void imgViewSearchButton() {

        searchKey = String.valueOf(edtTextSearchShop.getText());

        if (Utils.isEmptyOrNull(searchKey)) {
            SnackNotify.showMessage("Please enter shop name.", relLayMain);
        } else {
            callSearchDataApi();
        }
    }


    @OnClick(R.id.txtViewOrderNow)
    public void txtViewOrderNow() {

        Intent intent = new Intent(getActivity(), AddCategoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewPickDrop)
    public void txtViewPickDrop() {

        Intent intent = new Intent(getActivity(), WhereToDeliverActivity.class);

        if (mAddressDetailModel!=null) {
            intent.putExtra("city", mAddressDetailModel.getCity());
        }
        //addressDetailModel.getCity()
        startActivity(intent);
    }

    private void callSearchDataApi() {

        Utils.hideKeyboardIfOpen(getActivity());

        homePagePresenter.getSearchData(searchKey, "","", 1,true);
    }


}
