package com.deliverymyorder.mvp.dashboard.presenter;

import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.dashboard.model.CampusListResponse;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class HomePageContractor {

    public interface HomePagePresenter {
        void getHomePageData();

        void getSearchData(String searchKey, String categoryId,String subCategoryId, int filterType, boolean isInCampus);

        void getCampusList();
    }

    public interface HomePageView {
        void getHomePageDataSuccess(HomePageDataModel.Data arrayListHomePageDataModel);

        void getHomePageDataUnSucess(String message);

        void homePageDataInternetError();

        void getSearchDataSuccess(ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel);

        void getSearchDataUnSucess(String message);

        void searchDataInternetError();

        void getCampusApiDidFinishWith(Result result, List<Campus> campusList);
    }
}
