package com.deliverymyorder.mvp.intro_page;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.GetLocation;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;
import com.deliverymyorder.mvp.login.LoginActivity;

public class IntroActivity extends AppCompatActivity {


   // @BindView(R.id.pager)
    ViewPager pager;

    TabLayout indicator;


   // @BindView(R.id.btn_forward_intro)
   TextView btnNext;

   // @BindView(R.id.btn_skip_intro)
   TextView btnSkip;

    int curentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_intro);


        pager = (ViewPager)findViewById(R.id.pager);
        btnNext =(TextView) findViewById(R.id.txtNextButton);
        btnSkip = (TextView) findViewById(R.id.txtSkipButton);
        indicator = (TabLayout) findViewById(R.id.indicator);

        indicator.setupWithViewPager(pager, true);



        final IntroAdapter mCustomPagerAdapter = new IntroAdapter(this);
        pager.setAdapter(mCustomPagerAdapter);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                curentPage++;

                if (curentPage == mCustomPagerAdapter.getCount()) {
                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }


                pager.setCurrentItem(curentPage, true);

            }
        });


        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

                curentPage = position;
                Log.i("Pager", "second");

                if (position == mCustomPagerAdapter.getCount() - 1) {


                    btnNext.setText("Finish");
                    btnNext.setVisibility(View.VISIBLE);
                } else {
                   // btnSkip.setText("Next");
                    btnNext.setText("Next");
                    btnNext.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
    }


    public void navigateToNextActivity() {

        new GetLocation();
        GetLocation.getLocation(this);


        if (LoginManager.getInstance().isLoggedIn()) {
            goToActivities(DashboardActivity.class);
        } else {
            goToActivities(LoginActivity.class);
        }

        //   goToActivities(LoginActivity.class);

    }


    public void goToActivities(Class activty) {
        Intent intent = new Intent(IntroActivity.this, activty);
        intent.putExtra("ISOPEN_HOME_FRAGMENT", true);
        startActivity(intent);
        finish();
    }

}
