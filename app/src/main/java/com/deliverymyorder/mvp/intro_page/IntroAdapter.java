package com.deliverymyorder.mvp.intro_page;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.R;

/**
 * Created by mac on 12/05/18.
 */



public class IntroAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;

    int[] mResources = {
            R.mipmap.bg_1,
            R.mipmap.bg_2,
            R.mipmap.bg_3,
            R.mipmap.bg_4
    };






    public IntroAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.tutorial_scrolling_page, container, false);

        //ImageView imageView = (ImageView) itemView.findViewById(R.id.imgScrollingPage);
        ImageView imageBG = (ImageView) itemView.findViewById(R.id.imgScrollingPage);
        imageBG.setImageResource(mResources[position]);

        TextView txtTutorialTitle = (TextView) itemView.findViewById(R.id.txtTutorialTitle);
        TextView txtTutorialDesciption = (TextView) itemView.findViewById(R.id.txtTutorialDescription);



        switch (position)
        {

            case 0:
                imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title1));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description1));
                //imageBG.setImageResource(R.mipmap.intro_2);
               break;

            case 1:
                imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title2));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description2));
                //imageBG.setImageResource(R.mipmap.intro_3);
                break;

            case 2:
                imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title3));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description3));
               // imageBG.setImageResource(R.mipmap.intro_4);
                break;

            case 3:
                imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title4));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description4));
                // imageBG.setImageResource(R.mipmap.intro_4);
                break;

            default:
                imageBG.setVisibility(View.VISIBLE);
                txtTutorialTitle.setText(mContext.getResources().getString(R.string.totorial_title4));
                txtTutorialDesciption.setText(mContext.getResources().getString(R.string.totorial_description4));
                //imageBG.setImageResource(R.mipmap.intro_4);
                break;
        }



        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }



    public int   getPagesCount()
    {

        return mResources.length;
    }

}
