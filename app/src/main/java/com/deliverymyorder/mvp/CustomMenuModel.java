package com.deliverymyorder.mvp;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class CustomMenuModel implements Serializable, Parcelable {

    private String itemName;
    private String itemDesc;
    private int  qty = 1;
    private String itemImage;


    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.itemName);
        dest.writeString(this.itemDesc);
        dest.writeInt(this.qty);
        dest.writeString(this.itemImage);
    }

    public CustomMenuModel() {
    }

    protected CustomMenuModel(Parcel in) {
        this.itemName = in.readString();
        this.itemDesc = in.readString();
        this.qty = in.readInt();
        this.itemImage = in.readString();
    }

    public static final Parcelable.Creator<CustomMenuModel> CREATOR = new Parcelable.Creator<CustomMenuModel>() {
        @Override
        public CustomMenuModel createFromParcel(Parcel source) {
            return new CustomMenuModel(source);
        }

        @Override
        public CustomMenuModel[] newArray(int size) {
            return new CustomMenuModel[size];
        }
    };
}
