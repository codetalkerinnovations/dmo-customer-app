package com.deliverymyorder.mvp.pick_drop.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.add_category.presenter.CategoryContractor;
import com.deliverymyorder.mvp.pick_drop.model.CreatePickDropOrderResponseModel;
import com.deliverymyorder.mvp.pick_drop.model.PickDropResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickDropOrderDetailPresenterImpl implements PickDropOrderDetailContractor.PickDropOrderDetailPresenter {

    Activity activity;
    PickDropOrderDetailContractor.PickDropOrderDetailView pickDropOrderDetailView;

    public PickDropOrderDetailPresenterImpl(Activity activity, PickDropOrderDetailContractor.PickDropOrderDetailView pickDropOrderDetailView) {
        this.activity = activity;
        this.pickDropOrderDetailView = pickDropOrderDetailView;
    }

    @Override
    public void PickDropOrderDetailData() {
        try {
            ApiAdapter.getInstance(activity);
            callGetPickDropOrderDetailsDataApi();
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            pickDropOrderDetailView.pickDropOrderDetailInternetError();

        }
    }

    @Override
    public void createPickDropOrderData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            callCreatePickDropOrderDetailsDataApi(jsonObject);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            pickDropOrderDetailView.createPickDropOrderDetailInternetError();

        }
    }


    private void callGetPickDropOrderDetailsDataApi() {
        Progress.start(activity);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<PickDropResponseModel> getCreatePasswordResult = ApiAdapter.getApiService().getPickDropOrderDetails("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<PickDropResponseModel>() {
            @Override
            public void onResponse(Call<PickDropResponseModel> call, Response<PickDropResponseModel> response) {

                Progress.stop();
                try {
                    PickDropResponseModel categoryModel = response.body();


                    if (categoryModel.getError() == 0) {
                        pickDropOrderDetailView.getPickDropOrderDetailSuccess(categoryModel.getData());
                    } else {
                        pickDropOrderDetailView.getPickDropOrderDetailUnSucess(categoryModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    pickDropOrderDetailView.getPickDropOrderDetailUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<PickDropResponseModel> call, Throwable t) {
                Progress.stop();
                pickDropOrderDetailView.getPickDropOrderDetailUnSucess(activity.getString(R.string.server_error));
            }
        });

    }


    private void callCreatePickDropOrderDetailsDataApi(JSONObject jsonObject) {
        Progress.start(activity);


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CreatePickDropOrderResponseModel> getCreatePasswordResult = ApiAdapter.getApiService().createPickDropOrderDetails("application/json", "no-cache", body);

        getCreatePasswordResult.enqueue(new Callback<CreatePickDropOrderResponseModel>() {
            @Override
            public void onResponse(Call<CreatePickDropOrderResponseModel> call, Response<CreatePickDropOrderResponseModel> response) {

                Progress.stop();
                try {
                    CreatePickDropOrderResponseModel createPickDropOrderResponseModel = response.body();


                    if (createPickDropOrderResponseModel.getError() == 0) {
                        pickDropOrderDetailView.createPickDropOrderDetailSuccess(createPickDropOrderResponseModel.getData());
                    } else {
                        pickDropOrderDetailView.createPickDropOrderDetailUnSucess(createPickDropOrderResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    pickDropOrderDetailView.createPickDropOrderDetailUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CreatePickDropOrderResponseModel> call, Throwable t) {
                Progress.stop();
                pickDropOrderDetailView.createPickDropOrderDetailUnSucess(activity.getString(R.string.server_error));
            }
        });

    }

}
