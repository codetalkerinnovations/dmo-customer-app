package com.deliverymyorder.mvp.pick_drop.drop_location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LocationProvider;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.helpers.WorkaroundMapFragment;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.address.model.AddAddressModel;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.address.model.AddressListModel;
import com.deliverymyorder.mvp.address.model.DeleteAddressModel;
import com.deliverymyorder.mvp.address.model.UpdateCurrentAddressModel;
import com.deliverymyorder.mvp.address.presenter.AddressContractor;
import com.deliverymyorder.mvp.address.presenter.AddressDetailContractor;
import com.deliverymyorder.mvp.pick_drop.order_detail.OrderDetailPickDropActivity;
import com.deliverymyorder.mvp.pick_drop.pickup_location.Result;
import com.deliverymyorder.mvp.service_not_aviailble_page.CheckServiceAvailblityContractor;
import com.deliverymyorder.mvp.service_not_aviailble_page.CheckServiceAvailblityImpl;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DropLocationActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, AddressContractor.AddressView, AddressDetailContractor.AddressDetailView {


    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.edtTextName)
    EditText edtTextName;

    @BindView(R.id.edtTextFlatNo)
    EditText edtTextFlatNo;

    @BindView(R.id.edtTextLandmark)
    EditText edtTextLandmark;

    @BindView(R.id.edtTextContactInformation)
    EditText edtTextContactInformation;

    @BindView(R.id.edtTextCity)
    EditText edtTextCity;

    @BindView(R.id.edtTextState)
    EditText edtTextState;

    @BindView(R.id.edtTextCountry)
    EditText edtTextCountry;

    @BindView(R.id.txtViewConfirm)
    TextView txtViewConfirm;

    private GoogleMap mMap;

    JSONObject jsonObject;

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private final static int PERMISSION_CODE__LOCATION = 2910;
    private final static int PLACE_PICKER_REQUEST = 112;

    private LocationManager locationManager;

    double latitude = 0.0;
    double longitude = 0.0;
    int count = 1;

    boolean isInitializedMap = false;

    List<Address> addresses;
    String address;

    // AddressPresenterImpl addressPresenter;
    //AddressDetailPresenterImpl addressDetailPresenter;

    //AddressDetailModel.Data addressDetailModel;

    //CheckServiceAvailblityImpl checkServiceAvailblityImpl;

    Boolean isFirstTime = true;

    double pickUpLatitude = 0.0;
    double pickUpLongitude = 0.0;
    String pickUpName;
    String pickUpContactNo;
    String pickUpLandMark;
    String pickUpFlatNumber;
    String pickUpCountry;
    String pickUpCity;
    String pickUpState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_location);
        ButterKnife.bind(this);


        if (getIntent() != null) {

            pickUpLatitude = getIntent().getDoubleExtra(ConstIntent.CONST_PICKUP_LATITUDE, 0);
            pickUpLongitude = getIntent().getDoubleExtra(ConstIntent.CONST_PICKUP_LONGITUDE, 0);
            pickUpName = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_NAME);
            pickUpContactNo = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CONTACT_NO);
            pickUpLandMark = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_LANDMARK);
            pickUpFlatNumber = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_FLAT_NUMBER);
            pickUpCountry = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_COUNTRY);
            pickUpState = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_STATE);
            pickUpCity = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CITY);


        }

//        addressPresenter = new AddressPresenterImpl(this, this);
//        addressDetailPresenter = new AddressDetailPresenterImpl(this, this);


        // initialized map
        initializeMap();

    }

    @OnClick(R.id.imgSearchView)
    public void imgViewSearch(View view) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            AlertDialogHelper.showAlertDialog(this, e.getLocalizedMessage());
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            AlertDialogHelper.showAlertDialog(this, e.getLocalizedMessage());
            e.printStackTrace();
        }
    }



    private void initializeMap() {

        // custom map fragment to handle map scroll in nested scroll view
        WorkaroundMapFragment fragment_map = (WorkaroundMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.mapView);

        fragment_map.getMapAsync(this);

        fragment_map.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                //nestedScroll.requestDisallowInterceptTouchEvent(true);
            }
        });

        getCurrentLocation();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        //  getCurrentLocation();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        //  getCurrentLocation();
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        //googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);


        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                if (!isFirstTime) {
                    final LatLng mPosition = mMap.getCameraPosition().target;
                    checkService(mPosition.latitude, mPosition.longitude, new Result() {
                        @Override
                        public void resultDidFinish(boolean status) {

                            if (status) {
                                latitude = mPosition.latitude;
                                longitude = mPosition.longitude;

                                getAddressFromLatLongUpdated();
                            }
                        }
                    });

                }else
                {
                    isFirstTime = false;
                }

            }
        });
    }

    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {

        if (checkIsPermissionGranted()) {

            // enable current selectedItemLocationId in map
            //mMap.setMyLocationEnabled(true);

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                isInitializedMap = true;

            } else {
                getLocation();
            }
        } else {
            askLocationPermission();
        }
    }

    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 2910);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

                final Place place = PlacePicker.getPlace(this,data);

                //PlacePicker.getPlace(data, this);

                //Place mpace = PlacePicker.getPlace(this,data);

                checkService(place.getLatLng().latitude, place.getLatLng().longitude, new Result() {
                    @Override
                    public void resultDidFinish(boolean status) {
                        if (status)
                        {
                            latitude = place.getLatLng().latitude;
                            longitude = place.getLatLng().longitude;
                            getAddressFromLatLong();
                            getAddressFromLatLongUpdated();
                        }
                    }
                });

                // String toastMsg = String.format("Place: %s", place.getName());
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }


    private void getLocation() {

        Progress.start(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
    }

    private boolean checkIsPermissionGranted() {

        int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if ((permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        Progress.stop();



        if (count == 1) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();

//            latitude = Double.parseDouble(addressDetailModel.getLatitude());
//            longitude = Double.parseDouble(addressDetailModel.getLongitude());

            count++;
        }

        Log.e("latitude", "------>" + latitude);
        Log.e("longitude", "------>" + longitude);

        getAddressFromLatLong();
        getAddressFromLatLongUpdated();

     /*   // get selectedItemLocationId
        getLocations();*/

    }


    private void getAddressFromLatLong() {

        mMap.clear();

        // add marker on google map
        LatLng myLatLng = new LatLng(latitude, longitude);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        //  MarkerOptions markerOptMyLoc = new MarkerOptions().position(myLatLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_red)).title("You are here");
        // mMap.addMarker(markerOptMyLoc);

        // adding marker
        builder.include(myLatLng);

        LatLngBounds boundsMyLoc = builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsMyLoc, 4));

        //nestedScroll.fullScroll(View.FOCUS_DOWN);
    }

    private void getAddressFromLatLongUpdated() {

        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country =  "India";// addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                edtTextFlatNo.setText(knownName);
                edtTextLandmark.setText(addresses.get(0).getSubAdminArea());
//                edtTextContactInformation.setText(address);
                edtTextCity.setText(city);
                edtTextState.setText(state);
                edtTextCountry.setText(country);

                Log.e("Addr Lat", city + " " + state + " " + country + " " + postalCode + " " + knownName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {
        finish();
    }

    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirmClicked() {

        if (validation()) {

            String name = String.valueOf(edtTextName.getText());
            String flatNo = String.valueOf(edtTextFlatNo.getText());
            String landMark = String.valueOf(edtTextLandmark.getText());
            String contactInfo = String.valueOf(edtTextContactInformation.getText());
            String city = String.valueOf(edtTextCity.getText());
            String state = String.valueOf(edtTextState.getText());
            String country = String.valueOf(edtTextCountry.getText());


            Utils.hideKeyboardIfOpen(this);


            Intent intent = new Intent(this, OrderDetailPickDropActivity.class);
            intent.putExtra(ConstIntent.CONST_PICKUP_LATITUDE, pickUpLatitude);
            intent.putExtra(ConstIntent.CONST_PICKUP_LONGITUDE, pickUpLongitude);
            intent.putExtra(ConstIntent.CONST_PICKUP_NAME, pickUpName);
            intent.putExtra(ConstIntent.CONST_PICKUP_CONTACT_NO, pickUpContactNo);
            intent.putExtra(ConstIntent.CONST_PICKUP_LANDMARK, pickUpLandMark);
            intent.putExtra(ConstIntent.CONST_PICKUP_FLAT_NUMBER, pickUpFlatNumber);
            intent.putExtra(ConstIntent.CONST_PICKUP_COUNTRY, pickUpCountry);
            intent.putExtra(ConstIntent.CONST_PICKUP_STATE, pickUpState);
            intent.putExtra(ConstIntent.CONST_PICKUP_CITY, pickUpCity);

            // Drop


            intent.putExtra(ConstIntent.CONST_DROP_LATITUDE, latitude);
            intent.putExtra(ConstIntent.CONST_DROP_LONGITUDE, longitude);
            intent.putExtra(ConstIntent.CONST_DROP_NAME, name);
            intent.putExtra(ConstIntent.CONST_DROP_CONTACT_NO, contactInfo);
            intent.putExtra(ConstIntent.CONST_DROP_LANDMARK, landMark);
            intent.putExtra(ConstIntent.CONST_DROP_FLAT_NUMBER, flatNo);
            intent.putExtra(ConstIntent.CONST_DROP_COUNTRY, country);
            intent.putExtra(ConstIntent.CONST_DROP_STATE, state);
            intent.putExtra(ConstIntent.CONST_DROP_CITY, city);


            startActivity(intent);

        }
    }

    public  boolean validation()
    {

        if (latitude == 0 || longitude == 0)
        {
            AlertDialogHelper.showAlertDialog(this,"Please select any location");
            return  false;
        }


        if (Utils.isEmptyOrNull(edtTextName.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter name");
            return false;
        }

        if (Utils.isEmptyOrNull(edtTextFlatNo.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter flat number");
            return false;
        }

        if (Utils.isEmptyOrNull(edtTextLandmark.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter landmark");
            return false;
        }


        if (Utils.isEmptyOrNull(edtTextContactInformation.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter contact number");
            return false;
        }

        if (!(edtTextContactInformation.getText().toString().length() == 10) )
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter valid 10 digit contact number");
            return false;
        }

        if (Utils.isEmptyOrNull(edtTextCity.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter city");
            return false;
        }

        if (Utils.isEmptyOrNull(edtTextState.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter state");
            return false;
        }

        if (Utils.isEmptyOrNull(edtTextCountry.getText().toString()))
        {
            AlertDialogHelper.showAlertDialog(this,"Please enter country");
            return false;
        }

//        String name = String.valueOf(edtTextName.getText());
//        String flatNo = String.valueOf(edtTextFlatNo.getText());
//        String landMark = String.valueOf(edtTextLandmark.getText());
//        String contactInfo = String.valueOf(edtTextContactInformation.getText());
//        String city = String.valueOf(edtTextCity.getText());
//        String state = String.valueOf(edtTextState.getText());
//        String country = String.valueOf(edtTextCountry.getText());

        return  true;
    }

    @Override
    public void getAddAddressSuccess(AddAddressModel.Data addAddressModel) {

        PrefUtil.putInt(this, Const.KEY_ADDRESS_ID, addAddressModel.getId());

        //LoginManager.getInstance().getUserData().setCurrentAddressId(addAddressModel.getId());

        finish();
    }

    @Override
    public void getAddAddressUnSucess(String message) {

    }

    @Override
    public void addAddressInternetError() {

    }

    @Override
    public void getAllAddressSuccess(ArrayList<AddressListModel.Datum> arrayListAddresses) {
        //not used
    }

    @Override
    public void getAllAddressUnSucess(String message) {
        //not used
    }

    @Override
    public void allAddressInternetError() {
        //not used
    }

    @Override
    public void deleteAddressSuccess(DeleteAddressModel.Data deleteAddressModel) {
        //not used
    }

    @Override
    public void deleteAddressUnSucess(String message) {
//not used
    }

    @Override
    public void deleteAddressInternetError() {
//not used
    }

    @Override
    public void updateCurrentAddressSuccess(UpdateCurrentAddressModel.Data deleteAddressModel) {
        //not used
    }

    @Override
    public void updateCurrentAddressUnSucess(String message) {
//not used
    }

    @Override
    public void updateCurrentAddressInternetError() {
//not used
    }

    @Override
    public void getAddressDetailSuccess(AddressDetailModel.Data addressDetailModel) {

        Progress.stop();
        //  this.addressDetailModel = addressDetailModel;


        edtTextName.setText(addressDetailModel.getName());
        edtTextFlatNo.setText(addressDetailModel.getHouseNo());
        edtTextLandmark.setText(addressDetailModel.getLandmark());
        edtTextContactInformation.setText(addressDetailModel.getAreaLocality());
        edtTextCity.setText(addressDetailModel.getCity());
        edtTextState.setText(addressDetailModel.getState());
        edtTextCountry.setText(addressDetailModel.getCountry());

        try {

            latitude = Double.parseDouble(addressDetailModel.getLatitude());
            longitude = Double.parseDouble(addressDetailModel.getLongitude());

            LatLng myLatLng = new LatLng(latitude, longitude);

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 14));



        }catch (Exception e)
        {
            e.printStackTrace();
        }

        // getCurrentLocation();
    }

    @Override
    public void getAddressDetailUnSucess(String message) {

    }

    @Override
    public void getAddressDetailInternetError() {

    }


    private  void checkService(Double lat, Double lang, final Result result)
    {

        CheckServiceAvailblityImpl checkServiceAvailblity = new CheckServiceAvailblityImpl(this, new CheckServiceAvailblityContractor.CheckServiceAvailbityView() {
            @Override
            public void checkServiceAaiblityDidFinshWithResult(boolean status) {

                if (status)
                {
                    if (result!=null)
                        result.resultDidFinish(true);
                }else
                {
                    AlertDialogHelper.showMessage(DropLocationActivity.this,"Service is not Available at this location");

                    if (result!=null)
                        result.resultDidFinish(false);
                }

            }

            @Override
            public void checkServiceAvaiblityNetworkError() {
                AlertDialogHelper.showMessage(DropLocationActivity.this,getResources().getString(R.string.server_error));
                if (result!=null)
                    result.resultDidFinish(false);
            }
        });

        checkServiceAvailblity.checkServiceAvailablity(String.valueOf(lat),String.valueOf(lang));


    }




}




/*
public class DropLocationActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.edtTextLatLong)
    EditText edtTextLatLong;

    @BindView(R.id.edtTextName)
    EditText edtTextName;

    @BindView(R.id.edtTextContactNo)
    EditText edtTextContactNo;


    private GoogleMap mMap;

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private final static int PERMISSION_CODE__LOCATION = 2910;
    private final static  int PLACE_PICKER_REQUEST = 112;


    private LocationManager locationManager;

    double latitude = 0.0;
    double longitude = 0.0;

    double pickUpLatitude = 0.0;
    double pickUpLongitude = 0.0;
    String pickUpName;
    String pickUpContactNo;

    String pickUpLandMark;
    String pickUpFlatNumber;
    String pickUpCountry;
    String pickUpCity;
    String pickUpState;



    List<Address> addresses;
    String address;

    boolean isAskedPermission = false;
    boolean isInitializedMap = false;

    @BindView(R.id.txtViewConfirm)
    TextView txtViewConfirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_location);

        ButterKnife.bind(this);
        // load map
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);


        if (getIntent() != null) {

            pickUpLatitude = getIntent().getDoubleExtra(ConstIntent.CONST_PICKUP_LATITUDE, 0);
            pickUpLongitude = getIntent().getDoubleExtra(ConstIntent.CONST_PICKUP_LONGITUDE, 0);
            pickUpName = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_NAME);
            pickUpContactNo = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CONTACT_NO);

            pickUpLandMark = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_LANDMARK);
            pickUpFlatNumber = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_FLAT_NUMBER);
            pickUpCountry = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_COUNTRY);
            pickUpState = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_STATE);
            pickUpCity = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CITY);


        }

        // initialized map
        initializeMap();


    }

    private String getMapsApiDirectionsUrl() {

        // Origin of route
        String str_origin = "origin=" + latitude + "," + longitude;

        // Destination of route
        String str_dest = "destination=" + pickUpLatitude + "," + pickUpLongitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private void initializeMap() {

        // custom map fragment to handle map scroll in nested scroll view
        WorkaroundMapFragment fragment_map = (WorkaroundMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.mapView);

        fragment_map.getMapAsync(this);

        fragment_map.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                //nestedScroll.requestDisallowInterceptTouchEvent(true);
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        //  getCurrentLocation();
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        //googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        //googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);


        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {


                final LatLng mPosition = mMap.getCameraPosition().target;

               checkService(mPosition.latitude, mPosition.longitude, new Result() {
                   @Override
                   public void resultDidFinish(boolean status) {
                       latitude = mPosition.latitude;
                       longitude = mPosition.longitude;

                       getAddressFromLatLongUpdated();

                   }
               });

               }
        });



        getCurrentLocation();
    }

    @OnClick(R.id.imgViewSearch)
    public  void  imgViewSearch(View view)
    {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            AlertDialogHelper.showAlertDialog(this,e.getLocalizedMessage());
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            AlertDialogHelper.showAlertDialog(this,e.getLocalizedMessage());
            e.printStackTrace();
        }
    }


    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {

        if (checkIsPermissionGranted()) {

            // enable current selectedItemLocationId in map
            //mMap.setMyLocationEnabled(true);

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                isInitializedMap = true;

            } else {
                getLocation();
            }
        } else {
            askLocationPermission();
        }
    }

    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 2910);
        }
    }

    private void getLocation() {

        Progress.start(this);

        LocationProvider locationProvider = LocationProvider.getSharedInstacne();
        locationProvider.getLocation(this, new LocationProvider.LocationResult() {
            @Override
            public void gotLocation(Location location) {

                Progress.stop();

                latitude = location.getLatitude();
                longitude = location.getLongitude();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mMap!= null)
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 15.0f));

                    }
                });

                if (latitude!= 0 & longitude != 0)
                {
                    getAddressFromLatLongUpdated();
                }
                Log.e("latitude", "------>" + latitude);
                Log.e("longitude", "------>" + longitude);



            }
        });


        /*
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }


    }

    private boolean checkIsPermissionGranted() {

        int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if ((permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        Progress.stop();

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        Log.e("latitude", "------>" + latitude);
        Log.e("longitude", "------>" + longitude);

        getAddressFromLatLong();
        getAddressFromLatLongUpdated();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
              final Place place = PlacePicker.getPlace(data, this);

                checkService(place.getLatLng().latitude, place.getLatLng().longitude, new Result() {
                    @Override
                    public void resultDidFinish(boolean status) {
                        if (status)
                        {
                            latitude = place.getLatLng().latitude;
                            longitude = place.getLatLng().longitude;
                            getAddressFromLatLong();
                            getAddressFromLatLongUpdated();
                        }
                    }
                });

                // String toastMsg = String.format("Place: %s", place.getName());
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }



    private void getAddressFromLatLong() {

        mMap.clear();

        // add marker on google map
        LatLng myLatLng = new LatLng(latitude, longitude);
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        //  MarkerOptions markerOptMyLoc = new MarkerOptions().position(myLatLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_red)).title("You are here");
//        // mMap.addMarker(markerOptMyLoc);
//
//        // adding marker
//        builder.include(myLatLng);
//
//        LatLngBounds boundsMyLoc = builder.build();
//       mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsMyLoc, 2));
//       // mMap.animateCamera( CameraUpdateFactory.zoomTo( 5.0f ) );

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 15.0f));



        //nestedScroll.fullScroll(View.FOCUS_DOWN);
    }

    private void getAddressFromLatLongUpdated() {

        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();


                if (address == null)
                {
                    address = "unnamed";
                }
                //edtTextFlatNo.setText(knownName);
                //  edtTextLandmark.setText(city);
                edtTextLatLong.setText(address);
                //edtTextCity.setText(city);
                //edtTextState.setText(state);
                // edtTextCountry.setText(country);

                Log.e("Addr Lat", address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {
        finish();
    }

    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirmClicked() {

        Utils.hideKeyboardIfOpen(this);

        String name = String.valueOf(edtTextName.getText());
        String contactNo = String.valueOf(edtTextContactNo.getText());

        if (validate(name, contactNo)) {

            Intent intent = new Intent(this, OrderDetailPickDropActivity.class);
            intent.putExtra(ConstIntent.CONST_DROP_LATITUDE, latitude);
            intent.putExtra(ConstIntent.CONST_DROP_LONGITUDE, longitude);
            intent.putExtra(ConstIntent.CONST_DROP_NAME, name);
            intent.putExtra(ConstIntent.CONST_DROP_CONTACT_NO, contactNo);



            intent.putExtra(ConstIntent.CONST_PICKUP_LATITUDE, pickUpLatitude);
            intent.putExtra(ConstIntent.CONST_PICKUP_LONGITUDE, pickUpLongitude);
            intent.putExtra(ConstIntent.CONST_PICKUP_NAME, pickUpName);
            intent.putExtra(ConstIntent.CONST_PICKUP_CONTACT_NO, pickUpContactNo);
            intent.putExtra(ConstIntent.CONST_PICKUP_LANDMARK, pickUpLandMark);
            intent.putExtra(ConstIntent.CONST_PICKUP_FLAT_NUMBER, pickUpFlatNumber);
            intent.putExtra(ConstIntent.CONST_PICKUP_COUNTRY, pickUpCountry);
            intent.putExtra(ConstIntent.CONST_PICKUP_STATE, pickUpState);
            intent.putExtra(ConstIntent.CONST_PICKUP_CITY, pickUpCity);


            startActivity(intent);
        }
    }

    private boolean validate(String name, String contactNo) {

        if (Utils.isEmptyOrNull(name)) {

            SnackNotify.showMessage("Please enter name.", relLayMain);
            return false;

        } else if (Utils.isEmptyOrNull(contactNo)) {


            SnackNotify.showMessage("Please enter contact number.", relLayMain);
            return false;
        }

        if ((latitude == 0 )|| (longitude == 0))
        {
            SnackNotify.showMessage("Please select valid location", relLayMain);
            return false;
        }

        if ((latitude == pickUpLatitude )|| (longitude == pickUpLongitude))
        {
            SnackNotify.showMessage("Pick and Drop location can't be same", relLayMain);
            return false;
        }


        return true;
    }


    private  void checkService(Double lat, Double longitude, final Result result)
    {

        CheckServiceAvailblityImpl checkServiceAvailblity = new CheckServiceAvailblityImpl(this, new CheckServiceAvailblityContractor.CheckServiceAvailbityView() {
            @Override
            public void checkServiceAaiblityDidFinshWithResult(boolean status) {

                if (status)
                {
                    if (result!=null)
                        result.resultDidFinish(true);
                    txtViewConfirm.setEnabled(true);
                }else
                {
                    AlertDialogHelper.showMessage(DropLocationActivity.this,"Service is not Available at this location");

                    if (result!=null)
                        result.resultDidFinish(false);

                    txtViewConfirm.setEnabled(false);
                }

            }

            @Override
            public void checkServiceAvaiblityNetworkError() {
                AlertDialogHelper.showMessage(DropLocationActivity.this,getResources().getString(R.string.server_error));
                if (result!=null)
                    result.resultDidFinish(false);
                txtViewConfirm.setEnabled(false);


            }
        });

        checkServiceAvailblity.checkServiceAvailablity(String.valueOf(latitude),String.valueOf(longitude));


    }

}


*/