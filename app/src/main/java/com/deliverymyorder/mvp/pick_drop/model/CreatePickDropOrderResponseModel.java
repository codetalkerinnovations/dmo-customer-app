package com.deliverymyorder.mvp.pick_drop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreatePickDropOrderResponseModel implements Serializable {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Data implements Serializable {

        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("pickup_contact_name")
        @Expose
        private String pickupContactName;
        @SerializedName("pickup_contact_no")
        @Expose
        private String pickupContactNo;
        @SerializedName("pickup_lat")
        @Expose
        private Double pickupLat;
        @SerializedName("pickup_long")
        @Expose
        private Double pickupLong;
        @SerializedName("drop_contact_name")
        @Expose
        private String dropContactName;
        @SerializedName("drop_contact_no")
        @Expose
        private String dropContactNo;
        @SerializedName("drop_lat")
        @Expose
        private Double dropLat;
        @SerializedName("drop_long")
        @Expose
        private Double dropLong;
        @SerializedName("product_type_id")
        @Expose
        private Integer productTypeId;
        @SerializedName("weight_id")
        @Expose
        private Integer weightId;
        @SerializedName("size_id")
        @Expose
        private Integer sizeId;
        @SerializedName("total_cost")
        @Expose
        private Double totalCost;
        @SerializedName("delivery_partner_id")
        @Expose
        private Integer deliveryPartnerId;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public String getPickupContactName() {
            return pickupContactName;
        }

        public void setPickupContactName(String pickupContactName) {
            this.pickupContactName = pickupContactName;
        }

        public String getPickupContactNo() {
            return pickupContactNo;
        }

        public void setPickupContactNo(String pickupContactNo) {
            this.pickupContactNo = pickupContactNo;
        }

        public Double getPickupLat() {
            return pickupLat;
        }

        public void setPickupLat(Double pickupLat) {
            this.pickupLat = pickupLat;
        }

        public Double getPickupLong() {
            return pickupLong;
        }

        public void setPickupLong(Double pickupLong) {
            this.pickupLong = pickupLong;
        }

        public String getDropContactName() {
            return dropContactName;
        }

        public void setDropContactName(String dropContactName) {
            this.dropContactName = dropContactName;
        }

        public String getDropContactNo() {
            return dropContactNo;
        }

        public void setDropContactNo(String dropContactNo) {
            this.dropContactNo = dropContactNo;
        }

        public Double getDropLat() {
            return dropLat;
        }

        public void setDropLat(Double dropLat) {
            this.dropLat = dropLat;
        }

        public Double getDropLong() {
            return dropLong;
        }

        public void setDropLong(Double dropLong) {
            this.dropLong = dropLong;
        }

        public Integer getProductTypeId() {
            return productTypeId;
        }

        public void setProductTypeId(Integer productTypeId) {
            this.productTypeId = productTypeId;
        }

        public Integer getWeightId() {
            return weightId;
        }

        public void setWeightId(Integer weightId) {
            this.weightId = weightId;
        }

        public Integer getSizeId() {
            return sizeId;
        }

        public void setSizeId(Integer sizeId) {
            this.sizeId = sizeId;
        }

        public Double getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(Double totalCost) {
            this.totalCost = totalCost;
        }

        public Integer getDeliveryPartnerId() {
            return deliveryPartnerId;
        }

        public void setDeliveryPartnerId(Integer deliveryPartnerId) {
            this.deliveryPartnerId = deliveryPartnerId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }

}
