package com.deliverymyorder.mvp.pick_drop.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.R;
import com.deliverymyorder.mvp.pick_drop.model.PickDropResponseModel;
import com.deliverymyorder.mvp.pick_drop.order_detail.OrderDetailPickDropActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.Holder> {


    Activity activity;
    ArrayList<PickDropResponseModel.Data.Size> datum;
    String favBit;

    public SizeAdapter(Activity activity, ArrayList<PickDropResponseModel.Data.Size> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_weight, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final PickDropResponseModel.Data.Size result = datum.get(position);

        if (result.isSizeSelected()) {

            holder.txtViewWeight.setBackgroundResource(R.drawable.drawable_round_corner_selected_category);
            holder.txtViewWeight.setTextColor(ContextCompat.getColor(activity,R.color.color_white));
        } else {
            holder.txtViewWeight.setBackgroundResource(R.drawable.drawable_round_corner);
            holder.txtViewWeight.setTextColor(ContextCompat.getColor(activity,R.color.color_black));
        }


        holder.txtViewWeight.setText(result.getLength() + "*" + result.getHeight() + "*" + result.getWidth());

        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (int i = 0; i < datum.size(); i++) {

                    datum.get(i).setSizeSelected(false);
                }

                datum.get(position).setSizeSelected(true);

                ((OrderDetailPickDropActivity)activity).setSelectedSizeId(result.getId());

                notifyDataSetChanged();


            }
        });

    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewWeight)
        TextView txtViewWeight;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}


