package com.deliverymyorder.mvp.pick_drop;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.LocationProvider;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.pick_drop.pickup_location.PickupLocationActivity;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WhereToDeliverActivity extends AppCompatActivity implements LocationListener {

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private final static int PERMISSION_CODE__LOCATION = 2910;
    private final static  int PLACE_PICKER_REQUEST = 112;

    private LocationManager locationManager;

    double latitude = 0.0;
    double longitude = 0.0;
    List<Address> addresses;
    String address;

    boolean isLoaded = false;

    String cityName = "";


    @BindView(R.id.txtViewWithinCityName)
    TextView txtViewWithinCityName;

    @BindView(R.id.txtViewOutSideCityName)
    TextView txtViewOutSideCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_where_to_deliver);

        ButterKnife.bind(this);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
           cityName = bundle.getString("city");
        }

        getCurrentLocation();
        loadCityNameInLable();

    }

    private  void loadCityNameInLable()
    {
        if (cityName == null)
        {
            cityName = "";
        }


        txtViewOutSideCityName.setText("Out of \n"+cityName);
        txtViewWithinCityName.setText("Within \n"+cityName);

    }

    @OnClick(R.id.txtViewWithinCityName)
    public void txtViewWithinCityName(){

        Intent intent = new Intent(this, PickupLocationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {

        finish();
    }




    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {

        if (checkIsPermissionGranted()) {

            // enable current selectedItemLocationId in map
            //mMap.setMyLocationEnabled(true);

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

               // isInitializedMap = true;

            } else {
                getLocation();
            }
        } else {
            askLocationPermission();
        }
    }

    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 2910);
        }
    }


    private void getLocation() {

        Progress.start(this);


        LocationProvider locationProvider = LocationProvider.getSharedInstacne();
        locationProvider.getLocation(this, new LocationProvider.LocationResult() {
            @Override
            public void gotLocation(Location location) {


                Progress.stop();
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                if (latitude!= 0 & longitude != 0)
                {
                    isLoaded = true;
                }

                Log.e("latitude", "------>" + latitude);
                Log.e("longitude", "------>" + longitude);

                getAddressFromLatLongUpdated();
            }
        });


        /*
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 2000, 2000, this);
                locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
        */
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private boolean checkIsPermissionGranted() {

        int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if ((permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        Progress.stop();

        if (!isLoaded) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();

            if (latitude!= 0 & longitude != 0)
            {
                isLoaded = true;
            }

            Log.e("latitude", "------>" + latitude);
            Log.e("longitude", "------>" + longitude);

            getAddressFromLatLongUpdated();


        }

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;

                getAddressFromLatLongUpdated();
                // String toastMsg = String.format("Place: %s", place.getName());
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }



    private void getAddressFromLatLongUpdated() {

        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                String address = addresses.get(0).getAddressLine(0);

                if (address==null)
                {
                    address = "unnamed place";
                }

                String city = addresses.get(0).getLocality();
//                String state = addresses.get(0).getAdminArea();
//                String country = addresses.get(0).getCountryName();
//                String postalCode = addresses.get(0).getPostalCode();
//                String knownName = addresses.get(0).getFeatureName();

                if (city != null)
                {
                     cityName = city;
                }

                loadCityNameInLable();

                Log.e("Addr Lat", address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
