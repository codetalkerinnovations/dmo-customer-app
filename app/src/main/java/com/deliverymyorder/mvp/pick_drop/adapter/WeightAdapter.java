package com.deliverymyorder.mvp.pick_drop.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.R;
import com.deliverymyorder.mvp.pick_drop.model.PickDropResponseModel;
import com.deliverymyorder.mvp.pick_drop.order_detail.OrderDetailPickDropActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeightAdapter extends RecyclerView.Adapter<WeightAdapter.Holder> {


    Activity activity;
    ArrayList<PickDropResponseModel.Data.Weight> datum;
    String favBit;

    public WeightAdapter(Activity activity, ArrayList<PickDropResponseModel.Data.Weight> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_weight, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final PickDropResponseModel.Data.Weight result = datum.get(position);

        if (result.isWeightSelected()) {

            holder.txtViewWeight.setBackgroundResource(R.drawable.drawable_round_corner_selected_category);
            holder.txtViewWeight.setTextColor(ContextCompat.getColor(activity, R.color.color_white));
        } else {
            holder.txtViewWeight.setBackgroundResource(R.drawable.drawable_round_corner);
            holder.txtViewWeight.setTextColor(ContextCompat.getColor(activity, R.color.color_black));
        }

        holder.txtViewWeight.setText(result.getWFrom() + "-" + result.getWTo() + " Kg");

        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < datum.size(); i++) {

                    datum.get(i).setWeightSelected(false);
                }
                datum.get(position).setWeightSelected(true);

                ((OrderDetailPickDropActivity) activity).setSelectedWeightId(result.getId());

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewWeight)
        TextView txtViewWeight;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}



