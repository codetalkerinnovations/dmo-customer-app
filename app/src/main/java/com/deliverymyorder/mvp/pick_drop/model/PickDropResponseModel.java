package com.deliverymyorder.mvp.pick_drop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PickDropResponseModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public class Data {

        @SerializedName("ProductType")
        @Expose
        private ArrayList<ProductType> productType = null;
        @SerializedName("Weight")
        @Expose
        private ArrayList<Weight> weight = null;
        @SerializedName("Size")
        @Expose
        private ArrayList<Size> size = null;

        public ArrayList<ProductType> getProductType() {
            return productType;
        }

        public void setProductType(ArrayList<ProductType> productType) {
            this.productType = productType;
        }

        public ArrayList<Weight> getWeight() {
            return weight;
        }

        public void setWeight(ArrayList<Weight> weight) {
            this.weight = weight;
        }

        public ArrayList<Size> getSize() {
            return size;
        }

        public void setSize(ArrayList<Size> size) {
            this.size = size;
        }


        public class ProductType {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("img")
            @Expose
            private String img;

            private boolean isProductTypeSelected;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public boolean isProductTypeSelected() {
                return isProductTypeSelected;
            }

            public void setProductTypeSelected(boolean productTypeSelected) {
                isProductTypeSelected = productTypeSelected;
            }
        }


        public class Size {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("length")
            @Expose
            private Integer length;
            @SerializedName("height")
            @Expose
            private Integer height;
            @SerializedName("width")
            @Expose
            private Integer width;

            private boolean isSizeSelected;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getLength() {
                return length;
            }

            public void setLength(Integer length) {
                this.length = length;
            }

            public Integer getHeight() {
                return height;
            }

            public void setHeight(Integer height) {
                this.height = height;
            }

            public Integer getWidth() {
                return width;
            }

            public void setWidth(Integer width) {
                this.width = width;
            }

            public boolean isSizeSelected() {
                return isSizeSelected;
            }

            public void setSizeSelected(boolean sizeSelected) {
                isSizeSelected = sizeSelected;
            }
        }

        public class Weight {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("w_from")
            @Expose
            private Integer wFrom;
            @SerializedName("w_to")
            @Expose
            private Integer wTo;

            private boolean isWeightSelected;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getWFrom() {
                return wFrom;
            }

            public void setWFrom(Integer wFrom) {
                this.wFrom = wFrom;
            }

            public Integer getWTo() {
                return wTo;
            }

            public void setWTo(Integer wTo) {
                this.wTo = wTo;
            }

            public boolean isWeightSelected() {
                return isWeightSelected;
            }

            public void setWeightSelected(boolean weightSelected) {
                isWeightSelected = weightSelected;
            }
        }
    }
}
