package com.deliverymyorder.mvp.pick_drop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.CircleTransform;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.pick_drop.model.PickDropResponseModel;
import com.deliverymyorder.mvp.pick_drop.order_detail.OrderDetailPickDropActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeAdapter.Holder> {


    Activity activity;
    ArrayList<PickDropResponseModel.Data.ProductType> datum;
    String favBit;

    public ProductTypeAdapter(Activity activity, ArrayList<PickDropResponseModel.Data.ProductType> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_category, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final PickDropResponseModel.Data.ProductType result = datum.get(position);

        if (result.isProductTypeSelected()) {

            holder.imgViewCategory.setBackgroundResource(R.drawable.drawable_round_corner_selected_category);
            holder.imgViewCategory.setAlpha(.5f);
        } else {
            holder.imgViewCategory.setBackgroundResource(R.drawable.drawable_round_corner);
            holder.imgViewCategory.setAlpha(1f);
        }

        holder.txtViewCategoryName.setText(result.getTitle());

        if (Utils.isEmptyOrNull(result.getImg())) {
            Picasso.with(activity).load(R.mipmap.ic_cat_more).transform(new CircleTransform()).into(holder.imgViewCategory);
        } else {
            Picasso.with(activity).load(result.getImg()).transform(new CircleTransform()).into(holder.imgViewCategory);
        }

        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < datum.size(); i++) {

                    datum.get(i).setProductTypeSelected(false);
                }
                datum.get(position).setProductTypeSelected(true);

                ((OrderDetailPickDropActivity)activity).setSelectedProductTypeId(result.getId());

                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCategoryName)
        TextView txtViewCategoryName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.imgViewCategory)
        ImageView imgViewCategory;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // setFont();
        }
    }
}

