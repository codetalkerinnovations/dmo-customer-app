package com.deliverymyorder.mvp.pick_drop.order_detail;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.common.DistanceUtil;
import com.deliverymyorder.mvp.common.MapRouteContractor;
import com.deliverymyorder.mvp.common.MapRoutePresenterImpl;
import com.deliverymyorder.mvp.dashboard.adapter.PopularCategoryAdapter;
import com.deliverymyorder.mvp.delivery_charge.DeliveryChargeActivity;
import com.deliverymyorder.mvp.order_success.OrderConfirmedActivity;
import com.deliverymyorder.mvp.pick_drop.adapter.ProductTypeAdapter;
import com.deliverymyorder.mvp.pick_drop.adapter.SizeAdapter;
import com.deliverymyorder.mvp.pick_drop.adapter.WeightAdapter;
import com.deliverymyorder.mvp.pick_drop.model.CreatePickDropOrderResponseModel;
import com.deliverymyorder.mvp.pick_drop.model.PickDropResponseModel;
import com.deliverymyorder.mvp.pick_drop.presenter.PickDropOrderDetailContractor;
import com.deliverymyorder.mvp.pick_drop.presenter.PickDropOrderDetailPresenterImpl;
import com.deliverymyorder.mvp.track_order.TrackOrderMapActivity;
import com.deliverymyorder.mvp.track_order.presenter.DirectionsJSONParser;
import com.facebook.login.Login;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderDetailPickDropActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, MapRouteContractor.View, PickDropOrderDetailContractor.PickDropOrderDetailView {


    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewDropLocation)
    TextView txtViewDropLocation;

    @BindView(R.id.txtViewPickUpLocation)
    TextView txtViewPickUpLocation;


    @BindView(R.id.recyclerViewProduct)
    RecyclerView recyclerViewProduct;

    @BindView(R.id.recyclerViewApproximateWeight)
    RecyclerView recyclerViewApproximateWeight;

    @BindView(R.id.recyclerViewApproximateSize)
    RecyclerView recyclerViewApproximateSize;

    @BindView(R.id.txtViewConfirm)
    TextView txtViewConfirm;


    private GoogleMap mMap;

    MapView myMapView = null;

    private LocationManager locationManager;

    double latitude;
    double longitude;
    boolean isInitializedMap = false;

    double addressLatitude;
    double addressLongitude;

    String locationName = "";

    int selectedProductTypeId = 0;
    int selectedSizeId = 0;
    int selectedWeightId = 0;

//    double pickUpLatitude = 0.0;
//    double pickUpLongitude = 0.0;
//    String pickUpName;
//    String pickUpContactNo;
//
//    double dropLatitude = 0.0;
//    double dropLongitude = 0.0;
//    String dropName;
//    String dropContactNo;


    double pickUpLatitude = 0.0;
    double pickUpLongitude = 0.0;
    String pickUpName;
    String pickUpContactNo;
    String pickUpLandMark;
    String pickUpFlatNumber;
    String pickUpCountry;
    String pickUpCity;
    String pickUpState;



    double dropLatitude = 0.0;
    double dropLongitude = 0.0;
    String dropName;
    String dropContactNo;
    String dropLandMark;
    String dropFlatNumber;
    String dropCountry;
    String dropCity;
    String dropState;



    PickDropResponseModel.Data pickDropOrderDetailModel;

    ProductTypeAdapter productTypeAdapter;
    WeightAdapter weightAdapter;
    SizeAdapter sizeAdapter;

    MapRoutePresenterImpl mapRoutePresenter;
    PickDropOrderDetailPresenterImpl pickDropOrderDetailPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_pick_drop);

        ButterKnife.bind(this);
        mapRoutePresenter = new MapRoutePresenterImpl(this, this);
        pickDropOrderDetailPresenter = new PickDropOrderDetailPresenterImpl(this, this);


        if (getIntent() != null) {

            pickUpLatitude = getIntent().getDoubleExtra(ConstIntent.CONST_PICKUP_LATITUDE, 0);
            pickUpLongitude = getIntent().getDoubleExtra(ConstIntent.CONST_PICKUP_LONGITUDE, 0);
            pickUpName = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_NAME);
            pickUpContactNo = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CONTACT_NO);
            pickUpLandMark = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_LANDMARK);
            pickUpFlatNumber = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_FLAT_NUMBER);
            pickUpCountry = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_COUNTRY);
            pickUpState = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_STATE);
            pickUpCity = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CITY);


            dropLatitude = getIntent().getDoubleExtra(ConstIntent.CONST_DROP_LATITUDE, 0);
            dropLongitude = getIntent().getDoubleExtra(ConstIntent.CONST_DROP_LONGITUDE, 0);
            dropName = getIntent().getStringExtra(ConstIntent.CONST_DROP_NAME);
            dropContactNo = getIntent().getStringExtra(ConstIntent.CONST_DROP_CONTACT_NO);
            dropLandMark = getIntent().getStringExtra(ConstIntent.CONST_DROP_LANDMARK);
            dropFlatNumber = getIntent().getStringExtra(ConstIntent.CONST_DROP_FLAT_NUMBER);
            dropCountry = getIntent().getStringExtra(ConstIntent.CONST_DROP_COUNTRY);
            dropState = getIntent().getStringExtra(ConstIntent.CONST_DROP_STATE);
            dropCity = getIntent().getStringExtra(ConstIntent.CONST_DROP_CITY);

            txtViewPickUpLocation.setText("" + pickUpFlatNumber + ","+pickUpLandMark + "," + pickUpCity+ "," + pickUpState+ "," + pickUpCountry);
            txtViewDropLocation.setText("" + dropFlatNumber + ","+dropLandMark + "," + dropCity+ "," + dropState+ "," + dropCountry);
        }


        getPickDropOrderDetailDapaApi();

        // initialized map
        initializeMap();

//        getAddressFromLatlong();
//        getAddressDropLocalFromLatlong();
    }

    private  void  getAddressFromLatlong()
    {
        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        ///Address address = null;

        try {
           List<Address> addresses = geocoder.getFromLocation(pickUpLatitude, pickUpLongitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

               // String address = addresses.get(0).getAddressLine(0);

                if (address==null)
                {
                    address = "unnamed place";
                }

                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //  edtTextFlatNo.setText(knownName);
                //  edtTextLandmark.setText(city);
                txtViewPickUpLocation.setText(address); //latitude + " , " + longitude);
                //edtTextCity.setText(city);
                //edtTextState.setText(state);
                // edtTextCountry.setText(country);

                Log.e("Addr Lat", address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private  void  getAddressDropLocalFromLatlong()
    {
        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        ///Address address = null;

        try {
            List<Address> addresses = geocoder.getFromLocation(dropLatitude, dropLongitude, 1); // Here 1 represent max selectedItemLocationId result to returned, by documents it recommended 1 to 5

            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                // String address = addresses.get(0).getAddressLine(0);

                if (address==null)
                {
                    address = "unnamed place";
                }

                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //  edtTextFlatNo.setText(knownName);
                //  edtTextLandmark.setText(city);
                txtViewDropLocation.setText(address); //latitude + " , " + longitude);
                //edtTextCity.setText(city);
                //edtTextState.setText(state);
                // edtTextCountry.setText(country);

                Log.e("Addr Lat", address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getPickDropOrderDetailDapaApi() {

        pickDropOrderDetailPresenter.PickDropOrderDetailData();
    }


    public void setSelectedProductTypeId(int selectedProductTypeId) {
        this.selectedProductTypeId = selectedProductTypeId;
    }


    public void setSelectedSizeId(int selectedSizeId) {

        this.selectedSizeId = selectedSizeId;

    }


    public void setSelectedWeightId(int selectedWeightId) {
        this.selectedWeightId = selectedWeightId;
    }

    private void manageRecyclerViewProductType() {

        productTypeAdapter = new ProductTypeAdapter(this, pickDropOrderDetailModel.getProductType());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerViewProduct.setLayoutManager(mLayoutManager);
        recyclerViewProduct.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProduct.setAdapter(productTypeAdapter);
    }

    private void manageRecyclerViewWeight() {

        weightAdapter = new WeightAdapter(this, pickDropOrderDetailModel.getWeight());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerViewApproximateWeight.setLayoutManager(mLayoutManager);
        recyclerViewApproximateWeight.setItemAnimator(new DefaultItemAnimator());
        recyclerViewApproximateWeight.setAdapter(weightAdapter);
    }

    private void manageRecyclerViewSize() {

        sizeAdapter = new SizeAdapter(this, pickDropOrderDetailModel.getSize());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerViewApproximateSize.setLayoutManager(mLayoutManager);
        recyclerViewApproximateSize.setItemAnimator(new DefaultItemAnimator());
        recyclerViewApproximateSize.setAdapter(sizeAdapter);
    }


    private void initializeMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {

        if (checkIsPermissionGranted()) {

            // enable current location in map
            //mMap.setMyLocationEnabled(true);

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                isInitializedMap = true;

                AlertDialogHelper.alertEnableLocation(OrderDetailPickDropActivity.this);
                // return;
            } else {
                getLocation();
            }
        } else {
            askLocationPermission();
        }

    }

    public void getLocation() {

        Progress.start(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean networkLocationEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean gpsLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 2000, 2000, (LocationListener) this);
                locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 2000, 2000, (LocationListener) this);
                locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        Progress.stop();
        latitude = location.getLatitude();
        longitude = location.getLongitude();


        mMap.clear();

        // add marker on google map
       /* LatLng myLatLng = new LatLng(latitude, longitude);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        MarkerOptions markerOptMyLoc = new MarkerOptions().position(myLatLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_phone)).title("You are here");
        mMap.addMarker(markerOptMyLoc);*/

        // adding marker
        // builder.include(myLatLng);

        // LatLngBounds boundsMyLoc = builder.build();
        // mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsMyLoc, 2));

        // Zoom out just a little
        // mMap.animateCamera(CameraUpdateFactory.zoomTo(mMap.getCameraPosition().zoom + 10f));
        //move map camera
       /* mMap.moveCamera(CameraUpdateFactory.newLatLng(myLatLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));*/


        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        Log.e("latitude", "latitude--" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);


            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //edtTextLatLong.setText("" + latitude + " , " + longitude);
                //edtTextFlatNo.setText(knownName);
                //edtTextLandmark.setText(address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void downloadUrlSuccess(PolylineOptions lineOptions) {

        /*Polyline polygon = mMap.addPolyline(lineOptions);
        polygon.setWidth(10f);
        polygon.setGeodesic(true);
        polygon.setColor(Color.RED);
        polygon.setZIndex(10);*/

        if (lineOptions != null)
            mMap.addPolyline(lineOptions);


        //drawDashedPolyLine(mMap, lineOptions.getPoints(), R.color.color_red);


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

        /*---------------No Uses----------------*/
    }

    @Override
    public void onProviderEnabled(String provider) {

        /*---------------No Uses----------------*/
    }

    @Override
    public void onProviderDisabled(String provider) {

        /*---------------No Uses----------------*/
    }

    private boolean checkIsPermissionGranted() {

        int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if ((permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        // 2910 is for selectedItemLocation permission
        switch (requestCode) {
            case 2910: { // selectedItemLocation enable

                int permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

                int permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

                if ((permissionCheckLoc == 0) && (permissionCoarseLoc == 0)) {
                    getCurrentLocation();
                } else {
                    //SnackNotify.showMessage("Please provide security permission from aap setting.", coordinateLayout);
                    isInitializedMap = true;
                    AlertDialogHelper.alertEnableAppSetting(this);
                }
                return;
            }
        }
    }


    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 2910);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        //etCurrentLocation();

        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        // LatLng TutorialsPoint = new LatLng(28.7041, 77.1025);


        LatLng sourcePoint = new LatLng(pickUpLatitude, pickUpLongitude);
        LatLng destinationPoint = new LatLng(dropLatitude, dropLongitude);


        MarkerOptions markerOptMyLoc = new MarkerOptions().position(sourcePoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_pickup_pin_maps)).title("Pick up Location");
        MarkerOptions markerOptMyLoc1 = new MarkerOptions().position(destinationPoint).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_drop_pin_maps)).title("Drop Location");
        mMap.addMarker(markerOptMyLoc);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sourcePoint));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(5.0f));

        mMap.addMarker(markerOptMyLoc1);

        //mMap.moveCamera(CameraUpdateFactory.newLatLng(destinationPoint));


        String url = getDirectionsUrl(sourcePoint, destinationPoint);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyD6qT8r_rVEL4Ls20tRayJV6Fi8Tv0FhEI";

        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            if (points.size() != 0) mMap.addPolyline(lineOptions);


            // String totaldistance=CalculationByDistance(sourcePoint,destinationPoint);

            //Log.d("Distance ",totaldistance);


        }
    }


    @Override
    public void getPickDropOrderDetailSuccess(PickDropResponseModel.Data pickDropOrderDetailModel) {

        this.pickDropOrderDetailModel = pickDropOrderDetailModel;
        manageRecyclerViewProductType();
        manageRecyclerViewWeight();
        manageRecyclerViewSize();
    }

    @Override
    public void getPickDropOrderDetailUnSucess(String message) {

    }

    @Override
    public void pickDropOrderDetailInternetError() {

    }

    @Override
    public void createPickDropOrderDetailSuccess(CreatePickDropOrderResponseModel.Data pickDropOrderDetailModel) {

        Intent intent = new Intent(this, DeliveryChargeActivity.class);
        intent.putExtra("CreatePickDropOrderResponseModel", pickDropOrderDetailModel);
        startActivity(intent);
    }

    @Override
    public void createPickDropOrderDetailUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void createPickDropOrderDetailInternetError() {

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {
        finish();
    }


    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirmClicked() {

        if (validate()) {

            JSONObject jsonObject = new JSONObject();


//            pickUpContactNo = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CONTACT_NO);
//            pickUpLandMark = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_LANDMARK);
//            pickUpFlatNumber = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_FLAT_NUMBER);
//            pickUpCountry = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_COUNTRY);
//            pickUpState = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_STATE);
//            pickUpCity = getIntent().getStringExtra(ConstIntent.CONST_PICKUP_CITY);


            try {
                jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
                jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
                jsonObject.put(Const.PARAM_PICKUP_CONTACT_NAME, pickUpName);
                jsonObject.put(Const.PARAM_PICKUP_CONTACT_NO, pickUpContactNo);
                jsonObject.put(Const.PARAM_PICKUP_LAT, pickUpLatitude);
                jsonObject.put(Const.PARAM_PICKUP_LONG, pickUpLongitude);

                jsonObject.put(ConstIntent.CONST_PICKUP_LANDMARK, pickUpLandMark);
                jsonObject.put(ConstIntent.CONST_PICKUP_FLAT_NUMBER, pickUpFlatNumber);
                jsonObject.put(ConstIntent.CONST_PICKUP_COUNTRY, pickUpCountry);
                jsonObject.put(ConstIntent.CONST_PICKUP_STATE, pickUpState);
                jsonObject.put(ConstIntent.CONST_PICKUP_CITY, pickUpCity);


                jsonObject.put(Const.PARAM_DROP_CONTACT_NAME, dropName);
                jsonObject.put(Const.PARAM_DROP_CONTACT_NO, dropContactNo);
                jsonObject.put(Const.PARAM_DROP_LAT, dropLatitude);
                jsonObject.put(Const.PARAM_DROP_LONG, dropLongitude);

                jsonObject.put(ConstIntent.CONST_DROP_LANDMARK, dropLandMark);
                jsonObject.put(ConstIntent.CONST_DROP_FLAT_NUMBER, dropFlatNumber);
                jsonObject.put(ConstIntent.CONST_DROP_COUNTRY, dropCountry);
                jsonObject.put(ConstIntent.CONST_DROP_STATE, dropState);
                jsonObject.put(ConstIntent.CONST_DROP_CITY, dropCity);


                jsonObject.put(Const.PARAM_PRODUCT_TYPE_ID, selectedProductTypeId);
                jsonObject.put(Const.PARAM_WEIGHT_ID, selectedWeightId);
                jsonObject.put(Const.PARAM_SIZE_ID, selectedSizeId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            pickDropOrderDetailPresenter.createPickDropOrderData(jsonObject);
        }
    }

    private boolean validate() {

        if (selectedProductTypeId == 0) {

            SnackNotify.showMessage("Please select product type.", relLayMain);
            return false;

        } else if (selectedWeightId == 0) {


            SnackNotify.showMessage("Please select weight.", relLayMain);
            return false;
        } else if (selectedSizeId == 0) {

            SnackNotify.showMessage("Please select size.", relLayMain);
            return false;
        }
        return true;
    }


}
