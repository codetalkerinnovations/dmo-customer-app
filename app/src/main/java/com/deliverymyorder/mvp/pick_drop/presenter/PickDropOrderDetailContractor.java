package com.deliverymyorder.mvp.pick_drop.presenter;


import com.deliverymyorder.mvp.pick_drop.model.CreatePickDropOrderResponseModel;
import com.deliverymyorder.mvp.pick_drop.model.PickDropResponseModel;

import org.json.JSONObject;

import java.util.ArrayList;

public class PickDropOrderDetailContractor {

    public interface PickDropOrderDetailPresenter {

        void PickDropOrderDetailData();

        void createPickDropOrderData(JSONObject jsonObject);
    }

    public interface PickDropOrderDetailView {
        void getPickDropOrderDetailSuccess(PickDropResponseModel.Data pickDropOrderDetailModel);

        void getPickDropOrderDetailUnSucess(String message);

        void pickDropOrderDetailInternetError();

        void createPickDropOrderDetailSuccess(CreatePickDropOrderResponseModel.Data pickDropOrderDetailModel);

        void createPickDropOrderDetailUnSucess(String message);

        void createPickDropOrderDetailInternetError();
    }
}
