package com.deliverymyorder.mvp.delivery_charge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryChargeModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public class Data {

        @SerializedName("product_type_cost")
        @Expose
        private Integer productTypeCost;
        @SerializedName("size_cost")
        @Expose
        private Integer sizeCost;
        @SerializedName("weight_cost")
        @Expose
        private Integer weightCost;
        @SerializedName("per_km_cost")
        @Expose
        private Integer perKmCost;
        @SerializedName("base_km_cost")
        @Expose
        private Integer baseKmCost;
        @SerializedName("total_km")
        @Expose
        private Double totalKm;
        @SerializedName("base_km")
        @Expose
        private Integer baseKm;
        @SerializedName("total_delivery_cost")
        @Expose
        private Integer totalDeliveryCost;

        public Integer getProductTypeCost() {
            return productTypeCost;
        }

        public void setProductTypeCost(Integer productTypeCost) {
            this.productTypeCost = productTypeCost;
        }

        public Integer getSizeCost() {
            return sizeCost;
        }

        public void setSizeCost(Integer sizeCost) {
            this.sizeCost = sizeCost;
        }

        public Integer getWeightCost() {
            return weightCost;
        }

        public void setWeightCost(Integer weightCost) {
            this.weightCost = weightCost;
        }

        public Integer getPerKmCost() {
            return perKmCost;
        }

        public void setPerKmCost(Integer perKmCost) {
            this.perKmCost = perKmCost;
        }

        public Integer getBaseKmCost() {
            return baseKmCost;
        }

        public void setBaseKmCost(Integer baseKmCost) {
            this.baseKmCost = baseKmCost;
        }

        public Double getTotalKm() {
            return totalKm;
        }

        public void setTotalKm(Double totalKm) {
            this.totalKm = totalKm;
        }

        public Integer getBaseKm() {
            return baseKm;
        }

        public void setBaseKm(Integer baseKm) {
            this.baseKm = baseKm;
        }

        public Integer getTotalDeliveryCost() {
            return totalDeliveryCost;
        }

        public void setTotalDeliveryCost(Integer totalDeliveryCost) {
            this.totalDeliveryCost = totalDeliveryCost;
        }

    }

}
