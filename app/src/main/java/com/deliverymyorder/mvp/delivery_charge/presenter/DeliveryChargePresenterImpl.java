package com.deliverymyorder.mvp.delivery_charge.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.delivery_charge.DeliveryChargeModel;
import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.model.UpdateDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.presenter.DeliveryTypeContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryChargePresenterImpl implements DeliveryChargeContractor.DeliveryChargePresenter{

    Activity activity;
    DeliveryChargeContractor.DeliveryChargeView deliveryChargeView;

    public DeliveryChargePresenterImpl(Activity activity, DeliveryChargeContractor.DeliveryChargeView deliveryChargeView) {
        this.activity = activity;
        this.deliveryChargeView = deliveryChargeView;
    }



    @Override
    public void deliveryChargeData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            callGetDeliveryChargeDataApi(jsonObject);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            deliveryChargeView.deliveryChargeInternetError();

        }
    }

    
    private void callGetDeliveryChargeDataApi(JSONObject jsonObject) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<DeliveryChargeModel> getResult = ApiAdapter.getApiService().getCalculatePickDropCharges("application/json", "no-cache", body);

        getResult.enqueue(new Callback<DeliveryChargeModel>() {
            @Override
            public void onResponse(Call<DeliveryChargeModel> call, Response<DeliveryChargeModel> response) {

                Progress.stop();
                try {
                    DeliveryChargeModel deliveryChargeModel = response.body();

                    if (deliveryChargeModel.getError() == 0) {
                     deliveryChargeView.deliveryChargeSuccess(deliveryChargeModel.getData());
                    } else {
                        deliveryChargeView.deliveryChargeUnSucess(deliveryChargeModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    deliveryChargeView.deliveryChargeUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<DeliveryChargeModel> call, Throwable t) {
                Progress.stop();
                deliveryChargeView.deliveryChargeUnSucess(activity.getString(R.string.server_error));
            }
        });
    }

  
}