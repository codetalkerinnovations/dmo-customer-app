package com.deliverymyorder.mvp.delivery_charge;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.delivery_charge.presenter.DeliveryChargeContractor;
import com.deliverymyorder.mvp.delivery_charge.presenter.DeliveryChargePresenterImpl;
import com.deliverymyorder.mvp.order_success.OrderConfirmedActivity;
import com.deliverymyorder.mvp.pick_drop.model.CreatePickDropOrderResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeliveryChargeActivity extends AppCompatActivity implements DeliveryChargeContractor.DeliveryChargeView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.txtViewSizeCost)
    TextView txtViewSizeCost;

    @BindView(R.id.txtViewWeightCost)
    TextView txtViewWeightCost;

    @BindView(R.id.txtViewBaseKm)
    TextView txtViewBaseKm;

    @BindView(R.id.txtViewBaseKmCost)
    TextView txtViewBaseKmCost;

    @BindView(R.id.txtViewPerKmCost)
    TextView txtViewPerKmCost;

    @BindView(R.id.txtViewTotalDistance)
    TextView txtViewTotalDistance;

    @BindView(R.id.txtViewTotal)
    TextView txtViewTotal;

    @BindView(R.id.txtViewConfirm)
    TextView txtViewConfirm;


    DeliveryChargePresenterImpl deliveryChargePresenter;

    JSONObject jsonObject;
    CreatePickDropOrderResponseModel.Data pickDropOrderDetailModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_charge);
        ButterKnife.bind(this);
        deliveryChargePresenter = new DeliveryChargePresenterImpl(this, this);


        if (getIntent() != null) {

            pickDropOrderDetailModel = (CreatePickDropOrderResponseModel.Data) getIntent().getSerializableExtra("CreatePickDropOrderResponseModel");
        }

        callDeliveryChargeDataApi();
    }


    private void callDeliveryChargeDataApi() {

        jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_PICKUP_LAT, pickDropOrderDetailModel.getPickupLat());
            jsonObject.put(Const.PARAM_PICKUP_LONG, pickDropOrderDetailModel.getPickupLong());
            jsonObject.put(Const.PARAM_DROP_LAT, pickDropOrderDetailModel.getDropLat());
            jsonObject.put(Const.PARAM_DROP_LONG, pickDropOrderDetailModel.getDropLong());
            jsonObject.put(Const.PARAM_PRODUCT_TYPE_ID, pickDropOrderDetailModel.getProductTypeId());
            jsonObject.put(Const.PARAM_WEIGHT_ID, pickDropOrderDetailModel.getWeightId());
            jsonObject.put(Const.PARAM_SIZE_ID, pickDropOrderDetailModel.getSizeId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        deliveryChargePresenter.deliveryChargeData(jsonObject);
    }

    @Override
    public void deliveryChargeSuccess(DeliveryChargeModel.Data deliveryChargeModel) {

        //set the data
        txtViewSizeCost.setText("INR " + String.valueOf(deliveryChargeModel.getSizeCost()));
        txtViewWeightCost.setText("INR " + String.valueOf(deliveryChargeModel.getWeightCost()));
        txtViewBaseKm.setText(String.valueOf(deliveryChargeModel.getBaseKm() + " KM"));
        txtViewBaseKmCost.setText("INR " + String.valueOf(deliveryChargeModel.getBaseKmCost()));
        txtViewPerKmCost.setText("INR " + String.valueOf(deliveryChargeModel.getPerKmCost()));
        txtViewTotalDistance.setText(String.valueOf(deliveryChargeModel.getTotalKm()) + " KM");
        txtViewTotal.setText("INR " + String.valueOf(deliveryChargeModel.getTotalDeliveryCost()));
    }

    @Override
    public void deliveryChargeUnSucess(String message) {

        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void deliveryChargeInternetError() {

    }


    @OnClick(R.id.txtViewConfirm)
    public void txtViewConfirmClicked() {

        Intent intent = new Intent(this, OrderConfirmedActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {
        finish();
    }

}
