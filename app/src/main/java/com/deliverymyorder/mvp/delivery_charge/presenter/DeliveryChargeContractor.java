package com.deliverymyorder.mvp.delivery_charge.presenter;

import com.deliverymyorder.mvp.delivery_charge.DeliveryChargeModel;

import org.json.JSONObject;

import java.util.ArrayList;

public class DeliveryChargeContractor {

    public interface DeliveryChargePresenter {

        void deliveryChargeData(JSONObject jsonObject);
    }

    public interface DeliveryChargeView {

        void deliveryChargeSuccess(DeliveryChargeModel.Data deliveryChargeModel);

        void deliveryChargeUnSucess(String message);

        void deliveryChargeInternetError();
    }
}
