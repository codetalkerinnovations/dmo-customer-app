package com.deliverymyorder.mvp.shop_listing.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailsActivity;
import com.deliverymyorder.mvp.shop_listing.ShopListingActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.Holder> {


    Activity activity;
    ArrayList<SearchDataResponseModel.Datum> datum;
    String favBit;

    public ShoppingListAdapter(Activity activity, ArrayList<SearchDataResponseModel.Datum> datum) {
        this.activity = activity;
        this.datum = datum;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_shop, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        final SearchDataResponseModel.Datum result = datum.get(position);


        holder.txtViewShopName.setText(result.getTitle());
        holder.txtViewAddress.setText(result.getAddress());
        holder.txtViewDeliveryCharges.setText( "₹"+String.valueOf(result.getTotalDeliveryCost()));


        holder.relLayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, ShopDetailsActivity.class);
                intent.putExtra(ConstIntent.CONST_BUISNESS_ID, result.getId());
                intent.putExtra(ConstIntent.CONST_BUISNESS_ACCEPT_CUSTOM_ORDER, result.getCustom_order_allowed());
                intent.putExtra(ConstIntent.CONST_MERCHANT_ID, result.getMerchant_id());
                intent.putExtra(ConstIntent.CONST_SHOP_NAME, result.getTitle());
                intent.putExtra(ConstIntent.CONST_ADDRESS, result.getAddress());
                activity.startActivity(intent);

            }
        });

    }


    @Override
    public int getItemCount() {
        return datum.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewShopName)
        TextView txtViewShopName;

        @BindView(R.id.relLayItem)
        RelativeLayout relLayItem;

        @BindView(R.id.txtViewAddress)
        TextView txtViewAddress;

        @BindView(R.id.txtViewDeliveryChargesTitle)
        TextView txtViewDeliveryChargesTitle;

        @BindView(R.id.txtViewDeliveryCharges)
        TextView txtViewDeliveryCharges;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontHelper.setFontFace(txtViewShopName, FontHelper.FontType.FONT_ROBOTO_LIGHT, activity);

            // setFont();
        }
    }
}