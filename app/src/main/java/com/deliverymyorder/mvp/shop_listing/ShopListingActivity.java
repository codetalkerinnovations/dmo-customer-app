package com.deliverymyorder.mvp.shop_listing;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.FontHelper;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.Result;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.CustomShopOrderActivity;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.dashboard.presenter.HomePageContractor;
import com.deliverymyorder.mvp.dashboard.presenter.HomePagePresenterImpl;
import com.deliverymyorder.mvp.shop_listing.adapter.ShoppingListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShopListingActivity extends AppCompatActivity implements HomePageContractor.HomePageView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.relLaySearch)
    RelativeLayout relLaySearch;

    @BindView(R.id.edtTextSearchShop)
    EditText edtTextSearchShop;

    @BindView(R.id.imgViewSearchButton)
    ImageView imgViewSearchButton;

    @BindView(R.id.imgViewInCampus)
    ImageView imgViewInCampus;

    @BindView(R.id.imgViewOutCampus)
    ImageView imgViewOutCampus;


    @BindView(R.id.imgViewSearch)
    ImageView imgViewSearch;

    @BindView(R.id.txtViewHeading)
    TextView txtViewHeading;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgViewFilter)
    ImageView imgViewFilter;

    @BindView(R.id.linLayInCampusOutCampus)
    LinearLayout linLayInCampusOutCampus;

    @BindView(R.id.relLayNoRecord)
    RelativeLayout relLayNoRecord;

    @BindView(R.id.recyclerViewShop)
    RecyclerView recyclerViewShop;

    HomePagePresenterImpl homePagePresenter;
    ShoppingListAdapter shoppingListAdapter;
    ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel;


    int categoryId = 0;
    int subCategoryId = 0;
    int filter = 1;
    String categoryName;
    String searchKey = "";

    int campusId = 0;
    boolean isInCampus = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_listing);
        ButterKnife.bind(this);


        homePagePresenter = new HomePagePresenterImpl(this, this);

        if (getIntent() != null) {

            if (getIntent().hasExtra(ConstIntent.CONST_CATEGORY_ID) || getIntent().hasExtra(ConstIntent.CONST_SUBCATEGORY_ID)) {
                categoryId = getIntent().getIntExtra(ConstIntent.CONST_CATEGORY_ID, 0);
                subCategoryId = getIntent().getIntExtra(ConstIntent.CONST_SUBCATEGORY_ID, 0);
                categoryName = getIntent().getStringExtra(ConstIntent.CONST_CATEGORY_NAME);
            }
        }

        if (LoginManager.getInstance().getCurrentCampus() != null) {

            if (LoginManager.getInstance().getCurrentCampus().getId() != null) {
                campusId = LoginManager.getInstance().getCurrentCampus().getId();
                isInCampus = true;
                LoginManager.getInstance().setInCampusStatus(isInCampus);
            }
        }


        if (campusId != 0) {
            linLayInCampusOutCampus.setVisibility(View.VISIBLE);
        } else {
            linLayInCampusOutCampus.setVisibility(View.GONE);
        }


        setFont();

        getShopListDataApi();


    }

    @OnClick(R.id.txtCustomShopOrder)
    public void txtCustomShopOrder(View view)
    {
        Intent i = new Intent(this,CustomShopOrderActivity.class);
        startActivity(i);
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewTitle, FontHelper.FontType.FONT_ROBOTO_LIGHT, this);
    }

    private void getShopListDataApi() {

        homePagePresenter.getSearchData(searchKey, String.valueOf(categoryId),String.valueOf(subCategoryId), filter, isInCampus);
    }

    private void manageRecyclerView() {

        shoppingListAdapter = new ShoppingListAdapter(this, arrayListSearchDataResponseModel);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerViewShop.setLayoutManager(mLayoutManager);
        recyclerViewShop.setItemAnimator(new DefaultItemAnimator());
        recyclerViewShop.setAdapter(shoppingListAdapter);
    }

    @Override
    public void getHomePageDataSuccess(HomePageDataModel.Data arrayListHomePageDataModel) {
        //Not used
    }

    @Override
    public void getHomePageDataUnSucess(String message) {
        //Not used
    }

    @Override
    public void homePageDataInternetError() {
        //Not used
    }

    @Override
    public void getSearchDataSuccess(ArrayList<SearchDataResponseModel.Datum> arrayListSearchDataResponseModel) {

        this.arrayListSearchDataResponseModel = arrayListSearchDataResponseModel;


        txtViewHeading.setText(categoryName);

        if (arrayListSearchDataResponseModel.size() > 0) {

            txtViewHeading.setVisibility(View.VISIBLE);
            recyclerViewShop.setVisibility(View.VISIBLE);
            relLayNoRecord.setVisibility(View.GONE);
            manageRecyclerView();
        } else {

            txtViewHeading.setVisibility(View.VISIBLE);
            recyclerViewShop.setVisibility(View.GONE);
            relLayNoRecord.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void getSearchDataUnSucess(String message) {
        recyclerViewShop.setVisibility(View.GONE);
        relLayNoRecord.setVisibility(View.VISIBLE);
    }

    @Override
    public void searchDataInternetError() {

    }

    @Override
    public void getCampusApiDidFinishWith(Result result, List<Campus> campusList) {

    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBack() {

        finish();


    }


    @OnClick(R.id.imgViewSearch)
    public void imgViewSearch() {

        imgViewSearch.setVisibility(View.GONE);
        relLaySearch.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.imgViewSearchButton)
    public void imgViewSearchButton() {

        Utils.hideKeyboardIfOpen(this);

        searchKey = String.valueOf(edtTextSearchShop.getText());

       /* if (Utils.isEmptyOrNull(searchKey)) {

            SnackNotify.showMessage("Please enter shop name.", relLayMain);
        } else {

            getShopListDataApi();
        }*/

        getShopListDataApi();
    }

    @OnClick(R.id.imgViewInCampus)
    public void imgViewInCampus() {

        Utils.hideKeyboardIfOpen(this);

        isInCampus = true;

        imgViewOutCampus.setBackgroundResource(R.mipmap.ic_not_checked_in_campus);
        imgViewInCampus.setBackgroundResource(R.mipmap.ic_checked_in_campus);
        LoginManager.getInstance().setInCampusStatus(isInCampus);
        getShopListDataApi();
    }

    @OnClick(R.id.imgViewOutCampus)
    public void imgViewOutCampus() {

        Utils.hideKeyboardIfOpen(this);

        isInCampus = false;

        LoginManager.getInstance().setInCampusStatus(isInCampus);

        imgViewInCampus.setBackgroundResource(R.mipmap.ic_not_checked_in_campus);
        imgViewOutCampus.setBackgroundResource(R.mipmap.ic_checked_in_campus);


        getShopListDataApi();
    }


    @OnClick(R.id.imgViewFilter)
    public void imgViewFilter() {

        Utils.hideKeyboardIfOpen(this);

        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(ShopListingActivity.this, imgViewFilter);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.filter_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                //Toast.makeText(ShopListingActivity.this, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();

                if (item.getTitle().equals("Popular")) {

                    filter = 2;
                } else if (item.getTitle().equals("Rating")) {

                    filter = 3;
                } else {
                    filter = 1;
                }

                getShopListDataApi();

                return true;
            }
        });

        popup.show(); //showing popup menu

    }


}
