package com.deliverymyorder.mvp.register.presenter;

import com.deliverymyorder.mvp.login.LoginResponseModel;
import com.deliverymyorder.mvp.register.RegisterResponseModel;
import com.google.gson.JsonObject;

/**
 * Created by Braintech on 12-04-2018.
 */

public class RegisterContractor {

    public interface RegisterView {

        void getRegisterSuccess(RegisterResponseModel data);

        void getRegisterUnSuccess(String message);

        void getRegisterInternetError();
    }

    public interface Presenter {
        void verifySignUpData(String name, String lastName, String phoneNumber, String password, String email, String referalCode, String loginType, String socialId);
    }
}
