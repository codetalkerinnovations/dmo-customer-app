package com.deliverymyorder.mvp.register.presenter;

import android.app.Activity;
import android.provider.Settings;


import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.login.LoginResponseModel;
import com.deliverymyorder.mvp.register.RegisterResponseModel;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 12-04-2018.
 */

public class RegisterPresenterImpl implements RegisterContractor.Presenter {

    RegisterContractor.RegisterView registerView;
    Activity activity;
    JSONObject jsonObject;

    public RegisterPresenterImpl(RegisterContractor.RegisterView registerView, Activity activity) {
        this.registerView = registerView;
        this.activity = activity;
    }

    @Override
    public void verifySignUpData(String name, String lastName, String phoneNumber, String password, String email, String referalCode, String loginType, String socialId) {

        try {
            ApiAdapter.getInstance(activity);
            callingRegisterationApi(name, lastName, phoneNumber, password, email, referalCode, loginType, socialId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            registerView.getRegisterInternetError();
        }
    }

    private void callingRegisterationApi(String name, String lastName, String phoneNumber, String password, String email, String referalCode, String loginType, String socialId) {
        Progress.start(activity);

        String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);

        //String deviceToken = new FcmSession(activity).getFcmToken();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.KEY_FIRST_NAME, name);
            jsonObject.put(Const.KEY_LAST_NAME, lastName);
            jsonObject.put(Const.KEY_PHONE_NUMBER, phoneNumber);
            jsonObject.put(Const.KEY_EMAIL, email);
            jsonObject.put(Const.KEY_PASSWORD, password);
            jsonObject.put(Const.KEY_CONFIRM_PASSWORD, password);
            jsonObject.put(Const.KEY_IP_ADDRESS, "10.16.1.102");
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_REFERAL_CODE, referalCode);
            jsonObject.put(Const.KEY_LOGIN_TYPE, loginType);
            jsonObject.put(Const.KEY_SOCIAL_ID, socialId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<RegisterResponseModel> getRegisterMessage = ApiAdapter.getApiService().getSignupResult("application/json", "no-cache", body);

        getRegisterMessage.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                Progress.stop();

                try {

                    RegisterResponseModel responseModel = response.body();

                    if (responseModel.getError() == 0) {
                        LoginManager.getInstance();
                        // LoginManager.getInstance().createLoginSession(responseModel.getData());
                        registerView.getRegisterSuccess(responseModel);
                    } else {
                        registerView.getRegisterUnSuccess(responseModel.getReason());
                    }
                } catch (NullPointerException exp) {
                    registerView.getRegisterUnSuccess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                Progress.stop();
                registerView.getRegisterUnSuccess(activity.getString(R.string.server_error));
            }
        });
    }
}
