package com.deliverymyorder.mvp.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.method.PasswordTransformationMethod;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.Common.utility.Utils;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.otp.OtpActivity;
import com.deliverymyorder.mvp.register.presenter.RegisterContractor;
import com.deliverymyorder.mvp.register.presenter.RegisterPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity implements RegisterContractor.RegisterView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.imgViewBack)
    ImageView imgViewBack;

    @BindView(R.id.edtTextName)
    AppCompatEditText edtTextName;

    @BindView(R.id.edtTextLastName)
    AppCompatEditText edtTextLastName;

    @BindView(R.id.edtTextPhonenNumber)
    AppCompatEditText edtTextPhonenNumber;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.imgViewEye)
    ImageView imgViewEye;


    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.edtTextReferalCode)
    AppCompatEditText edtTextReferalCode;

    @BindView(R.id.txtViewSignUp)
    TextView txtViewSignUp;

    @BindView(R.id.imgViewFacebook)
    ImageView imgViewFacebook;

    @BindView(R.id.imgViewGoogle)
    ImageView imgViewGoogle;

    boolean isPasswordShow = false;


    RegisterPresenterImpl registerPresenter;


    String loginType;
    String socialId;
    String firstName;
    String lastName;
    String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);

        if (getIntent() != null) {

            loginType = getIntent().getStringExtra(ConstIntent.CONST_LOGIN_TYPE);
            socialId = getIntent().getStringExtra(ConstIntent.CONST_SOCIALLOGINID);
            firstName = getIntent().getStringExtra(ConstIntent.CONST_FIRST_NAME);
            lastName = getIntent().getStringExtra(ConstIntent.CONST_LAST_NAME);
            email = getIntent().getStringExtra(ConstIntent.CONST_EMAIL);


            if (!loginType.equals(Const.KEY_NORMAL)) {
                edtTextName.setText(firstName);
                edtTextLastName.setText(lastName);
                edtTextEmail.setText(email);

//                edtTextName.setEnabled(false);
//                edtTextLastName.setEnabled(false);
//                edtTextEmail.setEnabled(false);
            } else {
                edtTextName.setEnabled(true);
                edtTextLastName.setEnabled(true);
                edtTextEmail.setEnabled(true);
            }
        }

        registerPresenter = new RegisterPresenterImpl(this, this);

    }


    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked() {

        finish();
    }

    @OnClick(R.id.txtViewSignUp)
    public void txtViewSignUpClicked() {

        String name = String.valueOf(edtTextName.getText());
        String lastName = String.valueOf(edtTextLastName.getText());
        String phoneNumber = String.valueOf(edtTextPhonenNumber.getText());
        String password = String.valueOf(edtTextPassword.getText());
        String email = String.valueOf(edtTextEmail.getText());
        String referalCode = String.valueOf(edtTextReferalCode.getText());

        if (validate(name, lastName, phoneNumber, password, email)) {

            registerPresenter.verifySignUpData(name, lastName, phoneNumber, password, email, referalCode, loginType, socialId);
        }
    }

    private boolean validate(String name, String lastName, String phoneNumber, String password, String email) {

        if (Utils.isEmptyOrNull(name)) {
            SnackNotify.showMessage(getString(R.string.empty_first_name), relLayMain);
            return false;
        } else if (Utils.isEmptyOrNull(lastName)) {
            SnackNotify.showMessage(getString(R.string.empty_last_name), relLayMain);
            return false;
        } else if (Utils.isEmptyOrNull(phoneNumber)) {
            SnackNotify.showMessage(getString(R.string.empty_phone_number), relLayMain);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password), relLayMain);
            return false;
        } else if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_email), relLayMain);
            return false;
        } else if (!Utils.isValidEmail(email)) {
            SnackNotify.showMessage(getString(R.string.error_valid_email), relLayMain);
            return false;
        }

        return true;
    }

    @Override
    public void getRegisterSuccess(RegisterResponseModel data) {

        Intent intent = new Intent(this, OtpActivity.class);
        intent.putExtra(ConstIntent.CONST_INTENT_PHONE_NUMBER, String.valueOf(edtTextPhonenNumber.getText()));
        intent.putExtra(ConstIntent.CONST_PAGE_NAME, ConstIntent.CONST_REGISTER_PAGE);
        startActivity(intent);

        finish();
    }

    @Override
    public void getRegisterUnSuccess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getRegisterInternetError() {

    }

    @OnClick(R.id.imgViewEye)
    public void imgViewEye() {
        if (isPasswordShow) {
            edtTextPassword.setTransformationMethod(new PasswordTransformationMethod());
            int length = edtTextPassword.getText().toString().length();
            edtTextPassword.setSelection(length);
            isPasswordShow = false;
            imgViewEye.setImageResource(R.mipmap.ic_pass_show_cross);
        } else {
            edtTextPassword.setTransformationMethod(null);
            int length = edtTextPassword.getText().toString().length();
            edtTextPassword.setSelection(length);
            isPasswordShow = true;
            imgViewEye.setImageResource(R.mipmap.ic_pass_show);
        }

    }
}
