package com.deliverymyorder.mvp.feedback;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedbackResponseModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public class Data {

        @SerializedName("DeliveryPartnerRatings")
        @Expose
        private DeliveryPartnerRatings deliveryPartnerRatings;
        @SerializedName("BusinessRatings")
        @Expose
        private BusinessRatings businessRatings;

        public DeliveryPartnerRatings getDeliveryPartnerRatings() {
            return deliveryPartnerRatings;
        }

        public void setDeliveryPartnerRatings(DeliveryPartnerRatings deliveryPartnerRatings) {
            this.deliveryPartnerRatings = deliveryPartnerRatings;
        }

        public BusinessRatings getBusinessRatings() {
            return businessRatings;
        }

        public void setBusinessRatings(BusinessRatings businessRatings) {
            this.businessRatings = businessRatings;
        }

        public class BusinessRatings {

            @SerializedName("customer_id")
            @Expose
            private Integer customerId;
            @SerializedName("merchant_id")
            @Expose
            private Integer merchantId;
            @SerializedName("business_id")
            @Expose
            private Integer businessId;
            @SerializedName("order_id")
            @Expose
            private Integer orderId;
            @SerializedName("rating_points")
            @Expose
            private String ratingPoints;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("id")
            @Expose
            private Integer id;

            public Integer getCustomerId() {
                return customerId;
            }

            public void setCustomerId(Integer customerId) {
                this.customerId = customerId;
            }

            public Integer getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(Integer merchantId) {
                this.merchantId = merchantId;
            }

            public Integer getBusinessId() {
                return businessId;
            }

            public void setBusinessId(Integer businessId) {
                this.businessId = businessId;
            }

            public Integer getOrderId() {
                return orderId;
            }

            public void setOrderId(Integer orderId) {
                this.orderId = orderId;
            }

            public String getRatingPoints() {
                return ratingPoints;
            }

            public void setRatingPoints(String ratingPoints) {
                this.ratingPoints = ratingPoints;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

        }

        public class DeliveryPartnerRatings {

            @SerializedName("customer_id")
            @Expose
            private Integer customerId;
            @SerializedName("delivery_partner_id")
            @Expose
            private Integer deliveryPartnerId;
            @SerializedName("order_id")
            @Expose
            private Integer orderId;
            @SerializedName("rating_points")
            @Expose
            private String ratingPoints;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("id")
            @Expose
            private Integer id;

            public Integer getCustomerId() {
                return customerId;
            }

            public void setCustomerId(Integer customerId) {
                this.customerId = customerId;
            }

            public Integer getDeliveryPartnerId() {
                return deliveryPartnerId;
            }

            public void setDeliveryPartnerId(Integer deliveryPartnerId) {
                this.deliveryPartnerId = deliveryPartnerId;
            }

            public Integer getOrderId() {
                return orderId;
            }

            public void setOrderId(Integer orderId) {
                this.orderId = orderId;
            }

            public String getRatingPoints() {
                return ratingPoints;
            }

            public void setRatingPoints(String ratingPoints) {
                this.ratingPoints = ratingPoints;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

        }

    }


}
