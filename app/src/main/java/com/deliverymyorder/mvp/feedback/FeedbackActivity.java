package com.deliverymyorder.mvp.feedback;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.deliverymyorder.Common.helpers.LoginManager;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.SnackNotify;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.feedback.presenter.FeedbackContractor;
import com.deliverymyorder.mvp.feedback.presenter.FeedbackPresenterImpl;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedbackActivity extends AppCompatActivity implements FeedbackContractor.FeedbackView {

    @BindView(R.id.relLayMain)
    RelativeLayout relLayMain;

    @BindView(R.id.ratingBarDelivery)
    RatingBar ratingBarDelivery;

    @BindView(R.id.ratingBarShop)
    RatingBar ratingBarShop;

    @BindView(R.id.txtViewSubmit)
    TextView txtViewSubmit;

    JSONObject jsonObject;

    FeedbackPresenterImpl feedbackPresenter;

    int orderId;
    int merchantId;
    int deliveryPartnerId;
    String buisnessId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);

        feedbackPresenter = new FeedbackPresenterImpl(this, this);

        if (getIntent() != null) {

            orderId = getIntent().getIntExtra(ConstIntent.CONST_ORDER_ID, 0);
            merchantId = getIntent().getIntExtra(ConstIntent.CONST_MERCHANT_ID, 0);
            deliveryPartnerId = getIntent().getIntExtra(ConstIntent.CONST_DELIVERY_PARTNER_ID, 0);
            buisnessId = getIntent().getStringExtra(ConstIntent.CONST_BUISNESS_ID);
        }
    }

    @OnClick(R.id.imgViewBack)
    public void imgViewBackClicked(){

        finish();
    }

    @OnClick(R.id.txtViewSubmit)
    public void txtViewSubmitClicked() {
        String deliveryRating = String.valueOf(ratingBarDelivery.getRating());
        String shopRating = String.valueOf(ratingBarShop.getRating());

        Log.e("rating  ", deliveryRating + " " + shopRating);

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        int orderType = 0;
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType =1;
        }else if (obj instanceof PickAndDropOnGoingData)
        {
            orderType =2;
        }


        jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.KEY_CUSTOMER_ID, LoginManager.getInstance().getUserData().getCustomerId());
            jsonObject.put(Const.KEY_DELIVERY_PARTNER_ID, deliveryPartnerId);
            jsonObject.put(Const.KEY_DELIVERY_PARTNER_RATING, deliveryRating);
            jsonObject.put(Const.KEY_MERCHANT_ID, merchantId);
            jsonObject.put(Const.KEY_BUISNESS_ID, buisnessId);
            jsonObject.put(Const.KEY_BUISNESS_RATING, shopRating);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        feedbackPresenter.getFeedbackData(jsonObject);


    }


    @Override
    public void getFeedbackSuccess(FeedbackResponseModel.Data feedbackResponseModel) {

        finish();
    }

    @Override
    public void getFeedbackUnSucess(String message) {
        SnackNotify.showMessage(message, relLayMain);
    }

    @Override
    public void getFeedbackInternetError() {

    }
}
