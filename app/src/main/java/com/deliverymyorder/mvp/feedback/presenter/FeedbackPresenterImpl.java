package com.deliverymyorder.mvp.feedback.presenter;

import android.app.Activity;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.Progress;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.model.UpdateDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.presenter.DeliveryTypeContractor;
import com.deliverymyorder.mvp.feedback.FeedbackResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackPresenterImpl implements FeedbackContractor.FeedbackPresenter {

    Activity activity;
    FeedbackContractor.FeedbackView feedbackView;

    public FeedbackPresenterImpl(Activity activity, FeedbackContractor.FeedbackView feedbackView) {
        this.activity = activity;
        this.feedbackView = feedbackView;
    }

    @Override
    public void getFeedbackData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            callGetFeedbackDataApi(jsonObject);
        } catch (ApiAdapter.NoInternetException ex) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace();
            }
            feedbackView.getFeedbackInternetError();

        }
    }


    private void callGetFeedbackDataApi(JSONObject jsonObject) {
        Progress.start(activity);


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<FeedbackResponseModel> getResult = ApiAdapter.getApiService().giveFeedback("application/json", "no-cache", body);

        getResult.enqueue(new Callback<FeedbackResponseModel>() {
            @Override
            public void onResponse(Call<FeedbackResponseModel> call, Response<FeedbackResponseModel> response) {

                Progress.stop();
                try {
                    FeedbackResponseModel feedbackResponseModel = response.body();

                    if (feedbackResponseModel.getError() == 0) {
                        feedbackView.getFeedbackSuccess(feedbackResponseModel.getData());
                    } else {
                        feedbackView.getFeedbackUnSucess(feedbackResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    feedbackView.getFeedbackUnSucess(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<FeedbackResponseModel> call, Throwable t) {
                Progress.stop();
                feedbackView.getFeedbackUnSucess(activity.getString(R.string.server_error));
            }
        });
    }


}
