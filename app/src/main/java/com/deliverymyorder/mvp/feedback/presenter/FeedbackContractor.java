package com.deliverymyorder.mvp.feedback.presenter;

import com.deliverymyorder.mvp.feedback.FeedbackResponseModel;

import org.json.JSONObject;

public class FeedbackContractor {

    public interface FeedbackPresenter {

        void getFeedbackData(JSONObject jsonObject);

    }

    public interface FeedbackView {
        void getFeedbackSuccess(FeedbackResponseModel.Data feedbackResponseModel);

        void getFeedbackUnSucess(String message);

        void getFeedbackInternetError();

    }
}
