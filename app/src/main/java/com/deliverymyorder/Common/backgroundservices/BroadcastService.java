package com.deliverymyorder.Common.backgroundservices;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.requestresponse.ConstIntent;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.track_order.OrderSession.OrderSession;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;
import com.deliverymyorder.mvp.track_order.model.pickAndDropModel.PickAndDropOnGoingData;
import com.deliverymyorder.mvp.track_order.presenter.TrackOrderDetailPresenterImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 31-01-2019.
 */

public class BroadcastService extends Service {
    private static final String TAG = "BroadcastService";
    public static final String BROADCAST_ACTION = "com.websmithing.broadcasttest.displayevent";
    private final Handler handler = new Handler();
    Intent intent;
    int counter = 0;

    TrackOrderDetailPresenterImpl trackOrderDetailPresenterImpl;

    @Override
    public void onCreate() {
        super.onCreate();


        intent = new Intent(BROADCAST_ACTION);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 2000); // 1 second

    }

    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            callTracOrderDetailApi();
            handler.postDelayed(this, 3000); // 10 seconds
        }
    };

    private void DisplayLoggingInfo(TrackOrderDetailModel trackOrderDetailModel) {

        //Call your Api here which is geting latitude and Longitude After Some interval
        // callTracOrderDetailApi();

        Log.d(TAG, "entered DisplayLoggingInfo");

        OrderSession.getSharedInstance().setOrderStatus(trackOrderDetailModel.getData().getOrderStatus());

        intent.putExtra(ConstIntent.CONST_LATITUDE, trackOrderDetailModel.getData().getCurrent().getLatitude());
        intent.putExtra(ConstIntent.CONST_LONGITUDE, trackOrderDetailModel.getData().getCurrent().getLongitude());
        sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
    }


    private void callTracOrderDetailApi() {

        // Progress.start(activity);

        //int customerId = LoginManager.getInstance().getUserData().getCustomerId();

        Object obj = OrderSession.getSharedInstance().getCurrentOrder();

        int orderType = 0;
        if (obj instanceof OrderHistoryModel.Data.Ongoing) {
            orderType =1;
        }else if (obj instanceof PickAndDropOnGoingData)
        {
            orderType =2;
        }

       int orderId= PrefUtil.getInt(this,Const.PARAM_ORDER_ID,0);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.KEY_ACCESS_TOKEN, Const.VALUE_ACCESS_TOKEN);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);
            jsonObject.put(Const.PARAM_ORDER_TYPE, orderType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));
        Call<TrackOrderDetailModel> getCreatePasswordResult = ApiAdapter.getApiService().getTrackOrderDetails("application/json", "no-cache", body);
        getCreatePasswordResult.enqueue(new Callback<TrackOrderDetailModel>() {
            @Override
            public void onResponse(Call<TrackOrderDetailModel> call, Response<TrackOrderDetailModel> response) {

                //Progress.stop();
                try {
                    TrackOrderDetailModel trackOrderDetailModel = response.body();

                    if (trackOrderDetailModel.getError() == 0) {

                        DisplayLoggingInfo(trackOrderDetailModel);
                    } else {
                        //trackOrderView.onUnSuccessTrackOrderDetail(trackOrderDetailModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    //trackOrderView.onUnSuccessTrackOrderDetail(activity.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<TrackOrderDetailModel> call, Throwable t) {
                //  Progress.stop();
                //trackOrderView.onUnSuccessTrackOrderDetail(activity.getString(R.string.server_error));
            }
        });


    }
}


