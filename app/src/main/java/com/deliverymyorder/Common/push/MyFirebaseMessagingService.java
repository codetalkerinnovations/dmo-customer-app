package com.deliverymyorder.Common.push;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.deliverymyorder.Common.session.FcmSession;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.dashboard.DashboardActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Braintech on 8/8/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {


    //UserSession userSession;

    int sound = 0;
    int playSound;
    int notificationType = 0;
    String type;
    String loanId;
    String recommend;
    FcmSession fcmSession;
    public static final String channelId = "10001";
    String channelName = "Channel Name";
    int importance = NotificationManager.IMPORTANCE_HIGH;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            fcmSession = new FcmSession(this);

            //Calling method to generate notification
            //sendNotification(remoteMessage.getNotification().getBody());

            if (remoteMessage.getData() != null) {
                Map<String, String> data = remoteMessage.getData();
                Log.e("FireBase Data", "From: " + remoteMessage.getData());


                sendNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("title"));

            } else if (remoteMessage.getNotification().getBody() != null) {
                Log.e("Firebase body ", remoteMessage.getNotification().getBody());
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
            }

        } catch (NullPointerException ee) {
            ee.printStackTrace();

        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts

    private void sendNotification(String messageBody, String title) {

        Intent intent  = new Intent(this, DashboardActivity.class);



        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //  NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_app_logo_dark)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_app_logo_dark))
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        notificationBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(1234, notificationBuilder.build());
    }



}



