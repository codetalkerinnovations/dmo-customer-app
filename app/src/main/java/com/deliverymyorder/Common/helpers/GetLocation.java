package com.deliverymyorder.Common.helpers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.Common.requestresponse.Const;
import com.deliverymyorder.Common.utility.PrefUtil;


import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class GetLocation {
    private static Context ctxt;

    private static Geocoder geocoder;

    private static List<Address> addresses;

    private static Double latitude;
    private static Double longitude;

    public static void getLocation(Context context) {
        ctxt = context;

        try {
            ApiAdapter.getInstance(context);

            LocationManager location_manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            LocationListener listner = new MyLocationListener();
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            boolean networkLocationEnabled = location_manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            boolean gpsLocationEnabled = location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (networkLocationEnabled || gpsLocationEnabled) {

                if (networkLocationEnabled) {
                    location_manager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, 1000, 1000, listner);
                    location_manager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                } else if (gpsLocationEnabled) {
                    location_manager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, 1000, 1000, listner);
                    location_manager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
            }

        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            //loginView.onInternetError();
        }
    }

    public static class MyLocationListener implements LocationListener {

        StringBuilder str;


        @Override
        public void onLocationChanged(Location arg0) {

            latitude = arg0.getLatitude();
            longitude = arg0.getLongitude();


            try {

                geocoder = new Geocoder(ctxt, Locale.ENGLISH);
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (geocoder.isPresent()) {

                    Address returnAddress = addresses.get(0);

                    //fetching state
                    String state = returnAddress.getAdminArea();
                    // LogUtil.e(state);

                    //getting city
                    String city = returnAddress.getLocality();
                    //LogUtil.e(city);

                    String locality = returnAddress.getSubLocality();
                    // LogUtil.e(locality);

                    // LogUtil.e(returnAddress.getPostalCode());

                    PrefUtil.putString(ctxt, Const.KEY_LATITUDE,latitude.toString());
                    PrefUtil.putString(ctxt, Const.KEY_LONGITUDE, longitude.toString());
                    String error = returnAddress.getPostalCode();
                    Log.e("setting location", error+ returnAddress.getSubAdminArea() + returnAddress.getSubLocality());

                    //  Log.e("locality", locality);
                    /*AddressPref addressPref = new AddressPref(ctxt);
                    addressPref.storeUserLocation(state, city);*/

                  /*  AddressPref addressPref = new AddressPref(ctxt);
                    addressPref.storeUserLocation( city,locality);*/


                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProviderDisabled(String arg0) {
        }

        @Override
        public void onProviderEnabled(String arg0) {
        }

        @Override
        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        }
    }

    public static Double getLatitude() {
        return latitude;
    }

    public static Double getLongitude() {
        return longitude;
    }
}
