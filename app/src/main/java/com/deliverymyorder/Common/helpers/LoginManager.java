package com.deliverymyorder.Common.helpers;

import android.content.Context;

import com.deliverymyorder.Common.application.DeliverMyOrderApplication;
import com.deliverymyorder.Common.utility.PrefUtil;
import com.deliverymyorder.mvp.dashboard.model.Campus;
import com.deliverymyorder.mvp.login.LoginResponseModel;
import com.google.gson.Gson;


public class LoginManager {

    public static String KEY_USER_DATA = "user_data";
    public static String IS_LOGIN = "is_login";

    public static String IS_IN_CAMPUS = "is_in_campus";

    public static String KEY_CURRENT_CAMPUS = "current_campus";

    private static volatile LoginManager instance;

    public static LoginManager getInstance() {
        if (instance == null) {
            synchronized (LoginManager.class) {
                if (instance == null) {
                    instance = new LoginManager();
                }
            }
        }
        return instance;
    }

    public void createLoginSession(LoginResponseModel.Data loginResponseModel) {
        PrefUtil.putBoolean(DeliverMyOrderApplication.getInstance(), IS_LOGIN, true);
        PrefUtil.putString(DeliverMyOrderApplication.getInstance(), KEY_USER_DATA, new Gson().toJson(loginResponseModel));

    }
    public boolean isLoggedIn() {
        return PrefUtil.getBoolean(DeliverMyOrderApplication.getInstance(), IS_LOGIN, false);
    }

    public boolean isInCampus() {
        return PrefUtil.getBoolean(DeliverMyOrderApplication.getInstance(), IS_IN_CAMPUS, false);
    }

    public  void setInCampusStatus(Boolean inCampusStatus)
    {
        PrefUtil.putBoolean(DeliverMyOrderApplication.getInstance(), IS_IN_CAMPUS, inCampusStatus);
    }


    public LoginResponseModel.Data getUserData() {
        LoginResponseModel.Data data = new Gson().fromJson(PrefUtil.getString(DeliverMyOrderApplication.getInstance(), KEY_USER_DATA, ""), LoginResponseModel.Data.class);
        return data;
    }

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        //PrefUtil.clear(ctx);
        PrefUtil.remove(DeliverMyOrderApplication.getInstance(), KEY_USER_DATA);
        PrefUtil.remove(DeliverMyOrderApplication.getInstance(), IS_LOGIN);

    }

    public void setCurrentCampusSession(Campus campusSession) {
         PrefUtil.putString(DeliverMyOrderApplication.getInstance(), KEY_CURRENT_CAMPUS, new Gson().toJson(campusSession));
    }
    public Campus getCurrentCampus() {
        Campus data = new Gson().fromJson(PrefUtil.getString(DeliverMyOrderApplication.getInstance(), KEY_CURRENT_CAMPUS, ""), Campus.class);
        return data;
    }

    public void clearCurrentCampus() {
        // Clearing all data from Shared Preferences
        //PrefUtil.clear(ctx);
        PrefUtil.remove(DeliverMyOrderApplication.getInstance(), KEY_CURRENT_CAMPUS);
    }


}
