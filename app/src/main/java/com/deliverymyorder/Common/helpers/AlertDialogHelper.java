package com.deliverymyorder.Common.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;

import com.deliverymyorder.Common.interfaces.OnClickInterface;
import com.deliverymyorder.Common.requestresponse.ApiAdapter;
import com.deliverymyorder.R;
import com.deliverymyorder.mvp.SplashActivity;
import com.deliverymyorder.mvp.login.LoginActivity;


public class AlertDialogHelper {

    public static void showMessageToLogout(final Context context) {

        try{
        AlertDialog.Builder alertDialog = createNewAlert(context);
        // Setting Dialog Message
        alertDialog.setMessage(context.getString(R.string.logout_message));
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("logout", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(context, LoginActivity.class);

                ((Activity) context).startActivity(intent);;

                LoginManager.getInstance().logoutUser();
                ((Activity) context).finishAffinity();

                dialog.dismiss();


            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // Showing Alert Message
        alertDialog.show();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void showMessage(Context context, String msg) {
       try {

           if (context==null) return;

           AlertDialog.Builder alertDialog = createNewAlert(context);

           // Setting Dialog Message
           alertDialog.setMessage(msg);
           alertDialog.setCancelable(false);
           // Setting Positive "Yes" Button
           alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
                   dialog.dismiss();

               }
           });

           // Showing Alert Message
           alertDialog.show();
       }catch (Exception e)
       {
           e.printStackTrace();
       }
    }


    public static void showMessageAndLogout(final Context context, String msg) {
        try {
        AlertDialog.Builder alertDialog = createNewAlert(context);

        // Setting Dialog Message
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                ((Activity) context).finishAffinity();

            }
        });

        // Showing Alert Message
        alertDialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void showMsgAndLogout(final Context context, String msg) {
       try{
        AlertDialog.Builder alertDialog = createNewAlert(context);

        // Setting Dialog Message
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

               // Intent intent = new Intent(context, LoginActivity.class);

               // ((Activity) context).startActivity(intent);


              //  LoginManager.getInstance().logoutUser(context);
               // ((Activity) context).finishAffinity();

            }
        });

        // Showing Alert Message
        alertDialog.show();
       }catch (Exception e)
       {
           e.printStackTrace();
       }
    }


    public static void showAlertDialog(final Context context, String message) {


        try{
        AlertDialog.Builder alertDialog = createNewAlert(context);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });


        alertDialog.show();

    }catch (Exception e)
    {
        e.printStackTrace();
    }

    }

    public static void showAlertDialogAndFinish(final Context context, String message) {

        AlertDialog.Builder alertDialog = createNewAlert(context);
        alertDialog.setCancelable(true);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                ((Activity) context).finish();
                dialog.dismiss();

            }
        });


        alertDialog.show();

    }


    public static void showAlertAndFinishedActivity(final Context context, String message) {

        AlertDialog.Builder alertDialog = createNewAlert(context);
        alertDialog.setCancelable(false);

        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finish();
            }
        });

        alertDialog.show();

    }


    public static void alertEnableLocation(final Context context) {

        try{
        AlertDialog.Builder builder = createNewAlert(context);
        builder.setMessage(context.getString(R.string.title_gps))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.title_yes), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(context.getString(R.string.title_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        System.exit(0);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }catch (Exception e)
    {
        e.printStackTrace();
    }
    }

    public static void alertEnableAppSetting(final Context context) {
      try{
        AlertDialog.Builder builder = createNewAlert(context);
        builder.setMessage(context.getString(R.string.title_app_setting))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.title_yes), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        context.startActivity(new Intent(Settings.ACTION_APPLICATION_SETTINGS));

                        //openSettings(context);

                        dialog.dismiss();
                    }
                });
                /*.setNegativeButton(context.getString(R.string.title_no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        System.exit(0);
                    }
                });*/
        final AlertDialog alert = builder.create();
        alert.show();
      }catch (Exception e)
      {
          e.printStackTrace();
      }
    }

    // navigating user to app settings
    private static void openSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        ((SplashActivity) context).startActivityForResult(intent, 101);
    }

    public static void showAlertLogout(final Context context, String message) {

        try{
        final AlertDialog.Builder alertDialog = createNewAlert(context);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton(context.getString(R.string.title_yes1), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //UserSession userSession = new UserSession(context);
                // userSession.clearUserSession();

                ((Activity) context).finishAffinity();
            }
        })
                .setNegativeButton(context.getString(R.string.title_no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //*/ for retry api
    public static void alertInternetError(Context context, final OnClickInterface onClick) {
        AlertDialog.Builder alertDialog = createNewAlert(context);

        // Setting Dialog Message
        alertDialog.setMessage(context.getString(R.string.internet_error));
        alertDialog.setCancelable(true);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton(context.getString(R.string.title_retry), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                onClick.onClick();
                dialog.dismiss();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static void showAlertToConnectionTimeout(
            final ApiAdapter.ConnectionTimeout connectionTimeout,
            final Context context,
            final String apiType) {

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage(context.getString(R.string.connection_timeout_error));

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                connectionTimeout.retry(apiType);
                dialog.dismiss();
            }
        });

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((Activity) context).startActivityForResult(intent, 12345);
                dialog.dismiss();
            }
        });


        // Showing Alert Message
        alertDialog.show();
    }

    public static void showDialogToClose(final Context context) {
        AlertDialog.Builder alertDialog = createNewAlert(context);

        // Setting Dialog Message
        alertDialog.setMessage(context.getString(R.string.alert_loc_perm));

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finishAffinity();
                System.exit(0);
                dialog.dismiss();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    public static void showAlertDialogToOpenLocationSetting(final Context context) {
        AlertDialog.Builder alertDialog =  createNewAlert(context);
        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage(context.getString(R.string.loc_turn_on));

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((Activity) context).startActivityForResult(intent, 12345);
                dialog.dismiss();
            }
        });


        // Showing Alert Message
        alertDialog.show();
    }

    public  static AlertDialog.Builder  createNewAlert(Context context)
    {
       return new AlertDialog.Builder(context,R.style.AlertDialogTheme);
    }


}

