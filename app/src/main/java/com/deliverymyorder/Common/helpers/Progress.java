package com.deliverymyorder.Common.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.WindowManager;

import com.deliverymyorder.R;




public class Progress {
    static ProgressDialog dialog;

    public static void start(Context context) {

        try {
            if (dialog != null)
                dialog.dismiss();

            dialog = new ProgressDialog(context, ProgressDialog.THEME_HOLO_LIGHT);

            try {
                dialog.show();
            } catch (WindowManager.BadTokenException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }

            dialog.setIndeterminate(true);

            dialog.setContentView(R.layout.progressdialog);

            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public static void stop() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}