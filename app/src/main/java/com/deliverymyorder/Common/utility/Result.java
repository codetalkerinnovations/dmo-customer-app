package com.deliverymyorder.Common.utility;

public class Result
{
    ApiResult result;
    String errorMessage;

    public Result(ApiResult result, String errorMessage) {
        this.result = result;
        this.errorMessage = errorMessage;
    }

    public Result(ApiResult result) {
        this.result = result;
        this.errorMessage = "";
    }

    public ApiResult getResult() {
        return result;
    }

    public void setResult(ApiResult result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
