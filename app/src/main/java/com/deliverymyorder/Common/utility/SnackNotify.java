package com.deliverymyorder.Common.utility;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.deliverymyorder.Common.application.DeliverMyOrderApplication;
import com.deliverymyorder.Common.interfaces.OnClickInterface;
import com.deliverymyorder.R;



public class SnackNotify {

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    public static void checkConnection(final OnClickInterface onRetryClick, View coordinatorLayout) {

        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "No internet connection!.", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onRetryClick.onClick();
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        // Fonts.robotoRegular(activity, textView);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    public static void showSnakeBarForBackPress(View coordinatorLayout) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Please click BACK again to exit.", Snackbar.LENGTH_LONG);

        // Changing action button text color
        View sbView = snackbar.getView();

        sbView.setBackgroundColor(ContextCompat.getColor(DeliverMyOrderApplication.getInstance(), R.color.colorPrimary));

        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        // Fonts.robotoRegular(activity, textView);
        textView.setTextColor(Color.WHITE);


        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            System.exit(0);
        } else {
            snackbar.show();
        }
        back_pressed = System.currentTimeMillis();

    }


    public static void showMessage(String msg, View coordinatorLayout) {

      try {
          Snackbar snackbar = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);

          // Changing action button text color
          View sbView = snackbar.getView();
          TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
          // Fonts.robotoRegular(activity, textView);
          textView.setTextColor(Color.YELLOW);

          snackbar.show();
      }catch (Exception e)
      {e.printStackTrace();}
    }
}