package com.deliverymyorder.Common.utility;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Utils {

    // for keyboard down
    public static void hideKeyboardIfOpen(Activity activity) {

        View view = activity.getCurrentFocus();

        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    public static String getMimeType(String url) {
        String extension = url.substring(url.lastIndexOf("."));
        String mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extension);
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap);
        return mimeType;
    }

    public static byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(String password) {
        if (password.length() > 7) {
            return true;
        }
        return false;
    }

    public static String getRealPathFromURI(String contentURI, Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static Boolean isEmptyOrNull(String value) {
        if (value == null || value.isEmpty())
            return true;
        else
            return false;
    }

    public  static  Boolean  isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static Boolean isVisibleOrNull(Fragment fragment) {
        if (fragment != null) {
            if (fragment.isVisible())
                return true;
            else
                return false;
        } else
            return false;
    }


    public static String getStringDateFromDate(Context ctx, Date date)
    {

        if (ctx == null || date == null)
        {
            return  "-NA-";
        }

        return  ""+DateUtils.getRelativeDateTimeString(ctx, date.getTime(),
                DateUtils.MINUTE_IN_MILLIS,
                DateUtils.WEEK_IN_MILLIS,0);

        /*
        String str = DateUtils.getRelativeDateTimeString(

                this, // Suppose you are in an activity or other Context subclass

                date.getTime(), // The time to display

                DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                // minutes (no "3 seconds ago")


                DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                // to default date instead of spans. This will not
                // display "3 weeks ago" but a full date instead

                0);

        //return  DateUtils.getRelativeTimeSpanString(getDateInMillis(),new Date().getTime(),DateUtils.MINUTE_IN_MILLIS);
        // return   DateUtils.getRelativeTimeSpanString(long time, long now, long minResolution)
        */
    }


    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static String convertDate(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
        date = spf.format(newDate);
        return date;
    }

    public static   String getBase64StringFromImage(String imgPathhhh)
    {

        try {
            if (imgPathhhh == null || imgPathhhh.length() == 0) {
                return "";
            }


            File imgFile = new File(imgPathhhh);
            if (imgFile.exists() && imgFile.length() > 0) {
                Bitmap bm = BitmapFactory.decodeFile(imgPathhhh);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                bm.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                String base64Image = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                if (base64Image != null && base64Image.length() > 0) {
                    return "data:image/png;base64,"+base64Image;
                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return  "";
    }



}
