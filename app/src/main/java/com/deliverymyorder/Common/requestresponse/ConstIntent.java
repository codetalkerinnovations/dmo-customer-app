package com.deliverymyorder.Common.requestresponse;


public class ConstIntent {

    public static final String CONST_INTENT_PHONE_NUMBER = "phone_number";
    public static final String CONST_PAGE_NAME = "page_name";
    public static final String CONST_IS_COME_FOR_ADD = "is_come_for_add";
    public static final String CONST_SHOP_NAME = "shop_name";
    public static final String CONST_ADDRESS = "address";
    public static final String CONST_ADDRESS_ID = "address_id";
    public static final String CONST_REGISTER_PAGE = "register_page";
    public static final String CONST_FORGOT_PASSWORD_PAGE = "forgot_password_page";

    public  static final String CONST_ADDED_CUSTOMER_ORDER = "added_custom_orders";

    public static final String CONST_AMOUNT = "amount";
    public static final String CONST_ORDER_ID = "order_id";
    public static final String CONST_ORDER_TYPE = "order_type";
    public static final String CONST_ISCOMEFROM_YOUR_BILL = "is_come_from_your_bill";
    public static final String CONST_ORDER_DETAIL_MODEL = "order_detail_model";
    public static final String CONST_CATEGORY_ID = "category_id";
    public static final String CONST_SUBCATEGORY_ID = "subcategory_id";
    public static final String CONST_BUISNESS_ID = "buisness_id";
    public static final String CONST_BUISNESS_ACCEPT_CUSTOM_ORDER = "accept_custom_order";
    public static final String CONST_DELIVERY_PARTNER_ID = "delivery_partner_id";
    public static final String CONST_MERCHANT_ID = "merchant_id";
    public static final String CONST_CATEGORY_NAME = "category_name";


    public static final String CONST_PICKUP_NAME = "pickup_name";
    public static final String CONST_PICKUP_CONTACT_NO = "pickup_contact_no";
    public static final String CONST_PICKUP_LATITUDE = "pickup_latitude";
    public static final String CONST_PICKUP_LONGITUDE = "pickup_longitude";


    public static final String CONST_PICKUP_FLAT_NUMBER = "pickup_flat_number";
    public static final String CONST_PICKUP_LANDMARK = "pickup_landmark";
    public static final String CONST_PICKUP_CITY = "pickup_city";
    public static final String CONST_PICKUP_STATE = "pickup_state";
    public static final String CONST_PICKUP_COUNTRY = "pickup_country";



    public static final String CONST_DROP_NAME = "drop_name";
    public static final String CONST_DROP_CONTACT_NO = "drop_contact_no";
    public static final String CONST_DROP_LATITUDE = "drop_latitude";
    public static final String CONST_DROP_LONGITUDE = "drop_longitude";

    public static final String CONST_DROP_FLAT_NUMBER = "drop_flat_number";
    public static final String CONST_DROP_LANDMARK = "drop_landmark";
    public static final String CONST_DROP_CITY = "drop_city";
    public static final String CONST_DROP_STATE = "drop_state";
    public static final String CONST_DROP_COUNTRY = "drop_country";


    public static final String CONST_LONGITUDE = "longitude";
    public static final String CONST_LATITUDE = "latitude";

    public static final String CONST_COUPEN_CODE = "coupen_code";
    public static final String CONST_EXP_DATE = "exp_date";
    public static final String CONST_START_DATE = "start_date";
    public static final String CONST_COUPEN_IMAGE = "coupen_image";
    public static final String CONST_LOGIN_TYPE = "login_type";
    public static final String CONST_SOCIALLOGINID = "social_login_id";
    public static final String CONST_FIRST_NAME = "first_name";
    public static final String CONST_LAST_NAME = "last_name";
    public static final String CONST_EMAIL = "email";

    public static final String CONST_ARRAYLIST_CUSTOM_MENU = "arraylist_custom_menu";

    public static final String CONST_SHOP_HAS_PRODUCTS = "shop_has_product";


}
