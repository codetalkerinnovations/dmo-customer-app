package com.deliverymyorder.Common.requestresponse;

import android.content.Context;

import com.deliverymyorder.BuildConfig;
import com.deliverymyorder.Common.helpers.AlertDialogHelper;
import com.deliverymyorder.Common.helpers.NetworkHelper;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiAdapter {

    private static ApiService sInstance;

    public static ApiService getInstance(Context context) throws NoInternetException {


        if (NetworkHelper.isNetworkAvaialble(context)) {
            return getApiService();
        } else {
            AlertDialogHelper.showMessage(context,"No Internet connection available");
            throw new NoInternetException("No Internet connection available");
        }
    }

    public static ApiService getApiService() {
        if (sInstance == null) {
            synchronized (ApiAdapter.class) {
                if (sInstance == null) {

                    if (BuildConfig.DEBUG) {
                        sInstance = new Retrofit.Builder()
                                .baseUrl(Const.Base_URL)
                                .client(getOkHttpClient()).addConverterFactory(GsonConverterFactory.create()).build()
                                .create(ApiService.class);
                    } else {
                        sInstance = new Retrofit.Builder()
                                .baseUrl(Const.Base_URL)
                                .client(getOkHttpClient()).addConverterFactory(GsonConverterFactory.create()).build()
                                .create(ApiService.class);
                    }
                }
            }
        }
        return sInstance;
    }



    public static class NoInternetException extends Exception {
        public NoInternetException(String message) {
            super(message);
        }
    }

    private static OkHttpClient getOkHttpClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .retryOnConnectionFailure(true);

        /*Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header(Const.APP_KEY, Const.APP_KEY_VALUE);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        };*/

       // builder.addInterceptor(interceptor);

       if (BuildConfig.DEBUG) {
           //Print Log
           HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
           httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
           builder.addInterceptor(httpLoggingInterceptor);

         }
        return builder.readTimeout(90000, TimeUnit.SECONDS)
                .connectTimeout(90000, TimeUnit.SECONDS)
                .build();

    }

    public interface ConnectionTimeout {
        void retry(String type);
    }
}