package com.deliverymyorder.Common.requestresponse;


public class Const {

    //Dev Url
   // public static final String Base_URL = "http://45.79.100.201/deliverMyOrders/api/v1/";
    public static final String Base_URL = "http://delivermyorders.in/api/v1/";

    //public static final String PAY_U_MONEY_SUCCESS_URL = "https://test.payumoney.com/mobileapp/payumoney/success.php";
    //public static final String PAY_U_MONEY_FAILURE_URL = "https://test.payumoney.com/mobileapp/payumoney/failure.php";

    public static final String PAY_U_MONEY_SUCCESS_URL = Base_URL+"payment/success";
    public static final String PAY_U_MONEY_FAILURE_URL = Base_URL+"payment/failure";


    //public static final String Base_URL = "";

    public static final String APP_KEY = "accessToken";
    public static final String APP_KEY_VALUE = "DeliverMyOrders";
    public static final String VALUE_ACCESS_TOKEN = "DeliverMyOrders";
    public static final String VALUE_USER_TYPE = "customer";

    public static final String KEY_NORMAL = "Normal";
    public static final String KEY_FACEBOOK = "Facebook";
    public static final String KEY_GOOGLE = "Google";


    /*Location Permission*/
    public static String KEY_POSTAL_CODE = "post_code";
    public static String LOCATION_PERMISSION = "loc_perm";
    public static final String KEY_ZIPCODE = "zip_code";
    public static final String KEY = "key";
    public static final String KEY_SHORT = "short_by";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_ADDRESS_ID = "address_id";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_CATEGORY_ID = "category_id";
    public static final String KEY_SUBCATEGORY_ID = "subcategory_id";
    public static final String KEY_BUISNESS_ID = "business_id";
    public static final String KEY_BUISNESS_RATING = "business_rating";
    public static final String KEY_MERCHANT_ID = "merchant_id";
    public static final String KEY_CUSTOMER_ID = "customer_id";
    public static final String KEY_CAMPUS_ID = "campus_id";
    public static final String KEY_FAVORITE_STATUS = "favorite_status";
    public static final String KEY_DELIVERY_PARTNER_ID = "delivery_partner_id";
    public static final String KEY_DELIVERY_PARTNER_RATING = "delivery_partner_rating";
    public static final String KEY_CAPMUS_ID = "campus_id";
    public static final String KEY_IN_CAMPUS = "in_campus";


    public static final String PARAM_IMAGE = "image";
    /*login*/
    public static String KEY_FIRST_NAME = "first_name";
    public static String KEY_LAST_NAME = "last_name";
    public static String KEY_NAME = "name";
    public static String KEY_OTP = "otp";
    public static String KEY_USER_TYPE = "user_type";
    public static String KEY_USER_NAME = "username";
    public static String KEY_PHONE_NUMBER = "mobile";
    public static String KEY_PASSWORD = "password";
    public static String KEY_CONFIRM_PASSWORD = "confirm_password";
    public static String KEY_EMAIL = "email";
    public static String KEY_DOB = "dob";
    public static String KEY_IP_ADDRESS = "ip_address";
    public static final String KEY_ACCESS_TOKEN = "accessToken";
    public static String KEY_REFERAL_CODE = "referal_code";
    public static String KEY_LOGIN_TYPE = "login_type";
    public static String KEY_SOCIAL_ID = "social_id";

    public static final String PARAM_ITEMS = "items";
    public static final String PARAM_TOTAL_ITEMS = "total_items";
    public static final String PARAM_TOTAL_AMOUNT = "total_amount";
    public static final String PARAM_ID = "id";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_QTY = "qty";
    public static final String PARAM_PRICE = "price";


    public static final String PARAM_FLAT_OR_HOUSE = "flat_or_house";
    public static final String PARAM_AREA_LOCALITY = "area_locality";
    public static final String PARAM_LANDMARK = "landmark";
    public static final String PARAM_LATITUDE = "latitude";
    public static final String PARAM_LONGITUDE = "longitude";
    public static final String PARAM_STREET = "street";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_CITY = "city";

    public static final String PARAM_CANCEL_REASON_ID = "cancel_reason_id";
    public static final String PARAM_ORDER_ID = "order_id";
    public static final String PARAM_ORDER_TYPE = "order_type";
    public static final String PARAM_COUPEN_CODE = "coupon_code";
    public static final String PARAM_DELIVERY_TYPE_ID = "delivery_type_id";


    public static final String PARAM_PICKUP_CONTACT_NAME = "pickup_contact_name";
    public static final String PARAM_PICKUP_CONTACT_NO = "pickup_contact_no";
    public static final String PARAM_PICKUP_LAT = "pickup_lat";
    public static final String PARAM_PICKUP_LONG = "pickup_long";

    public static final String PARAM_DROP_CONTACT_NAME = "drop_contact_name";
    public static final String PARAM_DROP_CONTACT_NO = "drop_contact_no";
    public static final String PARAM_DROP_LAT = "drop_lat";
    public static final String PARAM_DROP_LONG = "drop_long";

    public static final String PARAM_PRODUCT_TYPE_ID = "product_type_id";
    public static final String PARAM_WEIGHT_ID = "weight_id";
    public static final String PARAM_SIZE_ID = "size_id";

public  static  final  String PARAM_ITEM_IMAGE = "item_image";

    public static final Integer KEY_CUSTOM_MENU_REQUEST_CODE = 1001;

    public static final String PARAM_DESCRIPTION = "description";


    public static final String PARAM_MERCHANT_ID = "merchant_id";
    public static final String PARAM_ADDRESS_ID = "address_id";
    public static final String PARAM_CUSTOMER_ID = "customer_id";
    public static final String PARAM_BUISNESS_ID = "business_id";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_GSTIN = "gstin";
    public static final String PARAM_COUNTRY_ID = "country_id";
    public static final String PARAM_STATE_ID = "state_id";
    public static final String PARAM_CITY_ID = "city_id";

    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_ACCOUNT_TYPE = "account_type";
    public static final String PARAM_ACCOUNT_NO = "acc_number";
    public static final String PARAM_BANK_NAME = "bank_name";
    public static final String PARAM_UPI_ID = "upi_id";
    public static final String PARAM_IFSC_CODE = "ifsc_code";
    public static final String PARAM_CAMPUS_ID = "campus_id";
    public static final String PARAM_CUSTOM_ORDER_TYPE = "custom_order_allowed";

    public  static  final String KEY_PHONE = "phone";



}
