package com.deliverymyorder.Common.requestresponse;


import com.deliverymyorder.mvp.add_category.model.CategoryModel;
import com.deliverymyorder.mvp.address.model.AddAddressModel;
import com.deliverymyorder.mvp.address.model.AddressDetailModel;
import com.deliverymyorder.mvp.address.model.AddressListModel;
import com.deliverymyorder.mvp.address.model.DeleteAddressModel;
import com.deliverymyorder.mvp.address.model.UpdateCurrentAddressModel;
import com.deliverymyorder.mvp.coupen.CoupenModel;
import com.deliverymyorder.mvp.create_password.CreatePasswordModel;
import com.deliverymyorder.mvp.custom_shop_order.AddBuisnessResponseModel;
import com.deliverymyorder.mvp.dashboard.model.CampusListResponse;
import com.deliverymyorder.mvp.dashboard.model.HomePageDataModel;
import com.deliverymyorder.mvp.dashboard.model.SearchDataResponseModel;
import com.deliverymyorder.mvp.delivery_charge.DeliveryChargeModel;
import com.deliverymyorder.mvp.delivery_type.model.GetDeliveryTypeModel;
import com.deliverymyorder.mvp.delivery_type.model.UpdateDeliveryTypeModel;
import com.deliverymyorder.mvp.favorite.model.FavoriteListModel;
import com.deliverymyorder.mvp.favorite.model.UpdateFavoriteResponseModel;
import com.deliverymyorder.mvp.feedback.FeedbackResponseModel;
import com.deliverymyorder.mvp.forgot_password.ForgotPasswordModel;
import com.deliverymyorder.mvp.login.LoginResponseModel;
import com.deliverymyorder.mvp.order_summary.model.ApplyCoupenRes;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryModel;
import com.deliverymyorder.mvp.order_summary.model.OrderSummaryPickAndDrop;
import com.deliverymyorder.mvp.otp.OtpModel;
import com.deliverymyorder.mvp.pick_drop.model.CreatePickDropOrderResponseModel;
import com.deliverymyorder.mvp.pick_drop.model.PickDropResponseModel;
import com.deliverymyorder.mvp.register.RegisterResponseModel;
import com.deliverymyorder.mvp.search_fragment.searchdata_model.SearchPageDataModel;
import com.deliverymyorder.mvp.service_not_aviailble_page.model.CheckServiceModel;
import com.deliverymyorder.mvp.shop_detail.ShopDetailModel;
import com.deliverymyorder.mvp.shop_detail.model.CreateOrderResponseModel;
import com.deliverymyorder.mvp.track_order.model.CancelOrderModel;
import com.deliverymyorder.mvp.track_order.model.CancelOrderReasonModel;
import com.deliverymyorder.mvp.track_order.model.OrderHistoryModel;
import com.deliverymyorder.mvp.track_order.model.ReOrderModel;
import com.deliverymyorder.mvp.track_order.model.TrackOrderDetailModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {


    Call<LoginResponseModel> userSocialLogin(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    /*register*/
    @POST("customer-register")
    Call<RegisterResponseModel> getSignupResult(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    /*forgot password*/
    @POST("forgot-password")
    Call<ForgotPasswordModel> getForgotPassword(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    /*login*/
    @POST("customer-login")
    Call<LoginResponseModel> userSimpleLogin(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);






    /*otp*/
    @POST("customer-resend-otp")
    Call<OtpModel> customerResendOTP(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("verify-otp")
    Call<OtpModel> confirmOtp(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("reset-password")
    Call<CreatePasswordModel> createPassword(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/get-home-data")
    Call<HomePageDataModel> getHomeData(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/search-shop")
    Call<SearchDataResponseModel> getSearchData(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/search-shop")
    Call<SearchPageDataModel> getSearchPageData(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("get-categories")
    Call<CategoryModel> getCategories(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/get-items-by-shop")
    Call<ShopDetailModel> getItemsByShop(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/create-order")
    Call<CreateOrderResponseModel> createOrder(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/get-order-history")
    Call<OrderHistoryModel> getOrderHistory(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/add-new-delivery-address")
    Call<AddAddressModel> getAddAddress(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/update-delivery-address")
    Call<AddAddressModel> getEditAddress(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/get-delivery-types")
    Call<GetDeliveryTypeModel> getDeliveryTypes(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/update-order-delivery-type")
    Call<UpdateDeliveryTypeModel> updateDeliveryTypes(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/get-order-cancel-reasons")
    Call<CancelOrderReasonModel> getOrderCancelReason(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/cancel-order")
    Call<CancelOrderModel> updateOrderCancelReason(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/get-cutomer-delivery-addresses")
    Call<AddressListModel> getAddressList(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/delete-delivery-address")
    Call<DeleteAddressModel> deleteAddress(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/update-customer-current-delivery-address")
    Call<UpdateCurrentAddressModel> updateCurrentAddress(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/give-feedback")
    Call<FeedbackResponseModel> giveFeedback(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/get-delivery-address-details")
    Call<AddressDetailModel> getAddressDetailById(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/track-order")
    Call<TrackOrderDetailModel> getTrackOrderDetails(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/get-pick-and-drop-form-options")
    Call<PickDropResponseModel> getPickDropOrderDetails(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/create-pick-and-drop-order")
    Call<CreatePickDropOrderResponseModel> createPickDropOrderDetails(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/get-coupons")
    Call<CoupenModel> getCoupenList(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/apply-coupon")
    Call<ApplyCoupenRes> verifyCoupen(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/cancel-coupon")
    Call<ApplyCoupenRes> removeCoupen(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/re-order")
    Call<ReOrderModel> reOrder(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("murchant/get-order-details")
    Call<OrderSummaryModel> getOrderDetails(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/confirm-order-purchase")
    Call<OrderSummaryModel> updateOrderStatus(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/calculate-pick-and-drop-charges")
    Call<DeliveryChargeModel> getCalculatePickDropCharges(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/add-business-favorite-status")
    Call<UpdateFavoriteResponseModel> updateFavorite(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("customer/get-customer-favorite-business")
    Call<FavoriteListModel> getFavoriteList(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

    @POST("customer/get-campus")
    Call<CampusListResponse> getCampusListData(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @Multipart
    @POST("murchant/add-business")
    Call<AddBuisnessResponseModel> addBuisness(@Part(Const.KEY_ACCESS_TOKEN) RequestBody accessToken,
                                               @Part(Const.PARAM_MERCHANT_ID) RequestBody merchantId,
                                               @Part(Const.PARAM_TITLE) RequestBody shopname,
                                               @Part(Const.PARAM_GSTIN) RequestBody gStIn,
                                               @Part(Const.PARAM_COUNTRY_ID) RequestBody countryIdBody,
                                               @Part(Const.PARAM_STATE_ID) RequestBody stateIdBody,
                                               @Part(Const.PARAM_CITY_ID) RequestBody cityIdBody,
                                               @Part(Const.PARAM_ADDRESS) RequestBody areaBody,
                                               @Part(Const.KEY_PHONE) RequestBody phoneNumberBody,
                                               @Part(Const.PARAM_LATITUDE) RequestBody latitudeBody,
                                               @Part(Const.PARAM_LONGITUDE) RequestBody longitudeBody,
                                               @Part(Const.PARAM_CAMPUS_ID) RequestBody campusIDRequest,
                                               @Part(Const.PARAM_CUSTOM_ORDER_TYPE) RequestBody customeOrderAccepted,
                                               @Part MultipartBody.Part bodyImage);



    @POST("get-near-by-cities")
    Call<CheckServiceModel> checkServiceAvailblity(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);


    @POST("murchant/get-order-details")
    Call<OrderSummaryPickAndDrop> getOrderDetailsPickAndDrop(
            @Header("Content-Type") String contentType,
            @Header("Cache-Control") String cache,
            @Body RequestBody params);

}

