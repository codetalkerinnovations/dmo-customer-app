package com.deliverymyorder.Common.application;

import android.app.Application;
import android.content.Context;

import com.deliverymyorder.Common.utility.TypeFaceUtil;
import com.deliverymyorder.R;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(mailTo = "dhiru.ard@gmail.com", customReportContent = {
        ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
        ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
        ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT},
        mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text)

public class DeliverMyOrderApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;

       // TypeFaceUtil.setDefaultFont(this, "MONOSPACE", "fonts/Monaco.ttf");
        TypeFaceUtil.setDefaultFont(this, "MONOSPACE", "fonts/Roboto-Regular.ttf");

        //ACRA.init(this);
    }


    public static Context getInstance() {
        return context;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}


